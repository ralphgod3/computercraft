local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")


local TAG = "Threads"
Dbg.setLogLevel(TAG, Dbg.Levels.Warning)
--Dbg.setLogLevel(TAG, Dbg.Levels.Info)


--Adheres to Interfaces/Modules/IThreads.lua
---create new thread manager
---@return Threads threadManager thread manager instance
local function new()
	Dbg.logI(TAG, "creating instance")
	local this = {
		id = 0,
		threads = {},
		filters = {},
		-- if marked as background thread then threading api will return even if thread is still alive, usefull for initialization
		isBackgroundThread = {},
		isRunning = false
	}

	---create a new thread (coroutine)
	---@param func function to use ase coroutine
	---@vararg ... function parameters for the coroutine
	---@return number thread thread id
	local function create(func, ...)
		PC.expect(1, func, "function")
		Dbg.logV(TAG, "creating new thread")
		this.id = this.id + 1
		local params = {...}
		this.threads[this.id] =
			coroutine.create(
			function()
				func(table.unpack(params))
			end
		)
		return this.id
	end

	---create a new thread as backgroundthread(coroutine)
	---@param func function to use ase coroutine
	---@vararg ... function parameters for the coroutine
	---@return number thread thread id
	local function createAsBackground(func, ...)
		PC.expect(1, func, "function")
		Dbg.logI(TAG, "creating new background thread")
		this.id = this.id + 1
		local params = {...}
		this.threads[this.id] =
			coroutine.create(
			function()
				func(table.unpack(params))
			end
		)
		this.isBackgroundThread[this.id] = true
		return this.id
	end

	---returns true if thread is stil alive
	---@param id number thread id
	---@return boolean threadAlive thread is alive
	local function isAlive(id)
		PC.expect(1, id, "number")
		if this.threads[id] ~= nil then
			Dbg.logI(TAG, "CHecking if thread is alive", id, "true")
			return true
		end
		Dbg.logI(TAG, "CHecking if thread is alive", id, "false")
		return false
	end

	---stops a running thread by id
	---@param id number
	local function stop(id)
		PC.expect(1, id, "number")
		Dbg.logI(TAG, "manually stopping thread", id)
		this.filters[id] = nil
		this.threads[id] = nil
		this.isBackgroundThread[id] = nil
	end

	---stops all running threads, also kills background threads, dont use unless absolutely nescesary
	local function stopAll()
		Dbg.logW(TAG, "stopping all threads")
		for k, _ in pairs(this.threads) do
			this.threads[k] = nil
			this.filters[k] = nil
			this.isBackgroundThread[k] = nil
		end
	end

	---starts the coroutine based threading, threads have to be added using the Create function first,
	--- this function will block indefinetly until an error is thrown or no threads are alive
	local function startRunning()
		Dbg.logI(TAG, "starting threadManager")
		local lastTime = 0
		local eventData = {n = 0}
		this.isRunning = true
		while true do
			for key, routine in pairs(this.threads) do
				if this.filters[key] == nil or this.filters[key] == eventData[1] or eventData[1] == "terminate" then
					if coroutine.status(routine) ~= "dead" then
						local ok, param = coroutine.resume(routine, table.unpack(eventData, 1, eventData.n))
						if not ok then
							this.isRunning = false
							error(param, 0)
						else
							this.filters[key] = param
						end
					end
				end
			end
			for key, routine in pairs(this.threads) do
				if coroutine.status(routine) == "dead" then
					this.threads[key] = nil
					this.filters[key] = nil
				end
			end
			if Utils.getTableLength(this.threads) == 0 or Utils.getTableLength(this.threads) == Utils.getTableLength(this.isBackgroundThread) then
				this.isRunning = false
				return
			end
			--fixes a bug in event queue
			if lastTime + 5 < os.clock() then
				Dbg.logI(TAG, "started new guardKitteh", os.clock())
				os.startTimer(5.1)
				lastTime = os.clock()
			end

			eventData = table.pack(os.pullEventRaw())
		end
	end

	---checks if threadingmanager is currently running using startRunning
	---@return boolean isRunning threadingManager is running
	local function isRunning()
		Dbg.logV(TAG, "checking if thread manager is running", this.isRunning)
		return this.isRunning
	end

	---@class Threads : Ithreads
	return {
		create = create,
		createAsBackground = createAsBackground,
		isAlive = isAlive,
		stop = stop,
		stopAll = stopAll,
		startRunning = startRunning,
		isRunning = isRunning
	}
end

return {new = new}
