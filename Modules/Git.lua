--[[ include the snippet below at the top of every "main" to ensure the apiloader is installed, initialized and updated
-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("/Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("/Modules.Git")
--git load finished
--]]

local gitUrl = "https://gitlab.com"
local gitFileUrl = "api/v4/projects/projectID/repository/files"
local branch = "master"
local Git = {}
--git version
Git.version = 1
Git.ComputerCraftProjectID = 23712047


---split a string
---@param inputString string
---@param seperator string
---@return table | nil split string or nil when input is 0
local function splitString(inputString, seperator)
    --seperating the strings
    local outTable = {}
    local sepLength = string.len(seperator)
    local sepLocation = string.find(inputString, seperator, nil, true)

    while sepLocation do
        local sub = string.sub(inputString, 0, sepLocation - 1)

        if sub ~= nil and sub ~= "" then
            table.insert(outTable, sub)
        end

        inputString = string.sub(inputString, sepLocation + sepLength)
        sepLocation = string.find(inputString, seperator, nil, true)
    end

    if inputString ~= nil and inputString ~= "" then
        table.insert(outTable, inputString)
    end

    if #outTable == 0 then return nil end

    return outTable
end



---download file from git and return contents
---@param projectID string | number git project id
---@param file string file path and location on git
---@return string | nil response http response or nil when no response was received
function Git.downloadFileFromGit(projectID, file)
    local url = gitUrl .. "/" .. gitFileUrl
    url = string.gsub(url, "projectID", tostring(projectID))
    url = url .. "/" .. textutils.urlEncode(file) .. "/raw?ref=" .. branch
    local response = http.get(url)
    if response then
        local contents = response.readAll()
        response.close()
        return contents
    else
        return nil
    end
end

---decodes folder contents into files, folders and other category
---@param responseContents string string returned from response.readAll()
---@return table table {files = {}, folders = {}, others = {}}
local function decodeFolderContentRequest(responseContents)
    responseContents = textutils.unserialiseJSON(responseContents)
    local retTable = {}
    retTable.files = {}
    retTable.folders = {}
    retTable.others = {}
    for _, v in pairs(responseContents) do
        local entry = {}
        entry.name = v.name
        entry.path = v.path
        if v.type == "tree" then
            table.insert(retTable.folders, entry)
        elseif v.type == "blob" then
            table.insert(retTable.files, entry)
        else
            table.insert(retTable.others, entry)
        end
    end
    return retTable
end


---Get folder contents in git repository
---@param projectID string | number project id of git project
---@param path string | nil folder to get contents for, nil to get root contents
---@return table | nil Output on failure nil, on success table with files, folders, and other fields, each field contains a name and path field
function Git.getFolderContentsFromGit(projectID, path)
    if (projectID == nil or (type(projectID) ~= "string" and type(projectID) ~= "number")) or (path ~= nil and type(path) ~= "string") then
        error("failed to get folder contents from git " .. path, 1)
    end

    local url = gitUrl .. "/api/v4/projects/projectID/repository/tree"
    if path ~= nil then
        url = url .. "?per_page=100&path=" .. textutils.urlEncode(path)
    end
    url = string.gsub(url, "projectID", tostring(projectID))
    local response = http.get(url)

    if response == nil then
        return nil
    end

    local toReturn = {}
    toReturn.folders = {}
    toReturn.files = {}
    toReturn.others = {}
    while response do
        local linkHeaders = splitString(response.getResponseHeaders().Link, ",")

        local hasNextPage = false
        if linkHeaders ~= nil then
            for _, v in pairs(linkHeaders) do
                if string.find(v, "rel=\"next\"") then
                    url = string.match(v, "<(.-)>")
                    hasNextPage = true
                    break
                end
            end
        end
        local curFolderContents = decodeFolderContentRequest(response.readAll())
        --append information to current return table
        for _, v in pairs(curFolderContents.folders) do
            table.insert(toReturn.folders, v)
        end
        for _, v in pairs(curFolderContents.files) do
            table.insert(toReturn.files, v)
        end
        for _, v in pairs(curFolderContents.others) do
            table.insert(toReturn.others, v)
        end

        --close and reset response to nil
        response.close()
        response = nil
        if hasNextPage == true then
            response = http.get(url)
        end
    end
    return toReturn
end

---download remote path from the git project id and save it as localpath
---@param gitProjectID number | string number or string with git project id
---@param remotePath string path to clone from
---@param localPath string path to clone in
---@return boolean success true if function finished normally false if something went wrong
function Git.getFile(gitProjectID, remotePath, localPath)
    print("Downloading " .. remotePath)
    local response = Git.downloadFileFromGit(gitProjectID, remotePath)
    if response == nil then
        return false
    end
    local file = fs.open(localPath, "w")
    file.write(response)
    file.close()
    return true
end

---check if localpath exists if it doesnt download remote path from the git project id and save it as localpath
---@param gitProjectID number | string number or string with git project id
---@param path string path to clone from/ in if needed
---@return boolean success true if function finished normally false if something went wrong
function Git.getFileIfNeeded(gitProjectID, path)
    if path == nil or type(path) ~= "string" or gitProjectID == nil then
        error("failed to get file from git " .. path, 1)
    end
    gitProjectID = tostring(gitProjectID)

    --file for module exists
    if fs.exists(path) then
        return true
    else
        print("Downloading " .. path)
        local response = Git.downloadFileFromGit(gitProjectID, path)
        if response == nil then
            print("something went wrong whilst downloading " .. path)
            return false
        end

        local file = fs.open(path, "w")
        file.write(response)
        file.close()
    end
    return true
end

return Git
