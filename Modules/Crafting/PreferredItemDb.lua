local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")

local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")

local TAG = "PIDB"
local mcVersion, _ = Utils.getVersionInfo()
local localItemDb = "/cache/itemDatabase/preferredItems.json"
local remoteItemDb = "DataFiles/" .. mcVersion .. "/itemDatabase/preferredItems.json"

---create new preferredItemDb instance, loads items from remote if set and /cache/itemDatabase/preferredItems.json
---@param useRemote boolean? use remote database
---@return IpreferredItemDb instance instance of preferred item db
local function new(useRemote)
    useRemote = useRemote or true
    local preferredItemDb = {}
    local this = {
        IsInitialized = false,
        localDb = {},
        remoteDb = {},
    }

    ---loads database if not yet loaded
    local function loadDbIfNotLoaded()
        if this.IsInitialized then
			return
		end
		if useRemote then
			local file = nil
			--detects if we are running in a git repo and uses the local version if we are to save on http requests
			Dbg.logV(TAG, "initializing prefferedItems from ", remoteItemDb)
			if not fs.exists(remoteItemDb) then
				file = Git.downloadFileFromGit(Git.ComputerCraftProjectID, remoteItemDb)
			else
				local f = fs.open(remoteItemDb, "r")
				file = f.readAll()
				f.close()
			end
			this.remoteDb = {}
			if file then
				this.remoteDb = textutils.unserialiseJSON(file)
			end
		end
        this.localDb = {}
		if fs.exists(localItemDb) then
			local file = fs.open(localItemDb, "r")
			this.localDb = file.readAll()
			file.close()
			this.localDb = textutils.unserialiseJSON(this.localDb)
            this.localDb = this.localDb or {}
		end
        this.IsInitialized = true
    end


    ---get preffered items for tag
    ---@param tag string tag to get items for
    ---@return table | nil items table of items if tag exists, nil otherwise
    function preferredItemDb.getItemsWithTag(tag)
        PC.expect(1, tag, "string")
		local outTable = {}
		if this.remoteDb[tag] ~= nil then
            for i = 1, #this.remoteDb[tag] do
				table.insert(outTable, this.remoteDb[tag][i])
			end
		end

		if this.localDb[tag] ~= nil then
			for i = 1, #this.localDb[tag] do
                local exists = false
                for j = 1, #outTable do
                    if this.localDb[tag][i] == outTable[j] then
                        exists = true
                        break
                    end
                end
				if not exists then
					table.insert(outTable, this.localDb[tag][i])
				end
			end
		end

        if #outTable == 0 then
            return nil
        end
		return outTable
    end


    ---add item to tag in preferred list
    ---@param tagName string tagName
    ---@param itemName string itemName
    function preferredItemDb.addItem(tagName, itemName)
        PC.expect(1,tagName, "string")
        PC.expect(2, itemName, "string")
        if this.localDb[tagName] == nil then
            this.localDb[tagName] = {}
        end
        table.insert(this.localDb[tagName], itemName)
        local file = fs.open(localItemDb, "w")
        file.write(textutils.serializeJSON( this.localDb))
        file.close()
    end


    loadDbIfNotLoaded()
    return preferredItemDb
end
return {new = new}