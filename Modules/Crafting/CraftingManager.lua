--handles interacting with crafting turtles and machines
local Git = require("Modules.Git")

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Itimer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Networking/Inetworking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")

local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")
local Utils = require("Modules.Utils")
local Messages = require("Modules.Networking.Messages")
local Inetworking = require("Interfaces.Modules.Networking.Inetworking")
local Ithreads = require("Interfaces.Modules.Ithreads")
local Itimer = require("Interfaces.Modules.Itimer")
local IitemDb = require("Interfaces.Modules.IitemDb")
local IitemInventory = require("Interfaces.Modules.IitemInventory")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local ItemInventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")

Dbg.setLogLevel("CLCM", Dbg.Levels.Warning)



---@class CLCraftingStatus
---@field status string message what is currently being done
---@field craftingStep number current crafting step
---@field maxCraftingStep number number of steps in crafting chain
---@field buffer string bufferInventory in use
---@field currentItemBeingMade string name of current item being made in chain
---@field currentItemBeingMadeCount number count of current item being made in chain
---@field currentItemBeingMadeAlreadyMade number number of items already created in chain
---@field outputItemName string item being made
---@field outputItemCount number count of items being made
---@field outputItemNbt string | nil optional nbt data for output item being made
---@field requestId number id of the original request

-- machine/ buffer inventory status
---@enum CraftingStatus
local CraftingStatus = {
	Idle = 0,
	Crafting = 1
}

local craftingProgressStatus =
{
	Queued = "Queued",
	Crafting = "Crafting",
	WaitingForBuffer = "waitingForBuffer",
	requestingItemsToBuffer = "requestingItemsToBuffer",
}

---helper function to sleep for slowMode when slowMode is used
---@param slowMode number | nil time to slow crafting operations by
local function handleSlowMode(slowMode)
	if slowMode ~= nil then
		sleep(slowMode)
	end
end


---creates a craftingManager, craftingManager manages autocrafting
---@param threadingManager Ithreads Ithreads implementation
---@param Timer Itimer ITimer implementation
---@param networking Inetworking Inetworking implementation
---@param itemDb IitemDb IitemDb implementation
---@param storageControllerId number storageControllerId
---@param pollTime number? time between inventory polls (defaults to 1)
---@param turtleSlotCount number? amount of slots turtle has (defaults to 16)
---@param turtleSlotLookupTable number[]? how turtle slots are layed out defaults to {1, 2, 3, 5, 6, 7, 9, 10, 11}
---@return IcraftingManager instance instance of CraftingManager
local function new(threadingManager, Timer, networking, itemDb, storageControllerId, pollTime, turtleSlotCount,
				   turtleSlotLookupTable)
	--local function new(threadingManager, networking, itemDb, storageControllerId, pollTime, turtleSlotCount,
	--				   turtleSlotLookupTable)
	PC.expectInterface(1, Ithreads, threadingManager)
	PC.expectInterface(2, Itimer, Timer)
	PC.expectInterface(3, Inetworking, networking)
	PC.expectInterface(4, IitemDb, itemDb)
	PC.expect(5, storageControllerId, "number")
	PC.expect(6, pollTime, "number", "nil")
	PC.expect(7, turtleSlotCount, "number", "nil")
	PC.expect(8, turtleSlotLookupTable, "table", "nil")
	turtleSlotCount = turtleSlotCount or 16
	turtleSlotLookupTable = turtleSlotLookupTable or { 1, 2, 3, 5, 6, 7, 9, 10, 11 }
	pollTime = pollTime or 1

	local TAG = "CLCM"
	local this = {
		curRequestId = Utils.generateId(),
		---@type IitemInventory storageInput
		storageInput = {},
		---@type CraftingManagerBufferInventory[]
		bufferInventories = {}, -- inventories with information used as bufferspace for crafting
		---@type CraftingManagerMachineEntry[]
		machines = {},    -- machineInformation for machines used in crafting
		---@type table<string, CraftingManagerCrafterQueueEntry> crafter info indexed by network name
		crafters = {},    -- crafter information for turtles used in crafting
		ThreadCount = 0,  -- amount of threads currently crafting stuff
		---@type CraftingManagerManagerQueueEntry[]
		craftingQueue = {}, -- queue of craftingrequests waiting for a craft to be done before starting
		---@type number | nil network id of CLSorter if available
		CLSorter = nil,   --holds CLSorter id if there is a sorter found on the network
		---@type CLCraftingStatus[]
		progressInfo = {}, --holds progress info for crafting requests
		---@type table<number, number> requestId, craftingThreadId
		craftingThreads = {},
	}

	---handles itemUpdateRedirectMessages coming from CLSorter
	local function CLSorterItemUpdateThread()
		local msg = networking.receiveMsgTypes(nil, this.CLSorter, { Messages.messageTypes.ItemRedirectUpdate })
		if msg ~= nil then
			local payload = msg.payload
			---@cast payload ItemRedirectUpdateMessage
			for k, v in pairs(this.progressInfo) do
				if v.buffer == payload.inventory then
					Dbg.logV(TAG, "updated items produced for request:", k)
					v.currentItemBeingMadeAlreadyMade = payload.redirected
					break
				end
			end
		end
	end

	---checks if CLSorter can be found in network on a timer
	local function sorterCheck()
		Dbg.logV(TAG, "checking for CLSorter")
		local whoIsMsg = Messages.createWhoIs({ Messages.remoteTypes.Sorter })
		local msgId = networking.transmit(networking.broadcastChannel, networking.QOS.ATMOST_ONCE, whoIsMsg)
		local llmsg = networking.receive(10, nil, msgId)
		if llmsg ~= nil then
			Dbg.logI(TAG, "found CL sorter")
			this.CLSorter = llmsg.from
			threadingManager.create(CLSorterItemUpdateThread)
		else
			Timer.add(10, sorterCheck)
		end
	end



	--dont want this polluting variables
	if true then
		Dbg.logI(TAG, "requesting storage input")
		local msg = Messages.createInputInventoryRequest()
		local msgId = networking.transmit(storageControllerId, networking.QOS.EXACTLY_ONCE, msg)
		local llmsg = networking.receive(nil, nil, msgId)
		if llmsg ~= nil then
			this.storageInput = ItemInventoryFactory.new(threadingManager, llmsg.payload.inputInventory)
			Dbg.logI(TAG, "checking for CLSorter")
			local whoIsMsg = Messages.createWhoIs({ Messages.remoteTypes.Sorter })
			msgId = networking.transmit(networking.broadcastChannel, networking.QOS.ATMOST_ONCE, whoIsMsg)
			llmsg = networking.receive(1, nil, msgId)
			if llmsg ~= nil then
				Dbg.logI(TAG, "found CL sorter")
				this.CLSorter = llmsg.from
				threadingManager.create(CLSorterItemUpdateThread)
			else
				Timer.add(10, sorterCheck)
			end
		end
	end

	------------------------------- PRIVATE FUNCTIONS ----------------------------------------

	---check if all machines that are needed for crafting exist in instance
	---@param craftingChain CraftingStep[] craftingchain
	---@return boolean | string suc true on succes, string with missing machine on failure
	local function haveAllMachinesForRecipe(craftingChain)
		for i = 1, #craftingChain do
			if craftingChain[i].type == "minecraft:crafting" then
				if Utils.getTableLength(this.crafters) == 0 then
					return craftingChain[i].type
				end
			elseif this.machines[craftingChain[i].type] == nil then
				return craftingChain[i].type
			end
		end
		return true
	end

	---calculate maximum amount of items that can be crafted in a turtle at once
	---@param craftingStep CraftingStep craftingStep
	---@return number craftableAtOnce amount of items that can be crafted at once
	local function getMaxCraftingCountForRecipe(craftingStep)
		local itemInfo = itemDb.getItem(craftingStep.creates)
		if itemInfo == nil then
			error(craftingStep.creates .. " not in ItemDB")
		end
		local maxCraftable = (itemInfo.maxCount * turtleSlotCount) / craftingStep.outputPerCraft
		if maxCraftable > 64 then
			maxCraftable = 64
		end

		for _, v in pairs(craftingStep.recipe) do
			itemInfo = itemDb.getItem(v.name)
			if itemInfo == nil then
				error(v.name .. " not in ItemDB")
			end
			if maxCraftable > itemInfo.maxCount then
				maxCraftable = itemInfo.maxCount
			end
		end
		return maxCraftable
	end

	---finds and returns a free bufferInventory that can be used for crafting, will block, set buffer status to idle after you are done with it
	---@return CraftingManagerBufferInventory bufferInventory bufferInventory to use
	local function findBufferInventoryForCrafting()
		while true do
			for _, v in pairs(this.bufferInventories) do
				if v.status == CraftingStatus.Idle then
					v.status = CraftingStatus.Crafting
					return v
				end
			end
			Dbg.logV(TAG, "waiting for buffer to be free")
			os.sleep(pollTime)
		end
	end

	---requests all item needed for a craft, blocking function will never exit until all items are received
	---@param itemsToRequest ItemEntry[] table of items needed for craft {name = 5, name2 = 6}
	---@param bufferChest CraftingManagerBufferInventory chest as given in CraftingManager.this.bufferInventories
	---@param slowMode number | nil delay each crafting operation by time given if nil does not slow down
	---@param requestCountTimeout number amount of requests to make before indicating a failure to user
	---@return boolean success indicates success
	---@return string message message if failed, empty string otherwise
	local function requestItemsToBufferInventory(itemsToRequest, bufferChest, slowMode, requestCountTimeout)
		local lastRequest = os.clock()
		local requestCount = 0
		while requestCount <= requestCountTimeout do
			local tempItemList = ItemInventoryUtils.transformItemList(bufferChest.wrapped.list())
			local itemList = ItemInventoryUtils.getMissingItems(itemsToRequest, tempItemList, true)
			--check to see if items are all in inventory
			if #itemList == 0 then
				break
			end
			--prepare item request
			local requestList = {}
			requestList[bufferChest.name] = itemList
			for i = 1, #itemList do
				Dbg.logI(TAG, "requesting ", itemList[i].count, itemList[i].name, tostring(itemList[i].nbt),
					tostring(itemList[i].damage))
			end
			--send item request
			local msg = Messages.createItemRequest(requestList)
			local msgId = networking.transmit(storageControllerId, networking.QOS.EXACTLY_ONCE, msg)
			networking.receive(5, os.getComputerID(), msgId)

			--check to see if we have all items
			local tempItemList = ItemInventoryUtils.transformItemList(bufferChest.wrapped.list())
			local itemList = ItemInventoryUtils.getMissingItems(itemsToRequest, tempItemList, true)
			--check to see if items are all in inventory
			if #itemList == 0 then
				break
			end
			if os.clock() - lastRequest < 5 then
				sleep(5 - (os.clock() - lastRequest))
				Dbg.logW(TAG, "item that was requested not given")
				lastRequest = os.clock()
			end
			requestCount = requestCount + 1
		end
		handleSlowMode(slowMode)
		if requestCount > requestCountTimeout then
			local tempItemList = ItemInventoryUtils.transformItemList(bufferChest.wrapped.list())
			local itemlist = ItemInventoryUtils.getMissingItems(itemsToRequest, tempItemList, true)
			Dbg.logE(TAG, "items missing from buffer inventory after request", itemlist)
			return false, "items missing from buffer inventory after request."
		else
			return true, ""
		end
	end

	---handles crafting for machine based recipes NOT CRAFTING TABLE RECIPES
	---@param craftingStep CraftingStep crafting step to perform
	---@param bufferChest CraftingManagerBufferInventory bufferchest information
	---@param sorterInputs string[] | nil table of sorter input names, used for determining about how to get the item from the system
	---@param requestId number requestId for craft
	---@return boolean success true if succesful false otherwise
	---@return string message message if failed, empty string otherwise
	local function handleMachineBasedCraftingStep(craftingStep, bufferChest, sorterInputs, requestId)
		--wait until machine is available to craft with, then take it and mark it busy
		local machine = this.machines[craftingStep.type]
		while machine.status ~= CraftingStatus.Idle do
			Dbg.logV(TAG, "waiting for " .. craftingStep.type .. " to idle")
			this.progressInfo[requestId].status = craftingProgressStatus.Queued
			os.sleep(pollTime)
		end
		machine.status = CraftingStatus.Crafting
		this.progressInfo[requestId].status = craftingProgressStatus.Crafting

		local itemsToTransfer = ItemInventoryUtils.multiplyItemList(craftingStep.recipe, craftingStep.craftsNeeded)
		local itemsLeftToCollect = craftingStep.itemsCreated
		Dbg.logI(TAG, "starting craft of ", craftingStep.itemsCreated, craftingStep.creates,
			"using " .. craftingStep.type)
		--check if we need to grab input ourselves or tell the sorter to grab it
		local sorterRetMsgId = nil
		if sorterInputs then
			for i = 1, #sorterInputs do
				if machine.output.getName() == sorterInputs[i] then
					Dbg.logV(TAG, "telling sorter to collect items for us")
					local msg = Messages.createItemRedirect(bufferChest.wrapped.getName(), craftingStep.creates,
						itemsLeftToCollect, craftingStep.nbt, craftingStep.damage)
					sorterRetMsgId = networking.transmit(this.CLSorter, networking.QOS.EXACTLY_ONCE, msg)
					break
				end
			end
		else
			Dbg.logV(TAG, "grabbing items from machine directly")
		end

		--push items to machine, get items from machine
		while true do
			--move items until we moved all, then check if we gave the sorter an assignment to move items to us
			if Utils.getTableLength(itemsToTransfer) > 0 then
				local itemsMoved = ItemInventoryUtils.pushItemsByName(threadingManager, bufferChest.wrapped,
					machine.input, itemsToTransfer)
				for k, _ in pairs(itemsToTransfer) do
					itemsToTransfer[k].count = itemsToTransfer[k].count - itemsMoved[k]
					if itemsToTransfer[k].count <= 0 then
						itemsToTransfer[k] = nil
						itemsMoved[k] = nil
					end
				end
				itemsToTransfer = Utils.rebuildArray(itemsToTransfer)
				--items pushed to machine exit out of the loop and wait for done message
			elseif sorterRetMsgId ~= nil then
				break
			end
			--items pushed to machine, retrieving items directly
			if not sorterRetMsgId then
				--handle outputting from machine
				local slot = ItemInventoryUtils.findSlotWithItem(machine.output.list(),
					{ name = craftingStep.creates, damage = craftingStep.damage, nbt = craftingStep.nbt })
				if slot ~= nil then
					itemsLeftToCollect = itemsLeftToCollect -
						machine.output.pushItems(bufferChest.wrapped, slot, itemsLeftToCollect)
					--update progressInfo
					this.progressInfo[requestId].currentItemBeingMadeAlreadyMade = this.progressInfo[requestId]
						.currentItemBeingMadeCount - itemsLeftToCollect
					if itemsLeftToCollect <= 0 then
						--done crafting, mark machine as free
						--push additional items into storage
						machine.output.pushInventory(this.storageInput)
						break
					end
				end
			end
			sleep(pollTime)
		end
		if sorterRetMsgId then
			networking.receive(nil, nil, sorterRetMsgId)
		end
		machine.status = CraftingStatus.Idle
		Dbg.logI(TAG, craftingStep.type .. " done")
		return true, ""
	end

	---handles crafting for crafting table based recipes
	---@param craftingStep CraftingStep crafting step to perform
	---@param bufferChest CraftingManagerBufferInventory bufferchest information
	---@param slowMode number | nil if given slows down each operation by time given, nil disables this
	---@param requestId number requestId for craft
	---@return boolean success true if succesfull false otherwise
	---@return string message message indicating why it failed, empty string if success
	local function handleTurtleBasedCraftingStep(craftingStep, bufferChest, slowMode, requestId)
		--wait until turtle is available to craft with, then take it and mark it busy
		local turtle = nil
		while turtle == nil do
			for networkId, v in pairs(this.crafters) do
				if v.status == CraftingStatus.Idle then
					v.status = CraftingStatus.Crafting
					turtle = networkId
					break
				end
			end

			if turtle == nil then
				this.progressInfo[requestId].status = craftingProgressStatus.Queued
				Dbg.logV(TAG, "waiting for turtle to idle")
				os.sleep(pollTime)
			end
		end
		this.progressInfo[requestId].status = craftingProgressStatus.Crafting

		local maxItemsCraftableAtOnce = getMaxCraftingCountForRecipe(craftingStep)
		local itemsToCraft = craftingStep.craftsNeeded
		while itemsToCraft > 0 do
			local itemsToMoveTable = {}
			for turtleSlot, itemInfo in pairs(craftingStep.recipe) do
				--calculate how many items to move this crafting iteration
				local itemsToMove = maxItemsCraftableAtOnce
				if itemsToMove > itemsToCraft then
					itemsToMove = itemsToCraft
				end
				--start moving items
				table.insert(itemsToMoveTable,
					{
						name = itemInfo.name,
						damage = itemInfo.damage,
						nbt = itemInfo.nbt,
						count = itemsToMove,
						slot = turtleSlotLookupTable[tonumber(turtleSlot)]
					})
			end
			ItemInventoryUtils.pushItemsByName(threadingManager, bufferChest.wrapped, this.crafters[turtle].wrapped,
				itemsToMoveTable)
			itemsToCraft = itemsToCraft - maxItemsCraftableAtOnce
			this.progressInfo[requestId].currentItemBeingMadeAlreadyMade = this.progressInfo[requestId]
				.currentItemBeingMadeCount - itemsToCraft
			--tell turtle to craft
			handleSlowMode(slowMode)
			local msg = Messages.createCraftOrder()
			local msgId = networking.transmit(turtle, networking.QOS.EXACTLY_ONCE, msg)
			local llmsg = networking.receive(10, os.getComputerID(), msgId)
			handleSlowMode(slowMode)
			if llmsg == nil then
				error("no response from turtle")
			end
			--start pulling items from turtle and pray we move all items
			local pullSlots = {}
			for i = 1, this.crafters[turtle].wrapped.size() do
				pullSlots[i] = 64
			end
			bufferChest.wrapped.pullItemsFromSlots(this.crafters[turtle].wrapped, pullSlots)
			handleSlowMode(slowMode)
		end
		this.crafters[turtle].status = CraftingStatus.Idle

		local items = bufferChest.wrapped.list()
		local found = 0
		for slot, item in pairs(items) do
			if item.name == craftingStep.creates then
				found = found + item.count
			end
		end
		if found < craftingStep.itemsCreated then
			Dbg.logE(TAG, "item", craftingStep.creates, "not found after crafting, cancelling craft")
			return false, "item " .. craftingStep.creates .. " missing after crafting"
		end
		return true, ""
	end

	---thread responsible for handling a crafting request
	---@param itemsToRequest ItemEntry[] items to request for craft
	---@param craftingChain CraftingStep[] chain of crafting steps to complete
	---@param callbackFnc function function called when crafting request is done
	---@param callbackFncUserArgs any arguments for callback function
	---@param requestId number number used for tracking craftRequest
	---@param slowMode number | nil if given slows down each crafting operation by given time, passing nil crafts at full speed
	local function craftingThread(itemsToRequest, craftingChain, callbackFnc, callbackFncUserArgs, requestId, slowMode)
		--find or wait for bufferchest to be free for crafting
		this.progressInfo[requestId].status = craftingProgressStatus.WaitingForBuffer
		local bufferInUse = findBufferInventoryForCrafting()
		local success, message = requestItemsToBufferInventory(itemsToRequest, bufferInUse, slowMode, 10)
		--clear buffer and indicate failure
		if not success then
			local items = bufferInUse.wrapped.list()
			while Utils.getTableLength(items) ~= 0 do
				bufferInUse.wrapped.pushInventory(this.storageInput, false)
				items = bufferInUse.wrapped.list()
				if Utils.getTableLength(items) ~= 0 then
					os.sleep(pollTime)
				end
			end
			return false, message
		end
		---@type CraftingStep[]
		local craftingChainCopy = Utils.copyTable(craftingChain)

		this.progressInfo[requestId].status = craftingProgressStatus.requestingItemsToBuffer
		this.progressInfo[requestId].buffer = bufferInUse.wrapped.getName()
		---@type string[] | nil
		local clSorterInventories = nil
		if this.CLSorter ~= nil then
			Dbg.logV(TAG, "requesting Sorter inputs")
			local msg = Messages.createInputInventoryRequest()
			local msgId = networking.transmit(this.CLSorter, networking.QOS.EXACTLY_ONCE, msg)
			local llmsg = networking.receive(5, nil, msgId)
			if llmsg then
				clSorterInventories = llmsg.payload.inputInventory
			else
				Dbg.logW(TAG, "CLSorter on network gave no response to inputInventory request")
			end
		end
		handleSlowMode(slowMode)

		this.progressInfo[requestId].status = craftingProgressStatus.Crafting
		while #craftingChainCopy > 0 and success == true do
			handleSlowMode(slowMode)
			---@type CraftingStep
			local craftingStep = table.remove(craftingChainCopy, #craftingChainCopy)
			this.progressInfo[requestId].currentItemBeingMade = craftingStep.creates
			this.progressInfo[requestId].currentItemBeingMadeCount = craftingStep.itemsCreated
			this.progressInfo[requestId].currentItemBeingMadeAlreadyMade = 0
			if craftingStep.type == "minecraft:crafting" then
				success, message = handleTurtleBasedCraftingStep(craftingStep, bufferInUse, slowMode, requestId)
			else
				success, message = handleMachineBasedCraftingStep(craftingStep, bufferInUse, clSorterInventories,
					requestId)
			end
			this.progressInfo[requestId].craftingStep = this.progressInfo[requestId].craftingStep + 1
		end
		this.progressInfo[requestId] = nil
		Dbg.logI(TAG, "crafting done")
		handleSlowMode(slowMode)

		--call callback function and when done handling clear buffer inventory
		if callbackFnc then
			if success then
				callbackFnc(requestId, true, callbackFncUserArgs, bufferInUse.name)
			else
				callbackFnc(requestId, false, nil, callbackFncUserArgs, message)
			end
		end

		local items = bufferInUse.wrapped.list()
		while Utils.getTableLength(items) ~= 0 do
			bufferInUse.wrapped.pushInventory(this.storageInput, false)
			items = bufferInUse.wrapped.list()
			if Utils.getTableLength(items) ~= 0 then
				os.sleep(pollTime) -- wait until other inventory has some space to accept items
			end
		end

		--mark bufferchest as usable
		bufferInUse.status = CraftingStatus.Idle

		--clean up thread id
		this.craftingThreads[this.curRequestId] = nil
		--create new thread for another item and remove from queue
		if Utils.getTableLength(this.craftingQueue) > 0 then
			local curThreadInfo = this.craftingQueue[1]
			table.remove(this.craftingQueue, 1)
			this.craftingThreads[this.curRequestId] = threadingManager.create(craftingThread,
				curThreadInfo.itemsToRequest, curThreadInfo.craftingChain,
				curThreadInfo.callbackFnc, curThreadInfo.callbackFncUserArgs, curThreadInfo.requestId)
		else
			this.ThreadCount = this.ThreadCount - 1
		end
	end

	--------------------------------- PUBLIC FUNCTIONS ----------------------------------

	---add buffer inventory for crafting
	---@param bufferInventory IitemInventory implementation of IitemInventory
	---@return boolean succes true wrapping succeeded, false inventory not found
	local function addBufferInventory(bufferInventory)
		PC.expectInterface(1, IitemInventory, bufferInventory)
		---@class CraftingManagerBufferInventory
		local inventory = {}
		---@type string name of buffer inventory in cct network
		inventory.name = bufferInventory.getName()
		---@type IitemInventory
		inventory.wrapped = bufferInventory
		inventory.status = CraftingStatus.Idle
		table.insert(this.bufferInventories, inventory)

		--clear buffer inventory
		local items = inventory.wrapped.list()
		while Utils.getTableLength(items) ~= 0 do
			inventory.wrapped.pushInventory(this.storageInput, false)
			items = inventory.wrapped.list()
			if Utils.getTableLength(items) ~= 0 then
				os.sleep(pollTime) -- wait until other inventory has some space to accept items
			end
		end
		return true
	end

	---add machine for crafting manager
	---@param name string machine operation name this is used to figure out what recipes the machine can perform ie: mekanism:crusher, thermal:pulverizer ...
	---@param input IitemInventory input inventory for machine, implementation of IitemInventory
	---@param output IitemInventory output inventory for machine, implementation of IitemInventory
	---@return boolean success true machine wrapped, false machine invalid
	local function addMachine(name, input, output)
		PC.expect(1, name, "string")
		PC.expectInterface(2, IitemInventory, input)
		PC.expectInterface(3, IitemInventory, output)
		---@class CraftingManagerMachineEntry
		local machine = {
			input = input,
			output = output,
			status = CraftingStatus.Idle,
		}
		this.machines[name] = machine
		return true
	end

	---add crafter to crafting manager if needed, will check for duplicates
	---@param turtleInventory IitemInventory implementation of IitemInventory interface that wraps turtle
	---@param networkId number crafter network id
	---@return boolean crafterChanged when true indicates something changed in the crafter array as a result of this call
	local function addCrafter(turtleInventory, networkId)
		PC.expectInterface(1, IitemInventory, turtleInventory)
		PC.expect(2, networkId, "number")
		--check if crafter exists by networkid
		if this.crafters[networkId] ~= nil then
			--name changed in network, should not happen normally
			if this.crafters[networkId].name ~= turtleInventory.getName() then
				--check if any other crafter has the old name and if it does remove the crafter
				for k, v in pairs(this.crafters) do
					if k ~= networkId then
						if v.name == turtleInventory.getName() then
							this.crafters[k] = nil
							break
						end
					end
				end
				this.crafters[networkId].name = turtleInventory.getName()
				this.crafters[networkId].wrapped = turtleInventory
				return true
			end
			return false
		else
			--check if any old crafter has the same name and if it does remove it
			for k, v in pairs(this.crafters) do
				if v.name == turtleInventory.getName() then
					this.crafters[k] = nil
					break
				end
			end
			---@class CraftingManagerCrafterQueueEntry
			local crafterStruct = {
				name = turtleInventory.getName(),
				wrapped = turtleInventory,
				status = CraftingStatus.Idle,
			}

			this.crafters[networkId] = Utils.copyTable(crafterStruct)

			--clear out crafter inventory
			local moveTbl = {}
			for i = 1, turtleSlotCount do
				moveTbl[i] = 64
			end
			this.storageInput.pullItemsFromSlots(crafterStruct.wrapped, moveTbl)
			return true
		end
	end

	---get crafting progress on a ongoing craft
	---@param requestId number requestId
	---@return CLCraftingStatus | nil requestInfo requestInfo on sucess, nil if craft has been completed or not found
	local function getCraftingProgress(requestId)
		return this.progressInfo[requestId]
	end

	---gets information about all crafts currently in progress
	---@return CLCraftingStatus[] progressInfo information about all crafts in progress
	local function getAllCraftingProgress()
		return this.progressInfo
	end

	---returns amount of crafters in manager
	---@return integer crafterAmount amount of crafters in manager
	local function getCrafterCount()
		return Utils.getTableLength(this.crafters)
	end

	---returns amount of buffer chests in manager
	---@return number bufferAmount amount of bufferChests in manager
	local function getBufferChestCount()
		return Utils.getTableLength(this.bufferInventories)
	end

	---start a craft
	---@param itemsToRequest ItemEntry[] items to request for craft
	---@param craftingChain CraftingStep[] information on what needs to be crafted
	---@param callbackFnc function | nil function that is called on  craft completion prototype void callbackFnc(number: requestId, boolean: success, any: callbackFncUserArgs, <string| nil: bufferInventory | message>)
	---@param callbackFncUserArgs any arguments for callback function
	---@param slowMode number | nil if a number is given waits that long between each crafting operation, useful for debugging
	---@return boolean success true if created, false otherwise
	---@return number | string msg craftRequestId when crafting has started, msg if prechecks failed
	local function craft(itemsToRequest, craftingChain, callbackFnc, callbackFncUserArgs, slowMode)
		PC.expect(1, itemsToRequest, "table")
		PC.expect(2, craftingChain, "table")
		PC.expect(3, callbackFnc, "function", "nil")
		local hasMachines = haveAllMachinesForRecipe(craftingChain)
		if hasMachines ~= true then
			return false, "missing machine " .. hasMachines
		end


		this.curRequestId = Utils.getNextId(this.curRequestId)
		this.progressInfo[this.curRequestId] = {
			status = craftingProgressStatus.Queued,
			craftingStep = 1,
			maxCraftingStep = #craftingChain,
			buffer = "",
			outputItemName = craftingChain[#craftingChain].creates,
			outputItemCount = craftingChain[#craftingChain].itemsCreated,
			requestId = this.curRequestId,
			currentItemBeingMade = craftingChain[1].creates,
			currentItemBeingMadeAlreadyMade = 0,
			currentItemBeingMadeCount = craftingChain[1].itemsCreated
		}
		--all buffers in use, queue the craft
		if this.ThreadCount > Utils.getTableLength(this.bufferInventories) then
			Dbg.logV(TAG, "more requests than buffers, queueing request until other is done")
			table.insert(this.craftingQueue,
				---@class CraftingManagerManagerQueueEntry
				{
					itemsToRequest = itemsToRequest,
					craftingChain = craftingChain,
					callbackFnc = callbackFnc,
					callbackFncUserArgs = callbackFncUserArgs,
					requestId = this.curRequestId
				})
			return true, this.curRequestId
		end
		this.ThreadCount = this.ThreadCount + 1
		this.craftingThreadIds[this.curRequestId] = threadingManager.create(craftingThread, itemsToRequest, craftingChain,
			callbackFnc, callbackFncUserArgs,
			this.curRequestId, slowMode)
		return true, this.curRequestId
	end

	---@class IcraftingManager
	local CraftingManager = {
		addBufferInventory = addBufferInventory,
		addMachine = addMachine,
		addCrafter = addCrafter,
		getCraftingProgress = getCraftingProgress,
		getAllCraftingProgress = getAllCraftingProgress,
		getCrafterCount = getCrafterCount,
		getBufferChestCount = getBufferChestCount,
		craft = craft,
	}
	return CraftingManager
end

return { new = new }
