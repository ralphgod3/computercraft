local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Crafting/IrecipeDatabase.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Crafting/IpreferredItemDb.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")
local IitemDb = require("Interfaces.Modules.IitemDb")
local IrecipeDatabase = require("Interfaces.Modules.Crafting.IrecipeDatabase")
local IpreferredItemDb = require("Interfaces.Modules.Crafting.IpreferredItemDb")

--debug tag
local TAG = "RS"
--item types we support crafting with
local itemTypeFilters = { "item", "tag" }
------------------------------------- STATIC PRIVATE FUNCTIONS -------------------------------------

---replaces any occurance of tagToReplace with itemToReplaceTagWith in recipe and returns new recipe
---@param recipe CraftingRecipeRecipe recipe to replace tags in
---@param tagToReplace string tag to replace with item
---@param itemToReplaceTagWith string item to replace tag with
---@return CraftingRecipeRecipe newRecipe modified recipe
local function replaceTagInRecipe(recipe, tagToReplace, itemToReplaceTagWith)
	---@type CraftingRecipeRecipe
	local recipeCopy = Utils.copyTable(recipe)
	--sparse table needs iterator
	for key, slotInfo in pairs(recipeCopy.recipe) do
		if slotInfo.name == tagToReplace then
			recipeCopy.recipe[key].type = "item"
			recipeCopy.recipe[key].name = itemToReplaceTagWith
		end
	end
	return recipeCopy
end

---increments curTagReplacement to next item variant
---@param curTagReplacement CraftingRecipeSolverCurTagReplacment[] current tag replacement table
---@param tagReplacements CraftingRecipeSolverTagReplacement[] replacements for tags
---@return CraftingRecipeSolverCurTagReplacment[] | nil newTagReplacement newTagReplacement if tags can still be incremented, nil if not
local function incrementItemTagReplacement(curTagReplacement, tagReplacements)
	---@type CraftingRecipeSolverCurTagReplacment[]
	local newTagReplacement = Utils.copyTable(curTagReplacement)
	local curIndex = 1
	while true do
		if newTagReplacement[curIndex].index == #tagReplacements[curIndex].items then
			newTagReplacement[curIndex].index = 1
			newTagReplacement[curIndex].item = tagReplacements[curIndex].items[1]
			curIndex = curIndex + 1
			--check if we have anything left to replace
			if curIndex > #tagReplacements then
				return nil
			end
		else
			newTagReplacement[curIndex].index = newTagReplacement[curIndex].index + 1
			newTagReplacement[curIndex].item = tagReplacements[curIndex].items[newTagReplacement[curIndex].index]
			return newTagReplacement
		end
	end
end

---generates all possible variations of a recipe by replacing tags with all items combinations
---@param recipe CraftingRecipeRecipe recipe to generate variants for
---@param ItemDb IitemDb implementation of IitemDb interface
---@param preferredItemDb IpreferredItemDb | nil implementation of IitemDb interface
---@return CraftingRecipeRecipe[] recipeVariants table containing all recipe variants
local function getRecipeVariants(recipe, ItemDb, preferredItemDb)
	---@class CraftingRecipeSolverTagReplacement
	---@field name string tag to find items for
	---@field nbt string | nil optional nbt hash of tag
	---@field items string[] | nil items that have the name tag


	--get items for tag(s)
	---@type CraftingRecipeSolverTagReplacement[]
	local tagReplacements = {}
	local replacedCount = 1
	--sparse table needs iterator
	for _, slotInfo in pairs(recipe.recipe) do
		if slotInfo.type == "tag" then
			local exists = false
			if replacedCount ~= 1 then
				for i = 1, #tagReplacements do
					if tagReplacements[i].name == slotInfo.name then
						exists = true
						break
					end
				end
			end
			if not exists then
				tagReplacements[replacedCount] = {
					name = slotInfo.name,
					nbt = slotInfo.nbt,
					items = {},
				}
				--todo: improve this so it grabs all tags from storage but only tries to craft with preferred items
				if preferredItemDb then
					tagReplacements[replacedCount].items = preferredItemDb.getItemsWithTag(slotInfo.name)
				end
				if tagReplacements[replacedCount].items == nil or #tagReplacements[replacedCount].items == 0 then
					tagReplacements[replacedCount].items = ItemDb.getItemsWithTag(slotInfo.name)
				end
				replacedCount = replacedCount + 1
			end
		end
	end
	--select first item for every tag
	---@class CraftingRecipeSolverCurTagReplacment
	---@field name string name of tag to replace
	---@field item string name of item that tag is replaced with
	---@field index number index of replaced tag

	---@type CraftingRecipeSolverCurTagReplacment[] | nil
	local curTagReplacement = {}
	if curTagReplacement ~= nil then
		for i = 1, #tagReplacements do
			curTagReplacement[i] = {
				name = tagReplacements[i].name,
				item = tagReplacements[i].items[1],
				index = 1,
			}
		end
	end

	--start generating all recipe variants
	local recipeVariants = {}
	if #tagReplacements > 0 then
		while curTagReplacement ~= nil do
			local curRecipe = recipe
			--replace tags in recipe
			for i = 1, #curTagReplacement do
				curRecipe = replaceTagInRecipe(curRecipe, curTagReplacement[i].name, curTagReplacement[i].item)
			end
			table.insert(recipeVariants, curRecipe)
			--change current tag
			curTagReplacement = incrementItemTagReplacement(curTagReplacement, tagReplacements)
		end
	end
	--recipe did not contain tags just insert it directly as a variant
	if #recipeVariants == 0 then
		table.insert(recipeVariants, recipe)
	end
	return recipeVariants
end

---get count of items needed to craft recipe
---@param recipe CraftingRecipeRecipe recipe to get costs for
---@param timesCrafted number times the recipe will be crafted
---@return CraftingRecipeSolverCost costs table {output = 1, costs = {{name = "minecraft:redstone", count = 2, nbt = nil}, {name = "minecraft:oak_planks", count = 10, nbt = nil} }} outputparam is amount crafted per craft
local function getRecipeCost(recipe, timesCrafted)
	---@class CraftingRecipeSolverCost
	---@field costs CraftingRecipeSolverCostInternal[]
	---@field count number count of items crafted as a result of this operation

	---@class CraftingRecipeSolverCostInternal
	---@field name string itemName field can be itemName or tagName
	---@field nbt string | nil optional nbt field for item
	---@field count number count of item required per craft


	timesCrafted = timesCrafted or 1
	---@type CraftingRecipeSolverCost
	local costs = {
		count = timesCrafted,
		costs = {},
	}
	--items produced per craft
	if recipe.count then
		costs.count = costs.count * timesCrafted
	end

	--traverse through each slot for recipe and add them to costs table
	--sparse table needs iterator
	for _, item in pairs(recipe.recipe) do
		local exists = false
		--check existing costs table
		for key = 1, #costs.costs do
			if costs.costs[key].name == item.name and costs.costs[key].nbt == item.nbt then
				exists = true
				costs.costs[key].count = costs.costs[key].count + (item.count * timesCrafted)
				break
			end
		end
		if not exists then
			local entry = {}
			entry.count = (item.count * timesCrafted)
			entry.name = item.name
			entry.nbt = item.nbt
			table.insert(costs.costs, entry)
		end
	end
	return costs
end

---gets items in itemsToGet table from storage or adds them to a list for items to be crafted later
---@param itemsToGet ItemEntry[] items to get {{name = "minecraft:redstone", nbt = nil, count = 10}, {name = "minecraft:cobblestone", nbt = nil, count = 20}}
---@param itemsInStorage ItemEntry[] items in the storage system same format as itemsToGet
---@return ItemEntry[] itemsInStorage items in storage after subtracting the items that can simply be requested
---@return ItemEntry[] itemsToRequest items that can be requested from the storage
---@return ItemEntry[] itemsToCraft items that need to be made since they can not be requested
local function getItems(itemsToGet, itemsInStorage)
	---internal function that subtracts items from itemsInStorage and returns remainder that could not be subtracted
	---@param itemToGet ItemEntry info about item to retrieve
	---@param itemsInStorage ItemEntry[] itemsInStorage table, does NOT make a copy
	---@return ItemEntry remainder items that can not be requested from storage and instead have to be gotten in another way
	---@return ItemEntry | nil itemsGotten items that can be requested from storage nil if items could not be pulled from storage
	local function getItemFromStorage(itemToGet, itemsInStorage)
		local itemsGotten = nil
		--sparse table needs iterator
		for itemKey, item in pairs(itemsInStorage) do
			if item.name == itemToGet.name and item.nbt == item.nbt then
				local fromStorageCount = math.min(itemToGet.count, item.count)
				itemsInStorage[itemKey].count = itemsInStorage[itemKey].count - fromStorageCount
				itemToGet.count = itemToGet.count - fromStorageCount
				--remove item from storage if count == 0
				if itemsInStorage[itemKey].count == 0 then
					itemsInStorage[itemKey] = nil
				end

				itemsGotten = Utils.copyTable(itemToGet)
				itemsGotten.count = fromStorageCount
				break
			end
		end
		return itemToGet, itemsGotten
	end
	---@type ItemEntry[]
	local copyOfItemsInStorage = Utils.copyTable(itemsInStorage)
	---@type ItemEntry[]
	local itemsToCraft = {}
	---@type ItemEntry[]
	local itemsToRequest = {}
	for i = 1, #itemsToGet do
		local toCraft, toRequest = getItemFromStorage(itemsToGet[i], copyOfItemsInStorage)
		if toRequest then
			table.insert(itemsToRequest, toRequest)
		end
		if toCraft.count > 0 then
			table.insert(itemsToCraft, toCraft)
		end
	end
	return copyOfItemsInStorage, itemsToRequest, itemsToCraft
end

---creates entry for craftingChain
---@param recipe CraftingRecipeRecipe recipe for entry
---@param requires ItemEntry[] items that need to be created before this recipe can start crafting
---@param count number items required as output from recipe NOT CRAFTS REQUIRED so if recipe producse 4 and you need 8 your put in 4 not 2
---@param itemProduced string itemId produced as result from recipe
---@return CraftingChainEntry craftingChainEntry entry that can be put directly into crafting chain of branch
local function createCraftingChainEntry(recipe, requires, count, itemProduced)
	---@class CraftingChainEntry : CraftingRecipeRecipe
	local outTable = {
		---@type number | nil crafting table height if not given defaults to 3
		height = recipe.height,
		---@type RecipeItemEntry[] | nil optional itemOutputs as a result of the crafting operation
		optional = recipe.optional,
		---@type table<number, RecipeItemEntry> sparse table with items/tags in recipe. the key is what slot they should be in for crafting
		recipe = recipe.recipe,
		---@type string describes type of crafting operation ie: minecraft:smelting or minecraft:crafting or mekanism:crusher ...
		type = recipe.type,
		---@type number | nil crafting table width if not given defaults to 3
		width = recipe.width,
		---@type string itemId produced from recipe
		creates = itemProduced,
		requires = requires,
		---@type number amount of crafts required to create nescesary item count
		craftsNeeded = math.ceil(count / recipe.count),
		---@type number amount of items produced per craft
		outputPerCraft = recipe.count,
		---@type number total amount of items produced per craft
		itemsCreated = math.ceil(count / recipe.count) * recipe.count,
	}



	return outTable
end

---checks if a branch is solved ie it is craftable
---@param branchInfo CraftingRecipeSolverBranchEntry branch to check
---@return boolean branchIsSolved true yes, false no
local function branchIsSolved(branchInfo)
	if #branchInfo.itemsToCraft == 0 then
		return true
	end
	return false
end

---merges itemTable1 with itemTable 2
---@param itemTable1 table table 1 to merge
---@param itemTable2 table table 2 to merge
---@return table newTable table with itemInfo added
local function mergeItemInfoTables(itemTable1, itemTable2)
	local localTable = Utils.copyTable(itemTable1)
	for i2 = 1, #itemTable2 do
		local exists = false
		for i = 1, #localTable do
			if localTable[i].name == itemTable2[i2].name and localTable[i].nbt == itemTable2[i2].nbt then
				exists = true
				localTable[i].count = localTable[i].count + itemTable2[i2].count
				break
			end
		end
		if not exists then
			table.insert(localTable, itemTable2[i2])
		end
	end
	return localTable
end

---checks if itemToCraftTables are equal meaning all the items contained are the same with name, nbt and count
---@param itemTable1 ItemEntry[] itemToCraft table 1 {name, count, <nbt>}
---@param itemTable2 ItemEntry[] itemToCraft table 2 {name, count, <nbt>}
---@return boolean itemToCraftTablesAreEqual true yes, false no
local function itemToCraftTablesAreEqual(itemTable1, itemTable2)
	if #itemTable1 ~= #itemTable2 then
		return false
	end
	for i = 1, #itemTable1 do
		local exists = false
		for j = 1, #itemTable2 do
			if itemTable1[i].name == itemTable2[j].name and itemTable1[i].nbt == itemTable2[j].nbt then
				exists = true
				break
			end
		end
		if not exists then
			return false
		end
	end
	return true
end

---selects and returns the best crafting solution
---@param solvedBranches CraftingRecipeSolverBranchEntry[]
---@return CraftingRecipeSolverBranchEntry[] | nil
local function getBestCraftingSolution(solvedBranches)
	if #solvedBranches == 0 then
		return nil
	end
	--simply select recipe with least amount of operations for now
	local curBest = solvedBranches[1]
	for i = 1, #solvedBranches do
		if #solvedBranches[i].craftingChain < #curBest.craftingChain then
			curBest = solvedBranches[i]
		end
	end
	return curBest
end

---create new instance of craftingRecipeSolver
---@param ItemDb IitemDb implementation of IitemDb interface
---@param recipeDatabase IrecipeDatabase implementation of IrecipeDatabase
---@param recipeTypesToUse string[] | nil table containing the recipe types that are ok to use for crafting solutions
---@param preferredItemDb IpreferredItemDb | nil implementation of IpreferredItemDb interface
---@param maxBranchDepth number | nil maximum length a branch is allowed to be before we kill it
---@return IcraftingRecipeSolver instance instance of recipe solver
local function new(ItemDb, recipeDatabase, recipeTypesToUse, preferredItemDb, maxBranchDepth)
	PC.expectInterface(1, IitemDb, ItemDb)
	PC.expectInterface(2, IrecipeDatabase, recipeDatabase)
	PC.expect(3, recipeTypesToUse, "table", "nil")

	maxBranchDepth = maxBranchDepth or 100
	if preferredItemDb then
		PC.expectInterface(4, IpreferredItemDb, preferredItemDb)
	end
	--holds all craftable items
	local craftableItems = nil
	------------------------------------- PRIVATE FUNCTIONS -------------------------------------

	---create initial branches for recipe solver to work with
	---@param itemId string itemId
	---@param count number amount of items that need to be created
	---@param nbt string | nil nbt value of item to produce
	---@param itemsInStorage ItemEntry[] items that are currently in storage, is not modified
	---@return CraftingRecipeSolverBranchEntry[] | nil branches branches to work with or nil if no recipes where found
	local function createInitialBranches(itemId, count, nbt, itemsInStorage)
		---@type CraftingRecipeSolverBranchEntry[]
		local branchInfo = {}
		--create initial branch(es) for solver
		local recipes = recipeDatabase.getRecipesFor(itemId, nbt, recipeTypesToUse, itemTypeFilters)
		--each recipe gets its own branch
		if recipes == nil then
			return nil
		end
		for i = 1, #recipes do
			local recipe = recipes[i]
			---@type string
			local recipeHash = textutils.serialize(recipe, { compact = true })
			local variants = getRecipeVariants(recipe, ItemDb, preferredItemDb)
			for j = 1, #variants do
				---@class CraftingRecipeSolverBranchEntry
				local branchEntry = {}
				---@type CraftingChainEntry[]
				branchEntry.craftingChain = {}
				---@type ItemEntry[]
				branchEntry.itemsToRequest = {}
				---@type ItemEntry[]
				branchEntry.itemsToCraft = {}
				---@type table<string, true>
				branchEntry.recipesUsed = {}
				branchEntry.recipesUsed[recipeHash] = true
				local craftsNeeded = math.ceil(count / variants[j].count)
				local itemCosts = getRecipeCost(variants[j], craftsNeeded)
				branchEntry.itemsInStorage, branchEntry.itemsToRequest, branchEntry.itemsToCraft = getItems(
					itemCosts.costs, itemsInStorage)
				---@type ItemEntry[]
				local requires = {}
				for k = 1, #branchEntry.itemsToCraft do
					requires[k] = Utils.copyTable(branchEntry.itemsToCraft[k])
					requires[k].count = nil
				end
				table.insert(branchEntry.craftingChain, createCraftingChainEntry(variants[j], requires, count, itemId))
				table.insert(branchInfo, branchEntry)
			end
		end
		if #branchInfo == 0 then
			return nil
		end
		return branchInfo
	end

	---generating the list of craftable recipes takes long so we cache if after creating the class
	local function updateCraftableRecipeCache()
		Dbg.logV(TAG, "creating craftable item db")
		local prevYield = Utils.yield()
		craftableItems = {}
		if recipeTypesToUse == nil then
			return
		end
		for i = 1, #recipeTypesToUse do
			local recipes = recipeDatabase.getAllRecipesForType(recipeTypesToUse[i])
			Dbg.logV(TAG, "adding recipes for type", recipeTypesToUse[i], " got ", #recipes)
			for j = 1, #recipes do
				if recipes[j].type == "item" then
					local exists = false
					for _, itemInfo in pairs(craftableItems) do
						if itemInfo.name == recipes[j].name and itemInfo.nbt == recipes[j].nbt then
							exists = true
						end
					end
					if not exists then
						local info = ItemDb.getItem(recipes[j].name, recipes[j].nbt)
						if info then
							table.insert(craftableItems,
								{ name = recipes[j].name, displayName = info.displayName, nbt = recipes[j].nbt })
						end
					end
				end
				prevYield = Utils.yield(prevYield)
			end
		end
		Dbg.logV(TAG, "done creating craftable item db")
	end

	------------------------------------- PUBLIC FUNCTIONS -------------------------------------

	local CraftingRecipeSolver = {}

	---solve a crafting recipe
	---@param itemId string itemName to craft
	---@param count number number of items to craft
	---@param nbt string | nil nbt data of item to craft (nil if none)
	---@param itemsInStorage ItemEntry[] items that are currently in storage should be input as {{name = "minecraft:cobblestone", count = 20, nbt = nil}, etc}
	---@param timeout number | nil time after which the solver gets killed, pass nill to continue until a solution is found
	---@return boolean success true on success, false on failure
	---@return CraftingSolution | string result IcraftingSolution (check IcraftingRecipeSolver.lua for structure) on success, msg on failure
	function CraftingRecipeSolver.solve(itemId, count, nbt, itemsInStorage, timeout)
		PC.expect(1, itemId, "string")
		PC.expect(2, count, "number")
		PC.expect(3, nbt, "string", "nil")
		PC.expect(4, itemsInStorage, "table")
		PC.field(itemsInStorage, 1, "table")
		PC.field(itemsInStorage[1], "name", "string")
		PC.field(itemsInStorage[1], "count", "number")
		PC.field(itemsInStorage[1], "nbt", "string", "nil")
		PC.expect(5, timeout, "number", "nil")
		--create starting branches to branch off of later
		---@type CraftingRecipeSolverBranchEntry[][]
		local solvedBranches = {}
		Dbg.logI(TAG, "creating initial branches")
		local initialBranches = createInitialBranches(itemId, count, nbt, itemsInStorage)
		--check if we have valid starting branches
		if initialBranches == nil then
			local retString = "No recipe for " .. itemId
			if nbt ~= nil then
				retString = retString .. " with nbt " .. nbt
			end
			return false, retString
		end
		--check if we already have a valid solution
		for k, v in pairs(initialBranches) do
			if branchIsSolved(v) then
				v.itemsInStorage = nil
				v.itemsToCraft = nil
				v.recipesUsed = nil
				table.insert(solvedBranches, v)
				initialBranches[k] = nil
			end
		end
		---@type CraftingRecipeSolverBranchEntry
		initialBranches = Utils.rebuildArray(initialBranches)
		--setup some variables for debugging, returning errors and tracking progress
		local prevYield = Utils.yield()
		local lastPrint = 0
		local iterations = 1
		local lastBranch = nil
		Dbg.logI(TAG, "starting solver loop")
		local startSolverTime = os.epoch("local")
		--start solving until no branches are left
		while #initialBranches > 0 do
			--timeout check
			if timeout then
				if startSolverTime + timeout < os.epoch("local") then
					break
				end
			end
			prevYield = Utils.yield(prevYield)
			---@type CraftingRecipeSolverBranchEntry
			local curBranch = table.remove(initialBranches, 1)
			--lastBranch is used to return errors about missing items to the user
			lastBranch = curBranch
			--since lua doesnt have continue alive will have to do for now
			local alive = true
			--check if branch is solved
			if #curBranch.itemsToCraft == 0 then
				curBranch.itemsInStorage = nil
				curBranch.itemsToCraft = nil
				curBranch.recipesUsed = nil
				table.insert(solvedBranches, curBranch)
				alive = false
				--todo: temporary until a better way can be found to reduce run time
				break
			end
			--print some debug info
			if os.epoch("local") - lastPrint > 1000 then
				local secsPassed = (startSolverTime - lastPrint) / 1000
				local avg = iterations / secsPassed
				avg = math.floor(avg * -1)
				Dbg.logV(
					TAG,
					"branches " ..
					tostring(#initialBranches) ..
					" cur lng " ..
					tostring(#curBranch.craftingChain) .. " itr " .. tostring(iterations) .. " avg/s " .. tostring(avg)
				)
				lastPrint = os.epoch("local")
			end
			--grab item that needs crafting from branch
			local curItem = nil
			if alive then
				---@type ItemEntry
				curItem = table.remove(curBranch.itemsToCraft, #curBranch.itemsToCraft)
				--kills branch if it gets too long
				if #curBranch.craftingChain >= maxBranchDepth then
					alive = false
				end
			end
			--grab recipes for item
			local recipes = nil
			if alive then
				recipes = recipeDatabase.getRecipesFor(curItem.name, curItem.nbt, recipeTypesToUse, itemTypeFilters)
				--if no recipes are found for the item the branch is dead. set relevant variables in lastBranch for error returns
				if recipes == nil then
					lastBranch.missingItem = { name = curItem.name, count = curItem.count }
					alive = false
				end
			end
			--start calculating recipe variations for item
			if alive then
				local newBranches = {}
				if recipes == nil then
					Dbg.logE(TAG, "recipes table == nil")
					break
				end
				for i = 1, #recipes do
					--get "hash" for recipe for loop detection, need to improve this later
					local recipeHash = textutils.serialize(recipes[i], { compact = true })
					if curBranch.recipesUsed[recipeHash] == nil then
						--get all variants of recipe
						local variants = getRecipeVariants(recipes[i], ItemDb, preferredItemDb)
						for j = 1, #variants do
							--create new branch for recipe
							---@type CraftingRecipeSolverBranchEntry
							local branchEntry = Utils.copyTable(curBranch)
							local craftsNeeded = math.ceil(curItem.count / variants[j].count)
							local itemCosts = getRecipeCost(variants[j], craftsNeeded)
							local newInStorage, addToRequest, addToCraft = getItems(itemCosts.costs,
								branchEntry.itemsInStorage)
							branchEntry.itemsInStorage = newInStorage
							branchEntry.itemsToRequest = mergeItemInfoTables(branchEntry.itemsToRequest, addToRequest)
							branchEntry.itemsToCraft = mergeItemInfoTables(branchEntry.itemsToCraft, addToCraft)
							branchEntry.recipesUsed[recipeHash] = true
							--create dependencies for this recipe for parallel procesing when crafting
							local requires = {}
							for k = 1, #addToCraft do
								requires[k] = Utils.copyTable(addToCraft[k])
								requires[k].count = nil
							end
							--add crafting recipe to crafting chain and branch to newBranches
							table.insert(branchEntry.craftingChain,
								createCraftingChainEntry(variants[j], requires, curItem.count, curItem.name))
							table.insert(newBranches, branchEntry)
						end
					end
				end
				--add new branches to solvable branches
				for i = 1, #newBranches do
					iterations = iterations + 1
					--check branch length to remove it should it get too long
					if #newBranches[i].craftingChain < maxBranchDepth then
						local exists = false
						for j = 1, #initialBranches do
							if (itemToCraftTablesAreEqual(newBranches[i].itemsToCraft, initialBranches[j].itemsToCraft)) then
								exists = true
								break
							end
						end
						if not exists then
							table.insert(initialBranches, newBranches[i])
						end
					end
				end
			end
		end
		--print some debugging statistics
		local stopSolverTime = os.epoch("local")
		Dbg.logI(
			TAG,
			"took  " ..
			tostring(iterations) ..
			" iterations and " ..
			tostring(stopSolverTime - startSolverTime) ..
			" ms" .. " iters/s " .. tostring(math.floor(iterations / ((stopSolverTime / startSolverTime) / 1000)))
		)

		--get best crafting solution, this currently is the first one it found
		local bestSolution = getBestCraftingSolution(solvedBranches)
		if bestSolution then
			return true, bestSolution
		end
		--no crafting solutions found, generate status message for user
		local outStr = "branch depth of " .. tostring(maxBranchDepth) .. " exceeded"
		if lastBranch.missingItem then
			outStr = "missing " .. tostring(lastBranch.missingItem.count) .. " " .. tostring(lastBranch.missingItem.name)
		else
		end

		return false, outStr
	end

	---dumps all craftable item entries
	---@return RecipeSolverCraftableItem[] craftableItems array where each entry contains {name, displayName, nbt}}
	function CraftingRecipeSolver.dumpCraftableItems()
		return craftableItems
	end

	updateCraftableRecipeCache()
	return CraftingRecipeSolver
end

return { new = new }
