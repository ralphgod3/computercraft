local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")

local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

local remoteLocation = "DataFiles/VERSION/recipeDatabase/"
local localLocation = "cache/localRecipeDb.json"
Dbg.setLogLevel("FBRD", Dbg.Levels.Warning)

---creates new file based recipe database, can load recipes from internet if given recipeSetName, will load recipes from "cache/localRecipeDb.json"
---@param recipeSetName string | nil recipe file to load from the internet, if set to nil will not load recipes from internet, add file extension
---@return IrecipeDatabase instance instance of recipeDatabase
local function new(recipeSetName)
	PC.expect(1, recipeSetName, "string", "nil")
	local TAG = "FBRD"
	local this = {
		RemoteDb = {},
		LocalDb = {}
	}

	---------------------------------------------- INTERNAL FUNCTIONS -----------------------------------------

	---load recipes from internet/ local file for later use
	local function loadRecipes()
		--load remote
		if recipeSetName then
			local version = Utils.getVersionInfo()
			local fileLoc = string.gsub(remoteLocation, "VERSION", version)
			fileLoc = fileLoc .. recipeSetName
			local file = nil
			if not fs.exists(fileLoc) then
				print("loading recipes from internet")
				file = Git.downloadFileFromGit(Git.ComputerCraftProjectID, fileLoc)
			else
				local f = fs.open(fileLoc, "r")
				file = f.readAll()
				f.close()
			end
			if file then
				this.RemoteDb = textutils.unserialiseJSON(file)
				this.RemoteDb = this.RemoteDb or {}
			else
				print("failed to load remote recipe database")
			end
		end
		--load local
		if fs.exists(localLocation) then
			local f = fs.open(localLocation, "r")
			local file = f.readAll()
			f.close()
			this.LocalDb = textutils.unserializeJSON(file)
			this.LocalDb = this.LocalDb or {}
		end
		this.LocalDb = Utils.rebuildArray(this.LocalDb)
		this.RemoteDb = Utils.rebuildArray(this.RemoteDb)
	end

	---gets all recipes for a specific recipe type
	---@param type string type to get recipes for
	---@param database table database to use for retrieving types
	---@return table recipeTable recipes for type
	local function internalGetAllRecipesForType(type, database)
		local outTable = {}
		for i = 1, #database do
			local add = false
			local entry = {}
			entry = Utils.copyTable(database[i])
			entry.recipes = {}
			for j = 1, #database[i].recipes do
				if database[i].recipes[j].type == type then
					table.insert(entry.recipes, database[i].recipes[j])
					add = true
				end
			end
			if add then
				table.insert(outTable, entry)
			end
		end
		return Utils.copyTable(outTable)
	end

	---gets recipe types in one of the databses
	---@param database table database to get types for
	---@return table types table containing types as names
	local function internalGetRecipeTypes(database)
		local outTable = {}
		for i = 1, #database do
			for j = 1, #database[i].recipes do
				if Utils.findElementInTable(outTable, database[i].recipes[j].type) == nil then
					table.insert(outTable, database[i].recipes[j].type)
				end
			end
		end
		--return a deep copy so we are sure no edits to the database are made
		return Utils.copyTable(outTable)
	end

	---get list of inventory's items
	---@param name string itemName of the item you want to get the recipes for
	---@param nbt string | nil nbtHash of the item you want to get the recipes for, if nil recipe without nbt output will be used
	---@param recipeTypeFilters table | nil recipeTypes to consider for output, ensures it wil only output recipes of the type you want, if nil will output all types
	---@param itemTypeFilters table | nil item types allowed for output ie: item, tag, fluid, etc
	---@param database table database to get recipes from
	---@return table | nil itemList table of recipes implementing Irecipe interface
	local function internalGetRecipesFor(name, nbt, recipeTypeFilters, itemTypeFilters, database)
		for i = 1, #database do
			local recipes = database[i]
			if recipes.name == name and recipes.nbt == nbt and (itemTypeFilters == nil or Utils.findElementInTable(itemTypeFilters, recipes.type)) then
				local add = false
				local entry = {}
				entry = Utils.copyTable(recipes)
				entry.recipes = {}
				for j = 1, #recipes.recipes do
					local recipe = recipes.recipes[j]
					local addRecipe = false
					--check recipe type is part of the filters or that no filters are set
					if recipeTypeFilters == nil or Utils.findElementInTable(recipeTypeFilters, recipe.type) ~= nil then
						if itemTypeFilters == nil then
							add = true
							addRecipe = true
						else
							--verify inputs
							local containsFilteredItemType = false
							for k = 1, #recipe.recipe do
								local itemInfo = recipe.recipe[k]
								if Utils.findElementInTable(itemTypeFilters, itemInfo.type) == nil then
									containsFilteredItemType = true
									break
								end
							end
							if not containsFilteredItemType then
								addRecipe = true
								add = true
							end
						end
					end
					if addRecipe then
						table.insert(entry.recipes, recipe)
					end
				end
				if add then
					return Utils.copyTable(entry)
				end
			end
		end
		return nil
	end

	---check if recipe is equal
	---@param recipeA CraftingRecipeRecipe
	---@param recipeB CraftingRecipeRecipe
	---@return boolean match true if recipes match
	local function internalCheckIfRecipeIsEqual(recipeA, recipeB)
		local aWidth = recipeA.width or 3
		local aHeight = recipeA.height or 3
		local bWidth = recipeB.width or 3
		local bHeight = recipeB.height or 3
		if aWidth ~= bWidth or aHeight ~= bHeight or recipeA.count ~= recipeB.count or recipeA.optional ~= recipeB.optional or recipeA.type ~= recipeB.type then
			Dbg.logV(TAG, "internalCheckIfRecipeIsEqual width, height, outputCount or optional dont match")
			return false
		end

		for i = 1, aWidth * aHeight do
			local iStr = tostring(i)
			local aItem = recipeA.recipe[iStr]
			local bItem = recipeB.recipe[iStr]
			if aItem ~= nil and bItem ~= nil then
				if aItem.name ~= bItem.name or aItem.count ~= bItem.count or aItem.type ~= bItem.type or aItem.nbt ~= bItem.nbt then
					Dbg.logV(TAG, "internalCheckIfRecipeIsEqual name, count, type or nbt dont match")
					return false
				end
			elseif aItem ~= bItem then
				Dbg.logV(TAG, "internalCheckIfRecipeIsEqual slot is empty in one of the recipes")
				return false
			end
		end
		Dbg.logV(TAG, "internalCheckIfRecipeIsEqual recipe match")
		return true
	end

	---------------------------------------------- EXPORTED FUNCTIONS -----------------------------------------

	local recipeDatabase = {}
	---get all recipes for a recipe type in database
	---@param type string recipe type
	---@return CraftingRecipe[] recipesForType outputs all recipes for a specific recipe type
	function recipeDatabase.getAllRecipesForType(type)
		PC.expect(1, type, "string")
		local recipesForType = internalGetAllRecipesForType(type, this.RemoteDb)
		local localTypes = internalGetAllRecipesForType(type, this.LocalDb)
		local notInRemote = {}
		for i = 1, #localTypes do
			local localRecipes = localTypes[i]
			local exists = false
			for remoteIndex = 1, #recipesForType do
				local remoteRecipes = recipesForType[remoteIndex]
				if remoteRecipes.name == localRecipes.name then
					exists = true
					for j = 1, #localRecipes do
						table.insert(recipesForType[remoteIndex].recipes, localRecipes[j])
					end
					break
				end
			end
			if not exists then
				table.insert(notInRemote, localRecipes)
			end
		end
		for i = 1, #notInRemote do
			table.insert(recipesForType, notInRemote[i])
		end
		return recipesForType
	end

	---get recipe types in the recipe database
	---@return string[] recipeTypes table containing name of the recipe types
	function recipeDatabase.getRecipeTypes()
		local recipeTypes = internalGetRecipeTypes(this.RemoteDb)
		local localTypes = internalGetRecipeTypes(this.LocalDb)

		for i = 1, #localTypes do
			local exists = false
			for j = 1, #recipeTypes do
				if recipeTypes[j] == localTypes[i] then
					exists = true
					break
				end
			end
			if not exists then
				table.insert(recipeTypes, localTypes[i])
			end
		end
		return recipeTypes
	end

	---get list of inventory's items
	---exampleUse: FileBasedRecipeDatabase.getRecipesFor("minecraft:oak_planks", nil, {"minecraft:crafting"}, { "item"})
	---@param name string itemName of the item you want to get the recipes for
	---@param nbt string | nil nbtHash of the item you want to get the recipes for, if nil recipe without nbt output will be used
	---@param recipeTypeFilters string[] | nil recipeTypes to consider for output, ensures it wil only output recipes of the type you want, if nil will output all types
	---@param itemTypeFilters string[] | nil item types allowed for output ie: item, tag, fluid, etc
	---@return CraftingRecipeRecipe[] | nil itemList crafting recipes
	function recipeDatabase.getRecipesFor(name, nbt, recipeTypeFilters, itemTypeFilters)
		PC.expect(1, name, "string")
		PC.expect(2, nbt, "string", "nil")
		PC.expect(3, recipeTypeFilters, "table", "nil")
		PC.expect(4, itemTypeFilters, "table", "nil")

		local recipesFor = internalGetRecipesFor(name, nbt, recipeTypeFilters, itemTypeFilters, this.RemoteDb)
		local localRecipesFor = internalGetRecipesFor(name, nbt, recipeTypeFilters, itemTypeFilters, this.LocalDb)
		if localRecipesFor then
			recipesFor = recipesFor or {}
			recipesFor.recipes = recipesFor.recipes or {}
			for i = 1, #localRecipesFor.recipes do
				table.insert(recipesFor.recipes, localRecipesFor.recipes[i])
			end
		end
		if recipesFor then
			return recipesFor.recipes
		end
		return nil
	end

	---adds a recipe to recipe database
	---@param recipe CraftingRecipe recipe to add
	function recipeDatabase.addRecipe(recipe)
		PC.expect(1, recipe, "table")
		PC.field(recipe, "type", "string")
		PC.field(recipe, "name", "string")
		PC.field(recipe, "nbt", "string", "nil")
		PC.field(recipe, "recipes", "table")
		--recipes subtable verification
		for _, rec in pairs(recipe.recipes) do
			PC.field(rec, "type", "string")
			PC.field(rec, "count", "number")
			PC.field(rec, "optionals", "table", "nil")
			PC.field(rec, "recipe", "table")
			--slot in recipe verification
			for _, slot in pairs(rec.recipe) do
				PC.field(slot, "type", "string")
				PC.field(slot, "name", "string")
				PC.field(slot, "count", "number")
				PC.field(slot, "nbt", "string", "nil")
			end
		end
		--iterate over local recipe db and add to existing recipe entry if needed
		local isNew = true
		for recipeEntry, recipes in pairs(this.LocalDb) do
			if recipes.name == recipe.name and recipes.nbt == recipe.nbt and recipes.type == recipe.type then
				Dbg.logV(TAG, "addRecipe output item is in db")
				isNew = false

				--check if recipe already exists
				for _, r in pairs(recipe.recipes) do
					local notNewRecipe = true
					for _, rr in pairs(this.LocalDb[recipeEntry].recipes) do
						notNewRecipe = internalCheckIfRecipeIsEqual(r, rr)
						if notNewRecipe then
							Dbg.logV(TAG, "addRecipe found matching recipe in db not adding recipe")
							break
						end
					end
					if not notNewRecipe then
						Dbg.logI(TAG, "adding recipe to db")
						table.insert(this.LocalDb[recipeEntry].recipes, r)
					end
				end
			end
		end
		if isNew then
			Dbg.logV(TAG, "addRecipe output item added to db")
			table.insert(this.LocalDb, recipe)
		end
		--save file
		local file = fs.open(localLocation, "w")
		file.write(textutils.serializeJSON(this.LocalDb))
		file.close()
	end

	loadRecipes()
	return recipeDatabase
end

---gets available options for recipe sets, used as an argument for creating an instance of FileBasedRecipeDatabase
---@return string[] recipeSetOptions table that holds names of all recipe set options
local function getRecipeSetOptions()
	local version = Utils.getVersionInfo()
	local fileLoc = string.gsub(remoteLocation, "VERSION", version)
	local contents = Git.getFolderContentsFromGit(Git.ComputerCraftProjectID, fileLoc)
	local output = {}
	if contents ~= nil then
		for k, v in pairs(contents.files) do
			output[k] = v.name
		end
	end
	return output
end

return {
	new = new,
	getRecipeSetOptions = getRecipeSetOptions
}
