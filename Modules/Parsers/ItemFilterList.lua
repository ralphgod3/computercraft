--allow for easy parsing of item and tag lists for filtering

local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local PC = require("Modules.ParamCheck")
local IitemDb = require("Interfaces.Modules.IitemDb")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")

---@class ItemFilterParser
local ItemFilterList =
{
    ---@enum ItemFilterType
    entryTypes = {
        Item = 0, --item type
        Tag = 1, --tag type
    }
}

---internal function, returns item entry for filter list
---@param entryInfo ItemFilterEntry[] entry to parse
---@return ItemEntry parsedEntry parsed entry
local function parseItemEntry(entryInfo)
	local name = nil
	---@type number?
	local count = nil
	local nbt = nil
    if #entryInfo > 1 then
        name = entryInfo[2]
    end
    if #entryInfo > 2 then
        count = tonumber(entryInfo[3])
    end
    if #entryInfo > 3 then
        nbt = entryInfo[4]
    end
    if count == false or count < 0 then
        count = nil
    end
    return {name = name, count = count, nbt = nbt}
end

---internal function, solves a tag and returns all matching items for said tag
---@param ItemDb IitemDb implementation of Iitemdb interface
---@param entryInfo ItemFilterEntry[] information about tag entry
---@return ItemEntry[] itemEntryTable table of item entries that match tag
local function parseTagEntry(ItemDb, entryInfo)
    if #entryInfo ~= 2 then
---@diagnostic disable-next-line: param-type-mismatch
        error("invalid entry in filter list " .. Utils.printTableRecursive(entryInfo))
    end
    local items = ItemDb.getItemsWithTag(entryInfo[2])
    local ret = {}
    for i = 1, #items do
        table.insert(ret, {name = items[i]})
    end
    return ret
end

---@alias ItemFilterEntry string

---create a sample entry for white or black list
---@param entryType ItemFilterType FilterList.entryTypes entry type to create
---@return ItemFilterEntry? entry created entry
function ItemFilterList.createSampleEntry(entryType)
    PC.expectEnum(1, ItemFilterList.entryTypes, entryType)
    if entryType == ItemFilterList.entryTypes.Item then
        return "I minecraft:redstone -1 1234567890abcdef"
    elseif entryType == ItemFilterList.entryTypes.Tag then
        ---@type ItemFilterEntry
        return "T forge:ores"
    end
    return nil
end

---creates commented explanation for how to use the itemEntrys or whitelist, blacklist in the config manually
---@return string explanation explanation that can be put into header of your config
function ItemFilterList.createExplanation()
    local explanation = "--------------------- explanation of ItemEntry, whitelist, blacklist ---------------------\r\n"
    .. "--each line in a white/blacklist consists of an itemEntry\r\n"
    .. "--each itemEntry can be either a tag or item entry\r\n"
    .. "--tagEntry: entry that describes a tag to be filtered\r\n"
    .. "--space is used as a seperator\r\n"
    .. "--fields marked with <> are mandatory () is optional\r\n"
    .. "--format for item entry is I <name> (count) (nbt)\r\n"
    .. "--if count does not make sense or is not nescesary it can be set as -1 to ignore it in parsing, nbt is the nbthash as used by cc\r\n"
    .. "--format for tag entry is T <name>\r\n"
    .. "--ex: T minecraft:logs  will add all items with minecraft:logs tag to the filter list\r\n"
    .. "--ex: I minecraft:oak_log will add oak log item to filter list\r\n"
    .. "--if a whitelist and blacklist is set then the blacklist will be subtracted from the whitelist allowing for whitelisting of tags but blacklisting certain items in that tag\r\n"
    .. "--------------------- end of ItemEntry, whitelist, blacklist explanation ---------------------\r\n"
    return explanation
end


---creates a sample table containing all variants of entries that can get parsed
---@return ItemFilterEntry[] sampleList sample that can be used for default config creation
function ItemFilterList.createSampleList()
    return
    {
        ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item),
        ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Tag)
    }
end

---parse an item or tag entry
---@param ItemDb IitemDb implementation of IitemDb interface
---@param entryToParse ItemFilterEntry entry to parse
---@return ItemEntry[] outTable table of items that entry converts into
function ItemFilterList.parseEntry(ItemDb, entryToParse)
    PC.expectInterface(1, IitemDb, ItemDb)
    PC.expect(2, entryToParse, "string")
    local retTable = {}
    local tempTable = Utils.splitString(entryToParse, " ")
    if tempTable == nil then
        error("invalid entry " .. entryToParse)
    end
    if tempTable[1] == "I" then
        table.insert(retTable, parseItemEntry(tempTable))
    elseif tempTable[1] == "T" then
        for _,v in pairs(parseTagEntry(ItemDb, tempTable)) do
            table.insert(retTable, v)
        end
    else
        error("invalid entry " .. entryToParse)
    end
    return retTable
end

---parse a full filterList (white or black list) and returns item results
---@param ItemDb IitemDb implementation of IitemDb interface
---@param listToParse ItemFilterEntry[] | nil filter list to parse, should be like {"T minecraft:logs", "I minecraft:redstone"} if nil function returns nil
---@return ItemEntry[]? itemList list of parsed items used for filtering
function ItemFilterList.parseFilterList(ItemDb, listToParse)
    PC.expectInterface(1, IitemDb, ItemDb)
    PC.expect(2, listToParse, "table", "nil")
    if listToParse == nil then
        return nil
    end
    local retTable = {}
    for i = 1, #listToParse do
        local tempList = ItemFilterList.parseEntry(ItemDb, listToParse[i])
        for j = 1, #tempList do
            table.insert(retTable, tempList[j])
        end
    end
    return retTable
end

---creates whitelist by subtracting items from blacklist, allows for putting tags on whitelist and blacklist individual items on blacklist to not be on whitelist
---ex: {"T forge:stone"}, {"I minecraft:stone", "I minecraft:granite"} will add all stone to whitelist but will remove stone and granite from it
---can be passed a white and blacklist directly from config and will return apropriate result for further use
---@param itemDb IitemDb implementation of IitemDb interface
---@param whitelist ItemFilterEntry[] | nil whitelist as given from createSampleList(), if passed nil will return other list
---@param blacklist ItemFilterEntry[] | nil blacklist as given from createSampleList(), if passed nil will return other list
---@return ItemEntry[] | nil whitelist whitelist with removed blacklist entries, nil if only blacklist was given
---@return ItemEntry[] | nil blacklist blacklist or nil if both black and whitelist where given
function ItemFilterList.createFilteredlists(itemDb, whitelist, blacklist)
    PC.expectInterface(1, IitemDb, itemDb)
    PC.expect(2, whitelist, "table", "nil")
    PC.expect(3, blacklist, "table", "nil")
    if whitelist then
        whitelist = ItemFilterList.parseFilterList(itemDb, whitelist)
    end
    if blacklist then
        blacklist = ItemFilterList.parseFilterList(itemDb, blacklist)
    end
    if whitelist == nil then
        return nil, blacklist
    end
    if blacklist == nil then
        return whitelist, nil
    end
    for i = 1, #blacklist do
        for j = 1, #whitelist do
            if blacklist[i].name == whitelist[j].name and blacklist[i].nbt == whitelist[j].nbt then
                table.remove(whitelist, j)
                break
            end
        end
    end
    return whitelist, nil
end

return ItemFilterList