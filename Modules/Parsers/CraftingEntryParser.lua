--allow for easy parsing of crafting entries in config files

local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")

---@class CraftingEntryParser
local CraftingEntryParser = {}

---creates commented explanation for how to use the craftingEntries in the config
---@return string explanation explanation that can be put into header of your config
function CraftingEntryParser.createExplanation()
    local explanation = "--------------------- explanation of craftingEnties ---------------------\r\n"
    .. "--each line is the request to keep a certain item in stock by ComputerizedLogistics\r\n"
    .. "--space is used as a seperator\r\n"
    .. "--format for item entry is: name nbt count maxCraftableAtOnce\r\n"
    .. "--if item does not have nbt then -1 can be used instead\r\n"
    .. "--the example below keeps 256 coal in storage and will craft in increments of 64\r\n"
    .. "--ie: minecraft:coal -1 256 64\r\n"
    .. "--------------------- end of craftingEnties explanation ---------------------\r\n"
    return explanation
end

---creates a sample table containing all variants of entries that can get parsed
---@return string[] sampleList sample that can be used for default config creation
function CraftingEntryParser.createSampleList()
    return
    {
        "computercraft:computer_normal 1234567890abcdef 64 4",
        "minecraft:coal -1 256 64"
    }
end

---parse a crafting item entry
---@param entryToParse string entry to parse
---@return configCraftingEntry outTable table of items that entry converts into
function CraftingEntryParser.parseEntry(entryToParse)
    PC.expect(1, entryToParse, "string")
    local tempTable = Utils.splitString(entryToParse, " ")
    if tempTable == nil or #tempTable ~= 4 then
        error("invalid entry " .. entryToParse)
    end

    ---@class configCraftingEntry
    local ret = {}
    ret.name = tempTable[1]
    if tempTable[2] ~= "-1" then
        ret.nbt = tempTable[2]
    end
    ret.count = tonumber(tempTable[3])
    ret.maxCraftableAtOnce = tonumber(tempTable[4])
    if ret.count < 1 or ret.maxCraftableAtOnce < 1 then
        error("invalid entry" .. entryToParse)
    end
    return ret
end

return CraftingEntryParser