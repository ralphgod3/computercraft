--allows for easy parsing of inventory entries in a config table

local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")

---@class InventoryConfigParser
local InventoryConfig = {
    ---@enum InventoryConfigType
    configTypes =
    {
        name = 0,
        range = 1,
        type = 2,
    }
}

---creates an explanation to paste into your config file that explains setup possibilities
---@return string explanation
function InventoryConfig.createExplanation()
    local explanation = "--------------------- InventoryEntryExplanation ---------------------\r\n"
    .. "--inventory entries have 3 ways of defining, this explanation is generic so check in your config file which methods are supported\r\n"
    .. "--defining an inventory by name\r\n"
    .. "--ex: {name = \"minecraft:chest_x\"} this will wrap exactly 1 inventory\r\n"
    .. "--defining an inventory by range\r\n"
    .. "--defining an inventory by range is used to wrap 0 or more inventories of the same type in 1 definition\r\n"
    .. "--ex: {type = \"minecraft:chest\", start = 0, stop = 10} will wrap minecraft:chest_0 till minecraft:chest_10\r\n"
    .. "--defining an inventory by type\r\n"
    .. "--ex: {type = \"minecraft:chest\"} wraps any minecraft:chest it can find, use with caution\r\n"
    .. "--------------------- end of InventoryEntryExplanation ---------------------\r\n"
    return explanation
end

---parses a config entry and returns names of inventories that need to be wrapped
---@param configEntry IconfigEntry config entry to parse
---@return string[] inventoryNames names of inventories that should be wrapped according to config entry
---@return InventoryConfigType entryType type of the inventory entry
function InventoryConfig.parseConfigEntry(configEntry)
    PC.expect(1, configEntry, "table")
    PC.field(configEntry, "name", "string", "nil")
    PC.field(configEntry, "type", "string", "nil")
    PC.field(configEntry, "start", "number", "nil")
    PC.field(configEntry, "stop", "number", "nil")
    --1 inventory by name
    if configEntry.name ~= nil and configEntry.start == nil and configEntry.stop == nil then
        return {configEntry.name}, InventoryConfig.configTypes.name
    --range of inventories
    elseif configEntry.type ~= nil and configEntry.start ~= nil and configEntry.stop ~= nil then
        local start = math.min(configEntry.start, configEntry.stop)
        local stop = math.max(configEntry.start, configEntry.stop)
        local retTable = {}
        for i = start, stop do
            table.insert(retTable, configEntry.type .. "_" .. tostring(i))
        end
        return retTable, InventoryConfig.configTypes.range
    --type of inventory
    elseif configEntry.type ~= nil and configEntry.start == nil and configEntry.stop == nil then
        local inventories = {peripheral.find(configEntry.type)}
        local retTable = {}
        for _, wrapped in ipairs(inventories) do
            table.insert(retTable, peripheral.getName(wrapped))
        end
        return retTable, InventoryConfig.configTypes.type
    end
    error("invalid storage entry in config", 2)
end

---creates an example config entry for an item
---@param type InventoryConfigType configTypes enum
---@return IconfigEntry exampleEntry entry that can be used as an example in the config
function InventoryConfig.createConfigEntry(type)
    PC.expectEnum(1, InventoryConfig.configTypes, type)
    ---@class IconfigEntry
    local outTable = {}
    if type == InventoryConfig.configTypes.name then
        outTable.name = "minecraft:chest_x"
    elseif type == InventoryConfig.configTypes.range then
        outTable.type = "minecraft:chest"
        outTable.start = 0
        outTable.stop = 10
    elseif type == InventoryConfig.configTypes.type then
        outTable.type = "minecraft:chest"
    end
    return outTable
end






return InventoryConfig