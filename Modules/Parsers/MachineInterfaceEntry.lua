local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")

local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local InventoryConfig = require("Modules.Parsers.InventoryConfig")

---@enum MachineInterfaceIOEntryType
local IOentryTypes =
{
	InputRecipe = "IR",
	InputMapping = "IM",
	Output = "O"
}

---parse output entry into table
---@param entry string[] split string from entry
---@return MachineInterfaceOutputEntry entry
local function parseOutputEntry(entry)
	if entry[1] ~= "O" or #entry ~= 3 then
		error("invalid entry " .. Utils.printTableRecursive(entry), 3)
	end
	---@class MachineInterfaceOutputEntry
	local parsedEntry = {}
	parsedEntry.type = entry[1]
	parsedEntry.slot = tonumber(entry[3])
	parsedEntry.inventory = entry[2]
	if type(parsedEntry.slot) ~= "number" then
		error("invalid entry, slot not a number " .. Utils.printTableRecursive(entry))
	end
	return parsedEntry
end

---parse input entry into table
---@param entry string[] split string from entry
---@return MachineInterfaceInputEntry entry
local function parseInputEntry(entry)
	if (entry[1] ~= IOentryTypes.InputMapping and entry[1] ~= IOentryTypes.InputRecipe) or (#entry ~= 2 and #entry ~= 4) then
		error("invalid entry " .. Utils.printTableRecursive(entry), 3)
	end
	---@class MachineInterfaceInputEntry
	local parsedEntry = {}
	parsedEntry.type = entry[1]
	parsedEntry.inventory = entry[2]
	if parsedEntry.type == IOentryTypes.InputMapping then
		parsedEntry.slot = tonumber(entry[3])
		parsedEntry.itemsToKeepStocked = tonumber(entry[4])
		if type(parsedEntry.slot) ~= "number" or type(parsedEntry.itemsToKeepStocked) ~= "number" then
			error("invalid entry, slot or itemsToKeepStocked not a number " .. Utils.printTableRecursive(entry))
		end
	end
	return parsedEntry
end

---create sample input or output entry
---@param entryType MachineInterfaceIOEntryType entry type to create
---@return string entryString
local function createIOEntry(entryType)
	PC.expectEnum(1, IOentryTypes, entryType)
	if entryType == IOentryTypes.InputMapping then
		return "IM minecraft:chest_x 1 64"
	elseif entryType == IOentryTypes.InputRecipe then
		return "IR minecraft:chest_x"
	elseif entryType == IOentryTypes.Output then
		return "O minecraft:chest_x 3"
	end
	return ""
end

---parse MachineInterfaceEntry
---@param entry MachineInterfaceEntry
---@return ParsedMachineInterfaceEntry
local function parse(entry)
	local parsedEntry = {}
	---@class ParsedMachineInterfaceEntry
	parsedEntry.machineNames = InventoryConfig.parseConfigEntry(entry)
	local inputs = {}
	local outputs = {}
	for _, v in pairs(entry.entries) do
		local split = Utils.splitString(v, " ")
		if split ~= nil then
			if split[1] == IOentryTypes.InputMapping or split[1] == IOentryTypes.InputRecipe then
				table.insert(inputs, parseInputEntry(split))
			elseif split[1] == IOentryTypes.Output then
				table.insert(outputs, parseOutputEntry(split))
			end
		end
	end
	if #inputs ~= 0 then
		parsedEntry.inputs = inputs
	end
	if #outputs ~= 0 then
		parsedEntry.outputs = outputs
	end
	return parsedEntry
end

---creates example entry for machine in machineInterface (creates minecraft:furnace entry)
---@return MachineInterfaceEntry entry
local function createEntry()
	---@class MachineInterfaceEntry : IconfigEntry
	local entry = {}
	entry.type = "minecraft:furnace"
	entry.entries = {}
	entry.entries[1] = "IM minecraft:chest_x 1 4" -- furnace item slot
	entry.entries[2] = "IM minecraft:chest_x 2 8" -- furnace fuel slot
	entry.entries[3] = "O minecraft:chest 3"   -- furnace output slot
	return entry
end

---creates explanation for config files
---@return string explanation
local function createExplanation()
	local explanation = "--------------------- MachineInterfaceEntryExplanation ---------------------\r\n"
		.. "--each MachineInterfaceEntry has a InventoryEntry to define what machine it should connect too\r\n"
		.. "--all 3 methods for defining inventories are supported\r\n"
		..
		"--It has an entries table that lists all the inventories it is supposed to connect with and the type of connection it has\r\n"
		.. "--There are 3 types of connections IM, IR, O InputMapping, InputRecipe, Output respectively\r\n"
		..
		"--InputMapping simply maps an inventory to that specific slot in the machine, put items in inventory and they end up in the machine\r\n"
		.. "-- format: IM <inventory> <slotToMapInto> <countToKeepInSlot>\r\n"
		.. "--map minecraft:chest_x to slot 1 and keep 4 in stock would be: IM minecraft:chest_x 1 4\r\n"
		..
		"--InputRecipe expects a full recipe worth of items to be in here and then moves them into correct machine slots as defined in recipe\r\n"
		.. "--format: IR <inventory>\r\n"
		.. "--ex: IR minecraft:chest_x\r\n"
		.. "--Output map a slot to inventory\r\n"
		.. "--format: O <inventory> <slotToExtractFrom>\r\n"
		.. "--ex: O minecraft:chest 3\r\n"
		.. "--This is a generic explanation check if the program supports everything\r\n"
		.. "--------------------- end of MachineInterfaceEntryExplanation ---------------------\r\n"
	return explanation
end

return {
	parse = parse,
	createEntry = createEntry,
	createIOEntry = createIOEntry,
	createExplanation = createExplanation,
}
