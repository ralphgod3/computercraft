local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/Generic/ItemInventoryBase.lua")

local PC = require("Modules.ParamCheck")
local IThreads = require("Interfaces.Modules.Ithreads")
local IitemInventory = require("Interfaces.Modules.IitemInventory")
local ItemInventoryBase = require("Modules.IitemInventory.Generic.ItemInventoryBase")

---create a new itemInventory
---@param threadingManager Ithreads implementation of IThreads interface
---@param name string name of the inventory to wrap around
---@return IitemInventory instance instance of itemInventory that implements IitemInventory
local function new(threadingManager, name)
    PC.expectInterface(1, IThreads, threadingManager)
    PC.expect(2,name, "string")
    local wrappedInventory = peripheral.wrap(name)
    if not wrappedInventory then
        error("can not find inventory on network for wrapping, " .. name, 4)
    end

    local ItemInventoryBase = ItemInventoryBase.new(threadingManager,name)

    local function pushItemsFromSlots(toIitemInventory, fromSlots, force)
        PC.expectInterface(1, IitemInventory, toIitemInventory)
        PC.expect(2, fromSlots, "table")
        PC.expect(3, force, "boolean", "nil")

        --use implementation of other inventory to allow for special logic to handle it
        if toIitemInventory.isBulkStorage() then
            return toIitemInventory.pullItemsFromSlots(ItemInventoryBase, fromSlots, force)
        end

        return ItemInventoryBase.pushItemsFromSlots(toIitemInventory, fromSlots, force)
    end

    local function pullItemsFromSlots(fromIitemInventory, fromSlots, force)
        PC.expectInterface(1, IitemInventory, fromIitemInventory)
        PC.expect(2, fromSlots, "table")
        PC.expect(3, force, "boolean", "nil")
        force = force or false
        --use implementation of other inventory to allow for special logic to handle it
        if fromIitemInventory.isBulkStorage() then
            return fromIitemInventory.pushItemsFromSlots(ItemInventoryBase, fromSlots, force)
        end
        return ItemInventoryBase.pullItemsFromSlots(fromIitemInventory, fromSlots, force)
    end

    local function pullInventory(fromIitemInventory, forceEmpty)
        PC.expectInterface(1, IitemInventory, fromIitemInventory)
        PC.expect(2,forceEmpty, "boolean", "nil")
        --use implementation of other inventory to allow for special logic to handle it
        if fromIitemInventory.isBulkStorage() then
            return fromIitemInventory.pushInventory(ItemInventoryBase, forceEmpty)
        end

        return ItemInventoryBase.pullInventory(fromIitemInventory, forceEmpty)
    end

    local function pushInventory(toIitemInventory, forceEmpty)
        PC.expectInterface(1, IitemInventory, toIitemInventory)
        PC.expect(2,forceEmpty, "boolean", "nil")
        if toIitemInventory.isBulkStorage() then
            return toIitemInventory.pullInventory(ItemInventoryBase, forceEmpty)
        end

        return ItemInventoryBase.pushInventory(toIitemInventory, forceEmpty)
    end

    return {
        list = ItemInventoryBase.list,
        getItemDetail = ItemInventoryBase.getItemDetail,
        getDetailedItemList = ItemInventoryBase.getDetailedItemList,
        size = ItemInventoryBase.size,
        getName = ItemInventoryBase.getName,
        pushItems = ItemInventoryBase.pushItems,
        pullItems = ItemInventoryBase.pullItems,
        pushItemsFromSlots = pushItemsFromSlots,
        pullItemsFromSlots = pullItemsFromSlots,
        pullInventory = pullInventory,
        pushInventory = pushInventory,
        isBulkStorage = ItemInventoryBase.isBulkStorage,
    }
end
return {new = new}