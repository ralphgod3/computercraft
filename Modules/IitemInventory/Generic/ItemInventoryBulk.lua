local Git = require("Modules.Git")

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/Generic/ItemInventoryBase.lua")

local PC = require("Modules.ParamCheck")
local IThreads = require("Interfaces.Modules.Ithreads")
local ItemInventoryBase = require("Modules.IitemInventory.Generic.ItemInventoryBase")

---create a new itemInventory
---@param threadingManager Ithreads implementation of IThreads interface
---@param name string name of the inventory to wrap around
---@return IitemInventory instance instance of itemInventory that implements IitemInventory
local function new(threadingManager, name)

    PC.expectInterface(1, IThreads, threadingManager)
    PC.expect(2,name, "string")
    local wrappedInventory = peripheral.wrap(name)
    if not wrappedInventory then
        error("can not find inventory on network for wrapping, " .. name, 4)
    end
    local ItemInventoryBase = ItemInventoryBase.new(threadingManager, name)

    local function isBulkStorage()
        return true
    end

    return {
        list = ItemInventoryBase.list,
        getItemDetail = ItemInventoryBase.getItemDetail,
        getDetailedItemList = ItemInventoryBase.getDetailedItemList,
        size = ItemInventoryBase.size,
        getName = ItemInventoryBase.getName,
        pushItems = ItemInventoryBase.pushItems,
        pullItems = ItemInventoryBase.pullItems,
        pushItemsFromSlots = ItemInventoryBase.pushItemsFromSlots,
        pullItemsFromSlots = ItemInventoryBase.pullItemsFromSlots,
        pullInventory = ItemInventoryBase.pullInventory,
        pushInventory = ItemInventoryBase.pushInventory,
        isBulkStorage = isBulkStorage,
    }
end
return {new = new}