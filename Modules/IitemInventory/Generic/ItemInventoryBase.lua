local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")

local PC = require("Modules.ParamCheck")
local IThreads = require("Interfaces.Modules.Ithreads")
local Utils = require("Modules.Utils")
local IitemInventory = require("Interfaces.Modules.IitemInventory")

---create a new itemInventory
---@param threadingManager Ithreads implementation of IThreads interface
---@param name string name of the inventory to wrap around
---@return IitemInventory instance instance of itemInventory that implements IitemInventory
local function new(threadingManager, name)

    PC.expectInterface(1, IThreads, threadingManager)
    PC.expect(2,name, "string")
    local wrappedInventory = peripheral.wrap(name)
    if not wrappedInventory then
        error("can not find inventory on network for wrapping, " .. name, 4)
    end

    --inventory size
    local invSize = wrappedInventory.size()

    local function getTotalItemCount(slotInfo)
        PC.expect(1, slotInfo, "table")
        local toMove = 0
        for _,v in pairs(slotInfo) do
            toMove = toMove + v
        end
        return toMove
    end

    local function list()
        return wrappedInventory.list()
    end

    local function getItemDetail(slot)
        PC.expect(1, slot, "number")
        return wrappedInventory.getItemDetail(slot)
    end

    local function getDetailedItemList()
        local sem = Utils.createCountingSemaphore()
        local items = list()
        for slot,_ in pairs(items) do
            Utils.takeCountingSemaphore(sem)
            threadingManager.create(function ()
                items[slot] = getItemDetail(slot)
                Utils.freeCountingSemaphore(sem)
            end)
        end

        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(sem)
        end
        return items
    end

    local function size()
        return invSize
    end

    local function getName()
        return name
    end

    local function pushItems(toIitemInventory, fromSlot, limit, toSlot)
        PC.expectInterface(1,IitemInventory, toIitemInventory)
        PC.expect(2, fromSlot, "number")
        PC.expect(3, limit, "number", "nil")
        PC.expect(4,toSlot, "number", "nil")
        return wrappedInventory.pushItems(toIitemInventory.getName(), fromSlot, limit, toSlot)
    end

    local function pullItems(fromIitemInventory, fromSlot, limit, toSlot)
        PC.expectInterface(1,IitemInventory, fromIitemInventory)
        PC.expect(2, fromSlot, "number")
        PC.expect(3, limit, "number", "nil")
        PC.expect(4,toSlot, "number", "nil")
        return wrappedInventory.pullItems(fromIitemInventory.getName(), fromSlot, limit, toSlot)
    end

    local function pushItemsFromSlots(toIitemInventory, fromSlots, force)
        PC.expectInterface(1, IitemInventory, toIitemInventory)
        PC.expect(2, fromSlots, "table")
        PC.expect(3, force, "boolean", "nil")
        force = force or false

        local sem = Utils.createCountingSemaphore()
        local itemsToMove = getTotalItemCount(fromSlots)
        local itemsMoved = {}
        for slot, toMove in pairs(fromSlots) do
            Utils.takeCountingSemaphore(sem)
            threadingManager.create(function ()
                itemsMoved[slot] = 0
                while toMove > 0 do
                    local moved = pushItems(toIitemInventory, slot, toMove)
                    itemsMoved[slot] = itemsMoved[slot] + moved
                    toMove = toMove - moved
                    if not force then
                        break
                    end
                end
                Utils.freeCountingSemaphore(sem)
            end)
        end

        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(sem)
        end
        return itemsToMove == getTotalItemCount(itemsMoved)
    end

    local function pullItemsFromSlots(fromIitemInventory, fromSlots, force)
        PC.expectInterface(1, IitemInventory, fromIitemInventory)
        PC.expect(2, fromSlots, "table")
        PC.expect(3, force, "boolean", "nil")
        force = force or false

        local sem = Utils.createCountingSemaphore()
        local itemsToMove = getTotalItemCount(fromSlots)
        local itemsMoved = {}
        for slot, toMove in pairs(fromSlots) do
            Utils.takeCountingSemaphore(sem)
            threadingManager.create(function ()
                itemsMoved[slot] = 0
                while toMove > 0 do
                    local moved = pullItems(fromIitemInventory, slot, toMove)
                    itemsMoved[slot] = itemsMoved[slot] + moved
                    toMove = toMove - moved
                    if not force then
                        break
                    end
                end
                Utils.freeCountingSemaphore(sem)
            end)
        end

        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(sem)
        end
        return itemsToMove == getTotalItemCount(itemsMoved)
    end

    local function pullInventory(fromIitemInventory, forceEmpty)
        PC.expectInterface(1, IitemInventory, fromIitemInventory)
        PC.expect(2,forceEmpty, "boolean", "nil")

        local slots = {}
        for slot,itemInfo in pairs(fromIitemInventory.list()) do
           slots[slot] = itemInfo.count
        end
        return pullItemsFromSlots(fromIitemInventory, slots, forceEmpty)
    end

    local function pushInventory(toIitemInventory, forceEmpty)
        PC.expectInterface(1, IitemInventory, toIitemInventory)
        PC.expect(2,forceEmpty, "boolean", "nil")

        local slots = {}
        for slot, itemInfo in pairs(list()) do
           slots[slot] = itemInfo.count
        end
        return pushItemsFromSlots(toIitemInventory, slots, forceEmpty)
    end

    local function isBulkStorage()
        return false
    end

    return {
        list = list,
        getItemDetail = getItemDetail,
        getDetailedItemList = getDetailedItemList,
        size = size,
        getName = getName,
        pushItems = pushItems,
        pullItems = pullItems,
        pushItemsFromSlots = pushItemsFromSlots,
        pullItemsFromSlots = pullItemsFromSlots,
        pullInventory = pullInventory,
        pushInventory = pushInventory,
        isBulkStorage = isBulkStorage,
    }
end
return {new = new}