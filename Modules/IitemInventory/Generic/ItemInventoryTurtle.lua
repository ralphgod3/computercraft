local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")

local PC = require("Modules.ParamCheck")
local IThreads = require("Interfaces.Modules.Ithreads")
local Dbg = require("Modules.Logger")

local TAG = "ItemInventoryTurtle"

---create a new itemInventory
---@param threadingManager Ithreads implementation of IThreads interface
---@param name string name of the inventory to wrap around
---@return IitemInventory instance instance of itemInventory that implements IitemInventory
local function new(threadingManager, name)


    PC.expectInterface(1, IThreads, threadingManager)
    PC.expect(2,name, "string")
    local wrappedInventory = peripheral.wrap(name)
    --check if local name is of turtle
    local modems = {peripheral.find("modem")}
    for i = 1, #modems do
        if modems[i].getNameLocal() == name then
            wrappedInventory = {}
            break
        end
    end
    if not wrappedInventory then
        error("can not find inventory on network for wrapping, " .. name, 4)
    end
    --inventory size
    local invSize = 16

    local function list()
        Dbg.logW(TAG, "function not available: list")
        local retTable = {}
        for i = 1,16 do
            retTable[i] = {count = 64}
        end
        return retTable
    end

    local function getItemDetail(slot)
        Dbg.logW(TAG, "function not available: getItemDetail")
        return {}
    end

    local function getDetailedItemList()
        Dbg.logW(TAG, "function not available: getDetailedItemList")
        return {}
    end

    local function size()
        return invSize
    end

    local function getName()
        return name
    end

    local function pushItems(toIitemInventory, fromSlot, limit, toSlot)
        Dbg.logW(TAG, "function not available: pushItems")
        return 0
    end

    local function pullItems(fromIitemInventory, fromSlot, limit, toSlot)
        Dbg.logW(TAG, "function not available: pullItems")
        return 0
    end

    local function pushItemsFromSlots(toIitemInventory, fromSlots, force)
        Dbg.logW(TAG, "function not available: pushItemsFromSlots")
        return false
    end

    local function pullItemsFromSlots(fromIitemInventory, fromSlots, force)
        Dbg.logW(TAG, "function not available: pullItemsFromSlots")
        return 0
    end

    local function pullInventory(fromIitemInventory, forceEmpty)
        Dbg.logW(TAG, "function not available: pullInventory")
        return 0
    end

    local function pushInventory(toIitemInventory, forceEmpty)
        Dbg.logW(TAG, "function not available: pushInventory")
        return 0
    end

    local function isBulkStorage()
        return false
    end

    return {
        list = list,
        getItemDetail = getItemDetail,
        getDetailedItemList = getDetailedItemList,
        size = size,
        getName = getName,
        pushItems = pushItems,
        pullItems = pullItems,
        pushItemsFromSlots = pushItemsFromSlots,
        pullItemsFromSlots = pullItemsFromSlots,
        pullInventory = pullInventory,
        pushInventory = pushInventory,
        isBulkStorage = isBulkStorage,
    }
end
return {new = new}