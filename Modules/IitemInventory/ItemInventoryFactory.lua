local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/Generic/ItemInventoryBulk.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/Generic/ItemInventoryNormal.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/Generic/ItemInventoryTurtle.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")

local PC = require("Modules.ParamCheck")
local IThreads = require("Interfaces.Modules.Ithreads")
local ItemInventoryNormal = require("Modules.IitemInventory.Generic.ItemInventoryNormal")
local ItemInventoryBulk = require("Modules.IitemInventory.Generic.ItemInventoryBulk")
local ItemInventoryTurtle =require("Modules.IitemInventory.Generic.ItemInventoryTurtle")


-- factory for implementations of IitemInventories

--inventories that should be created as bulk storage
local bulkNames =
{
    "standardDrawer", --storage drawers drawer
    --industrialforegoing black hole storage
    "industrialforegoing:pity_black_hole_unit",
    "industrialforegoing:common_black_hole_unit",
    "industrialforegoing:simple_black_hole_unit",
    "industrialforegoing:advanced_black_hole_unit",
    "industrialforegoing:supreme_black_hole_unit",
}

--inventories that can not be properly handled at the moment
local blockNames =
{
    "storagedrawers:controller", --weird behaviour that is not fitted easily in any implementation
}



---creates a ItemInventory that implements IitemInventory interface
---@param threadingManager Ithreads implementation of IThreads interface
---@param name string inventory name
---@vararg any extraInitializationArguments extra arguments needed for specific implementations (inventoryManager atm)
---@return IitemInventory IitemInventory IitemInventory interface implementation
local function new(threadingManager, name, ...)
    PC.expectInterface(1, IThreads, threadingManager)
    PC.expect(2,name, "string")
    --create special variants of IitemInventory implementation
    if string.find(name, "inventoryManager", nil, true) ~= nil then
        error("no support for inventoryManager")
        return nil
    end
    if string.find(name, "turtle_", nil, true) ~= nil then
        return ItemInventoryTurtle.new(threadingManager, name)
    end
    for _, bName in ipairs(bulkNames) do
        if string.find(name, bName, nil, true) ~= nil then
            return ItemInventoryBulk.new(threadingManager, name)
        end
    end

    for _, bName in ipairs(blockNames) do
        if string.find(name, bName, nil, true) ~= nil then
            error("inventory" .. name .. " has no IitemInventory implementation and does not work with standard implementation", 2)
        end
    end
    --nothing matches return a normal ItemInventory implementation
    return ItemInventoryNormal.new(threadingManager, name)
end
return {new = new}