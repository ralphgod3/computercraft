local Git = require("Modules.Git")

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")


local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")
local Utils = require("Modules.Utils")
local Ithreads = require("Interfaces.Modules.Ithreads")
local IitemInventory = require("Interfaces.Modules.IitemInventory")

local TAG = "IIU"

------------------------------ PRIVATE FUNCTIONS ------------------------------

---checks if items are equal, lazymatch enables matching for crafting recipes where nbt might not matter, if enabled ensure recipe item is itemA
---@param itemA ItemEntry item1 to compare
---@param itemB ItemEntry item2 to compare
---@param lazyMatch? boolean doLazyMatching
---@return boolean itemsAreEqual
local function internalItemsAreEqual(itemA, itemB, lazyMatch)
    --names dont match discard option
    if itemA.name ~= itemB.name then
        return false
    end
    --match on name only
    if itemA.nbt == nil and itemB.nbt == nil and itemA.damage == nil and itemB.damage == nil then
        return true
    --damage nil and 0 are equal as long as not both are the same yey for crafting recipes
    elseif (itemA.damage == 0 and itemB.damage == nil) or (itemA.damage == nil and itemB.damage == 0) then
        return true
    --match on damage as long as damage is not nil
    elseif (itemA.damage ~= nil and itemA.damage == itemB.damage) then
        return true
    --match on nbt if nbt is not nil
    elseif itemA.nbt ~= nil and itemA.nbt == itemB.nbt then
        return true
    --ignore itemB nbt if lazyMatch is set and itemA nbt is nil (for crafting recipes)
    elseif lazyMatch and itemA.nbt == nil then
        return true
    end
    -- no way to match items, return false
    return false
end


---finds slot with item
---@param curItemList table<number, ItemEntry> list of current items as returned by .list()
---@param itemToSearchFor ItemEntry item to search for
---@param lazyMatch boolean? enable lazy match
---@return number? slot slot or nil
local function internalFindSlotWithItem(curItemList, itemToSearchFor, lazyMatch)
    for slot, itemInfo in pairs(curItemList) do
        if internalItemsAreEqual(itemToSearchFor, itemInfo, lazyMatch) then
            return slot
        end
    end
    return nil
end


---create move plan entry
---@param fromSlot number from what slot should the item be pulled
---@param toSlot number to what slot should item be pushed
---@param count number amount of items to pull / push
---@param itemsToMoveIndex number | nil used for returning from executeMovePlan, defaults to 1
---@return MovePlanEntry
local function internalCreateMovePlanEntry(fromSlot, toSlot, count, itemsToMoveIndex)
    itemsToMoveIndex = itemsToMoveIndex or 1
    ---@class MovePlanEntry
    local entry = {
        fromSlot = fromSlot,
        toSlot = toSlot,
        count = count,
        itemsToMoveIndex = itemsToMoveIndex
    }
    return entry
end

---creates a move plan for moving items to other inventory
---@param itemList table<number, ItemEntry> list of items as returned from IitemInventory.list()
---@param itemsToMove ItemEntry[] table of items to move where each entry contains {string name, <number count>, <string nbt>, <number damage>, <number slot>}, slot indicates to what slot item should be moved
---@param onlyMoveAll boolean | nil only return plan if all items can be moved
---@param lazyMatch boolean matches item if items in itemsToMove dont have nbt (used for crafting recipes)
---@return MovePlanEntry[] | nil movePlan if onlyMoveAll returns nill when not all items can be moved, on success returns table where each entry is 1 move with pushItems table {number fromSlot, number toSlot, number count, <number itemsToMoveIndex>}
local function internalCreateMovePlan(itemList, itemsToMove, onlyMoveAll, lazyMatch)
    local movePlan = {}
    local itemListCopy = Utils.copyTable(itemList)
    local itemsToMoveCopy = Utils.copyTable(itemsToMove)
    --build a plan for moving every item
    for i, _ in pairs(itemsToMoveCopy) do
        while itemsToMoveCopy[i].count > 0 do
            local slotWithItem = internalFindSlotWithItem(itemListCopy,itemsToMoveCopy[i], lazyMatch)
            --can't find item in list anymore
            if slotWithItem == nil then
                if onlyMoveAll then
                    return nil
                end
                break
            end
            local toMove = itemListCopy[slotWithItem].count
            if itemsToMove[i].count ~= nil then
                toMove = math.min(itemListCopy[slotWithItem].count, itemsToMoveCopy[i].count)
            end
            table.insert(movePlan, internalCreateMovePlanEntry(slotWithItem, itemsToMoveCopy[i].slot, toMove, i))
            itemsToMoveCopy[i].count = itemsToMoveCopy[i].count - toMove
            itemListCopy[slotWithItem].count = itemListCopy[slotWithItem].count - toMove
            if itemListCopy[slotWithItem].count == 0 then
                itemListCopy[slotWithItem] = nil
            end
            --all items that need to move are planned for a move
            if itemsToMoveCopy[i].count == 0 and itemsToMove[i].count ~= nil then
                break
            end
        end
    end
    return movePlan
end

---transform list from .list() into list with count of items
---@param list table<number, ItemEntry> list from .list()
---@return ItemEntry[] itemList list with total item counts without slot
local function internalTransformItemList(list)
    local outList = {}
    for _,v in pairs(list) do
        local exists = false
        for _, itemData in pairs(outList) do
            if v.name == itemData.name and v.nbt == itemData.nbt and v.damage == itemData.damage then
                exists = true
                itemData.count = itemData.count + v.count
                break
            end
        end
        if not exists then
            table.insert(outList, {name = v.name, nbt = v.nbt, damage = v.damage, count = v.count})
        end
    end
    return outList
end

---executes a move plan
---@param threadingManager Ithreads IThreads implementation
---@param fromIitemInventory IitemInventory inventory to move items from
---@param toIitemInventory IitemInventory inventory to move items into
---@param movePlan MovePlanEntry[] moveplan
---@return number[] itemsMoved list of items moved indexed by itemsToMove entry
local function internalExecuteMovePlan(threadingManager, fromIitemInventory, toIitemInventory, movePlan)
    local countingSem = Utils.createCountingSemaphore()
    local moved = {}
    for i,_ in pairs(movePlan) do
        moved[movePlan[i].itemsToMoveIndex] = moved[movePlan[i].itemsToMoveIndex] or 0
        Utils.takeCountingSemaphore(countingSem)
        threadingManager.create(function ()
            local tempMoved = fromIitemInventory.pushItems(toIitemInventory, movePlan[i].fromSlot, movePlan[i].count, movePlan[i].toSlot)
            moved[movePlan[i].itemsToMoveIndex] = moved[movePlan[i].itemsToMoveIndex] + tempMoved
            Utils.freeCountingSemaphore(countingSem)
        end)
    end
    if not Utils.runThreadingManagerIfNeeded(threadingManager) then
        Utils.awaitCountingSemaphore(countingSem)
    end
    return moved
end

---find missing items from a list
---@param wantedItems ItemEntry[] items we have in inventory (use transformItemList if needed)
---@param itemsInInventory ItemEntry[] items we want in inventory
---@param isCraftingRecipe boolean is wantedItems part of crafting recipe (enables lazyMatching)
---@return ItemEntry[] itemList list of missing items
local function internalGetMissingItems(wantedItems, itemsInInventory, isCraftingRecipe)
    local outList = Utils.copyTable(wantedItems)
    for key, itemA in pairs(outList) do
        for _, itemB in pairs(itemsInInventory) do
            if internalItemsAreEqual(itemA, itemB, isCraftingRecipe) then
                itemA.count = itemA.count - itemB.count
                if itemA.count <= 0 then
                    outList[key] = nil
                end
                break
            end
        end
    end
    return Utils.rebuildArray(outList)
end

------------------------------ PUBLIC FUNCTIONS ------------------------------

---checks if items are equal to eachother based on name, damage and nbt
---@param itemA ItemEntry item table {string name, <number damage>, <string nbt>}
---@param itemB ItemEntry item table {string name, <number damage>, <string nbt>}
---@param lazyNbtMatching boolean when set to true ignores nbt on ItemB if itemA has no nbt (for crafting recipes)
---@return boolean itemsEqual true yes, false no
local function itemsAreEqual(itemA, itemB, lazyNbtMatching)
    PC.expect(1, itemA, "table")
    PC.expect(2, itemB, "table")
    PC.expect(3, lazyNbtMatching, "boolean")
    PC.field(itemA, "name", "string")
    PC.field(itemA, "damage", "number", "nil")
    PC.field(itemA, "nbt", "string", "nil")
    PC.field(itemB, "name", "string")
    PC.field(itemB, "damage", "number", "nil")
    PC.field(itemB, "nbt", "string", "nil")
    return internalItemsAreEqual(itemA, itemB, lazyNbtMatching)
end

---multiply given itemList.count with a given factor
---@param itemList ItemEntry[] itemList to multiply needs to atleast contain count field
---@param multiplicationFactor number factor to multiply with
---@return ItemEntry[] itemList multiplied itemlist
local function multiplyItemList(itemList, multiplicationFactor)
    local itemListCopy = Utils.copyTable(itemList)
    for k,_ in pairs(itemListCopy) do
        PC.field(itemList[k], "count", "number")
        itemListCopy[k].count = itemListCopy[k].count * multiplicationFactor
    end
    return itemListCopy
end

---transform a list returned from IitemInventory.list() into a list containing total itemCounts
---@param list table<number, ItemEntry> list from IitemInventory.list()
---@return ItemEntry[] list list containing total itemCounts in inventory
local function transformItemList(list)
    PC.expect(1,list, "table")
    for _,v in pairs(list) do
        PC.field(v, "name", "string")
        PC.field(v, "count", "number")
        PC.field(v, "nbt", "string", "nil")
        PC.field(v, "damage", "number", "nil")
    end
    return internalTransformItemList(list)
end

---calculates missing items in inventory based on list of wantedItems
---@param wantedItems ItemEntry[] table containing all the items we want each entry = {string name, number count, <string nbt>, <number damage>}
---@param itemsInInventory ItemEntry[] table containing all the items we have each entry = {string name, number count, <string nbt>, <number damage>} (use transformItemList if needed)
---@param lazyNbtMatching boolean when set to true ignores nbt on itemsInInventory if wantedItems dont have nbt (for crafting recipes)
---@return ItemEntry[] missingItemList list of missing items
local function getMissingItems(wantedItems, itemsInInventory, lazyNbtMatching)
    PC.expect(1, wantedItems, "table")
    for k,_ in pairs(wantedItems) do
        PC.field(wantedItems[k], "name", "string")
        PC.field(wantedItems[k], "nbt", "string", "nil")
        PC.field(wantedItems[k], "damage", "number", "nil")
    end
    PC.expect(2, itemsInInventory, "table")
    for k,_ in pairs(itemsInInventory) do
        PC.field(itemsInInventory[k], "name", "string")
        PC.field(itemsInInventory[k], "nbt", "string", "nil")
        PC.field(itemsInInventory[k], "damage", "number", "nil")
    end
    PC.expect(3, lazyNbtMatching, "boolean")

    return internalGetMissingItems(wantedItems, itemsInInventory, lazyNbtMatching)
end

---find slot with itemToSearchFor
---@param itemList table<number, ItemEntry> table as returned from IitemInventory.list()
---@param itemToSearchFor ItemEntry table with {string name, <string nbt>, <number damage>}
---@return number | nil slot number if found, nil if item not in inventory
local function findSlotWithItem(itemList, itemToSearchFor)
    PC.expect(1, itemList, "table")
    for k,_ in pairs(itemList) do
        PC.field(itemList[k], "name", "string")
        PC.field(itemList[k], "nbt", "string", "nil")
        PC.field(itemList[k], "damage", "number", "nil")
    end
    PC.expect(2, itemToSearchFor, "table")
    PC.field(itemToSearchFor, "name", "string")
    PC.field(itemToSearchFor, "nbt", "string", "nil")
    PC.field(itemToSearchFor, "damage", "number", "nil")

    return internalFindSlotWithItem(itemList, itemToSearchFor)
end

---create movePlanEntry for giving to executeMovePlan
---@param fromSlot number from what slot should item be moved
---@param toSlot number to what slot should item be moved
---@param count number count of items to move
---@return MovePlanEntry entry
local function createMovePlanEntry(fromSlot, toSlot, count)
    PC.expect(1, fromSlot, "number")
    PC.expect(2, toSlot, "number")
    PC.expect(3, count, "number")
    return internalCreateMovePlanEntry(fromSlot, toSlot, count)
end

---creates simpleMovePlan, unlike createMovePlan this modifies the list since it is expected user will perform multiple moves
---@param list table<number, ItemEntry> info as returned from .list(), this list is modified to subtract any planned moves
---@param itemInfo ItemEntry at minimal needs to contain { number count} but can contain full item info to match on ie name, damage, nbt
---@param toSlot number to what slot should item be moved
---@return MovePlanEntry[] moveplanEntries
local function createSimpleMovePlan(list, itemInfo, toSlot)
    PC.expect(1, list, "table")
    PC.expect(2, itemInfo, "table")
    PC.expect(3, toSlot, "number", "nil")
    if itemInfo.count == 0 then
        return {}
    end

    local movePlan = {}
    local toMove = itemInfo.count
    for slot, info in pairs(list) do
        if itemInfo.name == nil or internalItemsAreEqual(itemInfo, info) then
            local moved = math.min(info.count, toMove)
			if moved == nil then
				moved = 0
			end
            table.insert(movePlan, internalCreateMovePlanEntry(slot, toSlot, moved))
            info.count = info.count - moved
            if info.count == 0 then
                list[slot] = nil
            end
            toMove = toMove - moved
            if toMove == 0 then
                break
            end
        end
    end
    return movePlan
end


---creates a move plan for moving items to other inventory
---@param itemList table<number, ItemEntry> list of items as returned from IitemInventory.list()
---@param itemsToMove ItemEntry[] table of items to move where each entry contains {string name, <number count>, <string nbt>, <number damage>, <number slot>}, slot indicates to what slot item should be moved
---@param onlyMoveAll boolean | nil only return plan if all items can be moved
---@param lazyNbtMatching boolean matches items if items in itemsToMove dont have nbt but items in itemList do have nbt (used for crafting recipes)
---@return MovePlanEntry[] | nil movePlan if onlyMoveAll returns nill when not all items can be moved, on success returns table where each entry is 1 move with pushItems table {number fromSlot, number toSlot, number count, <number itemsToMoveIndex>}
local function createMovePlan(itemList, itemsToMove, onlyMoveAll, lazyNbtMatching)
    PC.expect(1, itemList, "table")
    PC.expect(2, itemsToMove, "table")
    PC.expect(3, itemList, "boolean", "nil")
    for k,_ in pairs(itemList) do
        PC.field(itemList[k], "name", "string")
        PC.field(itemList[k], "nbt", "string", "nil")
        PC.field(itemList[k], "damage", "number", "nil")
    end
    for k,_ in pairs(itemsToMove) do
        PC.field(itemsToMove[k], "name", "string")
        PC.field(itemsToMove[k], "count", "number", "nil")
        PC.field(itemsToMove[k], "nbt", "string", "nil")
        PC.field(itemsToMove[k], "damage", "number", "nil")
        PC.field(itemsToMove[k], "slot", "number", "nil")
    end
    return internalCreateMovePlan(itemList, itemsToMove, onlyMoveAll, lazyNbtMatching)
end

---execute a previously created movePlan
---@param threadingManager Ithreads implementation of Ithreads interface
---@param fromIitemInventory IitemInventory implementation of IitemInventory interface
---@param toIitemInventory IitemInventory implementation of IitemInventory interface
---@param movePlan MovePlanEntry[] movePlan as returned from createMovePlan()
---@return number[] moved items moved, index is the same as index that where used when creating movePlan
local function executeMovePlan(threadingManager, fromIitemInventory, toIitemInventory, movePlan)
    PC.expectInterface(1, Ithreads, threadingManager)
    PC.expectInterface(2, IitemInventory, fromIitemInventory)
    PC.expectInterface(3, IitemInventory, toIitemInventory)
    PC.expect(4, movePlan, "table")
    for i = 1, #movePlan do
        PC.field(movePlan[i], "fromSlot", "number")
        PC.field(movePlan[i], "toSlot", "number", "nil")
        PC.field(movePlan[i], "itemsToMoveIndex", "number")
        PC.field(movePlan[i], "fromSlot", "number")
        PC.field(movePlan[i], "count", "number", "nil")
    end

    return internalExecuteMovePlan(threadingManager, fromIitemInventory, toIitemInventory, movePlan)
end

---push items defined in itemsToMove in parallel, when setting specific slot as destination please ensure they accept the items this function does not check that
---@param threadingManager Ithreads implementation of Ithreads interface
---@param fromIitemInventory IitemInventory implementation of IitemInventory interface
---@param toIitemInventory IitemInventory implementation of IitemInventory interface
---@param itemsToMove ItemEntry[] table where each entry contains {string name, number count, <string nbt>, <number damage>, <number slot>}, slot is the slot the item should be moved to in destination
---@param itemList table<number, ItemEntry>? table as returned from IitemInventory.list(), if nill will call function itself causing 1 tick delay.
---@param lazyNbtMatching boolean? if set to true ignores nbt on items in itemList if item in itemsToMove has none, defaults to true
---@param onlyMoveAll boolean? if set to true will not move any item unless all items can be moved
---@return number[] moved returns moved item counts in same order as itemsToMove table is in
local function pushItemsByName(threadingManager, fromIitemInventory, toIitemInventory, itemsToMove, itemList, lazyNbtMatching, onlyMoveAll)
    PC.expectInterface(1, Ithreads, threadingManager)
    PC.expectInterface(2, IitemInventory, fromIitemInventory)
    PC.expectInterface(3, IitemInventory, toIitemInventory)
    PC.expect(4, itemsToMove, "table")
    PC.expect(5, itemList, "table", "nil")
    PC.expect(6, lazyNbtMatching, "boolean", "nil")
    PC.expect(7, onlyMoveAll, "boolean", "nil")
    lazyNbtMatching = lazyNbtMatching or true
    --paranoid checking of table entries
    for k,_ in pairs(itemsToMove) do
        PC.field(itemsToMove[k], "name", "string")
        PC.field(itemsToMove[k], "count", "number")
        PC.field(itemsToMove[k], "nbt", "string", "nil")
        PC.field(itemsToMove[k], "damage", "number", "nil")
        PC.field(itemsToMove[k], "slot", "number", "nil")
    end
    itemList = itemList or fromIitemInventory.list()
    local movePlan = internalCreateMovePlan(itemList, itemsToMove, onlyMoveAll, lazyNbtMatching)
    if movePlan == nil then
        return {}
    end
    local moved = internalExecuteMovePlan(threadingManager, fromIitemInventory, toIitemInventory, movePlan)
    return moved
end




return {
    itemsAreEqual = itemsAreEqual,
    multiplyItemList = multiplyItemList,
    transformItemList = transformItemList,
    getMissingItems = getMissingItems,
    createMovePlan = createMovePlan,
    executeMovePlan = executeMovePlan,
    findSlotWithItem = findSlotWithItem,
    pushItemsByName = pushItemsByName,
    createMovePlanEntry = createMovePlanEntry,
    createSimpleMovePlan = createSimpleMovePlan,
}