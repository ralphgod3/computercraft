local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "PocketAEGui/PocketAEGui.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "PocketAEGui/ListBoxItem.lua")

local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Utils = require("Modules.Utils")


local const = {
    mode = {keep = 1, drain = 2, fill = 3},
    itemOrTag = {item = 1, tag = 2},


}
local _rule = {}    --initialized in NewRule()
local currExecutingRules = {}   --keeps timer handles for cancelling them later
local inventory = nil   --IitemInventory implementation on which to apply rules, must also be able to retrieve items from and push items to "storage", usually the AE system

local function checkKeepRule(rule)
    local itemCount = inventory.getItemDetail(rule.slot).count
    if itemCount > rule.count then

        inventory.pushItems("storage", rule.slot, itemCount - rule.count) --empty excess in specified slot

        --empty specified item from every other slot, except the specified slot
        if rule.itemOrTag == const.itemOrTag.item then
            local currInv = inventory.list()
            for i = 1, #currInv do
                if  i ~= rule.slot and currInv[i].name == rule.itemOrTagData then
                    inventory.pushItems("storage", i)
                end
            end
        elseif rule.itemOrTag == const.itemOrTag.tag then
            local currInv = inventory.getDetailedItemList()
            for i = 1, #currInv do
                if i ~= rule.slot and currInv[i].tags ~= nil then
                    for _, tag in pairs(currInv[i].tags) do
                        if tag == rule.itemOrTagData then
                            inventory.pushItems("storage", i)
                        end
                    end
                end
            end
        end

    elseif itemCount < rule.count then
        inventory.pullItems("storage", nil, itemCount - rule.count)
    end
end

local function checkDrainRule(rule)
    if rule.itemOrTag == const.itemOrTag.item then
        local currInv = inventory.list()
        for i = 1, #currInv do
            if currInv[i].name == rule.itemOrTagData then
                inventory.pushItems("storage", i)
            end
        end
    elseif rule.itemOrTag == const.itemOrTag.tag then
        local currInv = inventory.getDetailedItemList()
        for i = 1, #currInv do
            if currInv[i].tags ~= nil then
                for _, tag in pairs(currInv[i].tags) do
                    if tag == rule.itemOrTagData then
                        inventory.pushItems("storage", i)
                    end
                end
            end
        end
    end
end

local function checkFillRule(rule)
    local itemCount = inventory.getItemDetail(rule.slot).count
    if itemCount < rule.count then
        inventory.pullItems("storage", nil, rule.count - itemCount)
    end
end

local function onRuleTimerOut(rule)
    if rule.mode == const.mode.keep then
        checkKeepRule(rule)
    elseif rule.mode == const.mode.drain then
        checkDrainRule(rule)
    elseif rule.mode == const.mode.fill then
        checkFillRule(rule)
    end
end

local function stopAllRules()
    for _, v in pairs(currExecutingRules) do
        Timer.Cancel(v)
    end
    currExecutingRules = {}
end

local function execRule(rule)
    if rule.isActive then
        local ruleTimerId = Timer.add(rule.timerInterval, onRuleTimerOut)
        currExecutingRules[rule] = ruleTimerId
    end
end

local function execRules(ruleList, itemInventory)
    stopAllRules()
    if itemInventory == nil and #ruleList > 0 then
        error("You didn't provide an inventory implementation")
    end
    itemInventory = itemInventory
    for rule in ruleList do
        execRule(rule)
    end
end

local function setMode(mode)
    if mode == const.mode.keep or mode == const.mode.drain or mode == const.mode.fill then
        _rule.mode = mode
    else
        return false, "invalid input" .. tostring(mode)
    end
    return true
end

local function setItemOrTag(choice)
    if choice == const.itemOrTag.item or choice == const.itemOrTag.tag then
        _rule.itemOrTag = choice
    else
        return false, "invalid input" .. tostring(choice)
    end
    return true
end

local function setItemOrTagData(string)
    if type(string) == "string" then
        _rule.itemOrTagData = string
    else
        return false, "invalid input" .. tostring(string)
    end
    return true
end

local function setTimerInterval(int)
    if type(int) == "number"  and int > -1 then
        _rule.timerInterval = int
    else
        return false, "invalid input" .. tostring(int)
    end
    return true
end

local function setSlot(int)
    if type(int) == "number" then
        _rule.slot = int
    else
        return false, "invalid input" .. tostring(int)
    end
    return true
end

local function setItemCount(int)
    if type(int) == "number" then
        _rule.count = int
    else
        return false, "invalid input" .. tostring(int)
    end
    return true
end
local function isFinalizeable()
    if _rule.mode ~= nil and _rule.mode == const.mode.keep or _rule.mode == const.mode.fill then    --check everything is set
        if _rule.itemOrTag ~= nil and _rule.itemOrTag == const.itemOrTag.item or _rule.itemOrTag == const.itemOrTag.tag then
            if _rule.itemOrTagData ~= nil then
                if _rule.timerInterval ~= nil and _rule.timerInterval > -1 then
                    if _rule.count ~= nil and _rule.count > 0 then
                        if _rule.slot ~= nil then
                            return true
                        end
                    end
                end
            end
        end
        
    elseif _rule.mode == const.mode.drain then
        if _rule.itemOrTag == const.itemOrTag.item or _rule.itemOrTag == const.itemOrTag.tag then
            if _rule.itemOrTagData ~= nil then
                return true
            end
        end
    end
    return false
end

local function finalize()
    if isFinalizeable() then
        Dbg.LogI("rule", Utils.PrintTableRecursive(_rule))
        return Utils.CopyTable(_rule)
    end
end

local function displayString(rule)
    local text = ""
    if rule.isActive then
        text = text .. "*"
    end
    text = text .. rule.itemOrTagData
end

local function newRule()
    _rule = {
        mode = nil,
        itemOrTag = nil,
        itemOrTagData = nil,
        timerInterval = nil,
        slot = nil,
        count = nil,
        isActive = false
    }
end

local InvRules = {
    newRule = newRule,
    finalize = finalize,
    isFinalizeable = isFinalizeable,
    setMode = setMode,
    setItemOrTag = setItemOrTag,
    setItemOrTagData = setItemOrTagData,
    setTimerInterval = setTimerInterval,
    setSlot = setSlot,
    setItemCount = setItemCount,
    const = const,
    displayString = displayString,
    execRules = execRules
}
newRule()
return InvRules