--modified to work with log writer to enable easier debugging. mostly source code from cc itself


--[[- The @{cc.expect} library provides helper functions for verifying that
function arguments are well-formed and of the correct type.
@module cc.expect
@usage Define a basic function and check it has the correct arguments.
    local expect = require "Modules.expect"
    local expect, field = expect.expect, expect.field
    local function add_person(name, info)
        expect(1, name, "string")
        expect(2, info, "table", "nil")
        if info then
            print("Got age=", field(info, "age", "number"))
            print("Got gender=", field(info, "gender", "string", "nil"))
        end
    end
    add_person("Anastazja") -- `info' is optional
    add_person("Kion", { age = 23 }) -- `gender' is optional
    add_person("Caoimhin", { age = 23, gender = true }) -- error!
]]
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local Dbg = require("Modules.Logger")

local native_select, native_type = select, type
---create string with possible types for argument
---@return string formattedString
local function get_type_names(...)
	local types = table.pack(...)
	for i = types.n, 1, -1 do
		if types[i] == "nil" then table.remove(types, i) end
	end

	if #types <= 1 then
		return tostring(...)
	else
		return table.concat(types, ", ", 1, #types - 1) .. " or " .. types[#types]
	end
end

---concatenates arguments returned from a interface function call
---@param argTable table table of arguments
---@return string argumentString formated string containing arguments
local function concatArgs(argTable)
	if #argTable == 0 then
		return "None"
	end
	local retString = ""
	for i = 1, #argTable - 1 do
		retString = retString .. tostring(i) .. ": " .. tostring(argTable[i]) .. ", "
	end
	retString = retString .. tostring(#argTable) .. ": " .. tostring(argTable[#argTable])
	return retString
end

---check if implementation adheres to interface
---@param index number The 1-based argument index.
---@param interface table interface the class should implement
---@param class table class that should be checked for interface implementation
local function expectInterface(index, interface, class)
	if type(index) ~= "number" then
		local err = "#" .. tostring(index) .. " #1 wrong type should be table"
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_INTERFACE", errMsg)
		error(err, 3)
	end
	if class == nil or type(class) ~= "table" then
		local err = "#" ..
			tostring(index) .. " No implementation given or implementation not a table, type: " .. type(class)
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_INTERFACE", errMsg)
		error(err, 3)
	end
	if interface == nil or type(interface) ~= "table" then
		local err = "#" .. tostring(index) .. " No interface given or interface not a table"
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_INTERFACE", errMsg)
		error(err, 3)
	end
	if interface == class then
		return
	end
	for funcName, _ in pairs(interface) do
		if type(interface[funcName]) == "function" then
			local fncInfo = interface[funcName]()
			if fncInfo == nil or fncInfo.arguments == nil or fncInfo.returns == nil then
				local err = "#" .. tostring(index) .. " Interface function " .. funcName .. " missing return"
				local _, errMsg = pcall(error, err, 4)
				Dbg.logE("EXPECT_INTERFACE", errMsg)
				error(err, 3)
			end
			local argString = concatArgs(fncInfo.arguments)
			local retString = concatArgs(fncInfo.returns)
			if class[funcName] == nil or type(class[funcName]) ~= "function" then
				local err = "#" ..
					tostring(index) ..
					" implementation missing function " ..
					funcName .. " ,args " .. argString .. " ,returns " .. retString
				local _, errMsg = pcall(error, err, 4)
				Dbg.logE("EXPECT_INTERFACE", errMsg)
				error(err, 3)
			end
			-- is debug replaceable by something else here?
			local classFncInfo = debug.getinfo(class[funcName], "u")
			local interfaceFncInfo = debug.getinfo(interface[funcName], "u")
			if classFncInfo.nparams < interfaceFncInfo.nparams then
				local err = "#" ..
					tostring(index) ..
					" implementation function " ..
					funcName .. " does not match prototype args " .. argString .. " ,returns" .. retString
				local _, errMsg = pcall(error, err, 4)
				Dbg.logE("EXPECT_INTERFACE", errMsg)
				error(err, 3)
			end
		end
	end
end

---check if value is in a enum
---@param index number the 1-based argument index
---@param enum table enum table
---@param value number | string value to check
local function expectEnum(index, enum, value)
	if index == nil or type(index) ~= "number" then
		local err = " #1 wrong type, should be table"
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_ENUM", errMsg)
		error(err, 3)
	end
	if enum == nil or type(enum) ~= "table" then
		local err = " #2 No enum given to compare with or enum type is wrong: " .. type(value)
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_ENUM", errMsg)
		error(err, 3)
	end
	if value == nil or type(value) ~= "number" then
		local err = " #3 wrong type, should be number"
		local _, errMsg = pcall(error, err, 4)
		Dbg.logE("EXPECT_ENUM", errMsg)
		error(err, 3)
	end
	for _, val in pairs(enum) do
		if val == value then
			return
		end
	end
	local err = "#" .. tostring(index) .. " value doe not exist in enum"
	local _, errMsg = pcall(error, err, 4)
	Dbg.logE("EXPECT_ENUM", errMsg)
	error(err, 3)
end

---Expect an argument to have a specific type.
---@param index number The 1-based argument index.
---@param value any The argument's value.
---@vararg ... The allowed types of the argument.
---@return any Value The given `value`
---@throws If the value is not one of the allowed types.
local function expect(index, value, ...)
	local t = native_type(value)
	for i = 1, native_select("#", ...) do
		if t == native_select(i, ...) then return value end
	end

	-- If we can determine the function name with a high level of confidence, try to include it.
	local name
	if native_type(debug) == "table" and native_type(debug.getinfo) == "function" then
		local ok, info = pcall(debug.getinfo, 3, "nS")
		if ok and info.name and info.name ~= "" and info.what ~= "C" then name = info.name end
	end

	local type_names = get_type_names(...)
	if name then
		--local _, msg = pcall(error((),3)
		local _, msg = pcall(error, ("bad argument #%d to '%s' (expected %s, got %s)"):format(index, name, type_names, t),
			4)
		Dbg.logE("EXPECT", msg)
		error(("bad argument #%d to '%s' (expected %s, got %s)"):format(index, name, type_names, t), 3)
	else
		local _, msg = pcall(error, ("bad argument #%d (expected %s, got %s)"):format(index, type_names, t), 4)
		Dbg.logE("EXPECT", msg)
		error(("bad argument #%d (expected %s, got %s)"):format(index, type_names, t), 3)
	end
end

---Expect an field to have a specific type.
---@param tbl table The table to index.
---@param index string | number the field name to check
---@vararg ... strings the allowed types of argument
---@return any Contents The contents of the given field.
local function field(tbl, index, ...)
	expect(1, tbl, "table")
	expect(2, index, "string", "number")

	local value = tbl[index]
	index = tostring(index)
	local t = native_type(value)
	for i = 1, native_select("#", ...) do
		if t == native_select(i, ...) then return value end
	end

	if value == nil then
		local _, msg = pcall(error, ("field '%s' missing from table"):format(index), 4)
		Dbg.logE("EXPECT", msg)
		error(("field '%s' missing from table"):format(index), 3)
	else
		local _, msg = pcall(error, ("bad field '%s' (expected %s, got %s)"):format(index, get_type_names(...), t), 4)
		Dbg.logE("EXPECT", msg)
		error(("bad field '%s' (expected %s, got %s)"):format(index, get_type_names(...), t), 3)
	end
end

return {
	expect = expect,
	field = field,
	expectInterface = expectInterface,
	expectEnum = expectEnum
}
