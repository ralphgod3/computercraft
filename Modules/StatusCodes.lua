---@class StatusCode
local StatusCode = {}
--statuscode api version
StatusCode.version = 1
---@enum StatusCodeCode
StatusCode.Code = {
	Queued = 1,
	Ok = 0,
	Fail = -1,
	NoFuel = -2,
	UnbreakableBlock = -3,
	InvalidArgument = -4,
	EncounteredBlock = -5,
	MaxRetriesExceeded = -6,
	FileNotFound = -7,
	NotInitialized = -8,
	AlreadyInitialized = -9,
	NoServerResponse = -10,
	NoSpace = -11,
}



---convert statuscode to string
---@param code StatusCodeCode status code
---@return string string version of statuscode
function StatusCode.toString(code)
	if code == StatusCode.Code.Queued then
		return "Queued"
	elseif code == StatusCode.Code.Ok then
		return "Ok"
	elseif code == StatusCode.Code.Fail then
		return "Fail"
	elseif code == StatusCode.Code.NoFuel then
		return "OutOfFuel"
	elseif code == StatusCode.Code.UnbreakableBlock then
		return "UnbreakableBlock"
	elseif code == StatusCode.Code.InvalidArgument then
		return "InvalidArgument"
	elseif code == StatusCode.Code.EncounteredBlock then
		return "EncounteredBlock"
	elseif code == StatusCode.Code.MaxRetriesExceeded then
		return "MaxRetriesExceeded"
	elseif code == StatusCode.Code.FileNotFound then
		return "File not found"
	elseif code == StatusCode.Code.NotInitialized then
		return "Not initialized"
	elseif code == StatusCode.Code.AlreadyInitialized then
		return "Already initialized"
	elseif code == StatusCode.Code.NoServerResponse then
		return "No server response"
	elseif code == StatusCode.Code.NoSpace then
		return "No space"
	end
	return "UnknownError"
end

return StatusCode
