---@diagnostic disable: redundant-parameter
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Ithreads = require("Interfaces.Modules.Ithreads")
local Dbg = require("Modules.Logger")

local TAG = "TIMER"
Dbg.setLogLevel(TAG, Dbg.Levels.Warning)

---create new timer manager
---@param threadManager Ithreads implementation of Ithreads
---@return Timer instance Itimer instance
local function new(threadManager)
	PC.expectInterface(1, Ithreads, threadManager)
	Dbg.logI(TAG, "creating timer instance")

	---@class Timer : Itimer
	local funcTable = {}
	local this = {
		id = 0,
		timerList = {},
		curTimerId = 0,
		busy = Utils.createBinarySemaphore(),
		threadId = nil
	}

	---coroutine that runs the timer functions
	local function thread()
		Dbg.logI(TAG, "starting timer thread")
		--fixes an issue where timers would not fire if the threadManager had not been started yet by the time the thread is created
		local initialPass = true
		while true do
			if not initialPass then
				repeat
					local _, id = os.pullEvent("timer")
				until id == this.curTimerId
			else
				initialPass = false
			end
			Utils.takeBinarySemaphore(this.busy)
			if #this.timerList > 0 then
				local curTime = os.clock()
				if this.timerList[1].time <= curTime then
					local ref = table.remove(this.timerList, 1)
					--Utils.freeBinarySemaphore(this.busy)
					threadManager.create(ref.func, table.unpack(ref.params))
				end
				curTime = os.clock()
				Utils.freeBinarySemaphore(this.busy)
				if #this.timerList > 0 then
					local toSleep = this.timerList[1].time - curTime
					if toSleep > 0 then
						Dbg.logI(TAG, "starting new timer")
						this.curTimerId = os.startTimer(toSleep)
					else
						Dbg.logI(TAG, "queueing dummy timer")
						this.curTimerId = this.curTimerId + 1
						os.queueEvent("timer", this.curTimerId)
					end
				else
					Dbg.logI(TAG, "killing timer thread")
					Utils.freeBinarySemaphore(this.busy)
					this.threadId = nil
					return
				end
			else
				Dbg.logI(TAG, "last timer got cancelled, killing thread")
				Utils.freeBinarySemaphore(this.busy)
				this.threadId = nil
			end
		end
	end

	---cancel a running timer
	---@param id number timer id of the timer that needs to be canceled
	function funcTable.cancel(id)
		PC.expect(1, id, "number")
		Dbg.logI(TAG, "canceling timer", id)
		Utils.takeBinarySemaphore(this.busy)
		for k, v in pairs(this.timerList) do
			if v.id == id then
				table.remove(this.timerList, k)
				if k == 1 then
					os.cancelTimer(this.curTimerId)
					if #this.timerList > 0 then
						this.curTimerId = os.startTimer(os.clock() - this.timerList[1].time)
					end
				end
			end
		end
		Utils.freeBinarySemaphore(this.busy)
	end

	---create a timer, requires calling Threads.StartRunning to function
	---@param time number timeout for the timer in seconds
	---@param func function function the timer should execute on timeout
	---@vararg ... parameters for the function that should be executed, may not contain nil parameters since lua var args wont handle that well
	---@return number id timer id used to cancel the timer
	function funcTable.add(time, func, ...)
		PC.expect(1, time, "number")
		PC.expect(2, func, "function")
		Dbg.logI(TAG, "adding timer for", time)
		Utils.takeBinarySemaphore(this.busy)
		this.id = this.id + 1
		local object = {
			id = this.id,
			time = os.clock() + time,
			func = func,
			params = { ... }
		}
		local position = 1
		for i = 1, #this.timerList do
			if object.time < this.timerList[i].time then
				break
			end
			position = position + 1
		end
		table.insert(this.timerList, position, object)
		if this.threadId == nil then
			this.threadId = threadManager.create(thread)
		end
		Utils.freeBinarySemaphore(this.busy)
		if position == 1 then
			os.cancelTimer(this.curTimerId)
			this.curTimerId = os.startTimer(time)
		end
		return object.id
	end

	return funcTable
end

return { new = new }
