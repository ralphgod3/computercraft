--FIXME: FIX THIS FUCKING CLASS

--includes
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")

local SaveFileLoc = "/cache/position"

Move = {} --not local so it works as a singleton
--move api version
Move.version = 1
Move.__index = Move
--private variables for move
Move._P = {}
Move._P.pos = Utils.createCoordinateSet(0, 0, 0, 0)
Move._P.MaxRetries = 10
Move.SaveLocationToFile = true

---save position to file if enabled
---@param pos Position { integer x, integer y, integer z, integer f}
local function saveState(pos)
	if not Move.SaveLocationToFile then
		return
	end
	PC.expect(1, pos, "table")
	PC.field(pos, "x", "number")
	PC.field(pos, "y", "number")
	PC.field(pos, "z", "number")
	PC.field(pos, "f", "number")
	local fh = fs.open(SaveFileLoc, "w")
	fh.write(textutils.serialize(pos))
	fh.close()
end

---load position from file
---@return StatusCodeCode (StatusCodeReturns)
local function loadState()
	if fs.exists(SaveFileLoc) then
		local fh = fs.open(SaveFileLoc, "r")
		local str = fh.readAll()
		Move._P.pos = textutils.unserialize(str)
		return StatusCode.Code.Ok
	end
	return StatusCode.Code.FileNotFound
end

---enable or disable location saving to a file
---@param enabled boolean save location to file enabled?
function Move.setPersistantLocationSaving(enabled)
	PC.expect(1, enabled, "boolean")
	Move.SaveLocationToFile = enabled
end

---clears the cached modules file so a call to require renews the modules
function Move.clearCache()
	if fs.exists(SaveFileLoc) then
		fs.delete(SaveFileLoc)
	end
end

---recover previous position
---@return StatusCodeCode statuscode
function Move.recoverPosition()
	print("recovering turtle position")
	local curX, curY, curZ = gps.locate()
	if turtle.getFuelLevel() < 2 or curX == nil then
		print("no fuel or no gps detected loading location from file")
		local ret = loadState()
		if ret == StatusCode.Code.Ok then
			print("pos: " .. Move.getPositionAsString())
		end
		return ret
	else
		curX = math.floor(curX + .5)
		curY = math.floor(curY + .5)
		curZ = math.floor(curZ + .5)
		--set position
		local turns = 0
		while turtle.detect() do
			turtle.turnRight()
			turns = turns + 1
			if turns == 4 then
				print("turtle is blocked in loading location from file")
				local ret = loadState()
				if ret == StatusCode.Code.Ok then
					print("pos: " .. Move.getPositionAsString())
				end
				return ret
			end
		end
		local ret = Move.forward(false)
		if ret == StatusCode.Code.Ok then
			local newX, newY, newZ = gps.locate()
			newX = math.floor(newX + .5)
			newY = math.floor(newY + .5)
			newZ = math.floor(newZ + .5)
			local facing = 0
			if newZ < curZ then --north z-
				facing = 0
			elseif newX > curX then --east x+
				facing = 1
			elseif newZ > curZ then --south z+
				facing = 2
			elseif newX < curX then -- west x-
				facing = 3
			end
			Move.setPosition({ x = newX, y = newY, z = newZ, f = facing })
			if facing < turns then
				turns = turns - 4
			end
			facing = facing - turns
			--return to old position and facing
			Move.goTo({ x = curX, y = curY, z = curZ, f = facing }, false)
			print("location loaded from gps")
			print("pos: " .. Move.getPositionAsString())
			return StatusCode.Code.Ok
		end
	end
	print("unknown error with gps recovery, recovering location from file")
	return Move.loadState()
end

---set the current position
---@param position Position {integer x, integer y, integer z, integer f}
function Move.setPosition(position)
	PC.expect(1, position, "table")
	PC.field(position, "x", "number")
	PC.field(position, "y", "number")
	PC.field(position, "z", "number")
	PC.field(position, "f", "number")
	Move._P.pos.x = position.x
	Move._P.pos.y = position.y
	Move._P.pos.z = position.z
	Move._P.pos.f = position.f
	saveState(position)
end

---get current turtle position
---@return Position position {integer x, integer y, integer z, integer f}
function Move.getPosition()
	return Utils.copyTable(Move._P.pos)
end

---gets current turtle position as printable string
---@return string position
function Move.getPositionAsString()
	local pos = Move.getPosition()
	return "x: " .. pos.x .. " y: " .. pos.y .. " z: " .. pos.z .. " f: " .. pos.f
end

---set maximum amount of retries before giving up on a move
---@param amountOfRetries number amount of tries before giving up on move
---@return StatusCodeCode statuscode statuscode indicating success
function Move.setMaxMoveRetries(amountOfRetries)
	PC.expect(1, amountOfRetries, "number")
	if amountOfRetries < 0 then return StatusCode.Code.InvalidArgument end
	Move._P.MaxRetries = amountOfRetries
	return StatusCode.Code.Ok
end

---move turtle Forward
---@param destroy boolean (is turtle allowed to destroy blocks)
---@param blocksToNotDestroy string[] | nil (whitelist of blocks turtle is not allowed to destroy leave blank to destroy everything (only used when destroy is true))
---@return StatusCodeCode statuscode status code indicating success
function Move.forward(destroy, blocksToNotDestroy)
	PC.expect(1, destroy, "boolean")
	PC.expect(2, blocksToNotDestroy, "table", "nil")
	if turtle.getFuelLevel() == 0 then
		return StatusCode.Code.NoFuel
	end
	local retries = 0
	while not turtle.forward() do
		local suc, ret = turtle.inspect()
		if suc and not turtle.attack() then
			if destroy == false then return StatusCode.Code.EncounteredBlock end

			--check if the block infront is allowed to be broken
			if blocksToNotDestroy ~= nil then
				for i = 1, #blocksToNotDestroy do
					if ret.name == blocksToNotDestroy[i] then return StatusCode.Code.EncounteredBlock end
				end
			end

			turtle.dig()
		end

		--check for maximum amount of retries being exceeded
		if Move._P.MaxRetries > 0 then
			retries = retries + 1
			if retries > Move._P.MaxRetries then return StatusCode.Code.MaxRetriesExceeded end
		end
	end

	if Move._P.pos.f == 0 then  --north z-
		Move._P.pos.z = Move._P.pos.z - 1
	elseif Move._P.pos.f == 1 then -- east x+
		Move._P.pos.x = Move._P.pos.x + 1
	elseif Move._P.pos.f == 2 then -- south z+
		Move._P.pos.z = Move._P.pos.z + 1
	elseif Move._P.pos.f == 3 then -- west  x-
		Move._P.pos.x = Move._P.pos.x - 1
	end
	saveState(Move._P.pos)
	return StatusCode.Code.Ok
end

---move turtle backward, will never destroy blocks and if blocked will give up after retries are reached
---@return StatusCodeCode statuscode status code indicating success
function Move.backward()
	if turtle.getFuelLevel() == 0 then
		return StatusCode.Code.NoFuel
	end
	local retries = 0
	while not turtle.back() do
		sleep(0.5)
		--check for maximum amount of retries being exceeded
		if Move._P.MaxRetries > 0 then
			retries = retries + 1
			if retries > Move._P.MaxRetries then return StatusCode.Code.MaxRetriesExceeded end
		end
	end

	if Move._P.pos.f == 0 then  --north z-
		Move._P.pos.z = Move._P.pos.z + 1
	elseif Move._P.pos.f == 1 then -- east x+
		Move._P.pos.x = Move._P.pos.x - 1
	elseif Move._P.pos.f == 2 then -- south z+
		Move._P.pos.z = Move._P.pos.z - 1
	elseif Move._P.pos.f == 3 then -- west  x-
		Move._P.pos.x = Move._P.pos.x + 1
	end
	saveState(Move._P.pos)
	return StatusCode.Code.Ok
end

---move turtle up
---@param destroy boolean (is turtle allowed to destroy blocks)
---@param blocksToNotDestroy string[] | nil (whitelist of blocks turtle is not allowed to destroy (only used when destroy is true))
---@return StatusCodeCode statusCode
function Move.up(destroy, blocksToNotDestroy)
	PC.expect(1, destroy, "boolean")
	PC.expect(2, blocksToNotDestroy, "table", "nil")
	if turtle.getFuelLevel() == 0 then
		return StatusCode.Code.NoFuel
	end
	local retries = 0

	while not turtle.up() and not turtle.attackUp() do
		local suc, ret = turtle.inspectUp()
		if suc then
			if destroy == false then return StatusCode.Code.EncounteredBlock end

			--check if the block infront is allowed to be broken
			if blocksToNotDestroy ~= nil then
				for i = 1, #blocksToNotDestroy do
					if ret.name == blocksToNotDestroy[i] then return StatusCode.Code.EncounteredBlock end
				end
			end

			turtle.digUp()
		end
		--check for maximum amount of retries being exceeded
		if Move._P.MaxRetries > 0 then
			retries = retries + 1
			if retries > Move._P.MaxRetries then return StatusCode.Code.MaxRetriesExceeded end
		end
	end

	Move._P.pos.y = Move._P.pos.y + 1
	saveState(Move._P.pos)
	return StatusCode.Code.Ok
end

---move turtle Down
---@param destroy boolean (is turtle allowed to destroy blocks)
---@param blocksToNotDestroy string[] | nil (whitelist of blocks turtle is not allowed to destroy (only used when destroy is true))
---@return StatusCodeCode statuscode
function Move.down(destroy, blocksToNotDestroy)
	PC.expect(1, destroy, "boolean")
	PC.expect(2, blocksToNotDestroy, "table", "nil")
	if turtle.getFuelLevel() == 0 then
		return StatusCode.Code.NoFuel
	end
	local retries = 0
	while not turtle.down() and not turtle.attackDown() do
		local suc, ret = turtle.inspectDown()
		if suc then
			if destroy == false then return StatusCode.Code.EncounteredBlock end

			--check if the block infront is allowed to be broken
			if blocksToNotDestroy ~= nil then
				for i = 1, #blocksToNotDestroy do
					if ret.name == blocksToNotDestroy[i] then return StatusCode.Code.EncounteredBlock end
				end
			end

			turtle.digDown()
		end
		--check for maximum amount of retries being exceeded
		if Move._P.MaxRetries > 0 then
			retries = retries + 1
			if retries > Move._P.MaxRetries then return StatusCode.Code.MaxRetriesExceeded end
		end
	end

	Move._P.pos.y = Move._P.pos.y - 1
	saveState(Move._P.pos)
	return StatusCode.Code.Ok
end

---turn turtle left
function Move.turnLeft()
	turtle.turnLeft()
	Move._P.pos.f = Move._P.pos.f - 1

	if Move._P.pos.f < 0 then
		Move._P.pos.f = 3
	end
	saveState(Move._P.pos)
end

---turn turtle right
function Move.turnRight()
	turtle.turnRight()
	Move._P.pos.f = Move._P.pos.f + 1

	if Move._P.pos.f > 3 then
		Move._P.pos.f = 0
	end
	saveState(Move._P.pos)
end

---turn turtle to a facing
---@param direction Facing facing 0,3
function Move.turnToDirection(direction)
	PC.expect(1, direction, "number")
	if direction < 0 or direction > 3 then
		error("direction < 0 or > 3 invalid", 2);
	end
	local dif = direction - Move._P.pos.f
	local absDif = math.abs(dif)

	if (dif < 0 and absDif > 2) or (dif > 0 and absDif < 2) then
		while direction ~= Move._P.pos.f do
			Move.turnRight()
		end
	elseif (dif > 0 and absDif > 2) or (dif < 0 and absDif < 2) then
		while direction ~= Move._P.pos.f do
			Move.turnLeft()
		end
	else
		while direction ~= Move._P.pos.f do
			Move.turnRight()
		end
	end
end

---calculates the amount of moves needed to reach a position from the current position
---@param newPosition Position {integer x, integer y, integer z, integer f}
---@return number movesNeeded amount of moves needed
function Move.calculateMovesNeeded(newPosition)
	PC.expect(1, newPosition, "table")
	PC.field(newPosition, "x", "number")
	PC.field(newPosition, "y", "number")
	PC.field(newPosition, "z", "number")
	PC.field(newPosition, "f", "number")
	return Utils.calculateDistance(Move._P.pos, newPosition)
end

---go to a location
---@param position Position {integer x, integer y, integer z, <optional>integer f}
---@param destroy boolean is turtle allowed to destroy blocks
---@param blocksToNotDestroy string[] | nil whitelist of blocks that turtle should not dig through, only used when destroy = true
---@return StatusCodeCode statuscode status code indicating success
function Move.goTo(position, destroy, blocksToNotDestroy)
	PC.expect(1, position, "table")
	PC.field(position, "x", "number")
	PC.field(position, "y", "number")
	PC.field(position, "z", "number")
	PC.field(position, "f", "number", "nil")
	PC.expect(2, destroy, "boolean")
	PC.expect(3, blocksToNotDestroy, "table", "nil")
	--check fuel levels
	if Move.calculateMovesNeeded(position) > turtle.getFuelLevel() then
		return StatusCode.Code.NoFuel
	end

	--GoTo x coordinate
	if Move._P.pos.x ~= position.x then
		if Move._P.pos.x > position.x then
			Move.turnToDirection(3) --face west
		else
			Move.turnToDirection(1) --face east
		end
		--go Forward until x coords match
		while Move._P.pos.x ~= position.x do
			local ret = Move.forward(destroy, blocksToNotDestroy)
			if ret ~= StatusCode.Code.Ok then return ret end
		end
	end
	--GoTo z coordinate
	if Move._P.pos.z ~= position.z then
		if Move._P.pos.z > position.z then
			Move.turnToDirection(0) --face north
		else
			Move.turnToDirection(2) --face south
		end
		--go Forward until x coords match
		while Move._P.pos.z ~= position.z do
			local ret = Move.forward(destroy, blocksToNotDestroy)
			if ret ~= StatusCode.Code.Ok then return ret end
		end
	end
	--GoTo y coordinate
	if Move._P.pos.y ~= position.y then
		while Move._P.pos.y < position.y do
			local ret = Move.up(destroy, blocksToNotDestroy)
			if ret ~= StatusCode.Code.Ok then return ret end
		end
		while Move._P.pos.y > position.y do
			local ret = Move.down(destroy, blocksToNotDestroy)
			if ret ~= StatusCode.Code.Ok then return ret end
		end
	end
	if position.f ~= nil then
		--GoTo correct direction
		if position.f ~= Move._P.pos.f then
			Move.turnToDirection(position.f)
		end
	end
	return StatusCode.Code.Ok
end

return Move
