---@diagnostic disable: redundant-parameter
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

local craftOsPCEmulatedVersion = "1.16.5"
local Utils = {}
--utils version
Utils.version = 1

---convert string to byte array
---@param stringToConvert string string to convert into byte array
---@return number[] byteArray byte array representing input string
function Utils.convertStringToByteArray(stringToConvert)
	PC.expect(1, stringToConvert, "string")
	local tempArr = {}
	for i = 1, string.len(stringToConvert) do
		table.insert(tempArr, string.byte(stringToConvert, i, i))
	end
	return tempArr
end

---convert byte array into string
---@param arrayToConvert number[] byte array to convert into string
---@return string string converted byte array
function Utils.convertByteArrayToString(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local ret = {}
	for i = 1, #arrayToConvert do
		ret[i] = string.char(arrayToConvert[i])
	end
	return table.concat(ret)
end

---convert byte array into nibble array
---@param arrayToConvert number[] byte array to convert
---@return number[] nibbleArray converted nibble array
function Utils.convertByteArrayToNibbleArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	for _, v in ipairs(arrayToConvert) do
		if v > 255 then
			error("invalid byte array given", 2)
		end
		table.insert(retTable, bit.band(v, 15))
		table.insert(retTable, bit.band(bit.brshift(v, 4), 15))
	end
	return retTable
end

---convert nibble array into byte array
---@param arrayToConvert number[] nibble array to convert
---@return number[] byteArray converted nibble array
function Utils.convertNibbleArrayToByteArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	if #arrayToConvert % 2 ~= 0 then
		error("nibble array has uneven length", 2)
	end
	for i = 1, #arrayToConvert, 2 do
		if arrayToConvert[i] > 15 or arrayToConvert[i + 1] > 15 then
			error("invallid nibble array given", 2)
		end
		local byte = arrayToConvert[i]
		byte = byte + bit.blshift(arrayToConvert[i + 1], 4)
		table.insert(retTable, byte)
	end
	return retTable
end

---convert byte array into dibit array
---@param arrayToConvert number[] byte array to convert
---@return number[] dibit array
function Utils.convertByteArrayToDibitArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	for _, v in ipairs(arrayToConvert) do
		if v > 255 then
			error("invalid byte array given", 2)
		end
		table.insert(retTable, bit.band(v, 3))
		table.insert(retTable, bit.band(bit.brshift(v, 2), 3))
		table.insert(retTable, bit.band(bit.brshift(v, 4), 3))
		table.insert(retTable, bit.band(bit.brshift(v, 6), 3))
	end
	return retTable
end

---converts a dibit array back into a byte array
---@param arrayToConvert number[] dibit array to convert
---@return number[] byteArray converted array
function Utils.convertDibitArrayToByteArray(arrayToConvert)
	PC.expect(1, arrayToConvert, "table")
	local retTable = {}
	if #arrayToConvert % 4 ~= 0 then
		error("Dibit array not dividable by 4", 2)
	end
	for i = 1, #arrayToConvert, 4 do
		if arrayToConvert[i] > 3 or arrayToConvert[i + 1] > 3 or arrayToConvert[i + 2] > 3 or arrayToConvert[i + 3] > 3 then
			error("invallid dibit array given", 2)
		end
		local byte = arrayToConvert[i]
		byte = byte + bit.blshift(arrayToConvert[i + 1], 2)
		byte = byte + bit.blshift(arrayToConvert[i + 2], 4)
		byte = byte + bit.blshift(arrayToConvert[i + 3], 6)
		table.insert(retTable, byte)
	end
	return retTable
end

---split a string
---@param inputString string string to split
---@param seperator string seperator
---@return string[] | nil splitString splitString or nil if no splits
function Utils.splitString(inputString, seperator)
	--input validation
	PC.expect(1, inputString, "string")
	PC.expect(2, seperator, "string")
	--seperating the strings
	local outTable = {}
	local sepLength = string.len(seperator)
	local sepLocation = string.find(inputString, seperator, nil, true)

	while sepLocation do
		local sub = string.sub(inputString, 0, sepLocation - 1)

		if sub ~= nil and sub ~= "" then
			table.insert(outTable, sub)
		end

		inputString = string.sub(inputString, sepLocation + sepLength)
		sepLocation = string.find(inputString, seperator, nil, true)
	end

	if inputString ~= nil and inputString ~= "" then
		table.insert(outTable, inputString)
	end

	if #outTable == 0 then
		return nil
	end

	return outTable
end

---find an element in a table returns key for element if element exists or nil on failure
---@param tableToFindElement table table to find element in
---@param element any element to find key for
---@return any key key for element if exists or nil
function Utils.findElementInTable(tableToFindElement, element)
	PC.expect(1, tableToFindElement, "table")
	for k, v in pairs(tableToFindElement) do
		if v == element then
			return k
		end
	end
	return nil
end

---check if any element of either table exists in the other table
---@param table1 table table 1 to check
---@param table2 table table 2 to check
---@return boolean exists table2 has an element that is the same as table 1
function Utils.elementOfTableExistInTable(table1, table2)
	PC.expect(1, table1, "table")
	PC.expect(2, table2, "table")
	for _, v in pairs(table1) do
		if Utils.findElementInTable(table2, v) ~= nil then
			return true
		end
	end
	return false
end

---checks if a key exists in a table
---@param tableToFindKeyIn table table to find key
---@param Key any key to find in table
---@return boolean keyExists true key exists false key does not exist
function Utils.keyExistsInTable(tableToFindKeyIn, Key)
	PC.expect(1, tableToFindKeyIn, "table")
	if type(tableToFindKeyIn) ~= "table" then
		return false
	end
	for k, _ in pairs(tableToFindKeyIn) do
		if tostring(k) == tostring(Key) then
			return true
		end
	end
	return false
end

---uses deep copy to copy a table
---@param tableToCopy table table to copy
---@return table copy table copy
function Utils.copyTable(tableToCopy)
	PC.expect(1, tableToCopy, "table")
	local copy = {}
	for k, v in pairs(tableToCopy) do
		if type(v) == "table" then
			copy[k] = Utils.copyTable(v)
		else
			copy[k] = v
		end
	end
	return copy
end

---prints key and value pairs in a table
---@param tableToPrint table table to print
---@return string table table in string form to feed to your printer of choice
function Utils.printTable(tableToPrint)
	PC.expect(1, tableToPrint, "table")
	local retString = "{\r\n"
	for k, v in pairs(tableToPrint) do
		retString = retString .. "\t" .. tostring(k) .. " : " .. tostring(v) .. "\r\n"
	end
	return retString .. "}\r\n"
end

---internal function to format recursive table printing
---@param curDepth number current depth in table
---@return string string string containing apropriate amount of tabs
local function handleCurDepthTabs(curDepth)
	PC.expect(1, curDepth, "number")
	local retString = ""
	for i = 1, curDepth do
		retString = retString .. "\t"
	end
	return retString
end

---print tables recursively (will print tables in tables in tables ...)
---@param tableToPrint table table to print
---@param curDepth number | nil optional parameter used by recursive calls that gets current array Depth used for fancy printing
---@return string table table in string form to feed to your printer of choice
function Utils.printTableRecursive(tableToPrint, curDepth)
	PC.expect(1, tableToPrint, "table")
	PC.expect(2, curDepth, "number", "nil")
	if curDepth == nil then
		curDepth = 1
	end

	local retString = handleCurDepthTabs(curDepth - 1) .. "{\r\n"
	for k, v in pairs(tableToPrint) do
		if type(v) == "table" then
			retString = retString .. handleCurDepthTabs(curDepth) .. tostring(k) .. " :\r\n"
			retString = retString .. Utils.printTableRecursive(v, curDepth + 1)
		else
			retString = retString .. handleCurDepthTabs(curDepth) .. tostring(k) .. " : " .. tostring(v) .. "\r\n"
		end
	end
	retString = retString .. handleCurDepthTabs(curDepth - 1) .. "}\r\n"
	return retString
end

---remove single element from table by element value, does NOT rebuild array order
---@param tableToRemoveElementFrom table table to remove element from
---@param elementToRemove any element to remove
---@return table outputTable table with element removed
function Utils.removeElementFromTable(tableToRemoveElementFrom, elementToRemove)
	PC.expect(1, tableToRemoveElementFrom, "table")
	if elementToRemove == nil then
		return tableToRemoveElementFrom
	end
	local tableCopy = Utils.copyTable(tableToRemoveElementFrom)
	local location = Utils.findElementInTable(tableCopy, elementToRemove)
	if location ~= nil then
		tableCopy[location] = nil
	end
	return tableCopy
end

---remove multiple elements from a table by element value, does NOT rebuild array order
---@param tableToRemoveElementsFrom table table to remove elements from
---@param elementsToRemove table table of elements to remove from first table
---@return table outTable table without elements of elementsToRemove
function Utils.removeElementsFromTable(tableToRemoveElementsFrom, elementsToRemove)
	PC.expect(1, tableToRemoveElementsFrom, "table")
	PC.expect(1, elementsToRemove, "table", "nil")
	--no elements needed to be removed so return
	if elementsToRemove == nil then
		return tableToRemoveElementsFrom
	end

	local tableCopy = Utils.copyTable(tableToRemoveElementsFrom)
	for _, v in pairs(elementsToRemove) do
		tableCopy = Utils.removeElementFromTable(tableCopy, v)
	end
	return tableCopy
end

---rebuild an array that has missing keys back to a number iterable array, may not preserve element order
---@param arrayToRebuild any[] array to rebuild
---@return any[] rebuildArray rebuild array without missing keys
function Utils.rebuildArray(arrayToRebuild)
	PC.expect(1, arrayToRebuild, "table")
	local retTable = {}
	for _, v in pairs(arrayToRebuild) do
		table.insert(retTable, v)
	end
	return retTable
end

---get amount of elements in table not indexed by number
---@param tableToCheck table to get length for
---@return number tableLen amount of elements in table
function Utils.getTableLength(tableToCheck)
	PC.expect(1, tableToCheck, "table")
	local len = 0
	for _, _ in pairs(tableToCheck) do
		len = len + 1
	end
	return len
end

---find first match of item(s) in turtle inventory
---@param itemToFind string | table string or table when a table is given will return when item in the table is found
---@param startSlot number slot to start searching from 0 > slot < 17
---@param exactMatch boolean | nil enables exact matching of name defaults to false
---@return number | nil value slot that item resides in or nil when no item is found
function Utils.findItemInInventory(itemToFind, startSlot, exactMatch)
	PC.expect(1, itemToFind, "string", "table")
	PC.expect(2, startSlot, "number", "nil")
	PC.expect(3, exactMatch, "boolean", "nil")
	exactMatch = exactMatch or false

	if startSlot == nil then
		startSlot = 1
	end
	if startSlot < 1 or startSlot > 16 then
		error("invalid slot number", 2)
	end
	for i = startSlot, 16 do
		local itemDetails = turtle.getItemDetail(i)
		if itemDetails ~= nil then
			if type(itemToFind) == "string" then
				if exactMatch then
					if itemDetails.name == itemToFind then
						return i
					end
				else
					if string.find(itemDetails.name, itemToFind, nil, true) ~= nil then
						return i
					end
				end
			elseif type(itemToFind) == "table" then
				if exactMatch then
					for _, v in pairs(itemToFind) do
						if itemDetails.name == v then
							return i
						end
					end
				else
					for _, v in pairs(itemToFind) do
						if string.find(itemDetails.name, v, nil, true) ~= nil then
							return i
						end
					end
				end
			end
		end
	end
	return nil
end

---find all items in the inventory matching the itemToFind filter does partial matching so searching for "computercraft" will return all computercraft items in inventory
---@param itemToFind string | table string or table when a table is given will return when item in the table is found
---@param startSlot number | nil slot to start searching from 0 > slot < 17
---@param exactMatch boolean | nil enables exact matching of name defaults to false
---@return nil | table value nil or table that item slots with that item resides in
function Utils.findItemsInInventory(itemToFind, startSlot, exactMatch)
	PC.expect(1, itemToFind, "string", "table")
	PC.expect(2, startSlot, "number", "nil")
	PC.expect(3, exactMatch, "boolean", "nil")
	if startSlot == nil or startSlot == 0 then
		startSlot = 1
	end
	local outTable = {}
	local out = Utils.findItemInInventory(itemToFind, startSlot, exactMatch)
	while out ~= nil do
		table.insert(outTable, out)
		out = Utils.findItemInInventory(itemToFind, out + 1, exactMatch)
		if out == 16 then
			table.insert(outTable, out)
			break
		end
	end
	if #outTable == 0 then
		return nil
	end
	return outTable
end

---yields program if not given lastYieldTime, if given only yields when nescesary
---@param lastYieldTime number? time from epoch that yield got called last time
---@return number lastYieldTime number that can be fed into next call of yield function
function Utils.yield(lastYieldTime)
	if not lastYieldTime or os.epoch("local") - lastYieldTime > 5000 then
		local myEvent = tostring({})
		os.queueEvent(myEvent)
		os.pullEvent(myEvent)
		return os.epoch("local")
	end
	return lastYieldTime
end

---get minecraft and computercraft version
---@return string minecraftVersion minecraft version
---@return string computercraftVersion computercraft version
function Utils.getVersionInfo()
	local mc_version = _HOST:match("Minecraft (%S+)")
	local cc_version = _HOST:match("ComputerCraft (%S+)")
	if mc_version then
		mc_version = string.sub(mc_version, 0, string.len(mc_version) - 1)
	else
		mc_version = craftOsPCEmulatedVersion
	end
	return mc_version, cc_version
end

---calculates the distance between 2 3dimensional points
---@param position1 Position table containing position1 {x,y,z}
---@param position2 Position table containing position2 {x,y,z}
---@return number distance returns distance
function Utils.calculateDistance(position1, position2)
	PC.expect(1, position1, "table")
	PC.field(position1, "x", "number")
	PC.field(position1, "y", "number")
	PC.field(position1, "z", "number")
	PC.expect(2, position2, "table")
	PC.field(position2, "x", "number")
	PC.field(position2, "y", "number")
	PC.field(position2, "z", "number")

	local x = math.abs(position1.x - position2.x)
	local y = math.abs(position1.y - position2.y)
	local z = math.abs(position1.z - position2.z)
	return x + y + z
end

---@enum Facing
Utils.Facing = {
	North = 0,
	East = 1,
	South = 2,
	West = 3,
}


---create a coordinate set as used by the Move api
---calling the function with no arguments will generate a coordinate set with { x = 0, y = 0, z = 0, f = 0}
---@param x number? x coord to use
---@param y number? y coord to use
---@param z number? z coord to use
---@param f Facing? facing to use
---@return Position coordinate set
function Utils.createCoordinateSet(x, y, z, f)
	PC.expect(1, x, "number", "nil")
	PC.expect(2, y, "number", "nil")
	PC.expect(3, z, "number", "nil")
	PC.expect(4, f, "number", "nil")

	if x == nil then
		x = 0
	end
	if y == nil then
		y = 0
	end
	if z == nil then
		z = 0
	end
	---@class Position
	return {
		x = x,
		y = y,
		z = z,
		---@type Facing?
		f = f
	}
end

---creates a binary semaphore and returns value
---@return BinarySemaphore binarySemaphore binary semaphore
function Utils.createBinarySemaphore()
	---@class BinarySemaphore
	return { false }
end

---takes binary semaphore if available, waits for it to be available otherwise
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreValue always true for now
function Utils.takeBinarySemaphore(sem)
	while sem[1] do
		Utils.yield()
	end
	sem[1] = true
	return sem[1]
end

---free binary semaphore
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreValue always false for now
function Utils.freeBinarySemaphore(sem)
	sem[1] = false
	return sem[1]
end

---checks if binary semaphore is free
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean semaphoreFree true if free, false if taken
function Utils.isBinarySemaphoreFree(sem)
	return not sem[1]
end

---takes binary semaphore if available, returns false if it could not take the semaphore
---@param sem BinarySemaphore binary semaphore created with createBinarySemaphore
---@return boolean tookSemaphore true if it took the semaphore, false otherwise
function Utils.takeBinarySemaphoreIfFree(sem)
	if sem[1] == false then
		sem[1] = true
		return true
	end
	return false
end

---create counting semaphore
---@return CountingSemaphore countingSemaphore
function Utils.createCountingSemaphore()
	---@class CountingSemaphore
	return { 0 }
end

---take a counting semaphore (adds +1 to it)
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
---@return number semaphoreValue value of the counting semaphote
function Utils.takeCountingSemaphore(sem)
	sem[1] = sem[1] + 1
	return sem[1]
end

---free a counting semaphore (subtracts 1 from it)
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
---@return number semaphoreValue value of the counting semaphore
function Utils.freeCountingSemaphore(sem)
	sem[1] = sem[1] - 1
	return sem[1]
end

---wait while counting semaphore > 0
---@param sem CountingSemaphore counting semaphore created with createCountingSemaphore
function Utils.awaitCountingSemaphore(sem)
	while sem[1] > 0 do
		Utils.yield()
	end
	return sem[1]
end

---runs threading manager if nescesary to paralelize inventory access
---@param threads Ithreads implementation of Ithreads.lua interface
---@return boolean threadingManagerNeededRunning true if we ran threading manager, false otherwise
function Utils.runThreadingManagerIfNeeded(threads)
	if threads.isRunning == nil then
		error("threading manager missing isRunning method", 2)
	end
	if threads.startRunning == nil then
		error("threading manager missing startRunning method", 2)
	end

	if not threads.isRunning() then
		threads.startRunning()
		return true
	end
	return false
end

---generates a psuedo random number for seeding ids
---@return integer id
function Utils.generateId()
	return math.floor(math.random() * math.pow(2, 52))
end

---next id rolls over to 1 if higher than 2^52
---@param id integer
---@return integer id
function Utils.getNextId(id)
	if id >= math.pow(2, 52) then
		return 1
	end
	return id + 1
end

return Utils
