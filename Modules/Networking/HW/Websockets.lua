local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")

local Websockets = {}
---create new websocket hardware
---@param broadcastChannel integer | nil broadcastchannel to use
---@param address string | nil websocket address to connect with
---@return InetworkingHardware websocketInstance instance of websocket hardware
function Websockets.new(broadcastChannel, address)
	PC.expect(1, address, "string", "nil")
	PC.expect(2, broadcastChannel, "number", "nil")
	address = address or "ws://webworking.ralphmontfort.com:25564"
	broadcastChannel = broadcastChannel or 65532
	local path = address .. "/" .. textutils.urlEncode(_HOST)
	local socket = nil
	local computerId = os.getComputerID() % 65001
	local funcTable = {}

	local function openWebSocket(channels)
		local connectMsg = {
			lowLeveltype = 3, --currently only 0, 1 and 2 are used by software layers, 3 should be safe
			id = computerId,
			openChannels = channels
		}
		local ret, reason = http.websocket(path)
		if ret == false then
			error("could not connect to " .. path .. " r: " .. reason)
		else
			ret.send(textutils.serializeJSON(connectMsg))
			socket = ret
		end
	end

	function funcTable.transmit(msg)
		socket.send(textutils.serializeJSON(msg))
	end

	function funcTable.receive()
		while true do
			local msg = socket.receive()
			if msg == nil then
				openWebSocket({broadcastChannel, computerId})
			else
				local decodedMsg = textutils.unserialiseJSON(msg)
				if decodedMsg ~= nil then
					return decodedMsg
				end
			end
		end
	end

	return funcTable
end

return Websockets
