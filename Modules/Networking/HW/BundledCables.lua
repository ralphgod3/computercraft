---@diagnostic disable: redundant-parameter
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")

local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

local Redworking = {}
---create networking hardware for transmision over one or more bundled cables
---@param broadcastChannel number empty argument for rs cables but is to keep compatibility with other hardware
---@param sides string[] sides to transmit, receive data over
---@return InetworkingHardware instance instance of redworking
function Redworking.new(broadcastChannel, sides)
	PC.expect(1, sides, "table", "nil")
	local this = {
		-- signal lines
		clk = colors.red,
		busy = colors.black,
		sides = { "back" },
		isSending = false
	}

	if #sides ~= 0 then
		this.sides = sides
	end
	-- reset any cables that are stil on
	for _, v in ipairs(Redworking.this.sides) do
		rs.setBundledOutput(v, 0)
	end
	-- give redstone some time to turn off
	sleep(0.5)

	-- send before every message so other side can split on this
	local preAmble = { 0, 1, 2, 3, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2 }

	-- send after every message so other side can split on this
	local postAmble = { 3, 2, 1, 0, 3, 2, 1, 3, 2, 1, 0, 3, 2, 1 }
	local funcTable = {}

	---find preamblePosition end in data received array
	---@param dataReceived table data received array
	---@param startPosition number | nil position to start searching from
	---@return number | nil preamblePosEnd position where pre amble ends or nil when no preamble is found
	local function getPreamblePosition(dataReceived, startPosition)
		startPosition = startPosition or 1
		local curAmblePos = 1
		for i = startPosition, #dataReceived do
			if preAmble[curAmblePos] == dataReceived[i] then
				curAmblePos = curAmblePos + 1
				if curAmblePos > #preAmble then
					return i
				end
			else
				curAmblePos = 1
			end
		end
		return nil
	end

	---finds postamblePosition start in data received array
	---@param dataReceived table data received array
	---@param startPosition number | nil position to start searching from
	---@return number | nil postamblePosStart postion where postamble starts or nil when no postamble is found
	local function getPostamblePosition(dataReceived, startPosition)
		startPosition = startPosition or 1
		local curAmblePos = 1
		for i = startPosition, #dataReceived do
			if postAmble[curAmblePos] == dataReceived[i] then
				curAmblePos = curAmblePos + 1
				if curAmblePos > #postAmble then
					return i - #postAmble
				end
			else
				curAmblePos = 1
			end
		end
		return nil
	end

	local function decodeReceivedMsg(receivedData)
		-- fixme: add case where multiple post or preambles are found within a message

		local preamblePos = getPreamblePosition(receivedData)
		print("pre " .. tostring(preamblePos))
		if preamblePos == nil then
			return
		end
		local postamblePos = getPostamblePosition(receivedData, preamblePos)
		print("post " .. tostring(preamblePos))
		if postamblePos == nil then
			return
		end
		-- ensure we always grab most data possible, can help when data contains postamble
		repeat
			local lastestPostAmblePos = getPostamblePosition(receivedData, postamblePos + #postAmble)
			if lastestPostAmblePos ~= nil then
				postamblePos = lastestPostAmblePos
			end
		until lastestPostAmblePos == nil

		print("pre " .. tostring(preamblePos) .. " post " .. tostring(postamblePos))
		local retTable = {}
		for i = preamblePos + 1, postamblePos do
			table.insert(retTable, receivedData[i])
		end
		return retTable
	end

	-- reads all bundled cable inputs and returns them as a table in order of sides
	local function getInputs()
		local retTable = {}
		for k, v in ipairs(this.sides) do
			retTable[k] = rs.getBundledInput(v)
		end
		return retTable
	end

	---split input in 2 bit pairs for easier procesing
	---@param inputFromSide number output from rs.getBundledInput()
	---@return table outTable dibit values for table
	local function getDibitsFromSide(inputFromSide)
		local retTable = {}
		for i = 0, 14, 2 do
			local dibit = bit.band(bit.brshift(inputFromSide, i), 3)
			table.insert(retTable, dibit)
		end
		return retTable
	end

	---handles receiving of a message, call after the busy signal is high on network cable
	---@return table receivedMsg received msg in the form of an array of nibbles, includes pre and postamble
	local function receiveMsg()
		local msg = {}
		-- clock will always start at high when transmitting
		local oldclck = 0
		while true do
			-- get cable status
			local inputs = getInputs()
			if bit.band(inputs[1], this.clk) ~= oldclck then
				for i = 1, #this.sides do
					local dibitArr = getDibitsFromSide(inputs[i])
					if i == 1 then
						for j = 1, 7 do
							table.insert(msg, dibitArr[j])
						end
					else
						for j = 1, 8 do
							table.insert(msg, dibitArr[j])
						end
					end
				end
				oldclck = bit.band(inputs[1], this.clk)
			end
			-- check for message end
			if bit.band(inputs[1], this.busy) == 0 then
				break
			end
			os.pullEvent("redstone")
		end
		return msg
	end

	---receive a message
	---@return table | nil receivedMsg received message
	function funcTable.receive()
		while true do
			-- redstone event has no other info
			os.pullEvent("redstone")
			if rs.testBundledInput(this.sides[1], this.busy) == true and not this.isSending then -- check if bus went on
				print("msg started")
				local msg = receiveMsg()
				-- Dbg.logE("received", Utils.PrintTableRecursive(msg))
				local decoded = decodeReceivedMsg(msg)
				if decoded ~= nil then
					print("received " .. tostring(#decoded))
					if #decoded % 4 ~= 0 then
						print("received message with invalid length")
					else
						msg = Utils.convertDibitArrayToByteArray(decoded)
						local msgString = Utils.convertByteArrayToString(msg)
						msg = textutils.unserialise(msgString)
						return msg
					end
				end
			end
		end
	end

	---sets outputs and relevant control signals for sending data
	---@param side string cable side
	---@param dibitInput table array of 7 dibits to send
	---@param clkVal boolean value of clock bit
	local function setOutputsControlCable(side, dibitInput, clkVal)
		local output = this.busy + dibitInput[1] + bit.blshift(dibitInput[2], 2) + bit.blshift(dibitInput[3], 4)
		output =
			output + bit.blshift(dibitInput[4], 6) + bit.blshift(dibitInput[5], 8) + bit.blshift(dibitInput[6], 10) +
			bit.blshift(dibitInput[7], 12)
		if clkVal == true then
			output = output + this.clk
		end
		rs.setBundledOutput(side, output)
	end

	---set outputs for cable without control signals
	---@param side string cable side
	---@param dibitInput table 8 dibits to set on cable
	local function setOutputs(side, dibitInput)
		local output =
			dibitInput[1] + bit.blshift(dibitInput[2], 2) + bit.blshift(dibitInput[3], 4) + bit.blshift(dibitInput[4], 6)
		output =
			output + bit.blshift(dibitInput[5], 8) + bit.blshift(dibitInput[6], 10) + bit.blshift(dibitInput[7], 12) +
			bit.blshift(dibitInput[8], 14)
		rs.setBundledOutput(side, output)
	end

	---drives hardware to send the message
	---@param msg table table to send
	function funcTable.transmit(msg)
		local dibits = Utils.convertStringToByteArray(textutils.serialize(msg, { compact = true }))
		dibits = Utils.convertByteArrayToDibitArray(dibits)

		local toSend = {}
		for _, v in ipairs(preAmble) do
			table.insert(toSend, v)
		end
		for _, v in ipairs(dibits) do
			table.insert(toSend, v)
		end
		for _, v in ipairs(postAmble) do
			table.insert(toSend, v)
		end
		print("msg length " .. tostring(#dibits))
		print("unpadded length " .. tostring(#toSend))
		local cablesAvailable = #this.sides - 1
		local toAdd = ((cablesAvailable * 8) + 7) - (#toSend % ((cablesAvailable * 8) + 7))
		for _ = 1, toAdd do
			table.insert(toSend, 0)
		end
		print("padded length " .. tostring(#toSend))
		-- wait for bus to free up
		while rs.testBundledInput(this.sides[1], this.busy) == true do
			-- wait here
			print("waiting for cable to free up")
			sleep(0.5)
		end

		this.isSending = true
		-- bus is free take it and start sending bytes
		rs.setBundledOutput(this.sides[1], this.busy)
		-- print("starting to send")
		local clkVal = true
		for i = 1, #toSend, (cablesAvailable * 8) + 7 do
			local dibitArr = {
				toSend[i],
				toSend[i + 1],
				toSend[i + 2],
				toSend[i + 3],
				toSend[i + 4],
				toSend[i + 5],
				toSend[i + 6]
			}

			-- set non control cable outputs
			local pos = i + 7
			for cables = 2, cablesAvailable + 1, 1 do
				local outputDibitArr = {
					toSend[pos],
					toSend[pos + 1],
					toSend[pos + 2],
					toSend[pos + 3],
					toSend[pos + 4],
					toSend[pos + 5],
					toSend[pos + 6],
					toSend[pos + 7]
				}
				pos = pos + 8
				setOutputs(this.sides[cables], outputDibitArr)
			end

			-- set control output cable last to make sure it works should ticks not align properly for redstone
			setOutputsControlCable(this.sides[1], dibitArr, clkVal)
			clkVal = not clkVal
			Utils.Yield()
			sleep(0)
		end

		for _, v in pairs(this.sides) do
			rs.setBundledOutput(v, 0)
		end
		sleep(0.1)
		this.isSending = false
	end

	return funcTable
end

return Redworking
