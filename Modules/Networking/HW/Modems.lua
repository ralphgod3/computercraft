---@diagnostic disable: redundant-parameter
local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")

local networkingHardware = {}
local TAG = "HWM"

Dbg.setLogLevel(TAG, Dbg.Levels.Warning)

---create new networkingHardware for modem
---@param modemChannel number | nil modem channel to use, if nil uses default channel of 65532
---@param modemsToUse table | nil wrapped modems to use packaged in a table ( as returned from {peripheral.find("modem")}), if nil wraps all modems
---@return InetworkingHardware networkingHardwareModem modem hardware object for use with networking class
function networkingHardware.new(modemChannel, modemsToUse)
	PC.expect(1, modemChannel, "number", "nil")
	PC.expect(2, modemsToUse, "table", "nil")

	Dbg.logV(TAG, "created new modem hardware")
	-- private variables for networking stack
	local this = {
		modemNames = {},
		modems = { peripheral.find("modem") },
		modemChannel = modemChannel or 65532
	}
	if modemsToUse ~= nil then
		this.modems = modemsToUse
		for _, mod in pairs(modemsToUse) do
			table.insert(this.modemNames, peripheral.getName(mod))
		end
	else
		for _, mod in pairs(this.modems) do
			table.insert(this.modemNames, peripheral.getName(mod))
		end
	end


	local funcTable = {}

	---open channel on all modems
	---@param channel number channel to open
	local function openChannelOnModems(channel)
		Dbg.logV(TAG, "opening channel", channel)
		for _, v in pairs(this.modems) do
			v.open(channel)
		end
	end

	---close channel on all connected modems
	---@param channel number channel to close
	local function closeChannelOnModems(channel)
		Dbg.logV(TAG, "closing channel", channel)
		for _, v in pairs(this.modems) do
			v.close(channel)
		end
	end

	---send a message over all connected modems on the specified msg to channel
	---@param msg table msg being send
	function funcTable.transmit(msg)
		Dbg.logI(TAG, "transmitting message")
		for _, v in pairs(this.modems) do
			v.transmit(this.modemChannel, this.modemChannel, msg)
		end
	end

	---receive a message, blocking
	---@return any message message that was received
	function funcTable.receive()
		Dbg.logV(TAG, "waiting for message")
		local eventInfo = {}
		while type(eventInfo[5]) ~= "table" or not Utils.findElementInTable(this.modemNames, eventInfo[2]) do
			eventInfo = { os.pullEvent("modem_message") }
			Dbg.logV(TAG, "received msg")
		end
		Dbg.logI(TAG, "received message for us")
		return eventInfo[5]
	end

	openChannelOnModems(this.modemChannel)
	return funcTable
end

return networkingHardware
