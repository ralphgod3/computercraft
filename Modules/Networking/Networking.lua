local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Networking/InetworkingHardware.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Itimer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Networking/Inetworking.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local Dbg = require("Modules.Logger")
local Inetworking = require("Interfaces.Modules.Networking.Inetworking")
local InetworkingHardware = require("Interfaces.Modules.Networking.InetworkingHardware")
local Ithreads = require("Interfaces.Modules.Ithreads")
local Itimer = require("Interfaces.Modules.Itimer")
local TAG = "NET"
Dbg.setLogLevel(TAG, Dbg.Levels.Warning)

---create new networking stack
---@param threadingManager Ithreads implementation of IThreads.lua interface
---@param timerManager Itimer implementation of Itimer.lua interface
---@param networkingHardware InetworkingHardware implementation of InetworkingHardware interface
---@param broadcastChannel number | nil broadcast channel to use
---@param retransmitTime number | nil retransmit time for messages, defaults to 1 second
---@param maxRetransmits number | nil max amount of retransmits, defaults to 10
---@param bufferSize number | nil buffer size for received messages, defaults to 100
---@return Networking NetworkingInstance instance of networking stack
local function new(threadingManager, timerManager, networkingHardware, broadcastChannel, retransmitTime, maxRetransmits,
				   bufferSize)
	PC.expectInterface(1, Ithreads, threadingManager)
	PC.expectInterface(2, Itimer, timerManager)
	PC.expectInterface(3, InetworkingHardware, networkingHardware)
	PC.expect(4, broadcastChannel, "number", "nil")
	PC.expect(5, retransmitTime, "number", "nil")
	PC.expect(6, maxRetransmits, "number", "nil")
	Dbg.logI(TAG, "created")

	---@class NetworkingWaitingEntry
	---@field id number
	---@field channel number | nil
	---@field msgTypes MessageTypes[] | nil
	---@field responseTo number | nil
	---@field type string

	---@class Networking : Inetworking
	local networking = {
		broadcastChannel = broadcastChannel or 65532,
		retransmitTime = retransmitTime or 1,
		maxRetransmits = maxRetransmits or 10,
		bufferSize = bufferSize or 100,
		QOS = Inetworking.QOS
	}
	local this = {
		customMsgReceivedEvent = tostring(Utils.generateId()),
		computerId = os.getComputerID(),
		---@type Imessage[]
		retransmitBuffer = {},
		---@type ILLmessage[]
		receiveBuffer = {},
		timerId = 0,
		msgId = Utils.generateId(),
		threadId = nil,
		---@type NetworkingWaitingEntry[]
		waitingBuffer = {},
	}

	---removes an entry from waiting buffer with matching id
	---@param id number buffer.id field
	---@return boolean entryRemoved
	local function removeEntryFromWaitingBuffer(id)
		for k, v in pairs(this.waitingBuffer) do
			if v.id == id then
				table.remove(this.waitingBuffer, k)
				return true
			end
		end
		return false
	end

	---checks if anything in the waitingbuffer would match the current message
	---@param message ILLmessage
	---@return boolean isWaiting
	local function isOtherThreadWaitingForMatchingMessage(message)
		Dbg.logV(TAG, "waitingBuffer ", this.waitingBuffer)
		for _, filterEntry in pairs(this.waitingBuffer) do
			if filterEntry.type == "receiveMsgTypes" then
				if filterEntry.msgTypes ~= nil and filterEntry.channel ~= nil then
					if message.payload.type ~= nil and Utils.findElementInTable(filterEntry.msgTypes, message.payload.type) ~= nil and message.to == filterEntry.channel then
						Dbg.logV(TAG,
							"isOtherThreadWaitingForMatchingMessage true receiveMsgTypes channel and msgTypes filter")
						return true
					end
				else
					if message.payload.type ~= nil and Utils.findElementInTable(filterEntry.msgTypes, message.payload.type) ~= nil then
						Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage true receiveMsgTypes msgTypes filter")
						return true
					end
				end
			elseif filterEntry.type == "receive" then
				if filterEntry.responseTo ~= nil and filterEntry.channel ~= nil then
					if message.responseTo ~= nil and message.responseTo == filterEntry.responseTo and message.responseTo == filterEntry.channel then
						Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage true receive responseTo and channel filter")
						return true
					end
				elseif filterEntry.channel ~= nil then
					if message.to == filterEntry.channel then
						Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage true receive channel filter")
						return true
					end
				elseif filterEntry.responseTo ~= nil then
					if message.responseTo ~= nil and message.responseTo == filterEntry.responseTo then
						Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage true receive responseTo filter")
						return true
					end
				else
					Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage true receive no filters")
					return true
				end
			else
				Dbg.logE(TAG, "invalid filterType in waiting buffer", filterEntry)
			end
		end
		Dbg.logV(TAG, "isOtherThreadWaitingForMatchingMessage false")
		return false
	end


	---create a message to be send out
	---@param to number channel of the message will be sent too
	---@param qos number quality of service for the message
	---@param payload string | boolean | table | number message payload
	---@return ILLFullMsg message message structure that is used by other internal functions to send the message
	local function createMessage(to, qos, payload)
		Dbg.logV(TAG, "creating message to", to)
		this.msgId = Utils.getNextId(this.msgId)
		---@class ILLFullMsg
		local message = {
			--variables needed for this sie
			lastSent = os.clock(), -- last time a message was sent
			retries = 0,  -- amount of times the message has been sent
			--fields send to other client
			---@type ILLmessage
			msg = {
				llType = Inetworking.MsgTypes.message, -- signifies it is a message being transmitted
				llId = this.msgId,         -- id of the message being sent
				to = to,                   -- pc the message will be sent too
				from = this.computerId,    -- pc the message is from
				QOS = qos,                 --quality of service for message
				-- disabled since message types are not known in this file so we only know we need a type.
				---@diagnostic disable-next-line: missing-fields
				payload = {}, --payload of the message
			}
		}
		if type(payload) == "table" then
			message.msg.payload = Utils.copyTable(payload)
		else
			message.msg.payload = payload
		end
		return message
	end

	---create ack for msg that needs to be acked
	---@param messageToAck table msg to create ack for
	---@return ILLAck Ack ack for msg
	local function createAck(messageToAck)
		Dbg.logV(TAG, "creating ack for", messageToAck.llId)
		this.msgId = Utils.getNextId(this.msgId)
		---@class ILLAck
		local ack = {
			--low level identifiers
			lastSent = 0,              -- last time a message was sent
			retries = 0,               -- amount of times the message has been sent
			msg = {
				llType = Inetworking.MsgTypes.ack, -- signifies it is a ack being transmitted
				llId = this.msgId,     -- message id of the ack
				to = messageToAck.from, -- pc the message will be sent too
				from = this.computerId, -- pc the message is from
				QOS = messageToAck.QOS, -- Quality of service of the message being acked
				idAck = messageToAck.llId -- message id of the message being acked
			}
		}
		return ack
	end

	---create a synAck message for a received ack
	---@param messageToSynAck table msg that needs to be synAckd
	---@return ILLSynAck SynAck synack msg
	local function createSynAck(messageToSynAck)
		Dbg.logV(TAG, "creating synAck for", messageToSynAck.llId)
		this.msgId = Utils.getNextId(this.msgId)
		---@class ILLSynAck
		local synAck = {
			--low level identifiers
			msg = {
				llType = Inetworking.MsgTypes.synAck, -- signifies it is a synAck being transmitted
				llId = this.msgId,        -- id of the message being sent
				to = messageToSynAck.from, -- pc the message will be sent too
				from = this.computerId,   -- pc the message is from
				idAck = messageToSynAck.llId -- message id of the message being acked
			}
		}
		return synAck
	end

	---handles received Ack messages
	---@param msg table msg received
	local function handleReceivedAck(msg)
		if msg.QOS == networking.QOS.EXACTLY_ONCE then
			Dbg.logV(TAG, "handling received ack, EXACTLY_ONCE")
			for k, v in pairs(this.retransmitBuffer) do
				if v.msg.llType == Inetworking.MsgTypes.message and v.msg.llId == msg.idAck and v.msg.to == msg.from then
					table.remove(this.retransmitBuffer, k)
					Dbg.logV(TAG, "removed msg from retransmit buffer")
					break
				end
			end
			local synAck = createSynAck(msg)
			networkingHardware.transmit(synAck.msg)
		elseif msg.QOS == networking.QOS.ATLEAST_ONCE then
			Dbg.logV(TAG, "handling received ack, ATLEAST_ONCE")
			for k, v in pairs(this.retransmitBuffer) do
				if v.msg.llType == Inetworking.MsgTypes.message and v.msg.llId == msg.idAck and v.msg.to == msg.from then
					table.remove(this.retransmitBuffer, k)
					Dbg.logV(TAG, "removed msg from retransmit buffer")
					break
				end
			end
		end
	end

	---handles received SynAck messages
	---@param msg table msg received
	local function handleReceivedSynAck(msg)
		Dbg.logV(TAG, "handling received synack")
		for k, v in pairs(this.retransmitBuffer) do
			if v.msg.llType == Inetworking.MsgTypes.ack and v.msg.llId == msg.idAck and v.msg.to == msg.from then
				table.remove(this.retransmitBuffer, k)
				Dbg.logV(TAG, "removed msg from retransmit buffer")
				-- removed message from buffer
				return
			end
		end
	end

	---handles received messages
	---@param msg ILLmessage msg received
	local function handleReceivedMsg(msg)
		Dbg.logV(TAG, "handling received message")
		--todo: filter duplicate messages received on multiple modems
		if msg.QOS == networking.QOS.ATMOST_ONCE then
			table.insert(this.receiveBuffer, msg)
			os.queueEvent(this.customMsgReceivedEvent)
		elseif msg.QOS == networking.QOS.ATLEAST_ONCE then
			local ack = createAck(msg)
			networkingHardware.transmit(ack.msg)
			for _, v in pairs(this.receiveBuffer) do
				if msg.llId == v.llId and msg.from == v.from then return end -- message already in buffer
			end

			table.insert(this.receiveBuffer, msg)
			os.queueEvent(this.customMsgReceivedEvent)
		elseif msg.QOS == networking.QOS.EXACTLY_ONCE then
			for _, v in pairs(this.retransmitBuffer) do
				if v.msg.llType == Inetworking.MsgTypes.ack and v.msg.idAck == msg.llId and v.msg.from == msg.from then return end -- message is already being handled
			end

			local ack = createAck(msg)
			table.insert(this.retransmitBuffer, ack)
			networkingHardware.transmit(ack.msg)

			for _, v in pairs(this.receiveBuffer) do
				if msg.llId == v.llId and msg.from == v.from then return end -- message already exists in buffer
			end

			table.insert(this.receiveBuffer, msg)
			os.queueEvent(this.customMsgReceivedEvent)
		end
		Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
		if #this.receiveBuffer > networking.bufferSize then
			table.remove(this.receiveBuffer, 1)
		end
	end

	---handles received messages, used to push them to a different coroutine so we dont miss receiving any messages
	---@param receivedMsg ILLmessage msg
	local function handleRMsgOffThread(receivedMsg)
		Dbg.logV(TAG, "checking message type before handling")
		if receivedMsg and receivedMsg.to and (receivedMsg.to == this.computerId or receivedMsg.to == networking.broadcastChannel) then
			if receivedMsg.llType == Inetworking.MsgTypes.message then
				handleReceivedMsg(receivedMsg)
			elseif receivedMsg.llType == Inetworking.MsgTypes.ack then
				handleReceivedAck(receivedMsg)
			elseif receivedMsg.llType == Inetworking.MsgTypes.synAck then
				handleReceivedSynAck(receivedMsg)
			else
				Dbg.logW(TAG, "unknown msg type received", receivedMsg)
			end
		end
	end
	---handles messages being received and retransmitted, blocking is set up in init, to activate receiving use Threads.startRunning
	local function recvCoroutine()
		while true do
			Dbg.logV(TAG, "waiting for msg")
			local receivedMsg = networkingHardware.receive()
			threadingManager.create(handleRMsgOffThread, receivedMsg)
		end
	end

	--todo: fix this function
	local function retransmitTimer()
		Dbg.logV(TAG, "retransmitting, curRetransmitSize", #this.retransmitBuffer)
		local markedForRemoval = {}
		for k, v in ipairs(this.retransmitBuffer) do
			if v.lastSent + networking.retransmitTime < os.clock() then
				networkingHardware.transmit(v.msg)
				v.retries = v.retries + 1
				v.lastSent = os.clock()
				if v.retries > networking.maxRetransmits then
					table.insert(markedForRemoval, k)
				end
			end
		end
		--remove messages with too many retransmits
		for i = #markedForRemoval, 1, -1 do
			table.remove(this.retransmitBuffer, markedForRemoval[i])
		end
		if #this.retransmitBuffer > 0 then
			this.timerId = timerManager.add(this.retransmitBuffer[1].lastSent + networking.retransmitTime,
				retransmitTimer)
		else
			--mark timerId as 0 so we know to start a new timer when we send a message with QOS above ATMOST_ONCE
			this.timerId = 0
		end
		Dbg.logV(TAG, "retransmitting, size after retransmit", #this.retransmitBuffer)
	end

	---transmit a message to other computer
	---@param to number channel the receiver is listening on (its id or the broadcast channel)
	---@param QOS InetworkingQOS quality of service for the message class.QOS value
	---@param payload Imessage data being transmitted
	---@param responseTo number | nil optional field that can be set to indicate this message is a response to a received message, allows the receiver to filter incoming messages
	---@return number msgId of the message being sent
	function networking.transmit(to, QOS, payload, responseTo)
		PC.expect(1, to, "number")
		PC.expect(2, QOS, "number")
		PC.expect(3, payload, "table", "string", "number", "boolean")
		PC.expect(4, responseTo, "number", "nil")

		Dbg.logI(TAG, "transmitting message")
		if to == networking.broadcastChannel then
			QOS = networking.QOS.ATMOST_ONCE
		end
		local msg = createMessage(to, QOS, payload)
		msg.msg.responseTo = responseTo
		if QOS ~= networking.QOS.ATMOST_ONCE then
			table.insert(this.retransmitBuffer, msg)
			if this.timerId == 0 then
				this.timerId = timerManager.add(os.clock() + networking.retransmitTime, retransmitTimer)
			end
		end
		networkingHardware.transmit(msg.msg)
		return msg.msg.llId
	end

	---receive a specific msgtype as defined in Modules/Networking/Messages.lua
	---@param timeout number | nil timeout before function returns, nil is no timeout
	---@param channel number | nil channel to listen on, nil listens on all channels
	---@param msgTypes MessageTypes[] table of message types as defined in the Messages.lua file
	---@return ILLmessage | nil msg msg with metadata or nil when no msg that fits filters is received
	function networking.receiveMsgTypes(timeout, channel, msgTypes)
		PC.expect(1, timeout, "number", "nil")
		PC.expect(2, channel, "number", "nil")
		PC.expect(3, msgTypes, "table")
		Dbg.logV(TAG, "receive msg types")
		local lastCheck = false
		local timerId = 0

		--add ourselves to waiting
		---@type NetworkingWaitingEntry
		local bufferEntry = {
			id = Utils.generateId(),
			channel = channel,
			msgTypes = msgTypes,
			type = "receiveMsgTypes"
		}
		table.insert(this.waitingBuffer, bufferEntry)

		if timeout ~= nil then
			timerId = os.startTimer(timeout)
		end
		while true do
			if msgTypes ~= nil and channel ~= nil then
				for k, v in pairs(this.receiveBuffer) do
					if v.payload.type ~= nil and Utils.findElementInTable(msgTypes, v.payload.type) ~= nil and v.to == channel then
						os.cancelTimer(timerId)
						removeEntryFromWaitingBuffer(bufferEntry.id)
						local tempCpy = Utils.copyTable(this.receiveBuffer[k])
						if not isOtherThreadWaitingForMatchingMessage(v) then
							table.remove(this.receiveBuffer, k)
						end
						Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
						Utils.yield()
						return tempCpy
					end
				end
			else
				for k, v in pairs(this.receiveBuffer) do
					if v.payload.type ~= nil and Utils.findElementInTable(msgTypes, v.payload.type) ~= nil then
						os.cancelTimer(timerId)
						removeEntryFromWaitingBuffer(bufferEntry.id)
						local tempCpy = Utils.copyTable(this.receiveBuffer[k])
						if not isOtherThreadWaitingForMatchingMessage(v) then
							table.remove(this.receiveBuffer, k)
						end
						Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
						Utils.yield()
						return tempCpy
					end
				end
			end

			if lastCheck then
				removeEntryFromWaitingBuffer(bufferEntry.id)
				Utils.yield()
				return nil
			end
			--wait until a timer or msg received event and then recheck the buffer
			local event = ""
			local id = 0
			repeat
				event, id = os.pullEvent()
			until event == this.customMsgReceivedEvent or (event == "timer" and id == timerId)
			if event == "timer" then
				lastCheck = true
			end
		end
	end

	---Receive a message
	---@param timeout number | nil if nil will wait indefinently otherwise waits for timeout for message to come in
	---@param channel number | nil when a number is given only messages received on this channel will be returned (useful for broadcasts)
	---@param responseTo number | nil allows for precise message filtering when a pc sends a message back as a response to a msg this pc send then the responseTo field will be set to the message id it is responding too
	---@return ILLmessage | nil msg msg with metadata or nil when no msg that fits filters {llId (internal id), to (this pc or broadcast channel), from (sender), QOS (quality of service for message), payload (data that is being sent)}
	function networking.receive(timeout, channel, responseTo)
		PC.expect(1, timeout, "number", "nil")
		PC.expect(2, channel, "number", "nil")
		PC.expect(3, responseTo, "number", "nil")
		Dbg.logV(TAG, "receive msg")
		local lastCheck = false
		local timerId = 0
		if timeout ~= nil then
			timerId = os.startTimer(timeout)
		end

		--add ourselves to waiting
		---@type NetworkingWaitingEntry
		local bufferEntry = {
			id = Utils.generateId(),
			channel = channel,
			responseTo = responseTo,
			type = "receive"
		}
		table.insert(this.waitingBuffer, bufferEntry)

		while true do
			if responseTo ~= nil and channel ~= nil then
				for k, v in pairs(this.receiveBuffer) do
					if v.responseTo ~= nil and v.responseTo == responseTo and v.to == channel then
						os.cancelTimer(timerId)
						local tempCpy = Utils.copyTable(this.receiveBuffer[k])
						removeEntryFromWaitingBuffer(bufferEntry.id)
						if not isOtherThreadWaitingForMatchingMessage(v) then
							table.remove(this.receiveBuffer, k)
						end
						Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
						Utils.yield()
						return tempCpy
					end
				end
			elseif channel ~= nil then
				for k, v in pairs(this.receiveBuffer) do
					if v.to == channel then
						os.cancelTimer(timerId)
						local tempCpy = Utils.copyTable(this.receiveBuffer[k])
						removeEntryFromWaitingBuffer(bufferEntry.id)
						if not isOtherThreadWaitingForMatchingMessage(v) then
							table.remove(this.receiveBuffer, k)
						end
						Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
						Utils.yield()
						return tempCpy
					end
				end
			elseif responseTo ~= nil then
				for k, v in pairs(this.receiveBuffer) do
					if v.responseTo ~= nil and v.responseTo == responseTo then
						os.cancelTimer(timerId)
						local tempCpy = Utils.copyTable(this.receiveBuffer[k])
						removeEntryFromWaitingBuffer(bufferEntry.id)
						if not isOtherThreadWaitingForMatchingMessage(v) then
							table.remove(this.receiveBuffer, k)
						end
						Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
						Utils.yield()
						return tempCpy
					end
				end
			elseif #this.receiveBuffer > 0 then
				os.cancelTimer(timerId)
				local tempCpy = Utils.copyTable(this.receiveBuffer[1])
				removeEntryFromWaitingBuffer(bufferEntry.id)
				if not isOtherThreadWaitingForMatchingMessage(this.receiveBuffer[1]) then
					table.remove(this.receiveBuffer, 1)
				end
				Dbg.logV(TAG, "curRecBuf", #this.receiveBuffer)
				Utils.yield()
				return tempCpy
			end

			if lastCheck then
				removeEntryFromWaitingBuffer(bufferEntry.id)
				Utils.yield()
				return nil
			end
			--wait until a timer or msg received event and then recheck the buffer
			local event = ""
			local id = 0
			repeat
				event, id = os.pullEvent()
			until event == this.customMsgReceivedEvent or (event == "timer" and id == timerId)
			if event == "timer" then
				lastCheck = true
			end
		end
	end

	this.threadId = threadingManager.createAsBackground(recvCoroutine)
	return networking
end

return { new = new }
