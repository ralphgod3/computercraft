import json
from json.decoder import JSONDecoder
#{
# "lowLeveltype":3,
# "id":32,
# "tag":"Network",
# "openChannels":[65532,32],
# "world":"ComputerCraft 1.95.3 (Minecraft 1.16.5)"
# }
class Client:
    id = 0
    openChannels = {}
    websocket = ""
    def __init__(self, connection, msg):
        decoder = JSONDecoder()

        decoded = decoder.decode(msg)
        self.id = decoded["id"]
        self.openChannels = decoded["openChannels"]
        self.websocket = connection