#!/usr/bin/env python

# WS server example
from json.decoder import JSONDecoder
from typing import TYPE_CHECKING
import WebWorkingClasses
import asyncio
import websockets
import json

clients = {}

decoder = JSONDecoder()

#message types
#message = 0,
#ack = 1,
#syn_ack = 2,
#connect = 3,
ip = "192.168.0.14"
port = 25564

async def msgHandler(websocket, path):
    print("client connected on " + path)
    try:
        async for message in websocket:
            print("received msg")
            decodedMsg = decoder.decode(message)
            if decodedMsg["lowLeveltype"] == 3: #connect message
                client = WebWorkingClasses.Client(websocket, message)
                if not clients.get(path):
                    clients[path] = {}
                clients[path][str(client.id)] = client
            else:
                
                #send to clients listening to channel in same path
                for clientId in clients[path]:
                    for channel in clients[path][clientId].openChannels:
                        if decodedMsg["to"] == channel:
                            print("forwarding msg to " + path + " id " + str(decodedMsg["to"]))
                            await clients[path][clientId].websocket.send(message)

    except websockets.exceptions.ConnectionClosedError as e:
        print("client disconnected " + str(client.id))
    finally:
        for clientId in clients[path]:
            if clients[path][clientId].websocket == websocket:

                del clients[path][clientId]
                break



start_server = websockets.serve(msgHandler, ip, port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
