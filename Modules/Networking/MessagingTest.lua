---@diagnostic disable: redundant-parameter
local Messages = require("Modules.Networking.Messages")
local Networking = require("Modules.Networking.Networking")
local Threads = require("Modules.Threads")
local Timer = require("Modules.Timer")
local NetworkingHardware = require("Modules.Networking.HW.Modems")



local threads = Threads.new()
local timer = Timer.new(threads)
local networkingHardware = NetworkingHardware.new()
local networking = Networking.new(threads, timer, networkingHardware)

local function waitLoop()
	while true do
		local msg = networking.receive(nil, networking.broadcastChannel)

		if msg and msg.payload.type == Messages.messageTypes.WhoIs then
			print("received whoIs request responding")

			local ret = Messages.createWhoIsResponse({ Messages.remoteTypes.StorageTerminal }, "terminal")
			print("responding too " .. tostring(msg.llId))
			networking.transmit(msg.from, networking.QOS.EXACTLY_ONCE, ret, msg.llId)
		end
	end
end

local function SendWhoIs()
	while true do
		local event = { os.pullEvent("key") }

		if keys.getName(event[2]) == "enter" then
			print("sending who is")
			local msg = Messages.createWhoIs()
			local id = networking.transmit(networking.broadcastChannel, networking.QOS.ATMOST_ONCE, msg)
			print("sent id " .. id)
			local response = networking.receive(2, nil, id)
			print(response)
			while response do
				print("resp from " .. response.from .. " to " .. response.to .. " types " .. response.payload.types[1])
				response = networking.receive(2, nil, id)
			end

			print("no more responses received listening to enter again")
		end
	end
end

print("hit enter to start a whois request")
threads.create(waitLoop)
threads.create(SendWhoIs)
threads.startRunning()
