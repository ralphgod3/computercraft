local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")

local Messages = {
	--high level message types
	---@enum MessageTypes
	messageTypes =
	{
		--generic msg types
		WhoIs = 000,                          --- message type used for network discovery
		WhoIsResponse = 001,                  --- response to network discovery message
		WhoAmI = 002,                         --request for peripherals that need their own name (turtles) to move between inventories
		WhoAmIResponse = 003,                 -- response to whoami message
		FileRequest = 004,                    --request file from computer
		FileRequestResponse = 005,            --response to file request
		SaveFile = 006,                       --tell computer to save file
		reboot = 007,                         --ask remote computer to reboot
		--storagesystem messages
		ItemList = 100,                       -- list of available items
		ItemListRequest = 101,                -- request a list of available items
		CraftList = 102,                      -- request list of craftable items
		CraftListRequest = 103,               --list of craftable items
		CraftOrder = 104,                     --crafting order for crafting turtle
		CraftOrderCompleted = 105,            -- send from crafter to craftingController to tell controller crafting of items for CraftOrder has completed
		ItemRequest = 106,                    -- request for item
		ItemRequestResponse = 107,            -- response to itemRequest
		CraftRequest = 108,                   -- request craft of item
		CraftRequestResponse = 109,           --response to request of item
		GetInputInventory = 110,              -- request for input inventory
		GetInputInventoryResponse = 111,      -- response to inputInventory request
		ItemRedirect = 112,                   -- used to redirect items to another inventory from what they would normally end up in
		ItemRedirectUpdate = 113,             -- used to update CLCrafting of current item redirect status
		ItemRedirectCompleted = 114,          --indicates item redirect is completed
		CraftRequestCompleted = 115,          --indicates a crafting request has been completed
		CraftRequestItemsExtractedFromBuffer = 116, --indicates terminal is done extracting items from buffer inventory
		CraftRequestFailed = 117,             -- indicates a craft request failed
		CraftingProgress = 118,               --has info about all current running crafts
		--turtle messages
		InfoRequest = 200,                    -- request position and fuel level of a turtle
		InfoRequestResponse = 201,            --response to infoRequest with position and fuel level
		BlocksToSearchForList = 202,          --list of blocks that miner should try and find
		UnbreakableBlocksList = 203,          --list of unbreakable blocks
		--messages below trigger a taskDone message after they are performed as a response to original msg id
		EmergencyStop = 204,                  --tells the receiving computer to stop doing any movement or digging operations right now (useful for server restarts)
		GoTo = 205,                           -- go to location, canDestroyBlocks, DestroyBlockBlacklist
		DropItems = 206,                      -- tell turtle to drop all blocks in its inventory in specific direction
		Refuel = 207,                         -- tell turtle to refuel from direction up to a fuel level
		Drill = 208,                          --tell turtle to start drilling to Y level, uses BlocksToSearchForList as a list of blocks it will mine
		InventoryFull = 209,                  --indicates inventory is full
		TaskDone = 210,                       --indicate task turtle was asked to perform has been completed
		--pocket ae messagetypes
		RemoveItemFromInventory = 300,        -- PocketAE message to remove item from player inventory
		PlayerInventoryRequest = 301,         -- PocketAE message to get list of items in player inventory
		PlayerInventoryRequestResponse = 302, -- PocketAE response to PlayerInventoryRequest
	},
	--flags to give to whois message filters
	---@enum RemoteTypes
	remoteTypes = {
		None = "None",                         --- no type is assigned to computer (placeholder)
		StorageController = "StorageController", --- device is a storage controller
		StorageTerminal = "StorageTerminal",   --- device is a storage terminal
		CrafterController = "CrafterController", -- device is a controller for crafting requests
		StorageCrafter = "StorageCrafter",     --- device is a storage crafter (crafty turtle)
		MiningTurtleDriller = "MiningTurtleDriller", --device is mining turtle used for drilling program
		DrillerController = "DrillerController", --device is controller for miningDriller program
		PocketAE = "PocketAE",                 --decive is a pocket ae device
		PocketAEController = "PocketAEController", --device is a pocket ae controller
		Sorter = "Sorter",                     --device is a CL sorter
		Stocker = "Stocker",                   --device is a CL stocker
	},
	--direction enum for requests that require it
	direction = {
		Forward = 0,
		Up = 1,
		Down = 2,
	}
}

---@class Imessage
---@field type MessageTypes


----------------------------------- GENERIC MESSAGES ------------------------------------

---creates a who is message, used for network discovery, this message should be sent on a broadcast channel
---@param typeFilter RemoteTypes[] | string[] | nil types that should respond to this message types are defined in class.remoteTypes entering nil will make everyone respond
---@return WhoIsMessage message created message
function Messages.createWhoIs(typeFilter)
	PC.expect(1, typeFilter, "table", "nil")
	---@class WhoIsMessage : Imessage
	local msg = {
		type = Messages.messageTypes.WhoIs,
		types = typeFilter
	}
	return msg
end

---create a response to a who is message
---@param types RemoteTypes[] table of types that this computer is, types are defined in class.remoteTypes
---@param name string human readable name for computer
---@return WhoIsResponseMessage message created message
function Messages.createWhoIsResponse(types, name)
	PC.expect(1, types, "table")
	PC.expect(2, name, "string")
	---@class WhoIsResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.WhoIsResponse,
		types = types,
		name = name
	}
	return msg
end

---ask what name you have in the network
---@param visiblePeripherals table all peripherals you can wrap too, automaticly excludes the direction peripherals
---@return WhoAmIMessage message
function Messages.createWhoAmI(visiblePeripherals)
	PC.expect(1, visiblePeripherals, "table")
	local directionPeripherals =
	{
		"top",
		"bottom",
		"left",
		"right",
		"front",
		"back"
	}
	local visiblePeripherals = Utils.removeElementsFromTable(visiblePeripherals, directionPeripherals)
	visiblePeripherals = Utils.rebuildArray(visiblePeripherals)

	---@class WhoAmIMessage : Imessage
	local msg = {
		type = Messages.messageTypes.WhoAmI,
		peripherals = visiblePeripherals
	}
	return msg
end

---create response to whoami request
---@param name string remote peripheral name
---@return WhoAmIResponseMessage message
function Messages.createWhoAmIResponse(name)
	PC.expect(1, name, "string")
	---@class WhoAmIResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.WhoAmIResponse,
		name = name
	}
	return msg
end

---ask for a file from a remote computer
---@param fileToRequest string file to request including fullpath and extension
---@return FilterRequestMessage fileRequestMsg msg ready for transmission
function Messages.createFileRequest(fileToRequest)
	PC.expect(1, fileToRequest, "string")
	---@class FilterRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.FileRequest,
		fileToRequest = fileToRequest,
	}
	return msg
end

---response to file request that contains a statusCode and if statusCode == ok then fileContents contains the fileContents
---@param statusCode number Statuscode from Statuscodes.lua
---@param fileContents string | nil fileContents, message indicating failure or nil
---@return FilterRequestResponseMessage fileRequestResposne msg ready for transmission
function Messages.createFileRequestResponse(statusCode, fileContents)
	PC.expect(1, statusCode, "number")
	PC.expect(2, fileContents, "string", "nil")
	---@class FilterRequestResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.FileRequestResponse,
		statusCode = statusCode,
		fileContents = fileContents,
	}
	return msg
end

---create file save request
---@param filePath string path to save file into including extension
---@param fileContents string contents of the file to save
---@return SaveFileRequestMessage saveFileRequestMsg msg ready for transmission
function Messages.createSaveFileRequest(filePath, fileContents)
	PC.expect(1, filePath, "string")
	PC.expect(2, fileContents, "string")
	---@class SaveFileRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.saveFileRequest,
		filePath = filePath,
		fileContents = fileContents,
	}
	return msg
end

---tell remote computer to reboot
---@return RebootRequestMessage message message
function Messages.createRebootRequest()
	---@class RebootRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.reboot,
	}
	return msg
end

----------------------------------- STORAGE MESSAGES ------------------------------------

---create itemlist message
---@param Items table list of items
---@return ItemListMessage message message
function Messages.createItemList(Items)
	PC.expect(1, Items, "table")
	---@class ItemListMessage : Imessage
	local msg = {
		type = Messages.messageTypes.ItemList,
		Items = Items,
	}
	return msg
end

---create request for item list
---@return ItemListRequestMessage message request message
function Messages.createItemListRequest()
	---@class ItemListRequestMessage : Imessage
	local msg = { type = Messages.messageTypes.ItemListRequest }
	return msg
end

---create request to export item
---@param exportInfo table<string, ItemEntry[]> { invName1 = {{name(string), count(number), <optional>slot(number), <optional>damage(number), <optional>nbt(string)}}, inv2, etc }
---@return ItemRequestMessage message request message
function Messages.createItemRequest(exportInfo)
	PC.expect(1, exportInfo, "table")
	for inv, entries in pairs(exportInfo) do
		for _, info in pairs(entries) do
			PC.expect(1, inv, "string")
			PC.field(info, "name", "string")
			PC.field(info, "count", "number")
			PC.field(info, "damage", "number", "nil")
			PC.field(info, "nbt", "string", "nil")
			PC.field(info, "slot", "number", "nil")
		end
	end
	---@class ItemRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.ItemRequest,
		exportInfo = Utils.copyTable(exportInfo)
	}
	return msg
end

---create response to itemRequest
---@param statusCode StatusCode statuscode
---@param response table message
---@return ItemRequestResponseMessage message request message
function Messages.createItemRequestResponse(statusCode, response)
	---@class ItemRequestResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.ItemRequestResponse,
		statusCode = statusCode,
		response = response
	}
	return msg
end

---create request for storageController input inventory
---@return InputInventoryRequestMessage message
function Messages.createInputInventoryRequest()
	---@class InputInventoryRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.GetInputInventory,
	}
	return msg
end

---create inputInventory request response
---@param inputInventory string | string[] input inventor(y | ies)
---@return InputInventoryResponseMessage message
function Messages.createInputInventoryResponse(inputInventory)
	---@class InputInventoryResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.GetInputInventoryResponse,
		inputInventory = inputInventory
	}
	return msg
end

----------------------------------- CRAFTING MESSAGES ------------------------------------

---create craftable items list message
---@param Items table list of items
---@return CraftListMessage message message
function Messages.createCraftList(Items)
	PC.expect(1, Items, "table")
	---@class CraftListMessage : Imessage
	local msg = {
		type = Messages.messageTypes.CraftList,
		Items = Items,
	}
	return msg
end

---create request for craftable item list
---@return CraftListRequestMessage message request message
function Messages.createCraftListRequest()
	---@class CraftListRequestMessage : Imessage
	local msg = { type = Messages.messageTypes.CraftListRequest }
	return msg
end

---create crafting order for turtle
---@return CraftOrderMessage craftOrder message
function Messages.createCraftOrder()
	---@class CraftOrderMessage : Imessage
	local msg = { type = Messages.messageTypes.CraftOrder }
	return msg
end

---create CraftOrderCompleted message
---@return CraftOrderCompletedMessage message
function Messages.createCraftOrderCompleted()
	---@class CraftOrderCompletedMessage : Imessage
	local msg = { type = Messages.messageTypes.CraftOrderCompleted }
	return msg
end

---create request for craft of item
---@param name string item name
---@param count number number of items to craft
---@param damage number | nil damage value for item to export, nil for dont care or no damage value on item
---@param slowMode number amount of time to delay actions by per operaiont or 0 if no delay
---@return CraftRequestMessage message request message
function Messages.createCraftRequest(name, count, nbt, damage, slowMode)
	PC.expect(1, name, "string")
	PC.expect(2, count, "number")
	PC.expect(3, nbt, "string", "nil")
	PC.expect(4, damage, "number", "nil")
	PC.expect(5, slowMode, "number")
	---@class CraftRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.CraftRequest,
		name = name,
		count = count,
		nbt = nbt,
		damage = damage,
		slowMode = slowMode,
	}
	return msg
end

---create response to CraftRequest
---@param statusCode StatusCodeCode statuscode
---@param msg string message
---@return CraftRequestResponseMessage message request message
function Messages.createCraftRequestResponse(statusCode, msg)
	---@class CraftRequestResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.CraftRequestResponse,
		statusCode = statusCode,
		msg = msg
	}
	return msg
end

---create a message indicating we have sucked the items we need from crafting buffer
---@param inventory string bufferInventory items where extracted from
---@return CraftRequestItemsExtractedFromBuffer
function Messages.createCraftRequestItemsExtractedFromBuffer(inventory)
	---@class CraftRequestItemsExtractedFromBuffer : Imessage
	local msg = {
		type = Messages.messageTypes.CraftRequestItemsExtractedFromBuffer,
		inventory = inventory,
	}
	return msg;
end

---create item redirect message
---@param inventory string inventory to redirect item into
---@param name string itemName to redirect
---@param count number item amount to redirect -1 for infinite
---@param nbt string | nil item nbt for item to redirect
---@param damage number | nil item damage for item to redirect
---@return ItemRedirectMessage itemRedirectMessage item redirect message
function Messages.createItemRedirect(inventory, name, count, nbt, damage)
	PC.expect(1, inventory, "string")
	PC.expect(2, name, "string")
	PC.expect(3, count, "number")
	PC.expect(4, nbt, "string", "nil")
	PC.expect(5, damage, "number", "nil")
	---@class ItemRedirectMessage : Imessage
	local msg =
	{
		type = Messages.messageTypes.ItemRedirect,
		inventory = inventory,
		name = name,
		count = count,
		nbt = nbt,
		damage = damage,
	}
	return msg
end

---create item redirect update message
---@param inventory string inventory to redirect item into
---@param name string itemName to redirect
---@param count number item amount to redirect -1 for infinite
---@param nbt string | nil item nbt for item to redirect
---@param damage number | nil item damage for item to redirect
---@param redirected number number of items that already got redirected
---@return ItemRedirectUpdateMessage itemRedirectMessage item redirect message
function Messages.createItemRedirectUpdate(inventory, name, count, nbt, damage, redirected)
	PC.expect(1, inventory, "string")
	PC.expect(2, name, "string")
	PC.expect(3, count, "number")
	PC.expect(4, nbt, "string", "nil")
	PC.expect(5, damage, "number", "nil")
	PC.expect(6, redirected, "number")
	---@class ItemRedirectUpdateMessage : Imessage
	local msg =
	{
		type = Messages.messageTypes.ItemRedirect,
		inventory = inventory,
		name = name,
		count = count,
		redirected = redirected,
		nbt = nbt,
		damage = damage,
	}
	return msg
end

---create item redirect complete message
---@param inventory string inventory that items got redirected into
---@param name string itemName of item that got redirected
---@param count number amount of items that got redirected
---@param nbt string | nil item nbt for item that got redirected
---@param damage number | nil item damage value for item that got redirected
---@return ItemRedirectCompletedMessage itemredirectCompleted item redirect completed message
function Messages.createItemRedirectCompleted(inventory, name, count, nbt, damage)
	PC.expect(1, inventory, "string")
	PC.expect(2, name, "string")
	PC.expect(3, count, "number")
	PC.expect(4, nbt, "string", "nil")
	PC.expect(5, damage, "number", "nil")
	---@class ItemRedirectCompletedMessage : Imessage
	local msg = {
		type = Messages.messageTypes.ItemRedirectCompleted,
		inventory = inventory,
		name = name,
		count = count,
		nbt = nbt,
		damage = damage,
	}
	return msg
end

---create craftRequest completed message
---@param requestId number craft request id
---@param craftingRequest ItemEntry original crafting request {string name, number count, <string nbt>, <number damage>}
---@param inventory string | nil name of inventory items can be collected from
---@return CraftRequestCompletedMessage message
function Messages.createCraftRequestCompleted(requestId, craftingRequest, inventory)
	PC.expect(1, requestId, "number")
	PC.expect(3, inventory, "string", "nil")
	---@class CraftRequestCompletedMessage : Imessage
	local msg = {
		type = Messages.messageTypes.CraftRequestCompleted,
		requestId = requestId,
		request = craftingRequest,
		inventory = inventory
	}
	return msg
end

---create craft request failed message
---@param requestId number
---@param craftingRequest ItemEntry original crafting request
---@param message string message indicating why it failed
---@return CraftRequestFailed message
function Messages.createCraftRequestFailed(requestId, craftingRequest, message)
	---@class CraftRequestFailed : Imessage
	local msg = {
		type = Messages.messageTypes.CraftRequestFailed,
		requestId = requestId,
		request = craftingRequest,
		message = message,
	}
	return msg
end

---create message containing info on current running crafts
---@param craftingProgressStatus CLCraftingStatus[]
---@return CraftingProgressMessage
function Messages.createCraftingProgressMessage(craftingProgressStatus)
	---@class CraftingProgressMessage : Imessage
	local msg = {
		type = Messages.messageTypes.CraftingProgress,
		status = craftingProgressStatus
	}
	return msg
end

-------------------------------- STORAGE RELAY MESSAGES ---------------------------------

---creates request to remove item from player inventory
---@param name string itemID
---@param count number count of items to remove
---@param damage number | nil damage value of item to remove or nil
---@return RemoveItemFromInventoryMessage msg
function Messages.createRemoveItemFromInventory(name, count, damage)
	PC.expect(1, name, "string")
	PC.expect(2, count, "number")
	PC.expect(3, damage, "number", "nil")
	---@class RemoveItemFromInventoryMessage : Imessage
	local msg = {
		type = Messages.messageTypes.RemoveItemFromInventory,
		name = name,
		count = count,
		damage = damage,
	}
	return msg
end

---create request for player inventory items
---@return PlayerInventoryRequestMessage message request message
function Messages.createPlayerInventoryRequest()
	---@class PlayerInventoryRequestMessage : Imessage
	local msg = { type = Messages.messageTypes.PlayerInventoryRequest }
	return msg
end

---create response to player inventory request
---@param Items table list of items
---@return PlayerInventoryRequestResponseMessage message message
function Messages.createPlayerInventoryRequestResponse(Items)
	PC.expect(1, Items, "table")
	---@class PlayerInventoryRequestResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.PlayerInventoryRequestResponse,
		Items = Items,
	}
	return msg
end

------------------------------ TURTLE MINER MESSAGES --------------------------

---ask receiver for position, facing and fuel level if aplicable
---@return InfoRequestMessage msg
function Messages.createInfoRequest()
	---@class InfoRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.InfoRequest
	}
	return msg
end

---response to info request
---@param position Position {x,y,z,(optional) f} position table
---@param fuelLevel number | nil fuel level if applicable
---@return InfoRequestResponseMessage msg
function Messages.createInfoRequestResponse(position, fuelLevel)
	PC.expect(1, position, "table")
	PC.field(position, "x", "number")
	PC.field(position, "y", "number")
	PC.field(position, "z", "number")
	PC.field(position, "f", "number", "nil")
	PC.expect(2, fuelLevel, "number", "nil")
	---@class InfoRequestResponseMessage : Imessage
	local msg = {
		type = Messages.messageTypes.InfoRequestResponse,
		position = Utils.copyTable(position),
		fuelLevel = fuelLevel
	}
	return msg
end

---tell receiver that these blocks are valuable and should be mined when found
---@param blockList table list of valuable blocks
---@return BlockListToSearchForMessage msg
function Messages.createBlocksToSearchForList(blockList)
	PC.expect(1, blockList, "table")
	---@class BlockListToSearchForMessage : Imessage
	local msg = {
		type = Messages.messageTypes.BlocksToSearchForList,
		blockList = Utils.copyTable(blockList)
	}
	return msg
end

---tell receiver that blocks in this list are unbreakable and receiver should thus stop digging if it encounters any of these
---@param blockList table
---@return UnbreakableBlockListMessage msg
function Messages.createUnbreakableBlockList(blockList)
	PC.expect(1, blockList, "table")
	---@class UnbreakableBlockListMessage : Imessage
	local msg = {
		type = Messages.messageTypes.UnbreakableBlocksList,
		blockList = Utils.copyTable(blockList)
	}
	return msg
end

---tell receiver to halt all operations that change any states (halt movement of items, fluids, and for turtles also itself)
---@return EmergencyStopMessage msg
function Messages.createEmergencyStop()
	---@class EmergencyStopMessage : Imessage
	local msg = {
		type = Messages.messageTypes.EmergencyStop
	}
	return msg
end

---tell receiver to travel to location specified
---@param position Position {x,y,z,f} position table
---@param canDestroy boolean indicates to receiver if blocks can be destroyed
---@param destroyBlacklist table | nil if canDestroy is true then this table contains item that still should not be destroyed
---@return GoToMessage msg
function Messages.createGoTo(position, canDestroy, destroyBlacklist)
	PC.expect(1, position, "table")
	PC.field(position, "x", "number")
	PC.field(position, "y", "number")
	PC.field(position, "z", "number")
	PC.field(position, "f", "number")
	PC.expect(2, canDestroy, "boolean", "nil")
	PC.expect(3, destroyBlacklist, "table", "nil")

	if canDestroy ~= nil and canDestroy == true then
		---@class GoToMessage : Imessage
		local msg = {
			type = Messages.messageTypes.GoTo,
			position = Utils.copyTable(position),
			canDestroy = true,
			destroyBlacklist = destroyBlacklist,
		}
		return msg
	end
	---@type GoToMessage
	local msg = {
		type = Messages.messageTypes.GoTo,
		position = Utils.copyTable(position),
		canDestroy = false,
		destroyBlacklist = nil,
	}
	return msg
end

---tell receiver to drop items in direction
---@param direction number Messages.direction enum
---@return DropItemRequestMessage msg
function Messages.createDropItemRequest(direction)
	PC.expect(1, direction, "number")
	---@class DropItemRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.DropItems,
		direction = direction,
	}
	return msg
end

---tell receiver to start refueling from a direction until minFuelAmount is reached
---@param direction number Messages.direction enum
---@param minFuelAmount number minimum amount of fuel needed before refueling is stopped
---@return RefuelRequestMessage msg
function Messages.createRefuelRequest(direction, minFuelAmount)
	PC.expect(1, direction, "number")
	PC.expect(2, minFuelAmount, "number")
	---@class RefuelRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.Refuel,
		direction = direction,
		minFuelAmount = minFuelAmount,
	}
	return msg
end

---tell receiver to start drilling from current Y level to designated y level
---@param depth number depth to drill from current height
---@return DrillRequestMessage msg
function Messages.createDrillRequest(depth)
	PC.expect(1, depth, "number")
	---@class DrillRequestMessage : Imessage
	local msg = {
		type = Messages.messageTypes.Drill,
		depth = depth
	}
	return msg
end

---indicate to receiver that inventory is full
---@return InventoryFullMessage msg
function Messages.createInventoryFullMsg()
	---@class InventoryFullMessage : Imessage
	local msg = {
		type = Messages.messageTypes.InventoryFull
	}
	return msg
end

---indicate to receiver that a task that was sent from him was completed
---@param completedTask any information about completed task
---@return TaskDoneMessage msg
function Messages.createTaskDone(completedTask)
	---@class TaskDoneMessage : Imessage
	local msg = {
		type = Messages.messageTypes.TaskDone,
		completedTask = completedTask
	}
	return msg
end

return Messages
