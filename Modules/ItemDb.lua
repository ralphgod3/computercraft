local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")

local DBItemfile = "/cache/itemDatabase/Items.json"
local DBTagfile = "/cache/itemDatabase/Tags.json"
local mcVersion, _ = Utils.getVersionInfo()
local remoteItemDb = "DataFiles/" .. mcVersion .. "/itemDatabase/Items.json"
local remoteTagDb = "DataFiles/" .. mcVersion .. "/itemDatabase/Tags.json"

local TAG = "IDB"
local ItemDb = {}
---initialize itemDb
---@param useRemote boolean? downloads remote database on startup if true, false only uses local files, defaults to true
---@return ItemDb instance itemDb instance
function ItemDb.new(useRemote)
	if useRemote == nil then
		useRemote = true
	end
	---@class ItemDb : IitemDb
	local funcTable = {}
	local this = {
		IsInitialized = false,
		DB = {Items = {}, Tags = {}},
		localItemDb = {Items = {}, Tags = {}}
	}

	---loads the itemDatabase if it is not loaded yet
	---only call externally when you need to be sure ItemDB is loaded before doing something
	---ex: db lookup on multiple coroutines
	local function loadDbIfNotLoaded()
		if this.IsInitialized then
			return
		end
		if useRemote then
			local file = nil
			--detects if we are running in a git repo and uses the local version if we are to save on http requests
			Dbg.logV(TAG, "initializing ItemDB from " .. remoteItemDb)
			if not fs.exists(remoteItemDb) then
				file = Git.downloadFileFromGit(Git.ComputerCraftProjectID, remoteItemDb)
			else
				local f = fs.open(remoteItemDb, "r")
				file = f.readAll()
				f.close()
			end
			this.DB.Items = {}
			if file then
				this.DB.Items = textutils.unserialiseJSON(file)
			end
			if not fs.exists(remoteTagDb) then
				file = Git.downloadFileFromGit(Git.ComputerCraftProjectID, remoteTagDb)
			else
				local f = fs.open(remoteTagDb, "r")
				file = f.readAll()
				f.close()
			end
			this.DB.Tags = {}
			if file then
				this.DB.Tags = textutils.unserialiseJSON(file)
			end
		end
        this.localItemDb = {}
		if fs.exists(DBItemfile) then
			local file = fs.open(DBItemfile, "r")
			this.localItemDb.Items = file.readAll()
			file.close()
			this.localItemDb.Items = textutils.unserialiseJSON(this.localItemDb.Items)
		end
        if fs.exists(DBTagfile) then
			local file = fs.open(DBTagfile, "r")
			this.localItemDb.Tags = file.readAll()
			file.close()
			this.localItemDb.Tags = textutils.unserialiseJSON(this.localItemDb.Tags)
		end
        if this.localItemDb.Items == nil then
            this.localItemDb.Items = {}
        end
        if this.localItemDb.Tags == nil then
            this.localItemDb.Tags = {}
        end
		this.IsInitialized = true
	end

	---get information of item with itemName
	---@param itemName string itemName ie: minecraft:cobblestone
	---@param nbt string? hexadecimal hash of nbt tag, optional if none given will try to find item without nbt tag
	---@return DetailedItemEntry | nil itemInformation table containing item information if it exists, nil otherwise
	function funcTable.getItem(itemName, nbt)
		PC.expect(1, itemName, "string")
		PC.expect(2, nbt, "string", "nil")
		if nbt and nbt ~= "None" then
			local item = funcTable.getItem(itemName)
			if item then
				if item.maxDamage ~= nil then
					return item
				end
			end
		end
		nbt = nbt or "None"
		if this.DB.Items[itemName] ~= nil and this.DB.Items[itemName][nbt] ~= nil then
			return this.DB.Items[itemName][nbt]
		end
		if this.localItemDb.Items[itemName] ~= nil and this.localItemDb.Items[itemName][nbt] ~= nil then
			return this.localItemDb.Items[itemName][nbt]
		end
		return nil
	end

	---returns item names for items with the specified tag
	---@param tag string tagname to search items for
	---@return string[] itemInformation items that have tag (empty table if no item was found)
	function funcTable.getItemsWithTag(tag)
		PC.expect(1, tag, "string")
		local outTable = {}
		if this.DB.Tags[tag] ~= nil then
			for _, v in pairs(this.DB.Tags[tag]) do
				table.insert(outTable, v)
			end
		end

		if this.localItemDb.Tags[tag] ~= nil then
			for _, v in pairs(this.localItemDb.Tags[tag]) do
				if Utils.findElementInTable(outTable, v) == nil then
					table.insert(outTable, v)
				end
			end
		end
		return outTable
	end

	---returns a full copy of all item information currently in the database
	---@return DetailedItemEntry[] itemList list of all items contained in the current item database
	function funcTable.dumpItems()
		local outTable = {}
		for k, v in pairs(this.DB.Items) do
			outTable[k] = v
		end
		for k, v in pairs(this.localItemDb.Items) do
			outTable[k] = v
		end
		return outTable
	end

	---returns a full copy of all tags currently in the database
	---@return string[] tagList list of all tags contained in the current tag database
	function funcTable.dumpTags()
		local outTable = Utils.copyTable(this.DB.Tags)
		for k, v in pairs(this.localItemDb.Tags) do
			if outTable[k] ~= nil and Utils.findElementInTable(outTable[k], v) == nil then
				table.insert(outTable[k], v)
			elseif outTable[k] == nil then
				outTable[k] = {}
				table.insert(outTable[k], v)
			end
		end
		return outTable
	end

	---create item information in database for item
	---@param itemDetail DetailedItemEntry item information that has to be added as returned by getItemDetail
	function funcTable.addItem(itemDetail)
		PC.expect(1, itemDetail, "table")
		PC.field(itemDetail, "maxCount", "number")
		PC.field(itemDetail, "name", "string")
		PC.field(itemDetail, "displayName", "string")
		PC.field(itemDetail, "maxDamage", "number", "nil")
		PC.field(itemDetail, "tags", "table")
		PC.field(itemDetail, "nbt", "string", "nil")
		--do not modify original input, copy instead
		local details = Utils.copyTable(itemDetail)
		details.nbt = details.nbt or "None"
		if details.maxDamage ~= nil then
			details.nbt = "None"
		end
		if funcTable.itemIsInDb(details.name, details.nbt) then
			return
		end

		if this.localItemDb.Items[details.name] == nil then
			this.localItemDb.Items[details.name] = {}
		end

		-- create empty entry for item
		this.localItemDb.Items[details.name][details.nbt] = {}
		this.localItemDb.Items[details.name][details.nbt].displayName = details.displayName
		this.localItemDb.Items[details.name][details.nbt].maxCount = details.maxCount
		this.localItemDb.Items[details.name][details.nbt].maxDamage = details.maxDamage
		this.localItemDb.Items[details.name][details.nbt].tags = Utils.copyTable(details.tags)
		-- create tag entry for item
		for k, _ in pairs(details.tags) do
			if this.localItemDb.Tags[k] == nil then
				this.localItemDb.Tags[k] = {}
			end
			if Utils.findElementInTable(this.localItemDb.Tags[k], details.name) == nil then
				table.insert(this.localItemDb.Tags[k], details.name)
			end
		end
		-- save added item into file
		local file = fs.open(DBItemfile, "w")
		file.write(textutils.serializeJSON(this.localItemDb.Items))
		file.close()
        local file = fs.open(DBTagfile, "w")
		file.write(textutils.serializeJSON(this.localItemDb.Tags))
		file.close()
	end

	---check if item exists in the database
	---@param itemName string itemName ie: minecraft:cobblestone
	---@return boolean itemInDb true item is in db, false item is not in db
	function funcTable.itemIsInDb(itemName, nbt)
		PC.expect(1, itemName, "string")
		PC.expect(2, nbt, "string", "nil")
		nbt = nbt or "None"
		--check for items with damage values to get around storing god knows how many tool values
		if
			this.DB.Items[itemName] ~= nil and this.DB.Items[itemName]["None"] ~= nil and
				this.DB.Items[itemName]["None"].maxDamage ~= nil
		 then
			return true
		end
		if
			this.localItemDb.Items[itemName] ~= nil and this.localItemDb.Items[itemName]["None"] ~= nil and
				this.localItemDb.Items[itemName]["None"].maxDamage ~= nil
		 then
			return true
		end

		if this.DB.Items[itemName] ~= nil and this.DB.Items[itemName][nbt] ~= nil then
			return true
		end
		if this.localItemDb.Items[itemName] ~= nil and this.localItemDb.Items[itemName][nbt] ~= nil then
			return true
		end
		return false
	end

	loadDbIfNotLoaded()
	return funcTable
end
return ItemDb
