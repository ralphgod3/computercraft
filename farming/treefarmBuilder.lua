-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
local Move = require("Modules.Move")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
--------------------------------------- config variables --------------------------------------
local minFuel = 100
local dirtAmountNeeded = 60
local groundName = "minecraft:dirt"

---ensure turtle has enough fuel (~3333) and blocks to build treefarm
local function VerifyEnoughFuel()
    --fuel check
    if turtle.getFuelLevel() < 3500 then
        print("not enough fuel insert items worth atleast " .. tostring(minFuel - turtle.getFuelLevel()) .. "fuel points")

        return false
    end
end

local function VerifyEnoughBlocks()
    while true do   --ask for blocks to build walls etc.
        print("Insert full inventory (or 1009 blocks), then press Y")
        local playerInput = read()
        if playerInput == "Y" or playerInput == "y" then
            local itemCount = 0
            for i = 1, 16, 1 do
                itemCount = itemCount + turtle.getItemCount(i)
            end
            if itemCount < 1009 then
                print("Not enough blocks: " .. itemCount .. " items")
            else
                print("Found enough blocks!")
                break
            end
        end
    end

    local dirtSlots = Utils.findItemsInInventory(groundName)
    if dirtSlots == nil then return false end
    local dirtSlot = 0
    local dirtAmount = 0

    for k, v in pairs(dirtSlots) do
        if dirtSlot == 0 then
            dirtSlot = v
        end

        turtle.select(v)
        dirtAmount = dirtAmount + turtle.getItemCount(v)
        turtle.transferTo(dirtSlot)
    end

    if dirtAmount < dirtAmountNeeded then
        print("not enough dirt in inventory need atleast 56 dirt to build farm")

        return false
    end

    return true
end

local function MoveToPosition(position)
    local responseCode = Move.goTo(position, true)

    while responseCode ~= StatusCode.Code.Ok do
        print("something went wrong whilst moving" .. StatusCode.toString(responseCode) .. ", retrying")
        sleep(1)
        responseCode = Move.goTo(position, true)
    end
end

local selectedSlot = 1

local function DoSlotChangeOnItemsTooLow(itemCountNeeded)
    while turtle.getItemCount() < itemCountNeeded do
        selectedSlot = selectedSlot + 1

        if selectedSlot > 16 then
            selectedSlot = 1
            print("reached slot 16 rescanning inventory in 5 seconds")
            sleep(5)
        end

        turtle.select(selectedSlot)
    end
end

local function DoCorner(position)
    MoveToPosition(position)
    Move.down(true)
    turtle.digDown()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeDown()

    for i = 1, 9 do
        Move.up(true)
    end

    turtle.digUp()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()
end

local function BuildOutsideCorners()
    print("placing blocks on outside corners of main farm")
    print("corner 1")
    DoCorner(Utils.createCoordinateSet(-9, 0, 0, 0))
    print("corner 2")
    DoCorner(Utils.createCoordinateSet(-9, 0, -16, 0))
    print("corner 3")
    DoCorner(Utils.createCoordinateSet(9, 0, -16, 0))
    print("corner 4")
    DoCorner(Utils.createCoordinateSet(9, 0, 0, 0))
    print("moving to home")
    Move.goTo(Utils.createCoordinateSet(0, 0, 0, 0), true)
end

local function mine()
    turtle.digUp()
    turtle.digDown()
    sleep(0.1)

    while turtle.detectUp() do
        sleep(0.2)
        turtle.digUp()
    end
end

local function mineLine(x)
    for i = 1, x do
        mine()

        if i ~= x - 1 then
            Move.forward(true)
        end
    end
end

local function handleCorner(sideToRotate, goForwards)
    if sideToRotate ~= "left" and sideToRotate ~= "right" then
        error("invalid argument", 1)

        return
    end

    if sideToRotate == "left" then
        Move.turnLeft()
    elseif sideToRotate == "right" then
        Move.turnRight()
    end

    mine()
    sleep(0.1)

    while turtle.detectUp() do
        turtle.digUp()
        sleep(0.2)
    end

    if goForwards == true then
        Move.forward(true)
    end

    mine()
    sleep(0.1)

    while turtle.detectUp() do
        turtle.digUp()
        sleep(0.2)
    end

    if sideToRotate == "left" then
        Move.turnLeft()
    elseif sideToRotate == "right" then
        Move.turnRight()
    end
end

local function mineLayer(x, z, direction)
    local otherDirection = ""

    if direction == "left" then
        otherDirection = "right"
    elseif direction == "right" then
        otherDirection = "left"
    end
    for i = 1, x do
        mineLine(z)
        local forward = false

        if i ~= x then
            forward = true
        end

        if i % 2 == 0 then
            handleCorner(otherDirection, forward)
        else
            handleCorner(direction, forward)
        end
    end
end

local function mineAll(x, y, z)
    local digDepthLeft = y
    local running = true
    local direction = ""
    local layer = 0

    while running == true do
        --check if this is last run
        if digDepthLeft - 1 < 1 then
            running = false
        end

        --set direction variable if x is even to get proper start position for turtle
        if layer % 2 == 0 then
            direction = "right"
        else
            direction = "left"
        end

        --mine layer
        mineLayer(x, z, direction)

        --move down for next layer
        for i = 1, 3 do
            digDepthLeft = digDepthLeft - 1
            Move.down(true)
            if digDepthLeft - 1 < 1 then break end
        end

        if x % 2 == 0 then
            print("x even increasing layer")
            layer = layer + 1
            print("layer: " .. tostring(x))
        end
    end
end

local function placeWallPiece()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeDown()
    DoSlotChangeOnItemsTooLow(1)
    turtle.place()
end

local function BuildWallLayer()
    Move.backward()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeDown()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()
    for _ = 1, 15 do
        Move.backward()
        placeWallPiece()
    end
    Move.turnRight()

    for _ = 1, 18 do
        Move.backward()
        placeWallPiece()
    end

    Move.turnRight()

    for _ = 1, 16 do
        Move.backward()
        placeWallPiece()
    end

    Move.turnRight()

    for _ = 1, 17 do
        Move.backward()
        placeWallPiece()
    end

    Move.backward()
    DoSlotChangeOnItemsTooLow(1)
    turtle.place()
end

local function MoveTurtleLayerUp(amountToMoveUp)
    for _ = 1, amountToMoveUp do
        DoSlotChangeOnItemsTooLow(1)
        turtle.placeDown()
        Move.up(true)
    end
end

local function BuildRoofLine()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()

    for _ = 1, 14 do
        DoSlotChangeOnItemsTooLow(1)
        turtle.placeUp()
        Move.forward(true)
    end
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()
end

local function ClearArea()
    Move.goTo(Utils.createCoordinateSet(-9,8,0,0),true)
    mineAll(19,9,17)
    Move.goTo(Utils.createCoordinateSet(0,0,0,0),true)
    --dig out water trench
    Move.goTo(Utils.createCoordinateSet(0,-3,0,0), true)
    for i = 1,8 do
        mine()
        Move.forward(true)
    end
    mine()
    Move.up(true)
    for _ = 1,7 do
        mine()
        Move.forward(true)
    end
    mine()
    Move.goTo(Utils.createCoordinateSet(0,0,0,0), true)
end

local function BuildRoof()
    for i = 1, 16 do
        BuildRoofLine()

        if i % 2 == 1 then
            Move.turnRight()
            Move.forward(true)
            Move.turnRight()
        else
            Move.turnLeft()
            Move.forward(true)
            Move.turnLeft()
        end
    end

    BuildRoofLine()
    Move.goTo(Utils.createCoordinateSet(-1,1,-1,2),true)
    Move.goTo(Utils.createCoordinateSet(0,0,0,0),true)
    Move.goTo(Utils.createCoordinateSet(0,-4,0,0),true)
    Move.goTo(Utils.createCoordinateSet(0,0,0,0),true)
end

local function BuildWallAndRoof()
    Move.goTo(Utils.createCoordinateSet(-9, 0, 0, 2), true)
    BuildWallLayer()
    Move.turnRight()
    MoveTurtleLayerUp(3)
    BuildWallLayer()
    Move.turnRight()
    MoveTurtleLayerUp(3)
    BuildWallLayer()
    Move.turnRight()
    MoveTurtleLayerUp(2)
    BuildWallLayer()
    DoSlotChangeOnItemsTooLow(1)
    turtle.place()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeUp()
    DoSlotChangeOnItemsTooLow(1)
    turtle.placeDown()
    Move.turnLeft()
    Move.forward(true)
    Move.turnRight()
    Move.forward(true)
    Move.turnLeft()
    Move.turnLeft()
    DoSlotChangeOnItemsTooLow(1)
    turtle.place()
    Move.turnRight()
    BuildRoof()
end

local function BuildSaplingGridRow()
    for i = 1, 12, 2 do
        --Todo: add inventory check
        turtle.placeDown()
        Move.forward(false)
        Move.forward(false)
    end
    turtle.placeDown()
end

local function BuildSaplingGrid(dirtSlot)
    turtle.select(dirtSlot)
    Move.forward(false)
    Move.goTo(Utils.createCoordinateSet(-7, 1, -2, 0), false)
    for i = 1, 7, 1 do  --build rows
        BuildSaplingGridRow()
        local nextCoords = Move.getPosition()
        nextCoords.x = nextCoords.x+2
        nextCoords.f = (nextCoords.f+2) % 4
        Move.goTo(nextCoords, false)
    end
    BuildSaplingGridRow()   -- build last row
    local nextCoords = Move.getPosition()
    nextCoords.y  = 3
    Move.goTo(nextCoords, false)
    Move.goTo(Utils.createCoordinateSet(0, 0, -1, 2), false)
    Move.forward(false) -- back to start
end

local function PlaceChests(chestSlot)
    turtle.select(chestSlot)
    Move.goTo(Utils.createCoordinateSet(0, -3, 0, 2), false)
    turtle.digUp()
    turtle.placeUp()
    turtle.dig()
    turtle.place()
end

--todo: add if statement to check if inventory contains everything
Move.setPersistantLocationSaving(false)
VerifyEnoughFuel()
print("would you like to mark outside corners for visualization before building Y/N")
local playerInput = read()
--mark outside corners for player
if playerInput == "Y" or playerInput == "y" then
    BuildOutsideCorners()
    print("area marked, would you like to build treefarm Y/N")
    playerInput = read()
    if playerInput ~= "Y" and playerInput ~= "y" then
        --exit out of program
        return
    end
end

VerifyEnoughBlocks()
ClearArea()
BuildWallAndRoof()
while true do   --ask for dirt and 3 chests
    print("Insert 56 dirt and 3 chests, then press Y")
    local playerInput = read()
    if playerInput == "Y" or playerInput == "y" then
        local chestSlot = Utils.findItemsInInventory("chest", 1)[1]
        local dirtSlot = Utils.findItemInInventory("minecraft:dirt", 1)
        if dirtSlot ~= nil and turtle.getItemCount(dirtSlot) >= 56 then
            if chestSlot ~= nil and turtle.getItemCount(chestSlot) >= 3 then
                BuildSaplingGrid(dirtSlot)
                PlaceChests(chestSlot)
                break
            end
        end
        print("Not enough items:" .. turtle.getItemCount(chestSlot) .. " chests, " .. turtle.getItemCount(dirtSlot) .. " dirt")
    end
end
print("Done! Place water then run treefarming program")