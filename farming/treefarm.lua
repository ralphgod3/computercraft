---@diagnostic disable: redundant-parameter
-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")

local Move = require("Modules.Move")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local ItemDb = require("Modules.ItemDb")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")

ItemDb = ItemDb.new()

--------------------------------------- config variables --------------------------------------
local cacheFile = "cache/treefarm.cache"

local function GoToTrench(config)
	PC.expect(1, config, "table")
	local x = 0
	local z = 0
	if config.homeLocation.f == 0 then
		x = 0
		z = 1
	elseif config.homeLocation.f == 1 then
		x = -1
		z = 0
	elseif config.homeLocation.f == 2 then
		z = -1
		x = 0
	elseif config.homeLocation.f == 3 then
		z = 0
		x = 1
	end

	Move.goTo(
		Utils.createCoordinateSet(config.homeLocation.x + x, config.homeLocation.y, config.homeLocation.z + z,
			config.homeLocation.f), true)
end

local function MoveOutOfHome(config)
	PC.expect(1, config, "table")
	local x = 0
	local z = 0
	if config.homeLocation.f == 0 then
		x = 0
		z = 1
	elseif config.homeLocation.f == 1 then
		x = -1
		z = 0
	elseif config.homeLocation.f == 2 then
		z = -1
		x = 0
	elseif config.homeLocation.f == 3 then
		z = 0
		x = 1
	end

	Move.goTo(
		Utils.createCoordinateSet(config.homeLocation.x + x, config.homeLocation.y + 5, config.homeLocation.z + z,
			(config.homeLocation.f + 2) % 4), true)
end

local function GoHome(config)
	PC.expect(1, config, "table")
	print("Going home")
	GoToTrench(config)
	Move.goTo(config.homeLocation, false)
end

local function Vacuum(config)
	PC.expect(1, config, "table")
	turtle.select(2)
	while turtle.suckDown() ~= false do
		local sap = turtle.getItemDetail(2)
		if sap and Utils.findElementInTable(config.saplingTypes, sap.name) ~= nil then
			turtle.transferTo(1)
			turtle.drop()
		end
		turtle.drop()
	end
end

local function FuelUp(config)
	PC.expect(1, config, "table")
	print("refueling")
	while turtle.getFuelLevel() < config.maxFuel do
		turtle.select(16)
		if not turtle.suckUp(4) then
			os.sleep(2)
		end
		if turtle.refuel() == false then
			turtle.drop()
		end
	end
	turtle.dropUp()
	turtle.drop()
end

local function VerifyEnoughFuel(config)
	--fuel check
	if turtle.getFuelLevel() < config.minFuel then
		GoHome(config)
		FuelUp(config)
	end
end

local function DropoffHarvest(config)
	PC.expect(1, config, "table")
	GoHome(config)

	local saplingSlots = Utils.findItemsInInventory(config.saplingTypes, 1)
	if saplingSlots ~= nil then
		for _, v in pairs(saplingSlots) do
			turtle.select(v)
			turtle.transferTo(1)
		end
	end

	for i = 2, 16 do
		if turtle.getItemCount(i) > 0 then
			turtle.select(i)
			while not turtle.drop() and turtle.getItemCount(i) > 0 do
				print("Dropoff chest full, waiting...")
				os.sleep(5)
			end
		end
	end
	turtle.select(1)
end

local function Plant(config)
	PC.expect(1, config, "table")
	local slot = Utils.findItemInInventory(config.saplingTypes, 1)
	if slot == nil then
		GoHome(config)
		error("no sapling in inventory")
	else
		turtle.select(slot)
	end
	turtle.placeDown()
end

local function BreakTree(config)
	PC.expect(1, config, "table")
	local suc, detectedBlock = turtle.inspectDown()
	if suc and detectedBlock ~= nil then
		if Utils.findElementInTable(config.logTypes, detectedBlock.name) ~= nil then
			turtle.digDown()
			local treeHeight = 0
			while suc and Utils.findElementInTable(config.logTypes, detectedBlock.name) ~= nil do
				turtle.digUp()
				Move.up(true)
				treeHeight = treeHeight + 1
				suc, detectedBlock = turtle.inspectUp()
			end
			for _ = 1, treeHeight do
				Move.down(true)
			end
			Plant(config)
		end
	else
		Plant(config)
	end

	while turtle.suckUp() or turtle.suckDown() do
		--do nothing
	end
end

local function Harvest(config)
	PC.expect(1, config, "table")
	MoveOutOfHome(config)
	for i = 1, #config.treeCoords do
		Move.goTo(config.treeCoords[i], true)
		local suc, ret = turtle.inspectDown()
		if suc and ret.name ~= nil and Utils.findElementInTable(config.saplingTypes, ret.name) == nil then
			BreakTree(config)
		else
			if suc == false then --air where sapling should be
				Plant(config)
			end
			if turtle.getItemCount(16) > 0 then
				print("Inventory almost full!")
				DropoffHarvest(config) --inventory (almost) full, go dropoff
			end
		end
	end
end

local function CreateCacheFile()
	if Move.recoverPosition() ~= StatusCode.Code.Ok then
		--recover or set position if needed
		print("enter the current x,y,z and facing")
		print("north = 0 east = 1, south = 2, west = 3")
		print("enter as x,y,z,f")
		local input = read()
		print(input)
		local seperated = Utils.splitString(input, ',')
		if seperated == nil or #seperated ~= 4 then
			error("invalid position entered")
		end
		Move.setPosition({
			x = tonumber(seperated[1]),
			y = tonumber(seperated[2]),
			z = tonumber(seperated[3]),
			f = tonumber(seperated[4])
		})
	else
		print("loaded location from cache, delete position file to enter location again")
	end

	local config = {}
	config.homeLocation = Move.getPosition()
	config.minFuel = 600
	config.maxFuel = 1500
	config.harvestInterval = 30
	config.saplingTypes = {}
	config.logTypes = {}
	--create entries for sapling and log types
	local tempTable = ItemDb.getItemsWithTag("minecraft:saplings")
	for _, v in pairs(tempTable) do
		table.insert(config.saplingTypes, v)
	end
	tempTable = ItemDb.getItemsWithTag("minecraft:logs")
	for _, v in pairs(tempTable) do
		table.insert(config.logTypes, v)
	end

	local contents = textutils.serialize(config)
	local file = fs.open(cacheFile, "w")
	file.write(contents)
	file.close()

	print("created config file " .. cacheFile)
	print("modify and restart program")
end

local function GenerateTreeCoords(config)
	local x = 0
	local z = 0
	if config.homeLocation.f == 0 then --north
		x = 7
		z = 2
	elseif config.homeLocation.f == 1 then --east
		x = -2
		z = 7
	elseif config.homeLocation.f == 2 then --south
		z = -2
		x = -7
	elseif config.homeLocation.f == 3 then --west
		z = -7
		x = 2
	end
	--calculate tree positions
	local CoordsOffset = { config.homeLocation.x + x, config.homeLocation.y + 5, config.homeLocation.z + z,
		config.homeLocation.f } --Change for GPS offsets, default: 7, 0, 2
	local treeCoords = {}
	--invert every 2nd row for more mining efficiency
	---@type number?
	local row = 0
	if config.homeLocation.f == 0 then
		for x = 0, 14, 2 do
			local rowCoords = {}
			for z = 0, 13, 2 do
				table.insert(rowCoords,
					Utils.createCoordinateSet(CoordsOffset[1] - x, CoordsOffset[2], z + CoordsOffset[3]))
			end
			if row % 2 == 0 then
				for i = 1, #rowCoords do
					table.insert(treeCoords, rowCoords[i])
					rowCoords[i].f = (CoordsOffset[4] + 2) % 4
				end
			else
				for i = #rowCoords, 1, -1 do
					rowCoords[i].f = CoordsOffset[4] % 4
					table.insert(treeCoords, rowCoords[i])
				end
			end
			row = row + 1
		end
		row = nil
	elseif config.homeLocation.f == 1 then
		for x = 0, 14, 2 do
			local rowCoords = {}
			for z = 0, 13, 2 do
				table.insert(rowCoords,
					Utils.createCoordinateSet(CoordsOffset[1] - z, CoordsOffset[2], x - CoordsOffset[3]))
			end
			if row % 2 == 0 then
				for i = 1, #rowCoords do
					table.insert(treeCoords, rowCoords[i])
					rowCoords[i].f = (CoordsOffset[4] + 2) % 4
				end
			else
				for i = #rowCoords, 1, -1 do
					rowCoords[i].f = CoordsOffset[4] % 4
					table.insert(treeCoords, rowCoords[i])
				end
			end
			row = row + 1
		end
		row = nil
	elseif config.homeLocation.f == 2 then
		for x = 0, -14, -2 do
			local rowCoords = {}
			for z = 0, -13, -2 do
				table.insert(rowCoords,
					Utils.createCoordinateSet(CoordsOffset[1] - x, CoordsOffset[2], z + CoordsOffset[3]))
			end
			if row % 2 == 0 then
				for i = 1, #rowCoords do
					table.insert(treeCoords, rowCoords[i])
					rowCoords[i].f = (CoordsOffset[4] + 2) % 4
				end
			else
				for i = #rowCoords, 1, -1 do
					rowCoords[i].f = CoordsOffset[4] % 4
					table.insert(treeCoords, rowCoords[i])
				end
			end
			row = row + 1
		end
		row = nil
	elseif config.homeLocation.f == 3 then
		for z = 0, 14, 2 do
			local rowCoords = {}
			for x = 0, 13, 2 do
				table.insert(rowCoords,
					Utils.createCoordinateSet(CoordsOffset[1] + x, CoordsOffset[2], z + CoordsOffset[3]))
			end
			if row % 2 == 0 then
				for i = 1, #rowCoords do
					table.insert(treeCoords, rowCoords[i])
					rowCoords[i].f = (CoordsOffset[4] + 2) % 4
				end
			else
				for i = #rowCoords, 1, -1 do
					rowCoords[i].f = CoordsOffset[4] % 4
					table.insert(treeCoords, rowCoords[i])
				end
			end
			row = row + 1
		end
		row = nil
	end
	return treeCoords
end

--------------------------------------------------- MAIN ---------------------------------------------
term.clear()
term.setCursorPos(1, 1)

if not fs.exists(cacheFile) then
	CreateCacheFile()
	return
end

--load cache file
local file = fs.open(cacheFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialise(config)
config.treeCoords = {}
config.treeCoords = GenerateTreeCoords(config)

--recover or set position if needed
if Move.recoverPosition() ~= StatusCode.Code.Ok then
	print("could not recover position please enter the current x,y,z and facing")
	print("north = 0 east = 1, south = 2, west = 3")
	print("enter as x,y,z,f")
	local input = read()
	print(input)
	local seperated = Utils.splitString(input, ',')
	if seperated == nil or #seperated ~= 4 then
		error("invalid position entered")
	end
	Move.setPosition({
		x = tonumber(seperated[1]),
		y = tonumber(seperated[2]),
		z = tonumber(seperated[3]),
		f = tonumber(seperated[4])
	})
end

--check inventory for saplings
if Utils.findItemInInventory(config.saplingTypes, 1) == nil then
	print("insert saplings in inventory")
	while true do
		os.pullEvent("turtle_inventory")
		if Utils.findItemInInventory(config.saplingTypes, 1) ~= nil then
			break
		end
	end
end

if Move.getPosition().y > config.treeCoords[1].y then
	--check if we where currently harvesting a tree
	local suc, ret = turtle.inspectUp()
	if not suc then
		Move.up(true)
		suc, ret = turtle.inspectUp()
	end
	--we are harvesting a tree complete it
	while suc and Utils.findElementInTable(config.logTypes, ret.name) ~= nil do
		turtle.digUp()
		Move.up(true)
		suc, ret = turtle.inspectUp()
	end

	local noDestroy = {}
	for _, v in pairs(config.saplingTypes) do
		table.insert(noDestroy, v)
	end
	table.insert(noDestroy, "minecraft:dirt")
	--move down to correct level
	while Move.getPosition().y > config.treeCoords[1].y do
		Move.down(true)
	end
	local suc, ret = turtle.inspectDown()
	if not suc or (suc and Utils.findElementInTable(config.saplingTypes, ret.name) == nil) then
		Plant()
	end
end

--start main loop
while true do
	GoHome(config)
	print("starting harvesting loop")
	VerifyEnoughFuel(config)
	turtle.select(1)
	Harvest(config)
	DropoffHarvest(config)
	Vacuum(config)
	sleep(config.harvestInterval)
end
