---@diagnostic disable: redundant-parameter
-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
	"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.GetFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.GetFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.GetFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.GetFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Items.lua")
local Move = require("Modules.Move")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local Items = require("Modules.Items")
--------------------------------------- config variables --------------------------------------
local maxFuel = 5000
local minFuel = 500
local harvestInterval = 500


local coordsOffset = { x = 0, y = 0, z = 0 } --gps coordinate override, default: {0, 0, 0}
local homeCoords = Utils.createCoordinateSet(0 + coordsOffset.x, 0 + coordsOffset.y, 0 + coordsOffset.z, 0)
local currPlotChestLocation = Utils.createCoordinateSet(4 + coordsOffset.x, 0 + coordsOffset.y, 4 + coordsOffset.z, 0)

--inits, recalculated on runtime
local coords = { 0, 0, 0 }
local displayName = ""
local cropName = ""
local seedName = ""
local maxGrowth = 7

local plots = {} --table of fields to farm on, expects: Down right corner coords, crop (which corresponds to CropData.json)

--NOTE: Some things like melons are special and have a stalk.
--The turtle still needs one seed in the chest in this case, which it will take out and put away on every harvest cycle

----------------- PLOT DEFINITIONS -----------------
plots[1] = {
	coords = homeCoords,         --todo: save data to cache file
	crop = "minecraft:wheat"
}

plots[2] = {
	coords = Utils.createCoordinateSet(-9 + coordsOffset.x, 0 + coordsOffset.y, 0 + coordsOffset.z, 0),
	crop = "minecraft:carrots"
}

plots[3] = {
	coords = Utils.createCoordinateSet(0, 0, 9, 0),
	crop = "minecraft:melon"
}

----------------- READ CropData.json -----------------

local fh = fs.open("DataFiles/CropData.json", "r")
local cropData = textutils.unserialiseJSON(fh.readAll())
fh.close()

----------------- ----------------- -----------------



local function GoHome()
	Move.goTo(homeCoords, false)
end

local function DropoffHarvest()
	local seedSlots = Utils.findItemsInInventory(seedName, 2) --shuffle seeds to slot 1
	if seedSlots ~= nil then
		for _, v in pairs(seedSlots) do
			turtle.select(v)
			turtle.transferTo(1)
		end
	end


	local seedSlot = Utils.findItemInInventory(seedName, 1) --leave 1 stack of seeds in field chest
	if seedSlot ~= nil then
		Move.goTo(currPlotChestLocation, false)
		turtle.select(seedSlot)
		turtle.dropDown()
	end

	GoHome()
	for i = 1, 16 do
		if turtle.getItemCount(i) > 0 then
			turtle.select(i)
			while not turtle.drop() and turtle.getItemCount(i) > 0 do
				print("Dropoff chest full, waiting...")
				os.sleep(5)
			end
		end
	end
	turtle.select(1)
end

local function GetSeeds()
	Move.goTo(currPlotChestLocation, false)
	while true do
		local suc, ret = turtle.suckDown()
		if not suc and ret == "No space for items" then
			print("Inv full, dropping off harvest")
			DropoffHarvest()
		else
			if not suc and ret == "No items to take" then
				if Utils.findItemInInventory(seedName, 1) ~= nil then --Check if we got <1 stack of seeds from suck (it throws no items to take anyway)
					return                                            --if so we don't need to beg for seeds
				end

				GoHome()
				print("Out of seeds for " .. displayName .. "\n  Pls gib " .. seedName)
				while true do
					os.pullEvent("turtle_inventory")
					if Utils.findItemInInventory(seedName, 1) ~= nil then
						return
					end
				end
			end
		end
	end
end

local function Plant()
	local slot = Utils.findItemInInventory(seedName, 1)
	if slot ~= nil then
		turtle.select(slot)
		turtle.placeDown()
	else
		print("Out of seeds!")
		local retPos = Move.getPosition()
		GetSeeds()
		Move.goTo(retPos, false)
		Plant()
	end
end


local function BreakCrop()
	turtle.digDown()
	Plant()
end


local function CheckCrop()
	local suc, ret = turtle.inspectDown()
	if not suc then
		Plant()
	elseif suc and ret ~= nil and ret.name == cropName and ret.state.age == maxGrowth then --crop fully grown
		BreakCrop()
	end
end


local function HarvestPlot(plot)
	if plot == nil or plot.coords == nil then
		print("Error parsing plot data, did you fuck up in the definition?)")
		print("plot:" .. plot)
		print("plot coords: " .. plot.coords)
	end
	--set a bunch of fields from plot definition and CropData.json
	coords = plot.coords
	currPlotChestLocation = Utils.createCoordinateSet(coords.x + 4 + coordsOffset.x, coords.y + 0 + coordsOffset.y,
	coords.z + 4 + coordsOffset.z, 0)                                                                                                                 --plot chest is offset by 4, 0, 4 from plot corner, plus whatever (gps) offsets
	cropName = cropData[plot.crop].cropName
	seedName = cropData[plot.crop].seedName
	maxGrowth = cropData[plot.crop].maxGrowth

	local itemInfo = Items.getItem(cropData[plot.crop].cropName) --get displayName
	if itemInfo ~= nil and itemInfo.displayName ~= nil then
		displayName = itemInfo.displayName
	else
		displayName = cropName
	end


	if Utils.findItemInInventory(seedName, 1) == nil then
		GetSeeds()
	end

	for x = 0, 8 do
		if x % 2 == 0 then
			for z = 0, 8 do
				Move.goTo(
				Utils.createCoordinateSet(x + coords.x + coordsOffset.x, 0 + coordsOffset.y,
				z + coords.z + coordsOffset.z, 2), false)
				CheckCrop()
			end
		else
			for z = 8, 0, -1 do
				Move.goTo(
				Utils.createCoordinateSet(x + coords.x + coordsOffset.x, 0 + coordsOffset.y,
				z + coords.z + coordsOffset.z, 0), false)
				CheckCrop()
			end
		end
	end
	DropoffHarvest()
end

local function FuelUp()
	print("refueling")
	while turtle.getFuelLevel() < maxFuel do
		turtle.select(16)
		if not turtle.suckUp(4) then
			os.sleep(2)
		end
		if turtle.refuel() == false then
			turtle.drop()
		end
	end
	turtle.dropUp()
	turtle.drop()
end

local function VerifyEnoughFuel()
	--fuel check
	if turtle.getFuelLevel() < minFuel then
		GoHome()
		FuelUp()
	end
end

while true do
	GoHome()
	VerifyEnoughFuel()
	for _, plot in pairs(plots) do
		print("Harvesting plot:")
		print(Utils.printTableRecursive(plot))
		HarvestPlot(plot)
	end
	print("Sleeping for " .. harvestInterval)
	sleep(harvestInterval)
end
