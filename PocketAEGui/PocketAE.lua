-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
    "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Websockets.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "PocketAEGui/PocketAEGui.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "PocketAEGui/ListBoxItem.lua")

local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local StatusCodes = require("Modules.StatusCodes")
local Messages = require("Modules.Networking.Messages")

Threads = Threads.new()
Timer = Timer.new(Threads)
--load correct network driver
local Network = require("Modules.Networking.Networking")
local Modems = require("Modules.Networking.HW.Modems")
local Websockets = require("Modules.Networking.HW.Websockets")
if peripheral.find("modem") ~= nil then
    Network = Network.new(Threads, Timer, Modems.new())
else
    Network = Network.new(Threads, Timer, Websockets.new())
end
local Gui = require("PocketAEGui.PocketAEGui")
local ListBoxItem = require("PocketAEGui.ListBoxItem")

local TAG = "MAIN"
--set log level globaly
--Dbg.SetGlobalLogLevel(Dbg.Levels.Info)
Dbg.setLogLevel(TAG, Dbg.Levels.Verbose)
local cacheName = "cache/PocketAE.cache"

--todo check if i need all these
local outputInventory = nil
local config = {}
local PocketAEControllerId = nil
local name = Messages.remoteTypes.PocketAE
local isAutoUpdating = false
local isRulesActive = false
local types = { Messages.remoteTypes.PocketAE }


---callback function, used when items are requested in gui
---@param itemInformation table information about item being requested
---@param count number amount of item being requested
---@return string statuscode status code for item request
---@return string infoString string giving additional information about status of request
local function OnRequestCallback(itemInformation, count)
    --create request
    Dbg.logI(TAG, "requesting " .. tostring(count) .. " " .. itemInformation.name .. " from " .. PocketAEControllerId)
    local exportInfo = {}
    exportInfo[outputInventory] = {}
    table.insert(exportInfo[outputInventory],
        { name = itemInformation.name, count = count, damage = itemInformation.damage, nbt = itemInformation.nbt })
    local request = Messages.createItemRequest(exportInfo)
    local id = Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, request)
    --wait for response
    local response = Network.receive(10, nil, id)
    if response then
        Dbg.logD(TAG, "ItemRequest response code " .. StatusCodes.toString(response.payload.statusCode))
        if response.payload.statusCode ~= StatusCodes.Code.Ok then
            return response.payload.statusCode, "got " .. tostring(response.payload.msg)
        else
            return response.payload.statusCode,
                "request " ..
                StatusCodes.toString(response.payload.statusCode) .. " sending " .. tostring(response.payload.msg)
        end
    else
        Dbg.logW(TAG, "no response from server")
        return StatusCodes.Code.NoServerResponse, "no server response"
    end
    return StatusCodes.Code.NoServerResponse, "no server response"
end

---callback function, used when deposit of items are requested in gui
---@param itemInformation table information about item to deposit
---@param count number amount of item to deposit
local function OnDepositRequestCallback(itemInformation, count)
    --create request
    Dbg.logI(TAG, "depositing " .. tostring(count) .. " " .. itemInformation.name)
    local request = Messages.createRemoveItemFromInventory(itemInformation.name, count, itemInformation.damage)
    local id = Network.transmit(PocketAEControllerId, Network.QOS.EXACTLY_ONCE, request)
    --wait for response
    local response = Network.receive(10, nil, id)
    if response then
        Dbg.logD(TAG, "deposit response " .. StatusCodes.toString(response.payload.statusCode))
        if response.payload.statusCode ~= StatusCodes.Code.Ok then
            return response.payload.statusCode, tostring(response.payload.msg)
        else
            return response.payload.statusCode, tostring(response.payload.msg)
        end
    else
        Dbg.logW(TAG, "no response from server")
        return StatusCodes.Code.NoServerResponse, "no server response"
    end
    return StatusCodes.Code.NoServerResponse, "no server response"
end

---callback function, used when crafted items are requested in gui
---@param itemInformation table information about item being requested
---@param count number amount of item being requested
local function OnCraftRequestCallback(itemInformation, count)
    if PocketAEControllerId == nil then
        return StatusCodes.Code.Fail, "no controller on network"
    end
    --create request
    Dbg.logI(TAG,
        "requesting craft " .. tostring(count) .. " " .. itemInformation.name .. " from " .. PocketAEControllerId)
    local request = Messages.createCraftRequest(itemInformation.name, count, itemInformation.nbt, itemInformation.damage,
        0)
    local id = Network.transmit(PocketAEControllerId, Network.QOS.EXACTLY_ONCE, request)
    --wait for response
    local response = Network.receive(10, nil, id)
    if response then
        Dbg.logD(TAG, "CraftRequest response code " .. StatusCodes.toString(response.payload.statusCode))
        return response.payload.statusCode, tostring(response.payload.msg)
    else
        Dbg.logW(TAG, "no response from server")
        return StatusCodes.Code.NoServerResponse, "no server response"
    end
    return StatusCodes.Code.NoServerResponse, "no server response"
end

local function FetchDepItemList()
    local msg = Messages.createPlayerInventoryRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
    --receiving is handled in NetworkLoopRequests
end

---handle messages received on broadcast channel
local function NetworkLoopBroadcast()
    while true do
        local msg = Network.receive(nil, Network.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) == true then
                Dbg.logI(TAG, "received who is")
                local resp = Messages.createWhoIsResponse(types, name)
                Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp, msg.llId)
            end
        end
    end
end

---handle messages received on private channel
local function NetworkLoopRequests()
    local msgTypes = {
        Messages.messageTypes.WhoIs,
        Messages.messageTypes.ItemList,
        Messages.messageTypes.PlayerInventoryRequestResponse,
        Messages.messageTypes.CraftList,
    }
    while true do
        local msg = Network.receiveMsgTypes(nil, os.getComputerID(), msgTypes)
        if msg and msg.payload and msg.payload.type then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs then
                local resp = Messages.createWhoIsResponse(types, name)
                Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp, msg.llId)
                --item list
            elseif msg.payload.type == Messages.messageTypes.ItemList then
                Dbg.logI("main", "item list received")
                --clear old arrays
                local itemTable = {}
                for itemName, items in pairs(msg.payload.Items) do
                    for _, itemInfo in pairs(items) do
                        local entry = {

                            name = itemName,
                            displayName = itemInfo.displayName,
                            count = itemInfo.count,
                            damage = itemInfo.damage,
                            maxDamage = itemInfo.maxDamage,
                            nbt = itemInfo.nbt,
                        }
                        table.insert(itemTable, ListBoxItem.NewItem(entry))
                    end
                end
                Gui.SetRequestItemTable(itemTable)

                --inventory list
            elseif msg.payload.type == Messages.messageTypes.PlayerInventoryRequestResponse then
                Dbg.logI(TAG, "inventory list received")
                --clear old arrays
                local playerinventoryTable = {}
                for _, v in pairs(msg.payload.Items) do
                    table.insert(playerinventoryTable, ListBoxItem.NewItem(v))
                end
                Gui.SetDepositItemTable(playerinventoryTable)
                --craft list
            elseif msg.payload.type == Messages.messageTypes.CraftList then
                --clear old arrays
                local craftTable = {}
                for _, v in pairs(msg.payload.Items) do
                    table.insert(craftTable, ListBoxItem.NewItem(v))
                end
                Gui.SetCraftingTable(craftTable)
            else
                Dbg.logW(TAG, "unknown msg ", msg)
            end
        end
    end
end

---callback for rules button
--todo: make individual rules toggleable, probably pass a table with every rule you want active
---@param RulesActive boolean RulesActive?
local function rulesCallback(RulesActive)
    --[[
    isRulesActive = RulesActive
    if RulesActive then
        RulesActive()   --todo: create this function
    end
    ]]
end

---callback for query button
local function queryCallback()
    Dbg.logI(TAG, "querying item lists")
    local msg = Messages.createItemListRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
    msg = Messages.createCraftListRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
    msg = Messages.createPlayerInventoryRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
end

---perform all gui functions
local function GuiLoop()
    --request items to populate the gui with
    Gui.Init(OnRequestCallback, OnDepositRequestCallback, OnCraftRequestCallback, Dbg.Logs, rulesCallback, queryCallback,
        FetchDepItemList)
    --todo: add request to populate autocraftable items
    --main gui loop
    while true do
        local eventInfo = { os.pullEvent() }
        Gui.Handle(eventInfo)
    end
end

-- initialize terminal
local function Init()
    --get controller id
    local msg = Messages.createWhoIs({ Messages.remoteTypes.PocketAEController })
    local msgId = Network.transmit(Network.broadcastChannel, Network.QOS.ATMOST_ONCE, msg)
    Dbg.logI(TAG, "requesting PocketAEController id")
    print("requesting PocketAEController id")
    local resp = Network.receive(5, nil, msgId)
    while resp ~= nil do
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            Dbg.logI(TAG, "got controller id from: " .. tostring(resp.from) .. " Name: " .. resp.payload.name)
            if resp.payload.name == config.name then
                print("found matching controller " .. tostring(resp.from))
                PocketAEControllerId = resp.from
                break
            end
            resp = Network.receive(1, nil, msgId)
        end
    end
    --check if we found a controller
    if PocketAEControllerId == nil then
        error("controller binding to " .. config.name .. " can not be found")
    end

    --request information about inventory to deposit requested items into
    msg = Messages.createInputInventoryRequest()
    while outputInventory == nil do
        print("requesting input inventory")
        msgId = Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg)
        local resp = Network.receive(5, os.getComputerID(), msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            outputInventory = resp.payload.inputInventory
        end
    end

    print("Fetching lists from controller")
    msg = Messages.createItemListRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
    msg = Messages.createCraftListRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)
    msg = Messages.createPlayerInventoryRequest()
    Network.transmit(PocketAEControllerId, Network.QOS.ATMOST_ONCE, msg, nil)

    --start the threads and gui
    Threads.create(GuiLoop)
    Threads.create(NetworkLoopBroadcast)
    Threads.create(NetworkLoopRequests)
end

--------------------------------------- MAIN --------------------------------------
term.clear()
term.setCursorPos(1, 1)

--initial setup
if not fs.exists(cacheName) then
    print("Type in your player name (as it appears on your memory card)") --this is not secure
    local playerName = read()

    local cache = {
        name = playerName,
    }

    local file = fs.open(cacheName, "w")
    file.write(textutils.serialize(cache))
    file.close()
    print("configuration done to re enter configuration delete " .. cacheName)
    return
end


--load data from cachefile
local file = fs.open(cacheName, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)

if config == nil then
    error("config is corrupt please fix")
end

--initialize
Threads.create(Init)
Threads.startRunning()
