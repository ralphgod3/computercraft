local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/Button.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/InputField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/ListBox.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/NumericField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/TextField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxScroll.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxSearch.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxSearchNumeric.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/NumericUpDown.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/ListboxSearchAndFilterFunctions.lua")


local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")
local StatusCode = require("Modules.StatusCodes")
local advWindow = require("Gui.advWindow")
local Button = require("Gui.Elements.Button")
local InputField = require("Gui.Elements.InputField")
local ListBox = require("Gui.Elements.ListBox")
local NumericField = require("Gui.Elements.NumericField")
local TextField = require("Gui.Elements.TextField")
local GuiUtils = require("Gui.GuiUtils")
local RulesManager = require("Modules.InvRules")

local ListboxScroll = require("Gui.Compositions.ListboxScroll")
local ListboxSearch = require("Gui.Compositions.ListboxSearch")
local ListboxSearchNumeric = require("Gui.Compositions.ListboxSearchNumeric")
local NumericUpDown = require("Gui.Compositions.NumericUpDown")
local ListboxSearchAndFilterFunctions = require("Gui.ListboxSearchAndFilterFunctions")




--time that popup window pops up after request success
local requestSuccessPopupTime = 0.5
local requestFailurePopupTime = 2
local Gui = {
    _p = {
        curVisible = {}
    }
}

----------------------------------- MENU BAR ------------------------------------
--callback when exit button is clicked
local function MenuCallbackBtnExit(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        term.setCursorPos(1, 1)
        term.clear()
        error()
    end
end

--callback when query button is clicked
local function MenuCallbackBtnQuery(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        Gui._p.OnBtnQueryCallback()
    end
end

--callback for buttons that switch tabs
local function MenuCallbackBtnTabSwitch(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        Gui.Menu.UiElements.btnRequest.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnDeposit.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnCraft.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnRules.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnLog.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnQuery.setBackgroundColor(colors.lightGray)

        this.setBackgroundColor(colors.gray)
        Gui._p.curVisible.setVisible(false)
        if userParams == "req" then
            Gui._p.curVisible = Gui.Request
        elseif userParams == "dep" then
            Gui._p.curVisible = Gui.Deposit
        elseif userParams == "craft" then
            Gui._p.curVisible = Gui.Craft
        elseif userParams == "log" then
            Gui._p.curVisible = Gui.Log
        elseif userParams == "rules" then
            Gui._p.curVisible = Gui.Rules
        end
        Gui._p.curVisible.setVisible(true)
        Gui._p.curVisible.draw()
    end
end

--creates the menu bar for gui
local function CreateMenu()
    local terminal = term.current()
    local sizeX, _ = terminal.getSize()
    local rightX = 1
    local leftX = 1
    --left side aligned
    Gui.Menu = advWindow.new(terminal, 1, 1, sizeX, 1, true)
    local text = "Re"
    Gui.Menu.addUiElement("btnRequest", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.gray, text, MenuCallbackBtnTabSwitch, "req"))
    text = "De"
    leftX = leftX + string.len(text) + 2
    Gui.Menu.addUiElement("btnDeposit", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "dep"))
    text = "Cr"
    leftX = leftX + string.len(text) + 2
    Gui.Menu.addUiElement("btnCraft", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "craft"))
    text = "Lo"
    leftX = leftX + string.len(text) + 2
    Gui.Menu.addUiElement("btnLog", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "log"))
    text = "Ru"
    leftX = leftX + string.len(text) + 2
    Gui.Menu.addUiElement("btnRules", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "rules"))
    --right side aligned
    text = "?"
    rightX = sizeX - string.len(text) -1
    Gui.Menu.addUiElement("btnQuery", Button.new(Gui.Menu, rightX, 1, 1, 1, colors.white, colors.lightGray, text, MenuCallbackBtnQuery))
    text = "X"
    Gui.Menu.addUiElement("btnExit", Button.new(Gui.Menu, sizeX, 1, 1, 1, colors.white, colors.red, text, MenuCallbackBtnExit))
    Gui.Menu.setBackgroundColor(colors.cyan)
    Gui.Menu.setVisible(true)
    Gui.Menu.draw()
end

----------------------------------- REQUEST TAB ------------------------------------

local function RequestCallbackFnc(eventInfo, this, userParams)
    if eventInfo[1] == "element_click" then
        local itemInfo = eventInfo[2]
        if itemInfo ~= nil and Gui._p.OnRequestCallbackFnc ~= nil then
            Gui.Popup.UiElements.text.setText("Requested: " .. tostring(Gui.Request.getNumber()) .. ", " .. itemInfo.name)
            Gui.Popup.setVisible(true)
            local status ,msg = Gui._p.OnRequestCallbackFnc(itemInfo, Gui.Request.getNumber())
            if status ~= StatusCode.Code.Ok then
                Gui.Popup.UiElements.text.setText("Request failed, Reason " .. StatusCode.toString(status))
                os.sleep(requestFailurePopupTime)
            else
                Gui.Popup.UiElements.text.setText(msg)
                os.sleep(requestSuccessPopupTime)
            end
            Gui.Popup.setVisible(false)
            Gui.Request.draw()
        end
    end
end

--create request tab
local function CreateRequestTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Request = ListboxSearchNumeric.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, RequestCallbackFnc, nil, nil, ListboxSearchAndFilterFunctions.listboxItemFilterFunction, ListboxSearchAndFilterFunctions.listboxItemSortFunctionHighCount)
    Gui.Request.setBackgroundColor(colors.black)
    Gui.Request.setVisible(true)
    Gui.Request.draw()
    Gui._p.curVisible = Gui.Request
end



----------------------------------- DEPOSIT TAB ------------------------------------

--callback for clicking on list box
local function DepositCallbackFnc(eventInfo, this, userParams)
    if eventInfo[1] == "element_click" then
        local itemInfo = eventInfo[2]
        if itemInfo ~= nil and Gui._p.OnDepositCallbackFnc ~= nil then
            Gui.Popup.UiElements.text.setText("Deposited: " .. tostring(Gui.Deposit.getNumber()) .. ", " .. itemInfo.name)
            Gui.Popup.setVisible(true)
            local status ,msg = Gui._p.OnDepositCallbackFnc(itemInfo, Gui.getNumber())
            if status ~= StatusCode.Code.Ok then
                Gui.Popup.UiElements.text.setText("Deposit failed, Reason " .. StatusCode.ToString(status))
                sleep(requestFailurePopupTime)
            else
                Gui.Popup.UiElements.text.setText(msg)
                sleep(requestSuccessPopupTime)
            end
            Gui.Popup.setVisible(false)
            Gui.Deposit.draw()
        end
    end
end

--create Deposit tab
local function CreateDepositTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Deposit = ListboxSearchNumeric.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, DepositCallbackFnc, nil, nil, ListboxSearchAndFilterFunctions.listboxItemFilterFunction, ListboxSearchAndFilterFunctions.listboxItemSortFunctionHighCount)
    Gui.Deposit.setBackgroundColor(colors.black)
end

----------------------------------- CRAFT TAB ------------------------------------

--callback for listbox
local function CraftCallbackFnc(eventInfo, this, userParams)
    if eventInfo[1] == "element_click" then
        local itemInfo = eventInfo[2]
        if itemInfo ~= nil and Gui._p.OnCraftRequestCallbackFnc ~= nil then
            Gui.Popup.UiElements.text.setText("Crafting: " .. tostring(Gui.Craft.getNumber()) .. ", " .. itemInfo.name)
            Gui.Popup.setVisible(true)
            local status, msg = Gui._p.OnCraftRequestCallbackFnc(itemInfo, Gui.Craft.getNumber())
            if status ~= StatusCode.Code.Ok then
                Gui.Popup.UiElements.text.setText(msg)
                sleep(requestFailurePopupTime)
            else
                Gui.Popup.UiElements.text.setText(msg)
                sleep(requestSuccessPopupTime)
            end
            Gui.Popup.setVisible(false)
            Gui.Craft.draw()
        end
    end
end

--create Deposit tab
local function CreateCraftTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Craft = ListboxSearchNumeric.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, CraftCallbackFnc, nil, nil, ListboxSearchAndFilterFunctions.listboxItemFilterFunction, ListboxSearchAndFilterFunctions.listboxItemSortFunctionHighCount)
    Gui.Craft.setBackgroundColor(colors.black)
end


----------------------------------- RULES TAB ------------------------------------

local function UpdateRulesList()
    for i = 1, #Gui._p.RulesList do
        Gui.Rules.UiElements.listItemsWindow.UiElements.listItems[i] = RulesManager.displayString(Gui._p.RulesList[i])
    end
    if #Gui._p.RulesList < #Gui.Rules.UiElements.listItemsWindow.UiElements.listItems then
        for i = 1, #Gui.Rules.UiElements.listItemsWindow.UiElements.listItems do
            Gui.Rules.UiElements.listItemsWindow.UiElements.listItems[1] = nil
        end
    end
    Gui._p.TransmitRules()
    Gui.Rules.draw()
end

local function RulesAddWizardKeepFillDrainToggle(toToggle)
    Gui.Rules.UiElements.addWizard.UiElements.btnKeep.setBackgroundColor(colors.lightGray)
    Gui.Rules.UiElements.addWizard.UiElements.btnDrain.setBackgroundColor(colors.lightGray)
    Gui.Rules.UiElements.addWizard.UiElements.btnFill.setBackgroundColor(colors.lightGray)

    if toToggle == "Keep" then
        Gui.Rules.UiElements.addWizard.UiElements.btnKeep.setBackgroundColor(colors.blue)
        RulesManager.setMode(RulesManager.const.mode.keep)
    elseif toToggle == "Drain" then
        Gui.Rules.UiElements.addWizard.UiElements.btnDrain.setBackgroundColor(colors.blue)
        RulesManager.setMode(RulesManager.const.mode.drain)
    elseif toToggle == "Fill" then
        Gui.Rules.UiElements.addWizard.UiElements.btnFill.setBackgroundColor(colors.blue)
        RulesManager.setMode(RulesManager.const.mode.fill)
    end
end

local function RulesMenuBarAddClicked()
    RulesManager.newRule()
    Gui.Rules.UiElements.listItemsWindow.setVisible(false)
    Gui._p.curVisible = Gui.Rules.UiElements.addWizard
    Gui.Rules.UiElements.addWizard.setVisible(true)
    Gui.Rules.UiElements.addWizard.draw()
end

local function RulesAddWizardXClicked()
    Gui.Rules.UiElements.addWizard.setVisible(false)
    Gui.Rules.UiElements.listItemsWindow.setVisible(false)
    Gui._p.curVisible = Gui.Rules
    Gui.Rules.draw()
end

local function RulesMenuBarToggleClicked()
    Gui._p.RulesMenuBarToggleMode = true
    Gui.Rules.UiElements.menuBar.UiElements.btnToggle.setBackgroundColor(colors.green)
    Gui._p.RulesMenuBarDeleteMode = false
    Gui.Rules.UiElements.menuBar.UiElements.btnDelete.setBackgroundColor(colors.lightGray)
end

local function RulesMenuBarDeleteClicked()
    Gui._p.RulesMenuBarDeleteMode = true
    Gui.Rules.UiElements.menuBar.UiElements.btnDelete.setBackgroundColor(colors.red)
    Gui._p.RulesMenuBarToggleMode = false
    Gui.Rules.UiElements.menuBar.UiElements.btnToggle.setBackgroundColor(colors.lightGray)
end

local function OnClickInAddWizard(eventInfo, this, userParams)
    Dbg.LogI("ADDWZRD", "OnClickInAddWizard")
end

local function RulesCallbackListBox(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_scroll" then
        this.scroll(eventInfo[2])
    elseif eventInfo[1] == "bar_click" then
        this.resetScroll()
    elseif eventInfo[1] == "element_click" then
        if type(eventInfo[2]) ~= string then
            eventInfo[2] = tostring(eventInfo[2])
        end
        local clicked = eventInfo[2]
        local rule = nil
        for i = 1, #Gui._p.RulesList do --figure out what rule was clicked
            if RulesManager.displayString(Gui._p.RulesList[i]) == clicked then
                rule = Gui._p.RulesList[i]
                break
            end
        end
        if rule ~= nil then    --delete or toggle rule accordingly
            if Gui._p.RulesMenuBarDeleteMode then
                --todo: maybe add a confirmation popup here
                for i = 1, #Gui._p.RulesList do
                    if tostring(Gui._p.RulesList[i]) == rule then
                        table.remove(Gui._p.RulesList, i)
                    end
                end
            elseif Gui._p.RulesMenuBarToggleMode then   --todo: this needs a method to get the actual rule reference that was clicked in the listbox
                if rule.isActive == nil or rule.isActive then
                    rule.isActive = false
                    Dbg.LogI("Rule", "activated rule " .. rule)
                else
                    rule.isActive = true
                    Dbg.LogI("Rule", "disabled rule ".. rule)
                end
            end
            UpdateRulesList()
            Gui.Rules.draw()
        end
    end
end


--create rules tab
local function CreateRulesTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Rules = advWindow.new(terminal, 1, 2, sizeX, sizeY - 1, false)
    sizeX, sizeY = Gui.Rules.getSize()
    Gui.Rules.setBackgroundColor(colors.black)
    local fgColors = {colors.white}
    local bgColors = {colors.gray, colors.black}
    Gui._p.RulesList = {"AAAAAAAAA", "BBBBBBBBB", "CCCCCC"}
    --rules menubar
    Gui.Rules.addUiElement("menuBar", advWindow.new(Gui.Rules, 1, 2, sizeX, 1, true))
    Gui.Rules.UiElements.menuBar.setBackgroundColor(colors.cyan)
    local leftX = 1
    local text = "Add"
    Gui.Rules.UiElements.menuBar.addUiElement("btnAdd", Button.new(Gui.Rules, leftX, 1, 6 + 2, 1, colors.white, colors.blue, text, RulesMenuBarAddClicked))
    text = "Toggle"
    leftX = leftX + string.len(text) + 3
    Gui.Rules.UiElements.menuBar.addUiElement("btnToggle", Button.new(Gui.Rules, leftX, 1, 6 + 2, 1, colors.white, colors.green, text, RulesMenuBarToggleClicked))
    text = "Delete"
    leftX = leftX + string.len(text) + 3
    Gui.Rules.UiElements.menuBar.addUiElement("btnDelete", Button.new(Gui.Rules, leftX, 1, 6 + 2, 1, colors.white, colors.lightGray, text, RulesMenuBarDeleteClicked))
    leftX = 1
    text = "Delay:"
    Gui.Rules.UiElements.menuBar.addUiElement("textfieldDelay", TextField.new(Gui.Rules.UiElements.menuBar, leftX, 2, 6, 1, colors.white, colors.lightGray, text))
    leftX = leftX + 6
    text = "<"
    Gui.Rules.UiElements.menuBar.addUiElement("btnLessDelay", Button.new(Gui.Rules.UiElements.menuBar, leftX, 2, 1, 1, colors.white, colors.lightGray, text, onBtnLessDelayClicked))
    leftX = leftX + 1
    Gui.Rules.UiElements.menuBar.addUiElement("numfieldDelay", NumericField.new(Gui.Rules.UiElements.menuBar, leftX, 2, 4, 1, colors.white, colors.lightGray, 0, numfieldDelayInteraction, nil, 0))
    leftX = leftX + 4
    text = ">"
    Gui.Rules.UiElements.menuBar.addUiElement("btnMoreDelay", Button.new(Gui.Rules.UiElements.menuBar, leftX, 2, 1, 1, colors.white, colors.lightGray, text, onBtnMoreDelayClicked))
    Gui.Rules.UiElements.menuBar.setVisible(true)
    --rules listbox
    Gui.Rules.addUiElement("listItemsWindow", advWindow.new(Gui.Rules, 1, 3, sizeX, sizeY -2, true))
    Gui.Rules.UiElements.listItemsWindow.setVisible(true)
    Gui.Rules.UiElements.listItemsWindow.addUiElement("listItems", ListBox.new(Gui.Rules, 1, 2, sizeX, sizeY -1, fgColors, bgColors, RulesCallbackListBox))
    --rule add wizard
    Gui.Rules.addUiElement("addWizard", advWindow.new(Gui.Rules, 1, 1, sizeX, sizeY, false, OnClickInAddWizard))
    leftX = 1
    text = "Keep"
    Gui.Rules.UiElements.addWizard.addUiElement("btnKeep", Button.new(Gui.Rules.UiElements.addWizard, leftX, 1, 8, 1, colors.white, colors.lightGray, text, RulesAddWizardKeepFillDrainToggle))
    leftX = leftX + 8
    text = "Drain"
    Gui.Rules.UiElements.addWizard.addUiElement("btnDrain", Button.new(Gui.Rules.UiElements.addWizard, leftX, 1, 8, 1, colors.white, colors.lightGray, text, RulesAddWizardKeepFillDrainToggle))
    leftX = leftX + 8
    text = "Fill"
    Gui.Rules.UiElements.addWizard.addUiElement("btnFill", Button.new(Gui.Rules.UiElements.addWizard, leftX, 1, 8, 1, colors.white, colors.lightGray, text, RulesAddWizardKeepFillDrainToggle))
    text = "X"
    Gui.Rules.UiElements.addWizard.addUiElement("btnX", Button.new(Gui.Rules.UiElements.addWizard, sizeX, 1, 1, 1, colors.white, colors.lightGray, text, RulesAddWizardXClicked))
    leftX = 1
    text = "Amount"
    Gui.Rules.UiElements.addWizard.addUiElement("textfieldAmount", TextField.new(Gui.Rules.UiElements.addWizard, leftX, 2, 6, 1, colors.white, colors.lightGray, text))
    leftX = leftX + 7
    text = "<"
    Gui.Rules.UiElements.addWizard.addUiElement("btnLessAmount", Button.new(Gui.Rules.UiElements.addWizard, leftX, 2, 1, 1, colors.white, colors.lightGray, text, onBtnLessAmountClicked))
    leftX = leftX + 1
    Gui.Rules.UiElements.addWizard.addUiElement("numfieldAmount", NumericField.new(Gui.Rules.UiElements.addWizard, leftX, 2, 4, 1, colors.white, colors.lightGray, 0, numfieldAmountInteraction, nil, 1))
    leftX = leftX + 5
    text = ">"
    Gui.Rules.UiElements.addWizard.addUiElement("btnMoreAmount", Button.new(Gui.Rules.UiElements.addWizard, leftX, 2, 1, 1, colors.white, colors.lightGray, text, onBtnMoreAmountClicked))


end
----------------------------------- LOGS TAB ------------------------------------

--create log tab
local function CreateLogTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Log = ListboxScroll.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, nil, nil, GuiUtils.drawTextAlignmentTopLeft)
    Gui.Log.setItemTable(Gui._p.Logs)
    Gui.Log.setBackgroundColor(colors.black)
    Gui.Log.setVisible(false);
end

---handles all gui operations
---@param eventInfo table eventinfo as received by os.pullEvent
function Gui.Handle(eventInfo)
    Gui.Menu.handle(eventInfo)
    Gui.Request.handle(eventInfo)
    Gui.Deposit.handle(eventInfo)
    Gui.Craft.handle(eventInfo)
    Gui.Log.handle(eventInfo)
    Gui.Rules.handle(eventInfo)
    Gui.Query.handle(eventInfo)
end

----------------------------------- Query TAB ------------------------------------
--create ? "tab" (more like a button)
--todo: this needs a custom interface
local function CreateQueryTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Query = advWindow.new(terminal, 1, 2, sizeX, sizeY - 1, false)
    sizeX, sizeY = Gui.Query.getSize()
    Gui.Query.setBackgroundColor(colors.black)
end
----------------------------------- Popup Window ------------------------------------
--create popup window
local function CreatePopupWindow()
    local winXSize = 28
    local winYSize = 10

    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()


    Gui.Popup = advWindow.new(terminal, (sizeX / 2) - (winXSize / 2), (sizeY / 2) - (winYSize / 2), winXSize, winYSize, false)
    sizeX, sizeY = Gui.Popup.getSize()

    Gui.Popup.addUiElement("text", TextField.new(Gui.Popup, 1, 1, sizeX, sizeY, colors.white, colors.brown , "placeholder"))
    Gui.Popup.draw()
end

----------------------------------- generic functions ------------------------------------

---update items in the request tab of ui, forces redraw
---@param tableToSet table table to set table to
function Gui.SetRequestItemTable(tableToSet)
    Gui.Request.setItemTable(tableToSet)
end

---update items in the deposit tab of ui, forces redraw
---@param tableToSet table table to set table to
function Gui.SetDepositItemTable(tableToSet)
    Gui.Deposit.setItemTable(tableToSet)
end

---update items in the crafting tab of ui, forces redraw
---@param tableToSet table table to set table to
function Gui.SetCraftingTable(tableToSet)
    Gui.Craft.setItemTable(tableToSet)
end

function Gui.SetActiveRules(rules)
    Gui._p.RulesList = rules
    Gui.Rules.draw()
end

---initialize Gui
---@param OnRequestCallbackFnc function function called when a item request is made prototype StatusCode, responseMessageFromServer OnRequestCallback(itemInformation, count)
---@param OnDepositCallbackFnc function function called when a deposit request is made
---@param OnCraftRequestCallbackFnc function function called when a craft item request is made prototype StatusCode, responseMessageFromServer OnRequestCallback(itemInformation, count)
---@param Logs table table reference that links to the debug logger logs
---@param TransmitRules function function that sends rules to inventory manager
---@param OnBtnQueryCallback function is called when the ? (query) button is pressed
---@param UpdateDepItemList function is called when Deposit tab is pressed. Is supposed to request a new inventory list
function Gui.Init(OnRequestCallbackFnc, OnDepositCallbackFnc, OnCraftRequestCallbackFnc, Logs, TransmitRules, OnBtnQueryCallback, UpdateDepItemList)
    PC.expect(1, OnRequestCallbackFnc, "function")
    PC.expect(2, OnDepositCallbackFnc, "function")
    PC.expect(3, OnCraftRequestCallbackFnc, "function")
    PC.expect(4, Logs, "table")
    PC.expect(5, TransmitRules, "function")
    PC.expect(6, OnBtnQueryCallback, "function")
    Gui._p.OnRequestCallbackFnc = OnRequestCallbackFnc
    Gui._p.OnDepositCallbackFnc = OnDepositCallbackFnc
    Gui._p.OnCraftRequestCallbackFnc = OnCraftRequestCallbackFnc
    Gui._p.TransmitRules = TransmitRules
    Gui._p.OnBtnQueryCallback = OnBtnQueryCallback
    CreateMenu()
    CreateRequestTab()
    CreateDepositTab()
    CreateCraftTab()
    CreateRulesTab()
    CreateLogTab()
    CreateQueryTab()
    CreatePopupWindow()
    Gui.Log.setItemTable(Logs)

    Gui.Request.setVisible(true)
    Gui.Deposit.setVisible(false)
    Gui.Craft.setVisible(false)
    Gui.Log.setVisible(false)
    Gui.Rules.setVisible(false)
end

return Gui