local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")
local ListBoxItem = {}


---create listbox wrapper with standard tostring function
---@return table item item that can be placed in listbox
function ListBoxItem.NewItem(item)
    PC.expect(1, item, "table")
    PC.field(item, "name", "string")
    PC.field(item, "displayName", "string")
    PC.field(item, "count", "number")
    PC.field(item, "damage", "number", "nil")
    PC.field(item, "maxDamage", "number", "nil")
    local this = item
    ---override for tostring which is used by listbox
    ---@param width number | nil width in characters the text can take up
    ---@return string itemString string describing input item
    function this.tostring(self, width)
        --smartly truncates/pads item name to fit given width
        if width ~= nil and this.count ~= nil then
            local damageSuffix = ""
            if this.damage and this.maxDamage and this.damage > 0 and this.maxDamage > 0 then   --some items have negative durability values, they're probably meaningless to the user
                --damageSuffix = " " .. this.damage .. "/" .. this.maxDamage
                damageSuffix = " " .. (math.floor(((1- this.damage/this.maxDamage) * 100))) .. "%"  --math.floor is for truncating float
            end
            local countSuffix = " " .. tostring(this.count)
            local itemDisplayNameString = this.displayName

            local maxNameChars = width - #damageSuffix - #countSuffix
            
            if #itemDisplayNameString > maxNameChars then     --display name too long, truncate it
                for i = (maxNameChars -2), 1, -1 do
                    if itemDisplayNameString[i] ~= " " then
                        itemDisplayNameString = string.sub(itemDisplayNameString, 1, i) .. ".."
                        break
                    end
                    if i == 1 then  --item name is all spaces???
                        itemDisplayNameString = string.sub(itemDisplayNameString, 0, (width - #damageSuffix - #countSuffix -2)) .. ".."
                    end
                end
            end
            
            while #itemDisplayNameString < maxNameChars do    --display name too short, pad it
                itemDisplayNameString = itemDisplayNameString .. " "
            end

            return itemDisplayNameString .. damageSuffix .. countSuffix
        
        else
            return this.displayName
        end

    end
    return this
end


return ListBoxItem