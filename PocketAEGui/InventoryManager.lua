-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Websockets.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")

local Dbg = require("Modules.Logger")
local ItemDb = require("Modules.ItemDb")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local Threads = require("Modules.Threads")
local Timer = require("Modules.Timer")
local Messages = require("Modules.Networking.Messages")
local Network = require("Modules.Networking.Networking")
local ModemHardware = require("Modules.Networking.HW.Modems")
local WebSocketHardware = require("Modules.Networking.HW.Websockets")

Threads = Threads.new()
Timer = Timer.new(Threads)
ItemDb = ItemDb.new()

local inventoryManager = peripheral.find("inventoryManager")
local cacheFile = "cache/inventoryManager.cache"
local types = {Messages.remoteTypes.PocketAEController}
local config = {}
local requestableList = {}
local storageControllerId = nil
local CrafterControllerId = nil
local outputChest = nil
-- com interfaces
local wirelessHardware = nil
local wiredHardware = nil
local Wireless = nil
local Wired = nil
--------------------------------------- FUNCTIONS --------------------------------------

---creates a list of items in player inventory ready to be send to pocket computer
local function CreateInventoryList()
    local list = inventoryManager.getItems()
    local retTable = {}
    for _,itemInfo in pairs(list) do
        local exists = false
        --check if item is already in list
        for _, itemInList in pairs(retTable) do
            --check for items with same name
            if itemInList.name == itemInfo.name then
                --check for damage value
                if itemInfo.nbt.Damage ~= nil then
                    if tonumber(itemInfo.nbt.Damage) == itemInList.damage then
                        exists = true
                        itemInList.count = itemInList.count + 1
                    end
                else
                    exists = true
                    itemInList.count = itemInList.count + itemInfo.amount
                end
            end
        end
        if not exists then
            local newItem = {}
            newItem.name = itemInfo.name
            newItem.count = itemInfo.amount
            newItem.displayName = string.sub(itemInfo.displayName, 2, string.len(itemInfo.displayName) -1)
            if itemInfo.nbt.Damage ~= nil then
                local itemFromDb = ItemDb.getItem(itemInfo.name)
                newItem.maxDamage = -1
                if itemFromDb ~= nil then
                    newItem.maxDamage = tonumber(itemFromDb.maxDamage)
                end
                newItem.damage = tonumber(itemInfo.nbt.Damage)
            end
            table.insert(retTable, newItem)
        end
    end
    return retTable
end

local function PocketMainLoop()
    local msgTypes = {
        Messages.messageTypes.ItemListRequest,
        Messages.messageTypes.RemoveItemFromInventory,
        Messages.messageTypes.ItemRequest,
        Messages.messageTypes.GetInputInventory,
        Messages.messageTypes.CraftListRequest,
        Messages.messageTypes.PlayerInventoryRequest,
        Messages.messageTypes.CraftRequest, --todo:implement if statement for this
    }
    while true do
       local msg = Wireless.receiveMsgTypes(nil, os.getComputerID(), msgTypes)
       if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            if msg.payload.type == Messages.messageTypes.ItemListRequest then
               print("received request for item list")
               local resp = Messages.createItemList(requestableList)
               Wireless.transmit(msg.from, Wireless.QOS.ATMOST_ONCE, resp)
            elseif msg.payload.type == Messages.messageTypes.PlayerInventoryRequest then
                print("received player inventory request")
                local resp = Messages.createPlayerInventoryRequestResponse(CreateInventoryList())
                Wireless.transmit(msg.from, Wireless.QOS.ATMOST_ONCE, resp, msg.llId)
            elseif msg.payload.type == Messages.messageTypes.RemoveItemFromInventory then
                print("received removal request from " .. tostring(msg.from) .. " for " .. tostring(msg.payload.count) .. " " .. msg.payload.name)
                local removed = 0
                --fixme: comment back in once advanced peripherals adds nescesary feature
                --if msg.payload.damage ~= nil then
                --    local list = inventoryManager.getItems()
                --    Dbg.LogI("inventory", Utils.PrintTableRecursive(list))
                --    for slot, itemInfo in pairs(list) do
                --        if itemInfo.name == msg.payload.name and itemInfo.nbt.Damage ~= nil and tonumber(itemInfo.nbt.Damage) == msg.payload.damage then
                --            print("dir " .. config.exportFromPlayerDirection .. " slot " .. tostring(slot) .. " damage " .. itemInfo.nbt.Damage)
                --            removed = inventoryManager.removeItemFromPlayer(config.exportFromPlayerDirection, 1, slot)
                --            break
                --        end
                --    end
                --else
                -- -1 is slot, we dont care where to pull from just pull
                removed = inventoryManager.removeItemFromPlayer(config.exportFromPlayerDirection, tonumber(msg.payload.count), -1, msg.payload.name)
                --end
                --todo: maybe change to fail if amount doesnt match
                local resp = nil
                if removed == 0 then
                    resp = Messages.createItemRequestResponse(StatusCode.Code.Fail, "none removed")
                else
                    resp = Messages.createItemRequestResponse(StatusCode.Code.Ok, "removed " .. tostring(removed))
                end
                Wireless.transmit(msg.from, Wireless.QOS.EXACTLY_ONCE, resp, msg.llId)
                local resp = Messages.createPlayerInventoryRequestResponse(CreateInventoryList())
                Wireless.transmit(msg.from, Wireless.QOS.ATMOST_ONCE, resp)
            elseif msg.payload.type == Messages.messageTypes.ItemRequest then
                print("received item request from " .. tostring(msg.from))
                local resp = Messages.createItemRequest(msg.payload.exportInfo)
                local msgId = Wired.transmit(storageControllerId, Wired.QOS.EXACTLY_ONCE, resp)
                resp = Wired.receive(5, os.getComputerID(), msgId)
                if resp then
                    Wireless.transmit(msg.from, Wireless.QOS.EXACTLY_ONCE, resp.payload, msg.llId)
                end
                local _,playerInvInfo = next(msg.payload.exportInfo)
                for _, itemInfo in pairs(playerInvInfo) do
                    inventoryManager.addItemToPlayer(config.importToPlayerDirection, itemInfo.count, -1 ,itemInfo.name)
                end
            elseif msg.payload.type == Messages.messageTypes.GetInputInventory then
                print("received input inventory request")
                local resp = Messages.createInputInventoryResponse(config.importInventoryName)
                Wireless.transmit(msg.from, Wireless.QOS.ATMOST_ONCE, resp, msg.llId)
            elseif msg.payload.type == Messages.messageTypes.CraftListRequest then
                if CrafterControllerId ~= nil then
                    print("received craft list request")
                    local resp = Messages.createCraftListRequest()
                    local msgId = Wired.transmit(CrafterControllerId, Wired.QOS.EXACTLY_ONCE, resp)
                    resp = Wired.receive(5, os.getComputerID(), msgId)
                    if resp then
                        Wireless.transmit(msg.from, Wireless.QOS.ATMOST_ONCE, resp.payload, msg.llId)
                    end
                end
            end
        end
    end
end

local function wirelessBroadcastloop()
    while true do
        local msg = Wireless.receive(nil, Wireless.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) then
                print("received who is from " .. tostring(msg.from))
                print(inventoryManager.getOwner())
                local resp = Messages.createWhoIsResponse(types, inventoryManager.getOwner())
                Wireless.transmit(msg.from, Wireless.QOS.EXACTLY_ONCE, resp, msg.llId)
            end
        end
    end
end

local function wiredBroadcastLoop()
    while true do
        local msg = Wired.receive(nil, Wired.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) then
                print("received who is from " .. tostring(msg.from))
                local resp = Messages.createWhoIsResponse(types, inventoryManager.getOwner())
                Wired.transmit(msg.from, Wired.QOS.EXACTLY_ONCE, resp, msg.llId)
            elseif msg.payload.type == Messages.messageTypes.ItemList then
                print("received requestable item list update")
                requestableList = msg.payload.Items
            end
        end
    end
end


local function Init()
    --get controller id
    local msg = Messages.createWhoIs({Messages.remoteTypes.StorageController})
    local msgId = Wired.transmit(Wired.broadcastChannel, Wired.QOS.ATMOST_ONCE, msg)
    print("requesting storagecontroller id")

    while storageControllerId == nil do
        print("requesting storageController id")
        local resp = Wired.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            print("got controller id " .. tostring(resp.from))
            storageControllerId = resp.from
        else
            msgId = Wired.transmit(Wired.broadcastChannel, Wired.QOS.ATMOST_ONCE, msg)
        end
    end

    print("fetching import inventory from storage system")
    msg = Messages.createInputInventoryRequest()
    msgId = Wired.transmit(storageControllerId, Wired.QOS.EXACTLY_ONCE, msg)
    --wait for controller to send information about its import inventory
    while true do
        local resp = Wired.receive(nil, os.getComputerID(), msgId)
        if resp then
            outputChest = resp.payload.inputInventory
            break
        end
    end

    msg = Messages.createWhoIs({Messages.remoteTypes.CrafterController})
    msgId = Wired.transmit(Wired.broadcastChannel, Wired.QOS.ATMOST_ONCE, msg)
    while CrafterControllerId == nil do
        print("requesting CrafterController id")
        local resp = Wired.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            print("got CrafterController id " .. tostring(resp.from))
            CrafterControllerId = resp.from
        else
            print("no CrafterController attached")
            break
        end
    end
    Threads.create(wiredBroadcastLoop)
    Threads.create(wirelessBroadcastloop)
    Threads.create(PocketMainLoop)
end


local function CreateCacheFile()
    local conf = {}
    conf.modemBroadcastChannel = 65532
    conf.exportFromPlayerDirection = "down"
    conf.importToPlayerDirection = "up"
    conf.rulePollTime = 10
    conf.importInventoryName = "minecraft:chest_x"
    local file = fs.open(cacheFile, "w")
    file.write(textutils.serialize(conf))
    file.close()
end


--------------------------------------- MAIN --------------------------------------
if not fs.exists(cacheFile) then
    CreateCacheFile()
    print("file " .. cacheFile .. " created, modify and restart program")
    return
end
local file = fs.open(cacheFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)
if config == nil then
    error("config corrupt, please fix")
end

print("owner " .. tostring(inventoryManager.getOwner()))

--find attached modems and initialize apropriate coms
local modems = {peripheral.find("modem")}
if modems then
    local wirelessFound = false
    local wiredFound = false
    for i = 1, #modems do
        if modems[i].isWireless() then
            wirelessFound = true
            wirelessHardware = ModemHardware.new(config.modemBroadcastChannel, {peripheral.wrap(peripheral.getName(modems[i]))})
            Wireless = Network.new(Threads, Timer, wirelessHardware)
            print("wireless " ..  peripheral.getName(modems[i]))
        else
            wiredFound = true
            wiredHardware = ModemHardware.new(config.modemBroadcastChannel, {peripheral.wrap(peripheral.getName(modems[i]))})
            Wired = Network.new(Threads, Timer, wiredHardware)
            print("wired " ..  peripheral.getName(modems[i]))
        end
    end
    if not wiredFound then
        error("computer needs access to storageController, add a wired modem")
    end
    if wirelessFound then
        print("using wireless modem for pocket computer coms")
    else
        Wireless = Network.new(Threads, Timer, WebSocketHardware.new(config.modemBroadcastChannel))
        print("using websockets for communication")
    end
else
    print("this program needs atleast a wired modem to work")
end
Threads.create(Init)
Threads.startRunning()