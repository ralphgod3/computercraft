slave:
    slaves get position and facing of master with corner 1 and corner 2 to dig and figure out how to move from here.
    slaves should when possible always travel along the same lines to the master, destroying anything that is not part of the setup.

master:
    builds mining setup.
    plans out mining plan.
    deploys slaves when available.
    assigns slaves with jobs.
    maintains mining plan in a crash proof way.
    can put fuel in slaves on request.


messages:
* jobStartMessage {master to slave}
    arguments:
        jobId
        masterPosition
        corner1
        corner2
    returns: jobStartResponse {ok, fail}

* jobCompletedMessage {slave to master}
    arguments: jobId
    returns: new jobStartMessage or mine the turtle up and put back in box

* refuelRequest {slave to master}
    arguments: itemCount,
    returns: nothing but puts fuel items in turtle inventory.

