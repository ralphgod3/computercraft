import json
import os
from os.path import isfile, join

recipePath = "recipes"
outputFile = "itemIds.json"
tagPath = ""


def GetItemsFromRecipeFile(file):
    retVal = []
    f = open(file, "r")
    text = json.load(f)
    f.close()

    for t in text:
        for recipes in text[t]:
            if "key" in recipes: #shaped recipe
                for items in recipes["key"]:
                    for item in recipes["key"][items]:
                        if item == "item":
                            if not recipes["key"][items][item] in retVal:
                                retVal.append(recipes["key"][items][item])
            elif "ingredients" in recipes:
                for ingredient in recipes["ingredients"]:
                    if "item" in ingredient:
                        if not ingredient["item"] in retVal:
                            retVal.append(ingredient["item"])
        retVal.append(t)
    return retVal


items = []
#recipe file extraction
recipeFolder = os.listdir(recipePath)
for recipeFile in recipeFolder:
    ret = GetItemsFromRecipeFile(recipePath + "/" + recipeFile)
    for retItem in ret:
        if not retItem in items:
            items.append(retItem)
#tag file extraction
tagFile = open(tagPath + "tags.json", "r")
tags = json.load(tagFile)
tagFile.close()
for tag in tags:
    if not tag in items:
            items.append(tag)
    for item in tags[tag]:
        if not item in items:
            items.append(item)


file = open(outputFile, "w")
file.write(json.dumps(items, indent=4))
file.close()