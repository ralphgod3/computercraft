import json
import os
from os.path import isfile, join


folder = "extracted"

#get json from file
def GetJsonFromFile(path):
    file = open(path, "r")
    j = json.load(file)
    file.close()
    return j

#get all tags from a mod folder
def GetTagsFromFolder(baseName, path):
    if not os.path.exists(path + "\\items"):
        return
    itemFolder = os.listdir(path + "\\items")
    tags = {}
    for item in itemFolder:
        if item.endswith(".json"):
            tagName = item.replace(".json", "")
            loaded = GetJsonFromFile(path + "\\items\\" + item)
            vals =  loaded["values"]
            for val in vals:
                if isinstance(val, dict):
                    val = val["id"]
                if not val.startswith("#"):
                    if not baseName + ":" + tagName in tags:
                        tags[baseName + ":" + tagName] = []
                    tags[baseName + ":" + tagName].append(val)
        else:
            items = os.listdir(path + "\\items\\" + item)
            for i in items:
                if i.endswith(".json"):
                    tagName = i.replace(".json", "")
                    loaded = GetJsonFromFile(path + "\\items\\" + item + "\\" + i)
                    vals =  loaded["values"]
                    for val in vals:
                        if isinstance(val, dict):
                            val = val["id"]
                        if not val.startswith("#"):
                            name = baseName + ":" + item + "/" + tagName
                            if not name in tags:
                                tags[name] = []
                            tags[baseName + ":" + item + "/" + tagName].append(val)
    return tags




# get information on all the mods in the extracted folder
def GetModPaths(folder):
    path = os.path.abspath(folder)
    mods = os.listdir(folder)
    allFolders = []
    for mod in mods:
        print("found mod " + mod)
        for dirs in os.listdir(folder + "\\" + mod):
            print("found folder " + dirs)
            allFolders.append(path + "\\" + mod + "\\" + dirs)
    return allFolders

#do the thing
masterTagList = {}
folders = GetModPaths(folder)
for folder in folders:
    tags = GetTagsFromFolder(folder[folder.rfind("\\") + 1:] , folder + "\\" + "tags")
    if tags:
        for tag in tags:
            print(tag)
            if not tag in masterTagList:
                masterTagList[tag] = []
            for t in tags[tag]:
                masterTagList[tag].append(t)


tagfile = open("tags.json", "w")
tagfile.write(json.dumps(masterTagList, indent=4))
tagfile.close()