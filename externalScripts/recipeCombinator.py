import json
import os
from os.path import isfile, join


folder = "extracted"

#get json from file
def GetJsonFromFile(path):
    file = open(path, "r")
    j = json.load(file)
    file.close()
    return j

#get all tags from a mod folder
def GetRecipesFromFolder(path):
    if not os.path.exists(path):
        return
    itemFolder = os.listdir(path)
    recipes = {}
    for item in itemFolder:
        if item.endswith(".json"):
            loaded = GetJsonFromFile(path+ "\\" + item)
            if not "type" in loaded or (loaded["type"] != "minecraft:crafting_shapeless" and loaded["type"] != "minecraft:crafting_shaped"):
                continue
            recipe = {}
            if "ingredients" in loaded:
                recipe["ingredients"] = loaded["ingredients"]
            if "key" in loaded:
                recipe["key"] = loaded["key"]
            if "pattern" in loaded:
                recipe["pattern"] = loaded["pattern"]
            if "result" in loaded:
                if "count" in loaded["result"]:
                    recipe["count"] = loaded["result"]["count"]
            name = loaded["result"]["item"] 
            if not name in recipes:
                recipes[name] = []
            recipes[name].append(recipe)
    return recipes




# get information on all the mods in the extracted folder
def GetModPaths(folder):
    path = os.path.abspath(folder)
    mods = os.listdir(folder)
    allFolders = []
    for mod in mods:
        print("found mod " + mod)
        for dirs in os.listdir(folder + "\\" + mod):
            allFolders.append(path + "\\" + mod + "\\" + dirs)
    return allFolders

#do the thing
masterRecipeList = {}
folders = GetModPaths(folder)
for folder in folders:
    recipes = GetRecipesFromFolder(folder + "\\" + "recipes")
    if recipes:
        for recipe in recipes:
            if not recipe in masterRecipeList:
                masterRecipeList[recipe] = []
            for r in recipes[recipe]:
                masterRecipeList[recipe].append(r)

files = {}

for recipe in masterRecipeList:
    modName = recipe[0 : recipe.find(":")]
    print(recipe)
    if not modName in files:
        files[modName] = {}
    files[modName][recipe] = masterRecipeList[recipe]


if not os.path.exists("recipes"):
    os.makedirs("recipes")

for f in files:
    recipeFile = open("recipes/" + f + ".json", "w")
    recipeFile.write(json.dumps(files[f], indent=4))
    recipeFile.close()