import zipfile
import os
from os.path import isfile, join


def Extract(path, file):
    print("extracting from ", path + "\\" + file)
    zfile = zipfile.ZipFile(path + "\\" + file, "r" )
    extractPath = os.getcwd().replace("\\","/") + "/" + "extracted"
    for info in zfile.infolist():
        fname = info.filename
        if fname.startswith("data/") and fname.endswith(".json") and ("recipes" in fname or "tags" in fname) and not ("loot_table" in fname or "advancements" in fname):
            
            data = zfile.read(fname)
            filename = fname.replace("data/", "")
            loc = filename.rfind("/")
            pathToCheck = extractPath + "/" + file + "/" + filename[0: loc]
            if not os.path.exists(pathToCheck):
                os.makedirs(pathToCheck)
            if os.path.exists(extractPath + "/" + file + "/" + filename):
                print("already exists " + filename)
            fout = open(extractPath + "/" + file + "/" + filename, "wb")
            fout.write(data)
            fout.close()


print("put jar files in /toExtract")
path = os.path.abspath("toExtract")
files = os.listdir("toExtract")
for file in files:
    Extract(path, file)
print("done extracting")