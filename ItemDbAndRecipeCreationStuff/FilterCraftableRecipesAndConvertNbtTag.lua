
-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
local Utils = require("Modules.Utils")




local recipeFile = "convertedRecipes.json"
local nbtMappingFile = "nbtMappings.json"
local failedItemFile = "failedItems.json"
local validRecipeFile = "recipes.json"
local logLocation = "converterLogs.txt"

local logs = {}
local function printToLog(text)
    print(text)
    table.insert(logs, tostring(text) .. "\r\n")
end

local function saveLog(filePath)
    local file = fs.open(filePath, "w")
    for _, value in ipairs(logs) do
        file.write(value)
    end
    file.close()
end

local function loadFile(filePath)
    local file = fs.open(filePath, "r")
    if file == nil then
        error(filePath .. " does not exist")
    end
    local out = file.readAll()
    file.close()
    out = textutils.unserializeJSON(out)
    if out == nil then
        error(filePath .. " is corrupted")
    end
    return out
end

local function entryHasFailedItem(entry, failedItems)
    if Utils.findElementInTable(failedItems, entry.name) ~= nil then
        return true
    end
    return false
end

local function removeFailedItemsFromRecipe(recipe, failedItems)
    --check if output is the item that failed to spawn, if it is return nil
    if entryHasFailedItem(recipe, failedItems) then
        return nil
    end
    for index, rec in pairs(recipe.recipes) do
        local hasFailedItem = false
        for _, itemEntry in pairs(rec.recipe) do
            if entryHasFailedItem(itemEntry, failedItems) then
                hasFailedItem = true
                break
            end
        end
        if hasFailedItem then
            recipe.recipes[index] = nil
        end
    end
    recipe.recipes = Utils.rebuildArray(recipe.recipes)
    if #recipe.recipes == 0 then
        return nil
    end
    return recipe
end

local function entryHasNbtUnhashed(entry)
    if entry.nbtUnhashed ~= nil then
        return true
    end
    return false
end

local function findNbtMapping(name, nbtUnhashed, nbtMappings)
    local nbtVal = nbtUnhashed
    if type(nbtVal) ~= "string" then
        nbtVal = textutils.serializeJSON(nbtUnhashed)
    end
    for key, value in pairs(nbtMappings) do
        if value.name == name then
            if value.nbtUnhashed == nbtVal then
                return value
            end
        end
    end
    printToLog("could not find nbt " .. tostring(nbtVal))
    return nil
end

local function replaceNbtUnhashedWithNbtEntry(recipe, nbtMappings)
    --check if output is the item that failed to spawn, if it is return nil
    if entryHasNbtUnhashed(recipe) then
        local nbtMapping = findNbtMapping(recipe.name, recipe.nbtUnhashed, nbtMappings)
        if nbtMapping == nil then
            printToLog("did not find nbt mapping for " .. recipe.name)
            return nil
        end
        recipe.nbtUnhashed = nil
        recipe.nbt = nbtMapping.nbt
    end
    for index, rec in pairs(recipe.recipes) do
        local hasFailedEntry = false
        for _, itemEntry in pairs(rec.recipe) do
            if entryHasNbtUnhashed(itemEntry) then
                local nbtMapping = findNbtMapping(itemEntry.name, itemEntry.nbtUnhashed, nbtMappings)
                if nbtMapping == nil then
                    printToLog("did not find nbt mapping for " .. itemEntry.name)
                    hasFailedEntry = true
                    break
                end
                itemEntry.nbtUnhashed = nil
                itemEntry.nbt = nbtMapping.nbt
            end
        end
        if hasFailedEntry then
            recipe.recipes[index] = nil
        end
    end
    recipe.recipes = Utils.rebuildArray(recipe.recipes)
    if #recipe.recipes == 0 then
        return nil
    end
    return recipe
end


printToLog("loading recipes from " .. recipeFile)
local recipes = loadFile(recipeFile)
printToLog("loading failed items from " .. failedItemFile)
local tempFailedItems = loadFile(failedItemFile)
--flatten failedItems table
local failedItems = {}
for key,value in pairs(tempFailedItems) do
    table.insert(failedItems, value.name)
end
printToLog("loading nbt mappings from " .. nbtMappingFile)
local nbtMappings = loadFile(nbtMappingFile)

printToLog("removing recipes that have unspawnable items")
for key, recipe in pairs(recipes) do
    local tempRecipe = removeFailedItemsFromRecipe(recipe, failedItems)
    if tempRecipe ~= nil then
        recipes[key] = tempRecipe
    else
        printToLog("removed recipe for " .. recipe.name)
        recipes[key] = nil
    end
    Utils.yield()
end
printToLog("\r\n\r\n")
printToLog("converting nbtUnhashed to nbt entries")
for key, recipe in pairs(recipes) do
    local tempRecipe = replaceNbtUnhashedWithNbtEntry(recipe, nbtMappings)
    if tempRecipe ~= nil then
        recipes[key] = tempRecipe
    else
        printToLog("removed recipe for " .. recipe.name .. " based on nbt")
        recipes[key] = nil
    end
    Utils.yield()
end

local file = fs.open(validRecipeFile, "w")
file.write(textutils.serializeJSON(recipes))
file.close()
printToLog("saved recipes in " .. validRecipeFile)

printToLog("logs saved in " .. logLocation)
saveLog(logLocation)