-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("/Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
local Utils = require("Modules.Utils")
local ItemDb = require("Modules.ItemDb")


local delayBetweenSummons = 0.1
local itemsFailedToSpawnFile = "failedItems.json"
local itemsToSpawnFile = "items.json"
local nbtMappingFile = "nbtMappings.json"
local summonCommand = "summon minecraft:item %d %d %d {Item:{id:\"%s\", Count: 1%s}}"
local spawnHeightAboveComputer = 2

local function createNbtMappingEntry(itemDetails, nbtUnhashed)
    local entry = {}
    entry.name = itemDetails.name
    entry.nbtUnhashed = nbtUnhashed
    entry.nbt = itemDetails.nbt
    return entry
end

--setup variables
ItemDb = ItemDb.new(false)
local chest = peripheral.find("minecraft:chest")

local turtle = ""
for _, turtlePeripheral in pairs({peripheral.find("turtle")}) do
    local turtleName = peripheral.getName(turtlePeripheral)
    if Utils.findElementInTable(rs.getSides(),turtleName) == nil then
        turtle = turtleName
        break
    end
end
if turtle == "" then
    error("No turtle found to push items into")
end
local file = fs.open(itemsToSpawnFile,"r")
if not file then
    error("file not found " .. itemsToSpawnFile)
end
local items = file.readAll()
file.close()
items = textutils.unserialiseJSON(items)

local file = fs.open(nbtMappingFile, "r")
local nbtMappings = {}
if file ~= nil then
    nbtMappings = file.readAll()
    file.close()
    nbtMappings = textutils.unserialiseJSON(nbtMappings)
end
if nbtMappings == nil then
    nbtMappings = {}
end


local failedItems = {}
file = fs.open(itemsFailedToSpawnFile, "r")
if file ~= nil then
    failedItems = file.readAll()
    file.close()
    failedItems = textutils.unserialiseJSON(failedItems)
end
if failedItems == nil then
    failedItems = {}
end


--prepare turtle and chest
local x,y,z = commands.getBlockPosition()
y = y + spawnHeightAboveComputer
for slot,_ in pairs(chest.list()) do
    chest.pushItems(turtle,slot)
end
local itemStr = "#%d name %s"
--start spawning items
print("got " .. tostring(#items))
write("enter item to start from, defaults to 1: ")
local startLoc = tonumber(read())
if startLoc == nil then
    startLoc = 1
end
for i = startLoc,#items do
    print(itemStr:format(i, items[i].name))
    local nbtUnhashed = ""
    local command = ""
    if items[i].nbtUnhashed == nil then
        command = summonCommand:format(x, y , z, items[i].name, "")
    elseif type(items[i].nbtUnhashed) == "string" then
        nbtUnhashed = items[i].nbtUnhashed
        command = summonCommand:format(x, y , z, items[i].name, ", tag: " .. items[i].nbtUnhashed)
    else
        local nbt = textutils.serializeJSON(items[i].nbtUnhashed)
        nbtUnhashed = nbt
        command = summonCommand:format(x, y , z, items[i].name, ", tag: " .. nbt)
    end
    if (nbtUnhashed ~= "") or (nbtUnhashed == "" and not ItemDb.itemIsInDb(items[i].name)) then
        local _, feedback = commands.exec(command)
        if string.find(feedback[1], "Summoned new Air", nil, true) == nil then
            local detail = nil
            repeat
                detail = chest.getItemDetail(1)
                sleep(delayBetweenSummons)
            until detail ~= nil
            ItemDb.addItem(detail)
            if nbtUnhashed ~= "" then
                table.insert(nbtMappings, createNbtMappingEntry(detail, nbtUnhashed))
                local file = fs.open(nbtMappingFile, "w")
                file.write(textutils.serializeJSON(nbtMappings))
                file.close()
            end
            chest.pushItems(turtle, 1)
            chest.pushItems(turtle, 2)
        else
            table.insert(failedItems, items[i])
            local file = fs.open(itemsFailedToSpawnFile, "w")
            file.write(textutils.serializeJSON(failedItems))
            file.close()
        end
    end
end



