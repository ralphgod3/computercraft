-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "/cache/Energymonitor.cache"

-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
    "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("/Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local Dbg = require("Modules.Logger")
Threads = Threads.new()
Timer = Timer.new(Threads)

if not fs.exists(configFile) then
    local config = {}
    config.pollTime = 5
    config.redstoneSides = { "bottom", "top", "front", "back", "left", "right" }
    config.StartRedstoneEmitPercentage = 50
    config.StopRedsonteEmitePercentage = 95
    config.RedstoneStateAtStartup = false
    config.types = { "mekanism:basic_energy_cube", "mekanism:advanced_energy_cube", "mekanism:elite_energy_cube",
        "mekanism:ultimate_energy_cube" }
    local file = fs.open(configFile, "w")
    file.write(textutils.serialize(config))
    file.close()
    print("created config " .. configFile .. " edit it and restart program")
    return
end


local wrappedEnergy = {}
local totalStorage = 0

local file = fs.open(configFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialise(config)
if config == nil then
    error("invalid config please fix")
end

print("wrapping inventories")
for k, v in pairs(config.types) do
    local found = table.pack(peripheral.find(v))
    print("found " .. tostring(found.n) .. " inventories of type " .. v)
    for i = 1, found.n do
        wrappedEnergy[peripheral.getName(found[i])] = found[i]
    end
end

print("calculating total energy storage")
for k, v in pairs(wrappedEnergy) do
    totalStorage = totalStorage + v.getEnergyCapacity()
end
print("total storage " .. tostring(totalStorage))

--main function that handles monitoring
local function Handle()
    local curEnergy = 0
    for k, v in pairs(wrappedEnergy) do
        curEnergy = curEnergy + v.getEnergy()
    end

    local curPercentage = (curEnergy / totalStorage) * 100
    if curPercentage < config.StartRedstoneEmitPercentage then
        for k, v in pairs(config.redstoneSides) do
            rs.setOutput(v, true)
        end
    elseif curPercentage > config.StopRedsonteEmitePercentage then
        for k, v in pairs(config.redstoneSides) do
            rs.setOutput(v, false)
        end
    end
    print("current: " .. tostring(curPercentage))
    Timer.add(config.pollTime, Handle)
end




for k, v in pairs(config.redstoneSides) do
    rs.setOutput(v, config.RedstoneStateAtStartup)
end
Handle()
Threads.startRunning()
