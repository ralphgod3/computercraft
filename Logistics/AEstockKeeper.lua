-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "cache/AEstockKeeper.cache"

-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
        "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/ListBox.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/ListBoxItem.lua")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local Dbg = require("Modules.Logger")
local advWindow = require("Gui.advWindow")
local GuiUtils = require("Gui.GuiUtils")
local Listbox = require("Gui.Elements.ListBox")
local lbItem = require("Gui.Elements.ListBoxItem")



Threads = Threads.new()
Timer = Timer.new(Threads)
local TAG = "Crafter"
Dbg.setLogLevel(TAG, Dbg.Levels.Info)
Dbg.setOutputTerminal(term.current())

--check if config exists if it does not download default from internet and inform user
if not fs.exists(configFile) then
    local config = Git.downloadFileFromGit(Git.ComputerCraftProjectID, "DataFiles/AEStock.json")
    local file = fs.open(configFile, "w")
    file.write(config)
    file.close()
    print("created config " .. configFile .. " edit it and restart program")
    return
end

--read config
local file = fs.open(configFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialiseJSON(config)
if config == nil then
    print("corrupt config please fix")
    return
end



-- init hardware
local bridge = peripheral.find("meBridge")
--display = peripheral.find("monitor")
if bridge == nil then
    print("no me bridge connected to computer")
    return
end

--if display == nil then
--print("no monitor connected to computer")
--return
--end
if config.items == nil then
    print("invalid config, please fix")
    return
end
for k, v in pairs(config.items) do
    if v.name == nil or v.count == nil or v.batchSize == nil then
        print("invalid entry in config: " .. Utils.printTableRecursive(v))
        return
    end
end

---search for item amount in list of ae items
---@param ItemToFind string item to find
---@param list table list of ae items
---@return number amount amount of items in list
local function FindItemCountInAEList(ItemToFind, list)
    for k, v in pairs(list) do
        if v.name == ItemToFind then
            return v.amount
        end
    end
    return 0
end

---check if item can be crafted by attached ae system
---@param ItemToCheck string item to check for
---@param craftableList table list of craftable ae items
---@return boolean itemCraftable
local function ItemIsCraftableByAe(ItemToCheck, craftableList)
    for k, v in pairs(craftableList) do
        if v.name == ItemToCheck then
            return true
        end
    end
    return false
end



local function createMenu(monitor)
    monitor.setTextScale(2)
    local sizeX, sizeY = monitor.getSize()
    local window = advWindow.new(monitor, 1, 1, sizeX, sizeY, true)
    window.addUiElement("ListBox",
        Listbox.new(window, 1, 1, sizeX, sizeY, nil, nil, nil, nil, GuiUtils.drawTextAlignmentTopLeft))
    window.setBackgroundColor(colors.cyan)
    window.setVisible(true)
    window.draw(true)
    return window
end

local function lbItemToStr(self, width)
    local retstr = self.dispName
    local suffix = tostring(self.got) .. "/" .. tostring(self.want)
    local len = string.len(retstr) + string.len(suffix)
    if len < width then
        for _ = 1, width - len do
            retstr = retstr .. " "
        end
    end
    retstr = retstr .. suffix
    return retstr
end

---@class LbItemInternal
---@field name string
---@field dispName string
---@field want number
---@field got number
---@field crafting boolean


---sorts table from least available to most available
---@param a LbItemInternal
---@param b LbItemInternal
---@return boolean
local function sortTable(a, b)
    return a.got / a.want < b.got / b.want
end

local function updateGui(window)
    ---@type ListBoxItem[]
    local itemTable = {}
    for k, v in pairs(config.items) do
        local items = bridge.listItems()
        local InStock = FindItemCountInAEList(v.name, items)
        local colLoc = string.find(v.name, ":", nil, true)
        colLoc = colLoc or 0
        if colLoc ~= 0 then
            colLoc = colLoc + 1
        end
        local item = lbItem.new(
            { name = v.name, dispName = string.sub(v.name, colLoc), want = v.count, got = InStock, crafting = false, },
            nil,
            nil,
            lbItemToStr)
        table.insert(itemTable, item)
    end
    table.sort(itemTable, sortTable)
    window.UiElements["ListBox"].setItemTable(itemTable)
    Timer.add(1, updateGui, window)
end

--- main function
local function main()
    Dbg.logI(TAG, "performing item check")
    local items = bridge.listItems()
    local craftable = bridge.listCraftableItems()
    for k, v in pairs(config.items) do
        local InStock = FindItemCountInAEList(v.name, items)
        --Dbg.logV(TAG, v.name, "found", InStock)
        if InStock < v.count then
            Dbg.logV(TAG, "checking if item is craftable: ", v.name)
            local itemIsCraftable = ItemIsCraftableByAe(v.name, craftable)
            local isCrafting = bridge.isItemCrafting({ name = v.name })
            Dbg.logV(TAG, "item craftable, AE info: ", itemIsCraftable, ",being crafted: ", isCrafting)
            if itemIsCraftable and not isCrafting then
                Dbg.logV(TAG, "item craftable")
                local toCraft = math.min(v.count - InStock, v.batchSize)
                bridge.craftItem({ name = v.name, count = toCraft })
                Dbg.logI(TAG, "crafting " .. tostring(toCraft) .. " of " .. v.name)
            end
        end
    end
    Timer.add(config.polInterval, main)
end

-- create monitor menu
local mon = peripheral.find("monitor")
if mon then
    local window = createMenu(mon)
    updateGui(window)
end


main()
Threads.startRunning()
