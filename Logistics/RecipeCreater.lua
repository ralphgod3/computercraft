-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "cache/SimpleCrafter.cache"
-- load git
if not fs.exists("/Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("/Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/FileBasedRecipeDatabase.lua")

local Threads = require("Modules.Threads")
local Dbg = require("Modules.Logger")
local recipeDb = require("Modules.Crafting.FileBasedRecipeDatabase")
recipeDb = recipeDb.new(nil)

---------------------------------------- GLOBAL VARIABLES ----------------------------------------
Threads = Threads.new()
Dbg.setOutputTerminal(term.current())
local TAG = "RC"

---generates crafting recipe according to proper definition from turtle inventory,
---@note result slot should be 16
---@param turtleInventory table[]
---@return CraftingRecipe?
local function generateCraftingRecipe(turtleInventory)
	local turtleSlotLookupTable = { 1, 2, 3, nil, 4, 5, 6, nil, 7, 8, 9, nil, }
	local turtleSlotLookupTableReversed = { 1, 2, 3, 5, 6, 7, 9, 10, 11 }
	if turtleInventory[16] == nil then
		return nil
	end
	local hasInputItem = false
	for i = 1, 9 do
		if turtleInventory[turtleSlotLookupTableReversed[i]] ~= nil then
			hasInputItem = true
			break
		end
	end
	if not hasInputItem then
		return nil
	end

	local result = turtleInventory[16]
	turtleInventory[16] = nil
	local outRecipe = {}
	outRecipe["type"] = "item"
	outRecipe["recipes"] = {}
	outRecipe["name"] = result["name"]
	local rec = {}
	rec["type"] = "minecraft:crafting"
	rec["count"] = result.count
	rec["recipe"] = {}
	for slot, info in pairs(turtleInventory) do
		local slotEntry = {
			type = "item",
			count = 1,
			name = info.name
		}
		rec["recipe"][tostring(turtleSlotLookupTable[slot])] = slotEntry
	end
	table.insert(outRecipe["recipes"], rec)

	return outRecipe
end

---------------------------------------- FUNCTIONS ----------------------------------------

---------------------------------------- MAIN ----------------------------------------


local items = {}
for i = 1, 16 do
	Threads.create(function()
		items[i] = turtle.getItemDetail(i)
	end)
end
Threads.startRunning()
local recipe = generateCraftingRecipe(items)
if recipe == nil then
	Dbg.logE(TAG, "no valid recipe found")
	Dbg.logE(TAG, "put recipe output in slot 16 and items in slot")
	Dbg.logE(TAG, "1,2,3")
	Dbg.logE(TAG, "5,6,7")
	Dbg.logE(TAG, "9,10,11")
else
	Dbg.logI(TAG, "recipe added")
	recipeDb.addRecipe(recipe)
end
