-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "/cache/EnergizingOrb.cache"

-- load git
if not fs.exists("/Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("/Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local Dbg = require("Modules.Logger")
Threads = Threads.new()
Timer = Timer.new(Threads)

if not fs.exists(configFile) then
	local config = {}
	config.pollTime = 1
	config.InputChestType = "ironchest:diamond_chest"

	config.recipes = {
		{ "minecraft:nether_star", "minecraft:redstone_block", "minecraft:redstone_block",
			"powah:blazing_crystal_block", },
		{ "minecraft:emerald", },
		{ "minecraft:snowball", },
		{ "minecraft:blue_ice",     "minecraft:blue_ice", },
		{ "minecraft:blaze_rod", },
		{ "minecraft:blaze_powder", "minecraft:blaze_powder",  "minecraft:blaze_powder",     "minecraft:blaze_powder", },
		{ "minecraft:iron_ingot",   "minecraft:gold_ingot", },
		{ "minecraft:diamond", },
		{ "minecraft:ender_eye",    "powah:dielectric_casing", "powah:capacitor_basic_tiny", },
	}

	local file = fs.open(configFile, "w")
	file.write(textutils.serialize(config))
	file.close()
	print("created config " .. configFile .. " edit it and restart program")

	return
end

local file = fs.open(configFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialise(config)

if config == nil then
	error("invalid config please fix")
end

local orb = peripheral.find("powah:energizing_orb")
local chest = peripheral.find(config.InputChestType)

if orb == nil or chest == nil then
	print("could not find chest or energizing orb")

	return
end

local function CountInInventory(itemToCheck, inventoryList)
	local count = 0

	for k, v in pairs(inventoryList) do
		if v.name == itemToCheck then
			count = count + v.count
		end
	end

	return count
end

local function RecipeIsCraftable(recipe, itemsAvailable)
	local transformedTable = {}

	for k, v in pairs(itemsAvailable) do
		transformedTable[v.name] = CountInInventory(v.name, itemsAvailable)
	end

	local itemsNeeded = {}

	for k, v in pairs(recipe) do
		if itemsNeeded[v] == nil then
			itemsNeeded[v] = 0
		end

		itemsNeeded[v] = itemsNeeded[v] + 1
	end

	for k, v in pairs(itemsNeeded) do
		if transformedTable[k] == nil or transformedTable[k] < v then return false end
	end

	return true
end

local function Handle()
	local items = orb.list()
	local chestItems = chest.list()
	local orbName = peripheral.getName(orb)

	if Utils.getTableLength(chestItems) > 0 and Utils.getTableLength(items) == 0 then
		for k, v in pairs(config.recipes) do
			if RecipeIsCraftable(v, chestItems) then
				for key, item in pairs(v) do
					for slot, itemInfo in pairs(chest.list()) do
						if itemInfo.name == item then
							chest.pushItems(orbName, slot, 1)
							break
						end
					end
				end

				print("crafting ")
				break
			end
		end
	end

	Timer.add(config.pollTime, Handle)
end

Handle()
Threads.startRunning()
