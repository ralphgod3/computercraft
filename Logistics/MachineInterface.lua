-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "cache/MachineInterface.cache"
local machineFile = "DataFiles/MachineTypes.json"

-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
        "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("/Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/MachineInterfaceEntry.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")

local MachineInterfaceEntry = require("Modules.Parsers.MachineInterfaceEntry")
local InventoryConfig = require("Modules.Parsers.InventoryConfig")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local ItemInventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local Dbg = require("Modules.Logger")

---------------------------------------- GLOBAL VARIABLES ----------------------------------------
Threads = Threads.new()
Timer = Timer.new(Threads)
Dbg.setOutputTerminal(term.current())
local TAG = "MI"
---------------------------------------- FUNCTIONS ----------------------------------------

local function createConfigFile()
    local config = textutils.unserialiseJSON(Git.downloadFileFromGit(Git.ComputerCraftProjectID, machineFile))
    local i = 1
    local choices = {}
    Dbg.logI("select machine to interface with")
    for k, _ in pairs(config) do
        print("[" .. tostring(i) .. "] " .. tostring(k))
        table.insert(choices, k)
        i = i + 1
    end
    local machine = nil
    while true do
        term.write("choice: ")
        local input = read()
        input = tonumber(input)
        if input ~= nil and input <= i and input > 0 then
            machine = config[choices[tonumber(input)]]
            break
        end
    end
    local config = "--Currently does NOT support recipe based machine definitions.\r\n"
    config = config .. InventoryConfig.createExplanation()
    config = config .. MachineInterfaceEntry.createExplanation()
    config = config .. "\r\n\r\n"
    local unserializedConf = {}
    unserializedConf.pollTime = 5
    unserializedConf.machines = {}
    unserializedConf.machines[1] = machine

    config = config .. textutils.serialize(unserializedConf)
    local file = fs.open(configFile, "w")
    file.write(config)
    file.close()
    Dbg.logI("created config " .. configFile .. " edit it and restart program")
end

---list all inventories for machine in parallel
---@param machine table machineInfo
---@param wrappedInventories IitemInventory[] wrapped inventories
---@return table<string, table<number, ItemEntry>> inventoryList list<inventoryName>.list()
local function listMachine(machine, wrappedInventories)
    local list = {}
    local countingSem = Utils.createCountingSemaphore()

    if machine.inputs ~= nil then
        for _, v in pairs(machine.inputs) do
            Utils.takeCountingSemaphore(countingSem)
            Threads.create(function()
                list[v.inventory] = wrappedInventories[v.inventory].list()
                Utils.freeCountingSemaphore(countingSem)
            end)
        end
    end
    if machine.outputs ~= nil then
        for _, v in pairs(machine.outputs) do
            Utils.takeCountingSemaphore(countingSem)
            Threads.create(function()
                list[v.inventory] = wrappedInventories[v.inventory].list()
                Utils.freeCountingSemaphore(countingSem)
            end)
        end
    end
    for _, v in pairs(machine.machineNames) do
        Utils.takeCountingSemaphore(countingSem)
        Threads.create(function()
            list[v] = wrappedInventories[v].list()
            Utils.freeCountingSemaphore(countingSem)
        end)
    end
    Utils.awaitCountingSemaphore(countingSem)
    return list
end

local function createOutputMove(inventoryList, machineNames, output)
    local moves = {}
    for _, machineName in pairs(machineNames) do
        if inventoryList[machineName][output.slot] ~= nil then
            local moveEntry = ItemInventoryUtils.createMovePlanEntry(output.slot, 1,
                inventoryList[machineName][output.slot].count)
            moveEntry.fromInventory = machineName
            moveEntry.toInventory = output.inventory
            table.insert(moves, moveEntry)
        end
    end
    return moves
end

local function createInputMove(inventoryList, machineNames, input)
    local moves = {}
    for _, machineName in pairs(machineNames) do
        local toMoveEntry = {}
        toMoveEntry.count = input.itemsToKeepStocked
        if inventoryList[machineName][input.slot] ~= nil then
            toMoveEntry.count = input.itemsToKeepStocked - inventoryList[machineName][input.slot].count
            toMoveEntry.name = inventoryList[machineName][input.slot].name
            toMoveEntry.damage = inventoryList[machineName][input.slot].damage
            toMoveEntry.nbt = inventoryList[machineName][input.slot].nbt
        end
        if toMoveEntry.count > 0 then
            local moveEntries = ItemInventoryUtils.createSimpleMovePlan(inventoryList[input.inventory], toMoveEntry,
                input.slot)
            for _, entry in pairs(moveEntries) do
                entry.fromInventory = input.inventory
                entry.toInventory = machineName
                table.insert(moves, entry)
            end
        end
    end
    return moves
end

---handles machine type
---@param machine table machineInfo
---@param time number timer before repolling
---@param wrappedInventories IitemInventory[] inventories
local function handleMachine(machine, time, wrappedInventories)
    --todo fix this print
    Dbg.logI(TAG, "moving for", string.sub(machine.machineNames[1], 1, string.len(machine.machineNames[1]) - 3),
        os.clock())
    local list = listMachine(machine, wrappedInventories)
    local moves = {}
    if machine.inputs ~= nil then
        --create input moves
        for _, v in pairs(machine.inputs) do
            local tempMoves = createInputMove(list, machine.machineNames, v)
            for _, move in pairs(tempMoves) do
                table.insert(moves, move)
            end
        end
    end
    if machine.outputs ~= nil then
        --create output moves
        for _, v in pairs(machine.outputs) do
            local tempMoves = createOutputMove(list, machine.machineNames, v)
            for _, move in pairs(tempMoves) do
                table.insert(moves, move)
            end
        end
    end

    --perform all moves
    local countingSem = Utils.createCountingSemaphore()
    for _, move in pairs(moves) do
        Utils.takeCountingSemaphore(countingSem)
        Threads.create(function()
            wrappedInventories[move.fromInventory].pushItems(wrappedInventories[move.toInventory], move.fromSlot,
                move.count, move.toSlot)
            Utils.freeCountingSemaphore(countingSem)
        end)
    end
    Utils.awaitCountingSemaphore(countingSem)

    Timer.add(time, handleMachine, machine, time, wrappedInventories)
end

---initialize machines, inputs and oututs
local function init(config)
    local machines = {}
    local wrappedInventories = {}
    for _, machine in pairs(config.machines) do
        local en = MachineInterfaceEntry.parse(machine)
        for _, machineName in pairs(en.machineNames) do
            wrappedInventories[machineName] = wrappedInventories[machineName] or
                ItemInventoryFactory.new(Threads, machineName)
        end
        if en.inputs ~= nil then
            for _, entry in pairs(en.inputs) do
                wrappedInventories[entry.inventory] = wrappedInventories[entry.inventory] or
                    ItemInventoryFactory.new(Threads, entry.inventory)
            end
        end
        if en.outputs ~= nil then
            for _, entry in pairs(en.outputs) do
                wrappedInventories[entry.inventory] = wrappedInventories[entry.inventory] or
                    ItemInventoryFactory.new(Threads, entry.inventory)
            end
        end
        table.insert(machines, en)
    end
    return machines, wrappedInventories
end




---------------------------------------- MAIN ----------------------------------------

--create config file if not exists
if not fs.exists(configFile) then
    createConfigFile()
    return
end
--load config
local file = fs.open(configFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialize(config)
if config == nil then
    Dbg.logE(TAG, "corrupt config please fix")
    return
end

--add all machine entries and wrap all nescesary inventories
Dbg.logI(TAG, "parsing config")
local machines, wrappedInventories = init(config)


--create timer for each machine type
Dbg.logI(TAG, "creating coroutines")
for _, v in pairs(machines) do
    Threads.create(handleMachine, v, config.pollTime, wrappedInventories)
end
--start the threads
Dbg.logI(TAG, "starting coroutines")
Threads.startRunning()
