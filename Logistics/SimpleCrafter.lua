-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "cache/SimpleCrafter.cache"

-- load git
if not fs.exists("/Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("/Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/FileBasedRecipeDatabase.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")

local ItemDb = require("Modules.ItemDb")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local Dbg = require("Modules.Logger")
local recipeDb = require("Modules.Crafting.FileBasedRecipeDatabase")
local inventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")

---------------------------------------- CLASS DEFINITIONS ----------------------------------------

---@class CraftingRecipeRecipeCost : CraftingRecipeRecipe
---@field cost ItemEntry[]

---@class CraftingRecipeCost : CraftingRecipe
---@field recipes CraftingRecipeRecipeCost[]

---------------------------------------- GLOBAL VARIABLES ----------------------------------------
Threads = Threads.new()
Timer = Timer.new(Threads)
Dbg.setOutputTerminal(term.current())
ItemDb = ItemDb.new(true)
recipeDb = recipeDb.new(nil)
Dbg.setGlobalLogLevel(Dbg.Levels.Info)
local TAG = "SC"
--convert slots for turtle to crafting slots
local turtleSlotLookupTable = { 1, 2, 3, 5, 6, 7, 9, 10, 11 }
---------------------------------------- FUNCTIONS ----------------------------------------

local function createConfigFile()
	local config = {}
	config.input = "minecraft:chest_0"
	config.output = "minecraft:chest_0"
	config.turtleName = "turtle_3"
	local file = fs.open(configFile, "w")
	file.write(textutils.serialize(config))
	file.close()
end

---checks if all items for a recipe are available and how many times it can be crafted
---@param recipe ItemEntry[]
---@param totals ItemEntry[]
---@return number maximumCraftable maxium times recipe can be executed
local function itemCheck(recipe, totals)
	---@type ItemEntry[]
	local totalsCopy = Utils.copyTable(totals)
	Dbg.logV(TAG, "totalsCopy", totalsCopy)
	for _, item in pairs(recipe) do
		Dbg.logV(TAG, "checking for ", item.name, "count", item.count)
		local exists = false
		for _, tItem in pairs(totalsCopy) do
			Dbg.logV(TAG, "checking against ", tItem.name, tItem.count)
			if tItem.name == item.name and tItem.damage == item.damage and tItem.nbt == item.nbt then
				if item.count < tItem.count then
					tItem.required = item.count
					exists = true
					break
				else
					Dbg.logV(TAG, "item exists but not enough available")
					return 0
				end
			end
		end
		if not exists then
			Dbg.logV(TAG, "entire table checked no match")
			return 0
		end
	end
	Dbg.logV(TAG, totalsCopy)
	Dbg.logV(TAG, "option found, checking count")
	local maxCraftable = math.huge
	for _, item in pairs(totalsCopy) do
		if item.required then
			maxCraftable = math.min(math.floor(item.count / item.required), maxCraftable)
		end
	end
	Dbg.logV(TAG, "max craftable", maxCraftable)
	return maxCraftable
end

---adds cost to all recipes
---@param recipeSet CraftingRecipe
---@return CraftingRecipeCost recipesWithCosts
local function addCostToRecipes(recipeSet)
	local recipeSetCost = {}
	for _, recipe in pairs(recipeSet) do
		for _, r in pairs(recipe.recipes) do
			r.cost = {}
			for _, rItem in pairs(r.recipe) do
				local exists = false
				for _, item in pairs(r.cost) do
					if item.name == rItem.name and item.damage == rItem.damage and item.nbt == rItem.nbt then
						exists = true
						item.count = item.count + rItem.count
						break
					end
				end
				if not exists then
					table.insert(r.cost, Utils.copyTable(rItem))
				end
			end
		end
		Dbg.logV(TAG, "recipe with cost", recipe)
		table.insert(recipeSetCost, recipe)
	end
	return recipeSetCost
end

---moves items according to a recipe into turtle ready for crafting
---@param input IitemInventory
---@param turtleInv IitemInventory
---@param recipe CraftingRecipeRecipeCost
---@param count number number of crafts to do
local function moveItemsForRecipeIntoTurtle(input, turtleInv, recipe, count)
	local itemsToMoveTable = {}
	for turtleSlot, itemInfo in pairs(recipe.recipe) do
		--start moving items
		Dbg.logV(TAG, "count", itemInfo.count)
		table.insert(itemsToMoveTable,
			{
				name = itemInfo.name,
				damage = itemInfo.damage,
				nbt = itemInfo.nbt,
				count = count * itemInfo.count,
				slot = turtleSlotLookupTable[tonumber(turtleSlot)]
			})
	end
	Dbg.logV(TAG, "itemsToMove", itemsToMoveTable)
	inventoryUtils.pushItemsByName(Threads, input, turtleInv, itemsToMoveTable)
end

---checks if items can be crafted and if they can craft them
---@param input IitemInventory input inventory
---@param output IitemInventory output inventory
---@param turtleInv IitemInventory turtle inventory
---@param recipeSet CraftingRecipeCost[] set of crafting recipes
local function doCraftCheck(input, output, turtleInv, recipeSet)
	Dbg.logI(TAG, "starting defrag")
	input.pushInventory(input, false)
	Dbg.logI(TAG, "defrag completed")

	Dbg.logI(TAG, "checking for craftable items")
	local itemList = input.list()
	local totals = inventoryUtils.transformItemList(itemList)
	for _, r in pairs(recipeSet) do
		for _, recipe in pairs(r.recipes) do
			local craftable = itemCheck(recipe.cost, totals)
			if craftable > 0 then
				--todo: deal with items that do not stack to 64 in or output
				craftable = math.min(craftable, 64)
				Dbg.logI(TAG, r.name, "x", craftable)
				moveItemsForRecipeIntoTurtle(input, turtleInv, recipe, craftable)
				turtle.craft(craftable)
				--move remaining items in turtle to output inventory
				output.pullInventory(turtleInv, false)
				itemList = input.list()
				totals = inventoryUtils.transformItemList(itemList)
			end
		end
	end
	Timer.add(2, doCraftCheck, input, output, turtleInv, recipeSet)
end

---------------------------------------- MAIN ----------------------------------------

--create config file if not exists
if not fs.exists(configFile) then
	createConfigFile()
	return
end

--load config
local file = fs.open(configFile, "r")
local config = file.readAll()
file.close()
config = textutils.unserialize(config)
if config == nil then
	Dbg.logE(TAG, "corrupt config please fix")
	return
end


local inputInv = ItemInventoryFactory.new(Threads, config.input)
local outputInv = ItemInventoryFactory.new(Threads, config.output)
local turtleInv = ItemInventoryFactory.new(Threads, config.turtleName)
local recipeSet = recipeDb.getAllRecipesForType("minecraft:crafting")
Dbg.logV(TAG, "recipeDB", recipeSet)

outputInv.pullInventory(turtleInv, false)
--doCraftCheck(inputInv, outputInv, turtleInv, addCostToRecipes(recipeSet))
Timer.add(1, doCraftCheck, inputInv, outputInv, turtleInv, addCostToRecipes(recipeSet))
Threads.startRunning()
