-- run pastebin get TNxAyWCt installer in cc computer to instal
-- if the script errors ensure the script is run from root
local programFileLocation = "DataFiles/Programs.json"

-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

--get util library
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
local Utils = require("Modules.Utils")
local programData = Git.downloadFileFromGit(Git.ComputerCraftProjectID, programFileLocation)
if programData == nil then
    if not http then
        print("this program requires the http api to work")

        return
    else
        print("could not load list with programs from git check your internet or http api blacklist")

        return
    end
end

programData = textutils.unserializeJSON(programData)
print("available programs for installation")

for k, v in pairs(programData.programs) do
    print("[" .. k .. "] " .. v.programName)
end

print("choice: ")
local input = tonumber(read())
--check user input
if input == nil then
    print("invalid choice")
    return
end

if input > #programData.programs or input < 1 then
    print("out of range")
    return
end

--download files required for the program
local programFiles = programData.programs[input].files

for _, v in pairs(programFiles) do
    if not Git.getFile(v.projectID, v.remotePath, v.localPath) then
        error("failed to download " .. v.localPath)
    end
end
local CanAddToStartup = false
for _,v in pairs(programData.programs[input].files) do
    if v.addToStartupScript then
        CanAddToStartup = true
        break
    end
end

if CanAddToStartup then
    print("program can be added to startup script")
    print("regenerate startup script? y/n")
    if read() == "y" then
        print("regenerating startup script")
        local script = "local startup = true\nif startup == false then\n\treturn\n"
        for _, v in pairs(programData.programs) do
            for _, val in pairs(v.files) do
                if val.addToStartupScript == true then
                    local name = Utils.splitString(val.localPath, ".")
                    script = script .. "elseif fs.exists(\"" .. "cache/" .. name[1] .. ".cache\") then\n"
                    script = script .. "\tshell.run(\"" .. name[1] .. "\")\n"
                end
            end
        end
        script = script .. "end"
        local file = fs.open("startup.lua","w")
        file.write(script)
        file.close()
    end
end

print("installation done")