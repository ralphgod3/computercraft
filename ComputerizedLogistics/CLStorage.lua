-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Storage/Storage.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Utils = require("Modules.Utils")
local Threads = require("Modules.Threads")
local StatusCodes = require("Modules.StatusCodes")
local Messages = require("Modules.Networking.Messages")
local Networking = require("Modules.Networking.Networking")
local NetworkingHardware = require("Modules.Networking.HW.Modems")
local Storage = require("ComputerizedLogistics.Storage.Storage")
local StorageParser = require("Modules.Parsers.InventoryConfig")
local ItemDb = require("Modules.ItemDb")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")


------------------------------------------ GLOBAL VARIABLES ---------------------------------------
local cacheFile = "/cache/CLStorage.cache"
local TAG = "CLS"
local types = {Messages.remoteTypes.StorageController}
local config = {}
local importInventory = nil
Threads = Threads.new()
Timer = Timer.new(Threads)
ItemDb = ItemDb.new()
Storage = Storage.new(ItemDb, Threads)
Dbg.setOutputTerminal(term.current())

------------------------------------------ PROGRAM FUNCTIONS ---------------------------------------

---creates default configuration file
---@param cacheFile string cache file location
local function createConfigFile(cacheFile)
    Dbg.logI(TAG, "creating config file please edit and restart program " .. cacheFile)
    local config = {}
    Networking = Networking.new(Threads, Timer, NetworkingHardware.new())
    config.modemBroadcastChannel = Networking.broadcastChannel
    config.importChest = StorageParser.createConfigEntry(StorageParser.configTypes.name)
    config.importChest.pollTime = 2
    config.ItemListUpdateInterval = 5
    config.housekeepingTime = 300
    config.name = "controller 1"
    config.storageArray = {}
    table.insert(config.storageArray, StorageParser.createConfigEntry(StorageParser.configTypes.name))
    table.insert(config.storageArray, StorageParser.createConfigEntry(StorageParser.configTypes.range))
    table.insert(config.storageArray, StorageParser.createConfigEntry(StorageParser.configTypes.type))
    config.storageArray[1].priority = 10
    config.storageArray[2].priority = 9
    config.storageArray[3].priority = 8
    config.storageArray[1].whitelist = {"minecraft:cobblestone"}
    local file = fs.open(cacheFile, "w")
    local explanation = " --[[for the storagearray a whitelist entry is not nescesary.\r\n" ..
    "if type is provided a start and stop range can be given to wrap for example only chest 6 through 8 if no range is given all inventories of that type will be wrapped]]\r\n"
    file.write(explanation)
    file.write(textutils.serialize(config))
    file.close()
end

local function verifyConfigFile(config)
    local function internalCheckConfig()
        PC.field(config, "modemBroadcastChannel", "number")
        PC.field(config, "importChest", "table")
        PC.field(config.importChest, "name", "string")
        PC.field(config.importChest, "pollTime", "number")
        PC.field(config, "ItemListUpdateInterval", "number")
        PC.field(config, "housekeepingTime", "number")
        PC.field(config, "name", "string")
        PC.field(config, "storageArray", "table")
    end
    local suc, msg = pcall(internalCheckConfig)
    if not suc then
        Dbg.logE(TAG, "problem with config")
        error(msg)
    end
end

---initialize storage array with inventories from config.storageArray
---@param storageArray table config.storageArray value
---@return number inventoriesAdded amount of inventories added to storage array
local function initializeStorage(storageArray)
    local count = 0
    for _, entry in pairs(storageArray) do
        local storages = StorageParser.parseConfigEntry(entry)
        for _, storage in ipairs(storages) do
            count = count + 1
            Storage.addInventoryToStorageArray(ItemInventoryFactory.new(Threads, storage), entry.priority, entry.whitelist)
        end
    end
    return count
end

---restacks inventories in storage array
local function DoHousekeeping(houseKeepingInterval)
    Dbg.logV(TAG, "restacking inventories")
    Storage.restack()
    Dbg.logV(TAG, "restacking complete")
    Timer.add(houseKeepingInterval, DoHousekeeping, houseKeepingInterval)
end

---send complete list of items to broadcast channel for any terminals
local function DoItemListUpdate(repeatTime)
    local msg = Messages.createItemList(Storage.dump())
    Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
    Timer.add(repeatTime, DoItemListUpdate, repeatTime)
end

local function HandleItemRequest(msg)
    local resp = {}
    Dbg.logV(TAG, "received itemRequest from " .. tostring(msg.from))
    local moved = Storage.export(msg.payload.exportInfo)
    resp = Messages.createItemRequestResponse(StatusCodes.Code.Ok, moved)
    Networking.transmit(msg.from, Networking.QOS.ATLEAST_ONCE, resp, msg.llId)
end

--handles messages received on broadcast channel
local function NetworkLoopBroadcast()
    while true do
        local msg = Networking.receive(nil, Networking.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --handle who is
            if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.ATMOST_ONCE, resp, msg.llId)
            end
        end
    end
end

--handles messages received on personal channel
local function NetworkLoopRequests()
    local msgTypes = 
    {
        Messages.messageTypes.WhoIs,
        Messages.messageTypes.ItemRequest,
        Messages.messageTypes.ItemListRequest,
        Messages.messageTypes.GetInputInventory
    }

    while true do
        local msg = Networking.receiveMsgTypes(nil, os.getComputerID(), msgTypes)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --handle who is
            if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
            --handle itemRequest
            elseif msg.payload.type == Messages.messageTypes.ItemRequest then
                Threads.create(HandleItemRequest, msg)
            --handle item list request
            elseif msg.payload.type == Messages.messageTypes.ItemListRequest then
                local resp = Messages.createItemList(Storage.dump())
                Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, resp, msg.llId)
            elseif msg.payload.type == Messages.messageTypes.GetInputInventory then
                Dbg.logV(TAG, "received request for input inventory")
                local resp = Messages.createInputInventoryResponse(config.importChest.name)
                Networking.transmit(msg.from, Networking.QOS.ATLEAST_ONCE, resp, msg.llId)
            else
                Dbg.LogE(TAG, "unknown msg " .. Utils.printTableRecursive(msg))
            end
        end
    end
end


local function DoImporting(pollTime)
    local toImport = importInventory.getDetailedItemList()
    Storage.importFromSlots(importInventory, toImport)
    Timer.add(pollTime, DoImporting, pollTime)
end

------------------------------------------ MAIN ---------------------------------------
--check if config file exists, create it if it does not
if not fs.exists(cacheFile) then
    createConfigFile(cacheFile)
    return
end

-- initialize config variables
local file = fs.open(cacheFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)

if config == nil then
    Dbg.LogE(TAG, "invalid config please fix")
    return
end
verifyConfigFile(config)

NetworkingHardware = NetworkingHardware.new(config.modemBroadcastChannel)
Networking = Networking.new(Threads, Timer, NetworkingHardware, config.modemBroadcastChannel)
os.setComputerLabel("CLStorage")

Dbg.logI(TAG, "added " .. tostring(initializeStorage(config.storageArray)) .. " to storage array")
local importInvEntry = StorageParser.parseConfigEntry(config.importChest)
importInventory = ItemInventoryFactory.new(Threads, importInvEntry[1])

Timer.add(config.importChest.pollTime, DoImporting, config.importChest.pollTime)
Timer.add(config.housekeepingTime, DoHousekeeping, config.housekeepingTime)
--item list update is timed but we want it to start the moment the controller is initialized
Threads.create(DoItemListUpdate, config.ItemListUpdateInterval)
Dbg.logI(TAG, "starting network loops")
Threads.create(NetworkLoopBroadcast)
Threads.create(NetworkLoopRequests)
Threads.startRunning()