local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Itimer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")

local Dbg = require("Modules.Logger")
local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local IitemInventory = require("Interfaces.Modules.IitemInventory")
local ItemInventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")
local IitemDb = require("Interfaces.Modules.IitemDb")
local Ithreads = require("Interfaces.Modules.Ithreads")
local Itimer = require("Interfaces.Modules.Itimer")

Dbg.setLogLevel("IOH", Dbg.Levels.Info)

---create new IOhandler
---@param threadingManager Ithreads thread manager implementation
---@param Timer Itimer timer implementation
---@param itemDb IitemDb itemDb implementation
---@param inventoryPollTime number time between polls for all inventories
---@param itemDbItemIsAddedCallbackFnc function function that is called when an item was added to the item database as a result of a inventory scan prototype void fnc(itemDbItemIsAddedCallbackFncUserParam)
---@param itemDbItemIsAddedCallbackFncUserParam any function parameters for the itemDbItemIsAddedCallbackFnc function
---@return IOHandler instance
local function new(threadingManager, Timer, itemDb, inventoryPollTime, itemDbItemIsAddedCallbackFnc,
                   itemDbItemIsAddedCallbackFncUserParam)
    TAG = "IOH"

    PC.expectInterface(1, Ithreads, threadingManager)
    PC.expectInterface(2, Itimer, Timer)
    PC.expectInterface(3, IitemDb, itemDb)
    PC.expect(4, inventoryPollTime, "number")
    PC.expect(5, itemDbItemIsAddedCallbackFnc, "function")
    Dbg.logV(TAG, "creating IOHandler with polltime of ", tostring(inventoryPollTime))

    local this = {
        ---@type IitemInventory[]
        inputs = {},
        ---@type IOHandlerOutputEntry[]
        outputs = {},
        ---@type IOHandlerOverideEntry[]
        overrides = {},
        locked = Utils.createBinarySemaphore()
    }

    -------------------------- PRIVATE FUNCTIONS --------------------------

    ---gets input inventory items in parallel
    ---@return table<number, DetailedItemEntry>[] inputItems outer array is inventory whilst inner array are item entries
    local function getInputInventoryInfo()
        Dbg.logV(TAG, "getInputInventory")
        ---@type table<number, DetailedItemEntry>[]
        local inputInfo = {}
        local countingSem = Utils.createCountingSemaphore()
        for i = 1, #this.inputs do
            Utils.takeCountingSemaphore(countingSem)
            threadingManager.create(function()
                inputInfo[i] = this.inputs[i].getDetailedItemList()
                Utils.freeCountingSemaphore(countingSem)
            end)
        end
        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(countingSem)
        end
        return inputInfo
    end

    ---checks if item exists in given filterlist
    ---@param itemName string itemId
    ---@param itemNbt string | nil item nbt data
    ---@param itemDamage number | nil item damage value
    ---@param filterList table list to find item in
    ---@return boolean itemInList true if item is in list, false otherwise
    local function itemIsInList(itemName, itemNbt, itemDamage, filterList)
        for i = 1, #filterList do
            if ItemInventoryUtils.itemsAreEqual(filterList[i], { name = itemName, nbt = itemNbt, damage = itemDamage }, true) then
                Dbg.logV(TAG, "itemIsInList true ", itemName, itemNbt)
                return true
            end
        end
        Dbg.logV(TAG, "itemIsInList false ", itemName, itemNbt)
        return false
    end

    ---returns highest priority inventory that accepts the item
    ---@param itemName string itemId
    ---@param itemNbt string | nil item nbt value
    ---@param itemDamage number | nil item damage value
    ---@return table | nil inventory inventory when an inventory can be found that accepts item, nill otherwise
    local function getHighestPriorityInventoryThatAcceptsItem(itemName, itemNbt, itemDamage)
        Dbg.logV(TAG, "getHighestPriorityInventoryThatAcceptsItem")
        local curHighest = nil
        for i = 1, #this.outputs do
            if curHighest == nil or curHighest.priority < this.outputs[i].priority then
                if (this.outputs[i].whitelist == nil or itemIsInList(itemName, itemNbt, itemDamage, this.outputs[i].whitelist))
                    and (this.outputs[i].blacklist == nil or not itemIsInList(itemName, itemNbt, itemDamage, this.outputs[i].blacklist)) then
                    curHighest = this.outputs[i]
                end
            end
        end
        return curHighest
    end

    ---checks and moves 1 item in the override table
    ---@param inputInfo table information about items that are contained in input inventories
    ---@param overrideItem table item from this.overrides table
    ---@return boolean itemMoves true if any item got moved, false otherwise
    local function moveOverrideItem(inputInfo, overrideItem)
        --get inventories that contain the item we want
        local inventoryItemInfo = {}
        for i = 1, #inputInfo do
            local slot = ItemInventoryUtils.findSlotWithItem(inputInfo[i], overrideItem)
            if slot then
                local entry = {
                    inventoryIndex = i,
                    slot = slot,
                    count = inputInfo[i][slot].count,
                }
                table.insert(inventoryItemInfo, entry)
            end
        end
        --generate move plan
        local movePlan = {}
        for i = 1, #inventoryItemInfo do
            local move = math.min(overrideItem.countLeft, inventoryItemInfo[i].count)
            table.insert(movePlan,
                { fromInventory = inventoryItemInfo[i].inventoryIndex, count = move, slot = inventoryItemInfo[i].slot })
            --subtract items from available items in input
            inputInfo[inventoryItemInfo[i].inventoryIndex][inventoryItemInfo[i].slot].count = inputInfo
                [inventoryItemInfo[i].inventoryIndex][inventoryItemInfo[i].slot].count - move
            if inputInfo[inventoryItemInfo[i].inventoryIndex][inventoryItemInfo[i].slot].count == 0 then
                inputInfo[inventoryItemInfo[i].inventoryIndex][inventoryItemInfo[i].slot] = nil
            end
            overrideItem.countLeft = overrideItem.countLeft - move
            if overrideItem.countLeft == 0 then
                break
            end
        end

        local countingSem = Utils.createCountingSemaphore()
        --move the items
        for i = 1, #movePlan do
            Utils.takeCountingSemaphore(countingSem)
            threadingManager.create(function()
                this.inputs[movePlan[i].fromInventory].pushItems(overrideItem.inventory, movePlan[i].slot,
                    movePlan[i].count)
                Utils.freeCountingSemaphore(countingSem)
            end)
        end
        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(countingSem)
        end

        if #movePlan > 0 then
            return true
        end
        return false
    end

    ---goes through entire this.overrides table and moves any items to overrides table if nescesary
    ---@param inputInfo table information about items in input inventories
    local function moveOverrideItems(inputInfo)
        for i = 1, #this.overrides do
            moveOverrideItem(inputInfo, this.overrides[i])
        end
        for i = #this.overrides, 1, -1 do
            if this.overrides[i].countLeft == 0 then
                if this.overrides[i].callbackFnc ~= nil then
                    this.overrides[i].callbackFnc(this.overrides[i].inventory.getName(), this.overrides[i].name,
                        this.overrides[i].count, this.overrides[i].nbt, this.overrides[i].damage,
                        this.overrides[i].callbackFncUserArgs)
                end
                this.overrides[i] = nil
            end
        end
        this.overrides = Utils.rebuildArray(this.overrides)
    end

    ---handles input to output inventory moves
    ---@param inventoryInfo table<number, DetailedItemEntry>
    ---@return table movePlan {{slot, toInventory},{slot, toInventory}}
    local function handleInventoryMove(inventoryInfo)
        local movePlan = {}
        for slot, itemInfo in pairs(inventoryInfo) do
            local toInventory = getHighestPriorityInventoryThatAcceptsItem(itemInfo.name, itemInfo.nbt, itemInfo.damage)
            if toInventory ~= nil then
                table.insert(movePlan, {
                    slot = slot,
                    toInventory = toInventory.inventory,
                })
            end
        end
        return movePlan
    end

    ---function that is called periodically to handle all the inventory sorting needs
    local function handle()
        Dbg.logV(TAG, "handle")
        --lock inventory modification to ensure we dont have issues with inventories being added or removed
        Utils.takeBinarySemaphore(this.locked)
        local inputInfo = getInputInventoryInfo()
        --check if item exists in item db and if not add it
        local itemDbUpdated = false
        for _, invInfo in pairs(inputInfo) do
            for _, itemInfo in pairs(invInfo) do
                if itemDb.getItem(itemInfo.name, itemInfo.nbt) == nil then
                    itemDbUpdated = true
                    itemDb.addItem(itemInfo)
                end
            end
        end
        if itemDbUpdated and itemDbItemIsAddedCallbackFnc ~= nil then
            Dbg.logV(TAG, "items added to itemDb")
            itemDbItemIsAddedCallbackFnc(itemDbItemIsAddedCallbackFncUserParam)
        end

        --move overrides first, (makes movement logic easier and the slight delay in movement should not be too bad)
        --todo: optimize for SPEED
        if #this.overrides > 0 then
            Dbg.logV(TAG, "moving overrides")
            Dbg.logV(TAG, inputInfo)
            moveOverrideItems(inputInfo)
        end
        Dbg.logV(TAG, "making moveplan for rest of items")
        Dbg.logV(TAG, inputInfo)
        --generate moveplan for normal moves
        local movePlan = {}
        for i = 1, #this.inputs do
            local tempMoves = handleInventoryMove(inputInfo[i])
            for j = 1, #tempMoves do
                local entry = {
                    fromInventory = i,
                    toInventory = tempMoves[j].toInventory,
                    slot = tempMoves[j].slot
                }
                table.insert(movePlan, entry)
            end
        end
        --execute moveplan
        local countingSem = Utils.createCountingSemaphore()
        for i = 1, # movePlan do
            Utils.takeCountingSemaphore(countingSem)
            threadingManager.create(function()
                this.inputs[movePlan[i].fromInventory].pushItems(movePlan[i].toInventory, movePlan[i].slot)
                Utils.freeCountingSemaphore(countingSem)
            end)
        end
        if not Utils.runThreadingManagerIfNeeded(threadingManager) then
            Utils.awaitCountingSemaphore(countingSem)
        end
        Utils.freeBinarySemaphore(this.locked)
        Timer.add(inventoryPollTime, handle)
        Dbg.logV(TAG, "handle done")
    end

    -------------------------- PUBLIC FUNCTIONS --------------------------

    ---add inventory as an input to the IOHandler, will add duplicates only once
    ---@param itemInventory IitemInventory implementation of IitemInventory interface
    local function addInput(itemInventory)
        PC.expectInterface(1, IitemInventory, itemInventory)
        Utils.takeBinarySemaphore(this.locked)
        local exists = false
        for i = 1, #this.inputs do
            if this.inputs[i].getName() == itemInventory.getName() then
                exists = true
                break
            end
        end
        if not exists then
            Dbg.logV(TAG, "adding new input")
            table.insert(this.inputs, itemInventory)
        end
        Utils.freeBinarySemaphore(this.locked)
    end

    ---add output inventory to IOHandler, removes old instance if new inventory with same name is added
    ---@param itemInventory IitemInventory
    ---@param priority number outputPriority higher number means that the items will go there before other inventories
    ---@param whitelist table | nil
    ---@param blacklist table | nil
    local function addOutput(itemInventory, priority, whitelist, blacklist)
        PC.expectInterface(1, IitemInventory, itemInventory)
        PC.expect(2, priority, "number", "nil")
        PC.expect(3, whitelist, "table", "nil")
        PC.expect(4, blacklist, "table", "nil")
        priority = priority or 0
        --Utils.takeBinarySemaphore(this.locked)
        local exists = false
        for i = 1, #this.outputs do
            if this.outputs[i].inventory.getName() == itemInventory.getName() then
                Dbg.logV(TAG, "output whitelist, blacklist and priority reloaded for",
                    this.outputs[i].inventory.getName())
                exists = true
                this.outputs[i].priority = priority
                this.outputs[i].whitelist = whitelist
                this.outputs[i].blacklist = blacklist
                break
            end
        end
        if not exists then
            Dbg.logV(TAG, "adding new output")
            ---@class IOHandlerOutputEntry
            local entry = {}
            entry.inventory = itemInventory
            entry.priority = priority
            entry.whitelist = whitelist
            entry.blacklist = blacklist
            table.insert(this.outputs, entry)
        end
        --Utils.freeBinarySemaphore(this.locked)
    end

    ---add temporary redirect for itemCount amount of items
    ---@param itemInventory table implementation of IitemInventory interface
    ---@param itemName string item id
    ---@param itemCount number amount of items to redirect
    ---@param itemNbt string | nil nbt of item to redirect
    ---@param itemDamage number | nil damage value of item to redirect
    ---@param callbackFnc function | nil function to call when redirect is completed called as callbackFnc(itemName, itemCount, itemNbt, itemDamage, callbackFncUserArgs)
    local function addOverride(itemInventory, itemName, itemCount, itemNbt, itemDamage, callbackFnc, callbackFncUserArgs)
        PC.expectInterface(1, IitemInventory, itemInventory)
        PC.expect(2, itemName, "string")
        PC.expect(3, itemCount, "number")
        PC.expect(4, itemNbt, "string", "nil")
        PC.expect(5, itemDamage, "number", "nil")
        PC.expect(6, callbackFnc, "function", "nil")
        Dbg.logV(TAG, "adding override ", itemName, " ", itemCount, " ", itemNbt, " ", itemDamage)
        ---@class IOHandlerOverideEntry
        local entry = {
            inventory = itemInventory,
            name = itemName,
            count = itemCount,
            countLeft = itemCount,
            nbt = itemNbt,
            damage = itemDamage,
            callbackFnc = callbackFnc,
            callbackFncUserArgs = callbackFncUserArgs
        }
        Utils.takeBinarySemaphore(this.locked)
        table.insert(this.overrides, entry)
        Utils.freeBinarySemaphore(this.locked)
    end

    ---return list of input inventories
    ---@return IitemInventory[] inputInventories
    local function getInputInventories()
        return this.inputs
    end

    ---return current IOoverride entries
    ---@return IOHandlerOverideEntry[]
    local function getOverrides()
        return this.overrides
    end

    --add handle function to timer
    Timer.add(inventoryPollTime, handle)

    ---@class IOHandler
    local retTable =
    {
        addInput = addInput,
        addOutput = addOutput,
        addOverride = addOverride,
        getOverrides = getOverrides,
        getInputInventories = getInputInventories,
    }
    return retTable
end
return { new = new }
