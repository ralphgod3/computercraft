local configFile = "cache/CLInterface.cache"

-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/ItemFilterList.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")

local Dbg = require("Modules.Logger")
local Threads = require("Modules.Threads")
local Timer = require("Modules.Timer")
local Networking = require("Modules.Networking.Networking")
local NetworkingHardware = require("Modules.Networking.HW.Modems")
local Messages = require("Modules.Networking.Messages")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local ItemInventoryParser = require("Modules.Parsers.InventoryConfig")
local ItemInventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")
local ItemFilterList = require("Modules.Parsers.ItemFilterList")
local Utils = require("Modules.Utils")
local ItemDb = require("Modules.ItemDb")


local TAG = "CLI"
Threads = Threads.new()
Timer = Timer.new(Threads)
ItemDb = ItemDb.new(false)

Dbg.setOutputTerminal(term.current())


local inputInventory = nil
local config = nil
local controllerId = nil

---handle an inventory from the config
---@param inventories table time before rechecking inventory
---@param pollTime number inventory name to check
local function handle(inventories, pollTime)
    Dbg.logI(TAG, "checking inventories")

    local itemLists = {}
    local countingSem = Utils.createCountingSemaphore()
    --get inventory itemInfo
    for i = 1, #inventories do
        Utils.takeCountingSemaphore(countingSem)
        Threads.create(function ()
            itemLists[i] = inventories[i].wrapped.list()
            Utils.freeCountingSemaphore(countingSem)
        end)
    end
    Utils.awaitCountingSemaphore(countingSem)
    --generate move and request list
    local toMove = {} -- items to move
    local toRequest = {} --items to request
    --compare items currently in inventory against items that need to be in there
    for i = 1, #itemLists do
        local name = inventories[i].wrapped.getName()
        local slotsChecked = {}
        for slot, itemInfo in pairs(itemLists[i]) do
            slotsChecked[slot] = true
            --there should not be any items in this slot
            if inventories[i].items[slot] == nil then
                toMove[i] = toMove[i] or {}
                toMove[i][slot] = itemInfo.count
            --item should be in there, check if it is the item we want
            elseif ItemInventoryUtils.itemsAreEqual(inventories[i].items[slot], itemLists[i][slot], true) then
                --item is of type we want check if its more than we want
                if itemLists[i][slot].count > inventories[i].items[slot].count then
                    toMove[i] = toMove[i] or {}
                    toMove[i][slot] = itemLists[i][slot].count - inventories[i].items[slot].count
                elseif itemLists[i][slot].count < inventories[i].items[slot].count then
                    toRequest[name] = toRequest[name] or {}
                    toRequest[name][slot] = Utils.copyTable(inventories[i].items[slot])
                    toRequest[name][slot].count = toRequest[name][slot].count - itemLists[i][slot].count
                    toRequest[name][slot].slot = slot
                end
            --item is in there but its not the one we want
            else
                toMove[i] = toMove[i] or {}
                toMove[i][slot] = itemLists[i][slot].count
                toRequest[name] = toRequest[name] or {}
                toRequest[name][slot] = Utils.copyTable(inventories[i].items[slot])
                toRequest[name][slot].slot = slot
            end
        end

        --add item requests for slots that did not get checked yet
        for slot, itemInfo in pairs(inventories[i].items) do
            if not slotsChecked[slot] then
                toRequest[name] = toRequest[name] or {}
                toRequest[name][slot] = Utils.copyTable(itemInfo)
                toRequest[name][slot].slot = slot
            end
        end
    end

    --move items out in parallel
    for invIndex, info in pairs(toMove) do
        Utils.takeCountingSemaphore(countingSem)
        Threads.create(function ()
            inventories[invIndex].wrapped.pushItemsFromSlots(inputInventory, info, true)
            Utils.freeCountingSemaphore(countingSem)
        end)
    end
    Utils.awaitCountingSemaphore(countingSem)

    --request new items
    if Utils.getTableLength(toRequest) > 0 then
        for key, inventory in pairs(toRequest) do
            toRequest[key] = Utils.rebuildArray(inventory)
        end
        Dbg.logI(TAG, "requesting items from storage")
        local msgId = Networking.transmit(controllerId, Networking.QOS.ATMOST_ONCE, Messages.createItemRequest(toRequest))
        --we dont care what is received back we just want a response to prevent spamming controller with requests
        local resp = Networking.receive(pollTime, nil, msgId)
        if resp then
            Dbg.logI(TAG, "received itemRequest response")
        end
    end
    Dbg.logI(TAG, "waiting", pollTime, "seconds")
    Timer.add(pollTime, handle, inventories, pollTime)
end


--parses config.inventories into their entries
local function parseConfigInventories(configInventories)
    if type(configInventories) ~= "table" then
        error("config.inventories of wrong type, should be table")
    end
    local outputTable = {}
    for _, invInfo in pairs(configInventories) do
        local invNames = ItemInventoryParser.parseConfigEntry(invInfo)

        local itemTbl = {}
        for k, v in pairs(invInfo.items) do
            local tempItems = ItemFilterList.parseEntry(ItemDb, v)
            if #tempItems ~= 1 then
                error("tag entries are currently not supported")
            end
            itemTbl[k] = tempItems[1]
            itemTbl[k].count = itemTbl[k].count or 64
        end

        for _, name in pairs(invNames) do
            local entry = {}
            entry.wrapped = ItemInventoryFactory.new(Threads, name)
            entry.items = itemTbl
            table.insert(outputTable, entry)
        end
    end
    return outputTable
end

--creates a default config file
local function CreateConfigFile()
    config = {}
    config.modemBroadcastChannel = 65532
    config.pollTime = 5
    config.inventories = {}

    local entry = ItemInventoryParser.createConfigEntry(ItemInventoryParser.configTypes.name)
    entry.items = {}
    entry.items[2] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    entry.items[4] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    table.insert(config.inventories, entry)
    entry = ItemInventoryParser.createConfigEntry(ItemInventoryParser.configTypes.range)
    entry.items = {}
    entry.items[3] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    entry.items[5] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    table.insert(config.inventories, entry)
    entry = ItemInventoryParser.createConfigEntry(ItemInventoryParser.configTypes.type)
    entry.items = {}
    entry.items[21] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    entry.items[42] = ItemFilterList.createSampleEntry(ItemFilterList.entryTypes.Item)
    table.insert(config.inventories, entry)
    config = textutils.serialize(config)

    local text = ItemInventoryParser.createExplanation()
    text = text .. ItemFilterList.createExplanation()
    text = text .. "--whitelists and blacklists are not supported (yet)\r\n"
    text = text .. "--this config file only supports item entries NOT tag entries\r\n\r\n"
    local file = fs.open(configFile, "w")
    file.write(text)
    file.write(config)
    file.close()
    Dbg.logI(TAG, "created config " .. configFile .. " please edit and restart program")
end

---initialize program, should be ran using threading api or networking will not function properly
local function Init(inventories, pollTime)
    while controllerId == nil do
        Dbg.logV(TAG, "requesting controller id")
        local message = Messages.createWhoIs( {Messages.remoteTypes.StorageController})
        local id = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, message)
        message = Networking.receive(5, nil, id)
        if message then
            Dbg.logV(TAG, "got controller id " .. tostring(message.from))
            controllerId = message.from
        end
    end
    while inputInventory == nil do
        Dbg.logV(TAG, "requesting input inventory from controller")
        local message = Messages.createInputInventoryRequest()
        local id = Networking.transmit(controllerId, Networking.QOS.ATMOST_ONCE, message)
        message = Networking.receive(5, nil, id)
        if message ~= nil then
            Dbg.logV(TAG, "input inventory " .. tostring(message.payload.inputInventory))
            inputInventory = message.payload.inputInventory
            inputInventory = ItemInventoryFactory.new(Threads, inputInventory)
        end
    end

    --call handle function, initializes timer on end
    handle(inventories, pollTime)
end

---------------------------------------- MAIN ----------------------------------------


if not fs.exists(configFile) then
    CreateConfigFile()
    return
end

--load config
local file = fs.open(configFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialize(config)
if config == nil then
    Dbg.logE(TAG, "corrupt config please fix")
    return
end

local inventories = parseConfigInventories(config.inventories)
if type(config.pollTime) ~= "number" then
    error("polltime should be a number")
end

Networking = Networking.new(Threads, Timer, NetworkingHardware.new(config.modemBroadcastChannel), config.modemBroadcastChannel)
os.setComputerLabel("CLInterface")
Threads.create(Init, inventories, config.pollTime)
Threads.startRunning()