# ComputerizedLogistics

## Info  

ComputerizedLogistics or CL is intended to be an early/midgame replacement for Applied Energistics or Refined Storage.  
It consists of several programs which are intended to be ran on different computers where each program provides a specific function allowing for easier expandability.  

## Features  

* Storage
* Auto crafting (both machine and crafting table recipes)
* Sorting of items for automatic processing
* AE interface like behaviour

## Limitations  

* no autocrafting for interfaces (yet)
* no wireless requesting of items (yet)
* probably worse for server performance than AE or RF due to (ab)use of coroutines to speed up item transfer
* requires running external programs to get autocrafting working as intended (no way around this)
* configuration is less user friendly than AE or RF  
* limited to 1.16 and up for now due to generic inventories in CC:tweaked  

## information about the project  

This is a set of programs I created because I wanted to make something like this from back in minecraft 1.0 but back then computercraft was not in a stage this was possible nor did I have the programming ability to do so.  
Back in 1.2.5 I think this could have been possible by using redpower and or a combination of peripheral mods but I still lacked the programming know how to pull it off.  
Recently I had some time (thanks Corona) and the knowledge to built a system like this. I however do not really follow best programming practice everywhere because if I want to do that I will go do it at work since at least then I will get paid for it.
All to say (especially to people that seem to think you must follow best practice everywhere) THIS IS A HOBBY PROJECT SHUT UP ABOUT SOMETHING NOT BEING THE BEST POSSIBLE THING EVERYWHERE.  
If something is especially bothering you you are free to make a merge request [at this repo](https://gitlab.com/ralphgod3/computercraft).  
Whilst I might be the sole contributor at the time of writing, this project is developed in collaberation with [VM](https://gitlab.com/VMSaarelainen).  

### License

It is under the I dont care what you do with the code license as long as you dont earn money from it.  
I would however like it if you mention me as the original author when redistributing parts and to create a merge request for any change or addition that may be useful to the main project.  

## Installation and setup  

Due to me only playing some packs on some versions this program is currently limited to 1.16 it should work with 1.17 but has not been tested yet.  
  
Most modules in CL benefit from having a proper itemDatabase and recipeDatabase, I do not have an ingame provider to create these (yet).  
This means that some setup is required outside of Minecraft to get the best experience.  
If you do not wish to do this or it is impossible then CLCrafting and CLSorter will be limited (no tag based sorting, no crafting since you can't create recipes outside of manually writing them yet).  
The scripts for creating these databases are located in another [repo](https://gitlab.com/ralphgod3/computercraftrecipeextractorandsolver).  
  
If you wish to use your own generated files instead of the ones that are hosted on gitlab be sure to place them in the correct directory.
The directory structure always starts with DataFiles/ and can be found [here](https://gitlab.com/ralphgod3/computercraft/-/tree/master/DataFiles).  
If you are using generated files you will need to increase computer space limit or the files wont fit on the computer, this is the reason I pull them from Git.  
  
Each program will get its own installation and setup section, These are still WIP tho.  

## Programs  

As previously mentioned CL consists of multiple programs or Modules.  
Below each module is explained and integrations and dependencies are listed.  
Unless otherwise mentioned there can only be 1 of each module in a network to prevent issues.  
Unless otherwise mentioned each module that has an integration or dependency on another module has to be on the same wired network and be able to find each other.  

### CLSorter  

Provides item sorting capabilities from input inventories to outputs based on white/black lists with priorities.  
Can sort items based on tags.  

#### Integrations  

* CLCrafting  
When an inventory is set as an input for CLSorter and an output for a CLCrafting machine communication will be setup to redirect items to CLCrafter properly.  

#### Dependencies  

None  

#### Setup

Sorter has a lot of options so I recomend reading the generating config when you first start it.  
Sample config is generated on first startup.  
example config.  

```lua
{
  name = "sorter1", --name used for network anouncements
  inputs = {
      --accepts same input types as CLStorage in this case it will wrap all copper chests and iron_chest_0 as an input
    {
      type = "ironchest:copper_chest",
    },
    {
      name = "ironchest:iron_chest_0",
    },
  },
  modemBroadcastChannel = 65532, --modem communication channel
  pollTime = 1, -- time between inventory polls
  outputs = { -- outputs for sorter
    {
      --lowest priority without white or black list is the default output, only accepts name based inventory definitions
      priority = 0,
      name = "ironchest:gold_chest_0",
    },
    {
    -- creates inventory with a blacklist for redstone and anything tagged as a forge ore
      priority = 10,
      name = "ironchest:gold_chest_2",
      blacklist = {
        "I minecraft:redstone", -- I = item minecraft:redstone = itemName, this can also hold nbt specified like I minecraft:redstone -1 0123456890abcded (-1 is count which is not used in these entries)
        "T forge:ores", -- blacklist all forge ores
      },
      --if a whitelist is defined (same as blacklist) then inventory will only accept those tags or items. if both are defined then items on blacklist will be pulled from whitelist
      --ex: whitelist contains T forge:ores and blacklist I minecraft:gold_ore will accept all gold ores except gold ore
    },
  },
}
```

### CLStorage  

Provides item storage and retrieval capability by using inventories as storage. is able to use storageDrawers and modded chests correctly.  
IMPORTANT: for optimization reasons users should NEVER interact with the storage inventories manually.  
IMPORTANT: it is not advised to input items manually into the chest defined as inputInventory for CLStorage use a mod or CLSorter to do this.  

#### Integrations  

* CLCrafting  
Provides autocrafting for CLStorage.  
* CLTerminal  
Allows users to extract and input items into storage.  
* CLInterface  
Allows for automatic requesting of items and depositing of items to/from an inventory.  

#### Dependencies  

Whilst this program has no strict dependency it would make sense to run at least 1 CLTerminal to ensure users can extract items.  

#### Setup  

Needs at least 2 inventories 2 work.  
1 inventory as an input and 1 as storage.  

Sample configuration file below (generates default on initial start).  

```lua
--sample configuration file
{
  modemBroadcastChannel = 65532, --channel the wired modem uses for communicating
  importChest = 
  {
    pollTime = 2, -- time between pulling items in from importChest (seconds)
    name = "ironchest:gold_chest_0", --name of the import inventory ONLY NAMES ARE SUPPORTED
  },
  housekeepingTime = 300, -- time between restack of chests and rebuilding itemList
  name = "controller 1", -- name of controller (only used for network anouncements at the moment)
  ItemListUpdateInterval = 10, --time between item list transmissions of the controller (dont put frequency too high since this can be a lot of data)
  storageArray = { --defines storageArray each entry contains one storageEntry which can hold multiple inventories
    {
      --priority of this entry higher priorities will receive items first and  pull items last (think ae priority system)
      --when entry has multiple inventories name is used to determine priority ie minecraft:chest_8 and minecraft:chest_9 both with same priority will assign minecraft:chest_9 with higher prio
      priority = 10,
      -- this entry type connects all inventories of a type to the CLStorage as a storage inventory in this case all diamond chests are storage chests, rescans on restart only
      type = "ironchest:diamond_chest",
    },
    {
      priority = 9,
      --wraps only inventory with this specific name
      name = "ironchest:gold_chest_5",
    },
    {
      priority = 7,
      --wraps gold_chest_1 til gold_chest_20 as storage inventory
      type = "ironchest:gold_chest",
      start = 1,
      stop = 20,
    },
  },
}
```

### CLTerminal  

Provides a GUI for extracting items from storage, autocrafting and depositing items into storage.  
IMPORTANT: it runs a GUI meaning you need and advanced computer or advanced turtle to use this program.  
Multiple CLTerminals can be on the same network.  

#### Integrations  

* CLStorage  
Allows user to extract or deposit items into network.  
* CLCrafting  
Allows user to send autocraft requests (upon completion items are depositied into storage).  

### Dependencies  

* CLStorage  

#### Setup  

Sample config file below (generates default on initial startup).  

```lua
{
  inventory = "turtle_6", --inventory name to export and import items from (can be itself if running on a turtle)
  name = "main", --network name (only used for network anouncements)
  importPollTime = 2, --time between import inventory checks when mode is set to import
}
```

### CLCraftingTurtle  

Provides autocrafting capabilities to CLCrafting program.  
IMPORTANT: obviously to provide autocrafting this must be a crafty turtle.  
Multiple CLCraftingTurtles can be in the same network.  

#### Integrations  

* CLCrafting  
Provides autocrafting capability.  

#### Dependencies  

Whilst this program has no strict dependencies it does need CLCrafting to be useful.  

#### Setup
Ensure turtle is a crafty turtle.  

Sample config file below.  

```lua
{
  modemBroadcastChannel = 65532, --modem transmit channel
  name = "craftyTurtle", --network name (used for network anouncements)
}
```

### CLCrafting

Provides autocrafting capabilities for Computerized Logistics.  
IMPORTANT: depending on the way you import recipes performance may be poor without some tweaking, unfortunantly autocrafting is NP hard and O(FckMe) complex. basicly use preferredItemDatabase entries if resolving is too slow.  

#### Integrations  

* CLCraftingTurtle  
Provides the crafting part of autocrafting for CLCrafting.  
* CLTerminal  
Can make crafting requests to CLCrafting.  

#### Dependencies  

* CLStorage  
Item provider for autocrafting.  

#### Setup  

Sample config file below (generates on inital starupt).  

```lua
{
  recipeSet = "dw20Modified.json", --recipe set to use located in DataFiles/<mcversion>/<recipeset>
  modemBroadcastChannel = 65532, --channel that modem broadcasts over
  machinePollTime = 2, -- time in between machine and inventory polls when CLSorter integration is not available
  bufferChestDefinitions = { -- contains all inventories used as bufferchests for crafting ENSURE YOUR CHEST IS LARGE ENOUGH FOR THE CURRENT REQUEST
    {
      -- accepts same inventory types as defined in CLStorage
      start = 0,
      type = "minecraft:chest",
      stop = 0,
    },
  },
  machines = { -- machine crafting definitions
    [ "minecraft:smelting" ] = { --key is crafting operation name NOT machine name, when generated will have sample entries for every machine your recipe set has
      input = "ironchest:iron_chest_1", --input inventory for machine (inputting into a machine is not recomended since it wont input in specific slots)
      output = "ironchest:iron_chest_0", --output inventory for machine (outputing from machine is not recomended since it wont pull from specific slots), used to enable CLSorter integration
    },
  },
  name = "craftController", --network name (used for network anouncements)
}
```

### CLInterface  

Provides AE interface like capabilities.  
Can keep certain supply of items in inventories and suck out items that are not said to be stocked.  
IMPORTANT: does NOT do crafting requests (yet) so items that need to be stocked have to be in CLStorage network.
Multiple instances can be on the network but there should be no need since CLInterface can handle an unlimited amount of inventories.  

#### Integrations  

* CLStorage  
Pulls items from and deposits items in CLStorage to work.  

#### Dependencies  

* CLStorage

#### Setup  
  
```lua
{
  inventories = { --inventories that interface controls
    {
      --supports same inventory definitions as CLStorage
      name = "minecraft:chest_16",
      items = {
        --key is slot that item should be in
        [ 2 ] = "I minecraft:stone 64", -- keeps 64 stone in slot 2, does NOT accept Tag entries
        [ 4 ] = "I minecraft:redstone 64",
      },
    },
  },
  modemBroadcastChannel = 65532, --modem communication channel
  pollTime = 5, --time between checks (do not make this too short since requests can contain a lot of items)
}
```
