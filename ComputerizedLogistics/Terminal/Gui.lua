local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/Button.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/TextField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxSearchNumeric.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/ListboxSearchAndFilterFunctions.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxScroll.lua")

local PC = require("Modules.ParamCheck")
local Dbg = require("Modules.Logger")
local StatusCode = require("Modules.StatusCodes")
local advWindow = require("Gui.advWindow")
local Button = require("Gui.Elements.Button")
local TextField = require("Gui.Elements.TextField")
local GuiUtils = require("Gui.GuiUtils")
local ListboxSearchNumeric = require("Gui.Compositions.ListboxSearchNumeric")
local ListboxSearchAndFilterFunctions = require("Gui.ListboxSearchAndFilterFunctions")
local ListboxScroll = require("Gui.Compositions.ListboxScroll")


--time that popup window pops up after request success
local requestSuccessPopupTime = 0.5
local requestFailurePopupTime = 2
local Gui = {
    _p = {
        curVisible = {}
    }
}

----------------------------------- MENU BAR ------------------------------------
--callback when exit button is clicked
local function MenuCallbackBtnExit(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        term.setCursorPos(1, 1)
        term.clear()
        error()
    end
end

--callback for buttons that switch tabs
local function MenuCallbackBtnTabSwitch(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        Gui.Menu.UiElements.btnRequest.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnCraft.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnSystem.setBackgroundColor(colors.lightGray)
        Gui.Menu.UiElements.btnLog.setBackgroundColor(colors.lightGray)

        this.setBackgroundColor(colors.gray)
        Gui._p.curVisible.setVisible(false)
        if userParams == "req" then
            Gui._p.curVisible = Gui.Request
        elseif userParams == "craft" then
            Gui._p.curVisible = Gui.Craft
        elseif userParams == "log" then
            Gui._p.curVisible = Gui.Log
        elseif userParams == "sys" then
            Gui._p.curVisible = Gui.Sys
        end
        Gui._p.curVisible.setVisible(true)
        Gui._p.curVisible.draw()
    end
end

--callback for when mode button is switched, switches between import and export mode
local BtnModeActive = false
local function MenuCallbackBtnMode(eventInfo, this, userParams)
    if eventInfo[1] == "mouse_click" then
        if BtnModeActive == false then
            BtnModeActive = true
            this.setBackgroundColor(colors.red)
            this.setText("M: Import")
        else
            BtnModeActive = false
            this.setBackgroundColor(colors.green)
            this.setText("M: Export")
        end
        if Gui._p.OnBtnModeCallback ~= nil then
            Gui._p.OnBtnModeCallback(BtnModeActive)
        end

    end
end

--creates the menu bar for gui
local function CreateMenu()
    local terminal = term.current()
    local sizeX, _ = terminal.getSize()
    local rightX = 1
    local leftX = 1
    --left side aligned
    Gui.Menu = advWindow.new(terminal, 1, 1, sizeX, 1, true)
    local text = "Req"
    Gui.Menu.addUiElement("btnRequest", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.gray, text, MenuCallbackBtnTabSwitch, "req"))
    leftX = leftX + string.len(text) + 3
    text = "Craft"
    Gui.Menu.addUiElement("btnCraft", Button.new(Gui.Menu, leftX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "craft"))
    --right side aligned
    text = "Log"
    rightX = sizeX - string.len(text) - 3
    Gui.Menu.addUiElement("btnLog", Button.new(Gui.Menu, rightX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "log"))
    text = "Sys"
    rightX = rightX - string.len(text) - 3
    Gui.Menu.addUiElement("btnSystem", Button.new(Gui.Menu, rightX, 1, string.len(text) + 2, 1, colors.white, colors.lightGray, text, MenuCallbackBtnTabSwitch, "sys"))
    text = "M: Export"
    rightX = rightX - string.len(text) - 3
    Gui.Menu.addUiElement("btnMode", Button.new(Gui.Menu, rightX, 1, string.len(text) + 2, 1, colors.white, colors.green, text, MenuCallbackBtnMode))
    text = "X"
    Gui.Menu.addUiElement("btnExit", Button.new(Gui.Menu, sizeX, 1, 1, 1, colors.white, colors.red, text, MenuCallbackBtnExit))
    Gui.Menu.setBackgroundColor(colors.cyan)
    Gui.Menu.setVisible(true)
    Gui.Menu.draw()
end

----------------------------------- REQUEST TAB ------------------------------------

local function RequestCallbackFnc(eventInfo, this, userParams)
    if eventInfo[1] == "element_click" then
        local itemInfo = eventInfo[2]
        if itemInfo ~= nil and Gui._p.OnRequestCallbackFnc ~= nil then
            if Gui._p.OnBtnModeCallback ~= nil then
                BtnModeActive = false
                Gui.Menu.UiElements.btnMode.setBackgroundColor(colors.green)
                Gui.Menu.UiElements.btnMode.setText("M: Export")
                Gui._p.OnBtnModeCallback(BtnModeActive)
            end
            Gui.Popup.UiElements.text.setText("Requested: " .. tostring(Gui.Request.getNumber()) .. ", " .. itemInfo.name)
            Gui.Popup.setVisible(true)
            local status ,msg = Gui._p.OnRequestCallbackFnc(itemInfo, Gui.Request.getNumber())
            if status ~= StatusCode.Code.Ok then
                Gui.Popup.UiElements.text.setText("Request failed, Reason " .. StatusCode.toString(status))
                os.sleep(requestFailurePopupTime)
            else
                Gui.Popup.UiElements.text.setText(msg)
                os.sleep(requestSuccessPopupTime)
            end
            Gui.Popup.setVisible(false)
            Gui.Request.draw()
        end
    end
end

--create request tab
local function CreateRequestTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Request = ListboxSearchNumeric.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, RequestCallbackFnc, nil, nil, ListboxSearchAndFilterFunctions.listboxItemFilterFunction, ListboxSearchAndFilterFunctions.listboxItemSortFunctionHighCount)
    Gui.Request.setItemTable(Gui._p.UnfilteredItems)
    Gui.Request.setBackgroundColor(colors.black)
    Gui.Request.setVisible(true)
    Gui.Request.draw()
    Gui._p.curVisible = Gui.Request
end

----------------------------------- CRAFT TAB ------------------------------------

local function CraftCallbackFnc(eventInfo, this, userParams)
    if eventInfo[1] == "element_click" then
        local itemInfo = eventInfo[2]
        if itemInfo ~= nil and Gui._p.OnCraftRequestCallbackFnc ~= nil then
            if Gui._p.OnBtnModeCallback ~= nil then
                BtnModeActive = false
                Gui.Menu.UiElements.btnMode.setBackgroundColor(colors.green)
                Gui.Menu.UiElements.btnMode.setText("M: Export")
                Gui._p.OnBtnModeCallback(BtnModeActive)
            end
            Gui.Popup.UiElements.text.setText("Requested: " .. tostring(Gui.Craft.getNumber()) .. ", " .. itemInfo.name)
            Gui.Popup.setVisible(true)
            local status, msg = Gui._p.OnCraftRequestCallbackFnc(itemInfo, Gui.Craft.getNumber())
            if status ~= StatusCode.Code.Ok then
                Gui.Popup.UiElements.text.setText(msg)
                os.sleep(requestFailurePopupTime)
            else
                Gui.Popup.UiElements.text.setText(msg)
                os.sleep(requestSuccessPopupTime)
            end
            Gui.Popup.setVisible(false)
            Gui.Craft.draw()
        end
    end
end

--create craft tab
local function CreateCraftTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Craft = ListboxSearchNumeric.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, CraftCallbackFnc, nil, nil, ListboxSearchAndFilterFunctions.listboxItemFilterFunction, ListboxSearchAndFilterFunctions.listboxItemSortFunctionHighCount)
    Gui.Craft.setItemTable(Gui._p.UnfilteredCrafting)
    Gui.Craft.setBackgroundColor(colors.black)
    Gui.Craft.setVisible(false)
end

----------------------------------- SYSTEM TAB ------------------------------------
--create system tab
local function CreateSystemTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Sys = advWindow.new(terminal, 1, 2, sizeX, sizeY - 1, false)
    sizeX, sizeY = Gui.Sys.getSize()
    Gui.Sys.setBackgroundColor(colors.black)
end
----------------------------------- LOGS TAB ------------------------------------

--create log tab
local function CreateLogTab()
    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()
    Gui.Log = ListboxScroll.new(terminal, 1, 2, sizeX, sizeY -1, {colors.white}, {colors.gray, colors.black}, nil, nil, GuiUtils.drawTextAlignmentTopLeft)
    Gui.Log.setItemTable(Gui._p.Logs)
    Gui.Log.setBackgroundColor(colors.black)
    Gui.Log.setVisible(false);
end

---handles all gui operations
---@param eventInfo table eventinfo as received by os.pullEvent
function Gui.Handle(eventInfo)
    Gui.Menu.handle(eventInfo)
    Gui.Request.handle(eventInfo)
    Gui.Craft.handle(eventInfo)
    Gui.Log.handle(eventInfo)
    Gui.Sys.handle(eventInfo)
end

----------------------------------- Popup Window ------------------------------------
--create popup window
local function CreatePopupWindow()
    local winXSize = 40
    local winYSize = 5

    local terminal = term.current()
    local sizeX, sizeY = terminal.getSize()


    Gui.Popup = advWindow.new(terminal, (sizeX / 2) - (winXSize / 2), (sizeY / 2) - (winYSize / 2), winXSize, winYSize, false)
    sizeX, sizeY = Gui.Popup.getSize()

    Gui.Popup.addUiElement("text", TextField.new(Gui.Popup, 1, 1, sizeX, sizeY, colors.white, colors.brown , "placeholder"))
    Gui.Popup.draw()
end

----------------------------------- generic functions ------------------------------------

---update items in the request tab of ui, forces redraw
---@param tableToSet table table to set table to
function Gui.SetItemTable(tableToSet)
    Gui._p.UnfilteredItems = tableToSet
    Gui.Request.setItemTable(tableToSet)
end

---update items in the crafting tab of ui, forces redraw
---@param tableToSet table table to set table to
function Gui.SetCraftingTable(tableToSet)
    Gui._p.UnfilteredCrafting = tableToSet
    Gui.Craft.setItemTable(tableToSet)
end

---initialize Gui
---@param OnRequestCallbackFnc function function called when a item request is made prototype StatusCode, responseMessageFromServer OnRequestCallback(itemInformation, count)
---@param OnCraftRequestCallbackFnc function function called when a craft item request is made prototype StatusCode, responseMessageFromServer OnRequestCallback(itemInformation, count)
---@param Logs table table reference that links to the debug logger logs
---@param OnBtnModeCallback function is called when the mode button is changed in top screen prototype OnBtnModeCallback(boolean state, true = import, false = export)
function Gui.Init(OnRequestCallbackFnc, OnCraftRequestCallbackFnc, Logs, OnBtnModeCallback)
    PC.expect(1, OnRequestCallbackFnc, "function")
    PC.expect(2, OnCraftRequestCallbackFnc, "function")
    PC.expect(3, Logs, "table")
    PC.expect(4, OnBtnModeCallback, "function")
    Gui._p.OnRequestCallbackFnc = OnRequestCallbackFnc
    Gui._p.OnCraftRequestCallbackFnc = OnCraftRequestCallbackFnc
    Gui._p.Logs = Logs
    Gui._p.OnBtnModeCallback = OnBtnModeCallback
    Gui._p.UnfilteredItems = {}
    Gui._p.UnfilteredCrafting = {}
    CreateMenu()
    CreateRequestTab()
    CreateCraftTab()
    CreateSystemTab()
    CreateLogTab()
    CreatePopupWindow()
    Gui.Request.draw()
end

return Gui