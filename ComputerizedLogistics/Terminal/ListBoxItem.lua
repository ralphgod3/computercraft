local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")
local ListBoxItem = {}


---create listbox wrapper with standard tostring function
---@return table item item that can be placed in listbox
function ListBoxItem.NewItem(item)
    PC.expect(1, item, "table")
    PC.field(item, "name", "string")
    PC.field(item, "displayName", "string")
    PC.field(item, "count", "number")
    PC.field(item, "damage", "number", "nil")
    PC.field(item, "maxDamage", "number", "nil")
    local this = item
    ---override for tostring which is used by listbox
    ---@param self table self
    ---@param width number | nil width in characters the text can take up
    ---@return string itemString string describing input item
    function this.tostring(self, width)
        local modNameEndLoc = string.find(this.name, ":", nil, true)
        local modName = ""
        if modNameEndLoc ~= nil then
            modNameEndLoc = modNameEndLoc - 1
            modName = string.sub(this.name, 0, modNameEndLoc)
        end
        local namePart = this.displayName
        if this.damage then
            namePart = namePart .. " " .. this.maxDamage - this.damage .. "/" .. this.maxDamage
        end
        if width ~= nil then
            local len = string.len(namePart)
            len = len + string.len(tostring(this.count))
            if modName ~= "" then
                len = len + string.len(modName) + 1
            end
            local addition = " "
            while len + string.len(addition) < width do
                addition = addition .. " "
            end
            return namePart .. addition .. modName .. " " .. tostring(this.count)
        end
        return namePart .. " " .. tostring(this.count)
    end

    return this
end

return ListBoxItem
