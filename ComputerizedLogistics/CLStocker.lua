-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "/cache/CLStocker.cache"

-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
        "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/CraftingEntryParser.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")

local Dbg = require("Modules.Logger")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local NetworkingHardware = require("Modules.Networking.HW.Modems")
---@type Networking
local Networking = require("Modules.Networking.Networking")
local Messages = require("Modules.Networking.Messages")
local CraftingEntryParser = require("Modules.Parsers.CraftingEntryParser")
local StatusCodes = require("Modules.StatusCodes")


--enable printing of debug output to screen
Dbg.setOutputTerminal(term.current())
local TAG = "CLS"
local config = {}
local types = { Messages.remoteTypes.Stocker }
Threads = Threads.new()
Timer = Timer.new(Threads)
--holds information about items that should be stocked and current ongoing craft requests
---@type configCraftingEntry[]
local itemsToKeepStocked = {}
local currentlyCrafting = {}
local crafterControllerId = nil
local itemList = {}

Dbg.setLogLevel(TAG, Dbg.Levels.Info)

---create configuration file
local function createConfigFile()
    local config = {}
    --create networking so we can find default broadcast channel
    NetworkingHardware = NetworkingHardware.new()
    Networking = Networking.new(Threads, Timer, NetworkingHardware)
    config.name = "CLstocker"
    config.modemBroadcastChannel = Networking.broadcastChannel
    config.itemsToKeepStocked = CraftingEntryParser.createSampleList()

    local text = "--config file for CLStocker\r\n"
    text = text .. CraftingEntryParser.createExplanation()
    text = text .. textutils.serialize(config)
    local file = fs.open(configFile, "w")
    file.write(text)
    file.close()
    Dbg.logI(TAG, "created " .. configFile .. " edit it and restart program")
end

---find itemcount of itemToKeepStocked in itemlist message
---@param itemList table itemlist.Items
---@param itemToKeepStocked configCraftingEntry
---@return number count count of itemToKeepStocked in itemList
local function getItemCountFromItemList(itemList, itemToKeepStocked)
    if itemList[itemToKeepStocked.name] == nil then
        return 0
    end
    for _, v in pairs(itemList[itemToKeepStocked.name]) do
        if itemToKeepStocked.nbt == v.nbt then
            return v.count
        end
    end
    return 0
end

local function handleCraftRequestCompletedMessage(message)
    local payload = message.payload
    ---@cast payload CraftRequestCompletedMessage


    for k, _ in pairs(currentlyCrafting) do
        if currentlyCrafting[k] == payload.requestId then
            local msg = Messages.createCraftRequestItemsExtractedFromBuffer(payload.inventory)
            Networking.transmit(message.from, Networking.QOS.ATLEAST_ONCE, msg)
            currentlyCrafting[k] = nil
            break
        end
    end
end

local function doCraftUpdate()
    term.clear()
    term.setCursorPos(1, 1)
    --figure out what items we need to craft
    for k, v in pairs(itemsToKeepStocked) do
        local count = getItemCountFromItemList(itemList, v)
        if currentlyCrafting[k] ~= nil then
            Dbg.logI(TAG, v.name, count, ":", v.count, "crafting")
        elseif count < v.count then
            Dbg.logI(TAG, v.name, count, ":", v.count, "crafting")
            count = math.min(v.count - count, v.maxCraftableAtOnce)
            --create request
            Dbg.logV(TAG, "requesting craft " .. tostring(count) .. " " .. v.name)
            local request = Messages.createCraftRequest(v.name, count, v.nbt, v.damage, 0)
            local id = Networking.transmit(crafterControllerId, Networking.QOS.EXACTLY_ONCE, request)
            --wait for response
            local response = Networking.receive(10, nil, id)
            if response then
                Dbg.logV(TAG, "CraftRequest response code " .. StatusCodes.toString(response.payload.statusCode))
                if response.payload.statusCode == StatusCodes.Code.Ok then
                    currentlyCrafting[k] = tonumber(response.payload.msg)
                end
            else
                Dbg.logW(TAG, "no response from server")
            end
        else
            Dbg.logI(TAG, v.name, count, ":", v.count)
        end
    end
    Timer.add(10, doCraftUpdate)
end

---handle messages received on broadcast channel
local function NetworkLoopBroadcast()
    while true do
        local msg = Networking.receive(nil, Networking.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) == true then
                Dbg.logI(TAG, "received who is")
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.ATMOST_ONCE, resp, msg.llId)
                --item list
            elseif msg.payload.type == Messages.messageTypes.ItemList then
                itemList = msg.payload.Items
            end
        end
    end
end

---handle messages received on private channel
local function NetworkLoopRequests()
    local msgTypes = {
        Messages.messageTypes.WhoIs,
        Messages.messageTypes.CraftRequestCompleted,
    }
    while true do
        local msg = Networking.receiveMsgTypes(nil, os.getComputerID(), msgTypes)
        if msg and msg.payload and msg.payload.type then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs then
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
                --craft request completed
            elseif msg.payload.type == Messages.messageTypes.CraftRequestCompleted then
                handleCraftRequestCompletedMessage(msg)
            else
                Dbg.logW(TAG, "unknown msg " .. Utils.printTableRecursive(msg))
            end
        end
    end
end

---initialize the program
local function Init()
    term.clear()
    term.setCursorPos(1, 1)

    local msg = Messages.createWhoIs({ Messages.remoteTypes.CrafterController })
    local msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
    Dbg.logI(TAG, "requesting CrafterController id")
    while crafterControllerId == nil do
        local resp = Networking.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            Dbg.logI(TAG, "got CrafterController id " .. tostring(resp.from))
            crafterControllerId = resp.from
        else
            msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
        end
    end
    --initialize other threads
    Threads.create(NetworkLoopBroadcast)
    Threads.create(NetworkLoopRequests)
    Timer.add(10, doCraftUpdate)
end

-------------------------------------- MAIN ---------------------------------------

--create config if it does not exist
if not fs.exists(configFile) then
    createConfigFile()
    return
end
--load config
local file = fs.open(configFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)
if config == nil then
    Dbg.logE(TAG, "corrupt config please fix")
    return
end

--parse config into usable tables
for k, v in pairs(config.itemsToKeepStocked) do
    Dbg.logI(TAG, "adding", v)
    itemsToKeepStocked[k] = CraftingEntryParser.parseEntry(v)
end

--create networking
NetworkingHardware = NetworkingHardware.new(config.modemBroadcastChannel)
Networking = Networking.new(Threads, Timer, NetworkingHardware, config.modemBroadcastChannel)
os.setComputerLabel("CLStocker")

--create network threads
Threads.create(Init)
--start the program
Threads.startRunning()
