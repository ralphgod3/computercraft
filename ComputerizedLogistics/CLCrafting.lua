-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/FileBasedRecipeDatabase.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/CraftingRecipeSolver.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/CraftingManager.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Crafting/PreferredItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")


local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local StatusCodes = require("Modules.StatusCodes")
local Messages = require("Modules.Networking.Messages")
---@type Networking
local Networking = require("Modules.Networking.Networking")
local NetworkingHw = require("Modules.Networking.HW.Modems")
local ItemDb = require("Modules.ItemDb")
local RecipeDatabase = require("Modules.Crafting.FileBasedRecipeDatabase")
---@type IcraftingRecipeSolver
local CraftingRecipeSolver = require("Modules.Crafting.CraftingRecipeSolver")
---@type IcraftingManager
local CraftingManager = require("Modules.Crafting.CraftingManager")
local InventoryConfig = require("Modules.Parsers.InventoryConfig")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local PreferredItemDb = require("Modules.Crafting.PreferredItemDb")

--slows crafting operations down by x seconds so checking inventories whilst crafting becomes easier (set to nil to disable)
local slowMode = nil
--cache file location
local cacheFile = "cache/CLCrafting.cache"
local TAG = "CLC"
--folder where recipes are stored on git
local gitRecipeFolder = "DataFiles/MCVERSION/recipeDatabase"
--type(s) of this computer used for filtering who is requests
local types = { Messages.remoteTypes.CrafterController }
--storageController id
local storageController = nil
--list of craftable items
local craftableList = {}
--event fired when itemList is received from storageController
local ItemListReceivedEvent = "ItemListReceived"
local config = nil


--initialize some instances
Threads = Threads.new()
Timer = Timer.new(Threads)
ItemDb = ItemDb.new()
PreferredItemDb = PreferredItemDb.new()
Dbg.setOutputTerminal(term.current())

---create the configuration file for program
---@param cacheFile string configFileLocation
local function CreateConfigFile(cacheFile)
	--find minecraft version and download available recipe sets for version'
	local mcVersion = Utils.getVersionInfo()
	Dbg.logI(TAG, "minecraft " .. mcVersion .. " detected")
	Dbg.logI(TAG, "requesting minecraft recipe version availability")
	gitRecipeFolder = string.gsub(gitRecipeFolder, "MCVERSION", mcVersion)
	local folderContents = RecipeDatabase.getRecipeSetOptions()
	--let user chooce recipe set to download
	print("enter recipe set to use")
	for i = 1, #folderContents do
		print("[" .. i .. "] " .. folderContents[i])
	end
	local validInput = nil
	while validInput == nil do
		term.write("choice: ")
		local input = tonumber(read())
		if input ~= nil and input <= #folderContents and input > 0 then
			validInput = input
		end
	end
	local config = {}
	config.recipeSet = folderContents[tonumber(validInput)]
	Dbg.logI(TAG, "creating config file please edit and restart program " .. cacheFile)


	local infoText =
		"--name = controller name, modemBroadcastChannel is the channel broadcasts are sent and received on\r\n"
		.. "--machineName = machines used for crafting ensure they dont deposit items somewhere else or crafting will break\r\n"
		.. "--bufferChestDefinitions are used to define the inventories used for buffering ingredients\r\n"
		.. InventoryConfig.createExplanation()
		.. "\r\n"
		.. "--machines = machines used for crafting the names refer to the recipe types they can perform\r\n"
		.. "--for example one could use minecraft_furnace recipes in pretty much any furnace\r\n"
		.. "--when the file was created all machines that have recipes for the selected recipe set will get entries\r\n"
		..
		"--if an entry is removed then no recipes for that machine will be loaded, you can also remove the relevant machineRecipes line to acomplish this\r\n"
		.. "--input = input inventory, output = output inventory, pollTime = time between checking if machine is done\r\n"
		.. "\r\n"
		.. "--machineRecipes entry lists all the files that are loaded from git with recipes for machine based crafting\r\n"
		.. "--this can be used to load in entries from different recipe sets manually\r\n"
		..
		"--craftingRecipes entry are the same as machineRecipes except instead of being sorted by machinetype they are sorted by items produced\r\n"
		..
		"--removing an entry will remove items from that mod from being autocrafted, entries can also be added to manually define your recipe set\r\n"
		.. "--all recipe files can be found here: https://gitlab.com/ralphgod3/computercraft/-/tree/master/DataFiles\r\n"
		..
		"--currently there is no way to overwrite individual recipes because the computers dont have enough disk space to download the recipes into a file\r\n"

	Networking = Networking.new(Threads, Timer, NetworkingHw.new())
	config.modemBroadcastChannel = Networking.broadcastChannel
	config.name = "craftController"

	RecipeDatabase = RecipeDatabase.new(config.recipeSet)
	config.machinePollTime = 2
	config.machines = {}
	--generate config for all machine types required by the recipe
	for _, machineName in pairs(RecipeDatabase.getRecipeTypes()) do
		if machineName ~= "minecraft:crafting" then
			config.machines[machineName] = {}
			config.machines[machineName].input = "minecraft:chest_x"
			config.machines[machineName].output = "minecraft:chest_y"
		end
	end

	config.bufferChestDefinitions = {}
	table.insert(config.bufferChestDefinitions, InventoryConfig.createConfigEntry(InventoryConfig.configTypes.name))
	table.insert(config.bufferChestDefinitions, InventoryConfig.createConfigEntry(InventoryConfig.configTypes.range))
	table.insert(config.bufferChestDefinitions, InventoryConfig.createConfigEntry(InventoryConfig.configTypes.type))

	local file = fs.open(cacheFile, "w")
	file.write(infoText)
	file.write(textutils.serialize(config))
	file.close()
end

---callback function, called when craft request is completed
---@param requestId number craftRequestId
---@param success boolean indicates success of request
---@param callbackFncUserArgs any user arguments
---@param bufferInventory string | nil inventory to collect items from or on failure a message to send to the user
local function craftCompleted(requestId, success, callbackFncUserArgs, bufferInventory)
	---@cast callbackFncUserArgs ILLmessage
	local payload = callbackFncUserArgs.payload
	---@cast payload CraftRequestMessage
	local item = {
		name = payload.name,
		count = payload.count,
		nbt = payload.nbt,
		damage = payload.damage
	}
	if success then
		Dbg.logI(TAG, "craft for ", payload.name, " ", payload.count, " completed")
		local message = Messages.createCraftRequestCompleted(requestId, item, bufferInventory)
		Networking.transmit(callbackFncUserArgs.from, Networking.QOS.EXACTLY_ONCE, message, callbackFncUserArgs.llId)
		--if either message is received or 5 seconds passed hand control back over to crafting manager to suck rest of items out of the inventory
		local msg = Networking.receiveMsgTypes(5, callbackFncUserArgs.from,
			{ Messages.messageTypes.CraftRequestItemsExtractedFromBuffer })
		while msg and msg.payload.inventory ~= bufferInventory do
			msg = Networking.receiveMsgTypes(2, callbackFncUserArgs.from,
				{ Messages.messageTypes.CraftRequestItemsExtractedFromBuffer })
		end
	elseif bufferInventory ~= nil then
		local message = Messages.createCraftRequestFailed(requestId, item, bufferInventory)
		Networking.transmit(callbackFncUserArgs.to, Networking.QOS.EXACTLY_ONCE, message, callbackFncUserArgs.llId)
	else
		Dbg.logE(TAG, "craftCompleted callback internal error bufferInventory value:", bufferInventory, "success value:",
			success)
	end
end

---callback function that handles any received craft task messages
---@param message ILLmessage
local function HandleCraftRequest(message)
	local payload = message.payload
	---@cast payload CraftRequestMessage
	--request updated list of available items to craft with
	local msg = Messages.createItemListRequest()
	Dbg.logI(TAG, "sending item list request")
	Networking.transmit(storageController, Networking.QOS.ATLEAST_ONCE, msg)
	--wait for recent list with items to arive
	---@diagnostic disable-next-line: redundant-parameter
	local _, itemList = os.pullEvent(ItemListReceivedEvent)
	Dbg.logI(TAG, "received item list from storage controller")
	--transform itemlist in usable format
	local tempItemList = {}
	for name, info in pairs(itemList) do
		for i = 1, #info do
			local entry = {}
			entry.name = name
			entry.nbt = info[i].nbt
			entry.count = info[i].count
			table.insert(tempItemList, entry)
		end
	end

	local sucess, result = CraftingRecipeSolver.solve(payload.name, payload.count, payload.nbt,
		tempItemList, payload.timeout)
	local resp = {}
	if slowMode ~= nil then sleep(slowMode) end
	if not sucess and type(result) == "string" then
		resp = Messages.createCraftRequestResponse(StatusCodes.Code.Fail, result)
		Networking.transmit(message.from, Networking.QOS.EXACTLY_ONCE, resp, message.llId)
	else
		Dbg.logI(TAG, "crafting " .. " " .. tostring(payload.count) .. " " .. payload.name)
		if slowMode ~= nil then
			Dbg.logV(TAG, result)
		end
		local suc, ret = CraftingManager.craft(result.itemsToRequest, result.craftingChain, craftCompleted, message,
			slowMode)
		if not suc and type(ret) == "string" then
			resp = Messages.createCraftRequestResponse(StatusCodes.Code.Fail, ret)
			Dbg.logE(TAG, ret)
		else
			resp = Messages.createCraftRequestResponse(StatusCodes.Code.Ok, tostring(ret))
		end
		Networking.transmit(message.from, Networking.QOS.EXACTLY_ONCE, resp, message.llId)
	end
end

local function HandleWhoAmI(msg)
	local peripherals = peripheral.getNames()
	peripherals = Utils.removeElementsFromTable(peripherals, rs.getSides())
	peripherals = Utils.removeElementsFromTable(peripherals, msg.payload.peripherals)
	peripherals = Utils.rebuildArray(peripherals)
	if #peripherals ~= 1 then
		-- cant determine turtle name
		Dbg.logW(TAG, "problem determining name for whoAmI request", peripherals)
	else
		Dbg.logI(TAG, "whoAmI id " .. tostring(msg.from) .. " is " .. tostring(peripherals[1]))
		local resp = Messages.createWhoAmIResponse(peripherals[1])
		Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
	end
end

--handles messages received on broadcast channel
local function NetworkLoopBroadcast()
	while true do
		local msg = Networking.receive(nil, Networking.broadcastChannel)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
				Dbg.logV(TAG, "received who is from " .. tostring(msg.from))
				local resp = Messages.createWhoIsResponse(types, config.name)
				Networking.transmit(msg.from, Networking.QOS.ATMOST_ONCE, resp, msg.llId)
				--handle itemList
			elseif msg.payload.type == Messages.messageTypes.ItemList then
				--clear old arrays
				os.queueEvent(ItemListReceivedEvent, msg.payload.Items)
			end
		end
	end
end

--handles messages received on personal channel
local function NetworkLoopRequests()
	local msgsToListenTo =
	{
		Messages.messageTypes.WhoIs,
		Messages.messageTypes.CraftListRequest,
		Messages.messageTypes.WhoAmI,
		Messages.messageTypes.WhoIsResponse,
		Messages.messageTypes.CraftRequest,
	}
	while true do
		local msg = Networking.receiveMsgTypes(nil, os.getComputerID(), msgsToListenTo)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
				Dbg.logI(TAG, "received who is from " .. tostring(msg.from))
				local resp = Messages.createWhoIsResponse(types, config.name)
				Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
				--handle request for craftable items
			elseif msg.payload.type == Messages.messageTypes.CraftListRequest then
				Dbg.logI(TAG, "received craftable list request")
				local resp = Messages.createCraftList(craftableList)
				Networking.transmit(msg.from, Networking.QOS.ATLEAST_ONCE, resp, msg.llId)
				--handle who am i
			elseif msg.payload.type == Messages.messageTypes.WhoAmI then
				Dbg.logI(TAG, "received whoAmI")
				HandleWhoAmI(msg)
				--handle crafting request
			elseif msg.payload.type == Messages.messageTypes.CraftRequest then
				Dbg.logI(TAG, "received craft request")
				---@diagnostic disable-next-line: param-type-mismatch
				HandleCraftRequest(msg)
				--handle who is response from crafting turtles
			elseif msg.payload.type == Messages.messageTypes.WhoIsResponse then
				if Utils.findElementInTable(msg.payload.types, Messages.remoteTypes.StorageCrafter) ~= nil then
					Dbg.logI(TAG, "received who is response")
					if CraftingManager.addCrafter(ItemInventoryFactory.new(Threads, msg.payload.name), msg.from) then
						Dbg.logI(TAG, "added crafting turtle total: " .. tostring(CraftingManager.getCrafterCount()))
					end
				end
			else
				Dbg.logW(TAG, "unknown msg", msg)
			end
		end
	end
end

local lastCraftingProgressCount = math.huge
local function sendCraftStatusUpdates()
	local craftingProgressStatus = CraftingManager.getAllCraftingProgress()
	if Utils.getTableLength(craftingProgressStatus) ~= 0 or lastCraftingProgressCount ~= 0 then
		local msg = Messages.createCraftingProgressMessage(craftingProgressStatus)
		Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
		lastCraftingProgressCount = Utils.getTableLength(craftingProgressStatus)
	end
	Timer.add(5, sendCraftStatusUpdates)
end


local function Init()
	--get storage controller id
	local msg = Messages.createWhoIs({ Messages.remoteTypes.StorageController })
	local msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
	while storageController == nil do
		Dbg.logI(TAG, "requesting storageController id")
		local resp = Networking.receive(5, nil, msgId)
		if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
			Dbg.logI(TAG, "got controller id " .. tostring(resp.from))
			storageController = resp.from
		else
			msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)
		end
	end
	CraftingManager = CraftingManager.new(Threads, Timer, Networking, ItemDb, storageController, nil, nil, nil)

	--add buffer chests
	for k, v in pairs(config.bufferChestDefinitions) do
		local chests = InventoryConfig.parseConfigEntry(v)
		for _, name in pairs(chests) do
			Dbg.logV(TAG, "adding " .. name .. " to buffer inventories")
			CraftingManager.addBufferInventory(ItemInventoryFactory.new(Threads, name))
		end
	end
	Dbg.logI(TAG, "found " .. tostring(CraftingManager.getBufferChestCount()) .. " buffer inventories")

	--add machines to crafting manager
	local recipeTypes = {}
	for k, v in pairs(config.machines) do
		Dbg.logI(TAG, "adding", k)
		local inputInv = ItemInventoryFactory.new(Threads, v.input)
		local outputInv = ItemInventoryFactory.new(Threads, v.output)
		if not CraftingManager.addMachine(k, inputInv, outputInv) then
			error("can not add " .. k .. ", check config")
		end
		table.insert(recipeTypes, k)
	end

	--initialize recipe solver
	table.insert(recipeTypes, "minecraft:crafting")
	CraftingRecipeSolver = CraftingRecipeSolver.new(ItemDb, RecipeDatabase, recipeTypes, PreferredItemDb)

	--build craftable itemlist to send to terminals
	Dbg.logI(TAG, "generating craftableItemList")
	local recipes = CraftingRecipeSolver.dumpCraftableItems()
	for _, itemInfo in pairs(recipes) do
		local tempItemRef = ItemDb.getItem(itemInfo.name, itemInfo.nbt)
		if tempItemRef == nil then
			error("no item found with name of " .. tostring(itemInfo.name) .. " in itemdb")
		else
			local tempItemTable = {}
			tempItemTable.name = itemInfo.name
			tempItemTable.nbt = itemInfo.nbt
			tempItemTable.displayName = tempItemRef.displayName
			tempItemTable.count = tempItemRef.maxCount
			table.insert(craftableList, tempItemTable)
		end
	end
	Dbg.logI(TAG, "loaded " .. tostring(#craftableList) .. " unique items")

	-- send initial craftable list
	Dbg.logI(TAG, "sending craftable item list broadcast")
	local resp = Messages.createCraftList(craftableList)
	Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, resp)

	--request crafting turtle crafters
	msg = Messages.createWhoIs({ Messages.remoteTypes.StorageCrafter })
	Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)

	Dbg.logI(TAG, "creating coroutines")
	Threads.create(NetworkLoopBroadcast)
	Threads.create(NetworkLoopRequests)
	Threads.create(sendCraftStatusUpdates)
	Dbg.logI(TAG, "ready for requests")
end


-------------------------------------------------- MAIN ----------------------------------------------
if not fs.exists(cacheFile) then
	CreateConfigFile(cacheFile)
	return
end
local file = fs.open(cacheFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)
if config == nil then
	Dbg.logE(TAG, "config is corrupt")
	return
end

os.setComputerLabel("CLCrafting")
Dbg.logI(TAG, "initializing networking")
NetworkingHw = NetworkingHw.new(config.modemBroadcastChannel)
Networking = Networking.new(Threads, Timer, NetworkingHw, config.modemBroadcastChannel)
Dbg.logI(TAG, "initializing recipe database")
RecipeDatabase = RecipeDatabase.new(config.recipeSet)
Dbg.logI(TAG, "starting threads")
Threads.create(Init)
Threads.startRunning()
