-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
        "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Terminal/Gui.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Terminal/ListBoxItem.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")

local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local StatusCodes = require("Modules.StatusCodes")
local Messages = require("Modules.Networking.Messages")
---@type Networking
local Networking = require("Modules.Networking.Networking")
local NetworkingHardware = require("Modules.Networking.HW.Modems")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local Gui = require("ComputerizedLogistics.Terminal.Gui")
local ListBoxItem = require("ComputerizedLogistics.Terminal.ListBoxItem")
------------------------------------------ GLOBAL VARIABLES ---------------------------------------
local TAG = "CLT"
local cacheName = "cache/CLTerminal.cache"
local types = { Messages.remoteTypes.StorageTerminal }
Threads = Threads.new()
Timer = Timer.new(Threads)
local config = nil
local crafterControllerId = nil
local storageControllerId = nil
local StorageArrayInputInventory = nil
local ourInventory = nil
local importTimer = 0

------------------------------------------ PROGRAM FUNCTIONS ---------------------------------------

---callback function, used when items are requested in gui
---@param itemInformation table information about item being requested
---@param count number amount of item being requested
---@return number statuscode status code for item request
---@return string infoString string giving additional information about status of request
local function OnRequestCallback(itemInformation, count)
    --create request
    Dbg.logI(TAG, "requesting " .. tostring(count) .. " " .. itemInformation.name .. " from " .. storageControllerId)
    local exportInfo = {}
    local ourName = ourInventory.getName()
    exportInfo[ourName] = {}
    table.insert(exportInfo[ourName],
        { name = itemInformation.name, count = count, damage = itemInformation.damage, nbt = itemInformation.nbt })
    local request = Messages.createItemRequest(exportInfo)
    local id = Networking.transmit(storageControllerId, Networking.QOS.EXACTLY_ONCE, request)
    --wait for response
    local response = Networking.receive(10, nil, id)
    if response then
        response.payload = response.payload or {}
        response.payload.response = response.payload.response or {}
        response.payload.response[itemInformation.name] = response.payload.response[itemInformation.name] or { 0 }
        local _, itemsGot = next(response.payload.response[itemInformation.name])
        Dbg.logD(TAG, "ItemRequest response code " .. StatusCodes.toString(response.payload.statusCode))
        if response.payload.statusCode ~= StatusCodes.Code.Ok then
            return response.payload.statusCode, "got " .. tostring(itemsGot)
        else
            return response.payload.statusCode,
                "request " .. StatusCodes.toString(response.payload.statusCode) .. " sending " .. tostring(itemsGot)
        end
    else
        Dbg.logW(TAG, "no response from server")
        return StatusCodes.Code.NoServerResponse, "no server response"
    end
    return StatusCodes.Code.NoServerResponse, "no server response"
end

---callback function, used when crafted items are requested in gui
---@param itemInformation table information about item being requested
---@param count number amount of item being requested
local function OnCraftRequestCallback(itemInformation, count)
    if crafterControllerId == nil then
        return StatusCodes.Code.Fail, "no controller on network"
    end
    --create request
    Dbg.logI(TAG,
        "requesting craft " .. tostring(count) .. " " .. itemInformation.name .. " from " .. crafterControllerId)
    --todo: implement slowMode
    local request = Messages.createCraftRequest(itemInformation.name, count, itemInformation.nbt, itemInformation.damage,
        0)
    local id = Networking.transmit(crafterControllerId, Networking.QOS.EXACTLY_ONCE, request)
    --wait for response
    local response = Networking.receive(10, nil, id)
    if response then
        Dbg.logD(TAG, "CraftRequest response code " .. StatusCodes.toString(response.payload.statusCode))
        if response.payload.statusCode == StatusCodes.Code.Ok then
            return response.payload.statusCode, "Starting craft"
        else
            return response.payload.statusCode, tostring(response.payload.msg)
        end
    else
        Dbg.logW(TAG, "no response from server")
        return StatusCodes.Code.NoServerResponse, "no server response"
    end
    return StatusCodes.Code.NoServerResponse, "no server response"
end

---handle messages received on broadcast channel
local function NetworkLoopBroadcast()
    while true do
        local msg = Networking.receive(nil, Networking.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) == true then
                Dbg.logI(TAG, "received who is")
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
                --item list
            elseif msg.payload.type == Messages.messageTypes.ItemList then
                --clear old arrays
                local itemTable = {}
                for itemName, items in pairs(msg.payload.Items) do
                    for _, itemInfo in pairs(items) do
                        local entry = {

                            name = itemName,
                            displayName = itemInfo.displayName,
                            count = itemInfo.count,
                            damage = itemInfo.damage,
                            maxDamage = itemInfo.maxDamage,
                            nbt = itemInfo.nbt,
                        }
                        table.insert(itemTable, ListBoxItem.NewItem(entry))
                    end
                end
                Gui.SetItemTable(itemTable)
                --craft list
            elseif msg.payload.type == Messages.messageTypes.CraftList then
                if crafterControllerId == nil then
                    crafterControllerId = msg.from
                end
                --clear old arrays
                local craftTable = {}
                for _, v in pairs(msg.payload.Items) do
                    table.insert(craftTable, ListBoxItem.NewItem(v))
                end
                Gui.SetCraftingTable(craftTable)
            end
        end
    end
end

---handle messages received on private channel
local function NetworkLoopRequests()
    local recvTypes = {
        Messages.messageTypes.WhoIs,
        Messages.messageTypes.CraftRequestFailed,
        Messages.messageTypes.CraftRequestCompleted,
    }
    while true do
        local msg = Networking.receiveMsgTypes(nil, os.getComputerID(), recvTypes)
        if msg and msg.payload and msg.payload.type then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs then
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
            elseif msg.payload.type == Messages.messageTypes.CraftRequestCompleted then
                local payload = msg.payload
                ---@cast payload CraftRequestCompletedMessage
                Dbg.logI(TAG, "got craft request completed message")
                --pull items from buffer inventory into output
                local inventory = ItemInventoryFactory.new(Threads, payload.inventory)
                local itemList = inventory.list()
                local pullTbl = {}
                local countLeft = payload.request.count
                ---@cast countLeft number
                for k, v in pairs(itemList) do
                    if v.name == payload.request.name then
                        local toExtract = math.min(countLeft, v.count)
                        pullTbl[k] = toExtract
                        countLeft = countLeft - toExtract
                        if countLeft <= 0 then
                            break
                        end
                    end
                end
                inventory.pushItemsFromSlots(ourInventory, pullTbl, false)

                local resp = Messages.createCraftRequestItemsExtractedFromBuffer(msg.payload.inventory)
                Networking.transmit(msg.from, Networking.QOS.ATLEAST_ONCE, resp)
                Timer.add(config.craftRequestOrderDelay, OnRequestCallback, msg.payload.request,
                    msg.payload.request.count)
            elseif msg.payload.type == Messages.messageTypes.CraftRequestFailed then
                local payload = msg.payload
                ---@cast payload CraftRequestFailed
                Dbg.logE(TAG, "craft request failed", payload.request.name, " ", payload.request.count, " ",
                    payload.message)
                --todo: add a popup for user to see
            else
                Dbg.logW(TAG, "unknown msg " .. Utils.printTableRecursive(msg))
            end
        end
    end
end

---import  items from chest into network
local function import(importInterval)
    if turtle then
        local toMove = {}
        for i = 1, ourInventory.size() do
            toMove[i] = 64
        end
        StorageArrayInputInventory.pullItemsFromSlots(ourInventory, toMove)
    else
        ourInventory.pushInventory(StorageArrayInputInventory)
    end
    importTimer = Timer.add(importInterval, import, importInterval)
end

---callback for import button
---@param importing boolean import?
local function modeCallback(importing)
    if importing then
        import(config.importPollTime)
    else
        Timer.cancel(importTimer)
    end
end

---perform all gui functions
local function GuiLoop()
    --main gui loop
    while true do
        local eventInfo = { os.pullEvent() }
        Gui.Handle(eventInfo)
    end
end

---initialize the program
local function Init()
    term.clear()
    term.setCursorPos(1, 1)
    ourInventory = ItemInventoryFactory.new(Threads, config.inventory)
    --get controller id
    local msgWhoIs = Messages.createWhoIs({ Messages.remoteTypes.StorageController })
    local msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msgWhoIs)
    Dbg.logI(TAG, "requesting storagecontroller id")
    while storageControllerId == nil do
        print("requesting storageController id")
        local resp = Networking.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            print("got controller id " .. tostring(resp.from))
            Dbg.logI(TAG, "got controller id " .. tostring(resp.from))
            storageControllerId = resp.from
        else
            msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msgWhoIs)
        end
    end

    print("fetching import inventory from storage system")
    local msgIVR = Messages.createInputInventoryRequest()
    msgId = Networking.transmit(storageControllerId, Networking.QOS.EXACTLY_ONCE, msgIVR)
    --wait for controller to send information about its import inventory
    while true do
        local resp = Networking.receive(nil, os.getComputerID(), msgId)
        if resp then
            StorageArrayInputInventory = ItemInventoryFactory.new(Threads, resp.payload.inputInventory)
            break
        end
    end

    local msgWhoIs = Messages.createWhoIs({ Messages.remoteTypes.CrafterController })
    msgId = Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msgWhoIs)
    Dbg.logI(TAG, "requesting CrafterController id")
    while crafterControllerId == nil do
        print("requesting CrafterController id")
        local resp = Networking.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
            print("got CrafterController id " .. tostring(resp.from))
            Dbg.logI(TAG, "got CrafterController id " .. tostring(resp.from))
            crafterControllerId = resp.from
        else
            print("no CrafterController attached")
            os.sleep(2)
            break
        end
    end

    --request items to populate the gui with
    local msgILR = Messages.createItemListRequest()
    Gui.Init(OnRequestCallback, OnCraftRequestCallback, Dbg.Logs, modeCallback)
    Networking.transmit(storageControllerId, Networking.QOS.EXACTLY_ONCE, msgILR)

    --request list of craftable items
    if crafterControllerId ~= nil then
        local msgCLR = Messages.createCraftListRequest()
        msgId = Networking.transmit(crafterControllerId, Networking.QOS.ATLEAST_ONCE, msgCLR)
        local resp = Networking.receive(5, nil, msgId)
        if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil and resp.payload.type == Messages.messageTypes.CraftList then
            --clear old arrays
            local craftTable = {}
            for _, v in pairs(resp.payload.Items) do
                table.insert(craftTable, ListBoxItem.NewItem(v))
            end
            Gui.SetCraftingTable(craftTable)
        end
    end

    --initialize other threads
    Threads.create(NetworkLoopBroadcast)
    Threads.create(NetworkLoopRequests)
    Threads.create(GuiLoop)
end


------------------------------------------ MAIN ---------------------------------------
--initial setup
if not fs.exists(cacheName) then
    print("enter name for inventory to use as output for this terminal.")
    print("ex: minecraft:iron_chest_0")
    local outInv = read()
    print("set a name for this controller to be identified by")
    local terminalName = read()

    local cache = {
        inventory = outInv,
        name = terminalName,
        importPollTime = 2,
        modemBroadcastChannel = 65532,
        craftRequestOrderDelay = 2,
    }

    local file = fs.open(cacheName, "w")
    file.write(textutils.serialize(cache))
    file.close()
    print("configuration done to re enter configuration delete " .. cacheName)
    os.reboot()
end

--load data from cachefile
local file = fs.open(cacheName, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)

os.setComputerLabel("CLTerminal")
NetworkingHardware = NetworkingHardware.new(config.modemBroadcastChannel)
Networking = Networking.new(Threads, Timer, NetworkingHardware, config.modemBroadcastChannel)

--run main code
Threads.create(Init)
Threads.startRunning()
