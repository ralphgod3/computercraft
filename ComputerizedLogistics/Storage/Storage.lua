local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Ithreads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Storage/StorageUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")


local StatusCode = require("Modules.StatusCodes")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local StorageUtils = require("ComputerizedLogistics.Storage.StorageUtils")
local IitemDb = require("Interfaces.Modules.IitemDb")
local IitemInventory = require("Interfaces.Modules.IitemInventory")
local IThreads = require("Interfaces.Modules.Ithreads")
local Dbg = require("Modules.Logger")
local ItemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")

local Storage = {}

---adds items in needsToBeAddedToDb table to itemDb
---@param ThreadingManager table implementation of Ithreads interface
---@param itemInventory table implementation of IitemInventory interface
---@param needsToBeAddedToDb table table containing slots of items that need to be added to itemDb
---@param itemDb table implementation of IitemDb interface
local function addMissingEntriesToItemDb(ThreadingManager, itemInventory, needsToBeAddedToDb, itemDb)
	-- add missing entries to database in parallel
	local countSem = Utils.createCountingSemaphore()
	for _, slot in pairs(needsToBeAddedToDb) do
		Utils.takeCountingSemaphore(countSem)
		ThreadingManager.create(
			function()
				local info = itemInventory.getItemDetail(slot)
				itemDb.addItem(info)
				Utils.freeCountingSemaphore(countSem)
			end
		)
	end
	if not Utils.runThreadingManagerIfNeeded(ThreadingManager) then
		Utils.awaitCountingSemaphore(countSem)
	end
end

---create entry for movePlan
---@param inventoryInfo table inventoryInfo entry from this.storageInventories
---@param count number number of items to move
---@param toSlot number slot to move items into
---@param fromSlot number? slot to move items from
---@param name string? itemName
---@param damage number? itemDamage
---@param nbt string? nbtHash
---@return table movePlanEntry entry that can be added to movePlan
local function createMovePlanEntry(inventoryInfo, count, toSlot, fromSlot, name, damage, nbt)
	return {
		inventoryInfo = inventoryInfo,
		count = count,
		slot = toSlot,
		fromSlot = fromSlot,
		name = name,
		damage = damage,
		nbt = nbt
	}
end

---add nescesary stuff to moveplan for moving items into bulk storage
---@param movePlan table current plan of moves to be executed
---@param inventoryInfo table inventoryInfo entry from this.storageInventories
---@param invName string name of inventoryInfo inventory
---@param itemListCopy table copy of items contained in storage array
---@param originInventoryslot number inventory slot in the inventory we are moving the items from
---@param itemInfo table information about the item (name, nbt, damage)
---@param itemCountToMove number amount of items to move
---@return number itemsLeftToMove items that can not fit in this inventory and still need to be moved to another inventory
local function addItemToMovePlanForBulkStorage(movePlan, inventoryInfo, invName, itemListCopy, originInventoryslot,
											   itemInfo, itemCountToMove)
	local slots = StorageUtils.getItemCountWithSlot(itemListCopy[invName], itemInfo.name, itemInfo.nbt, itemInfo.damage)
	for slotWithItem, _ in pairs(slots) do
		itemListCopy[invName][slotWithItem].count = itemListCopy[invName][slotWithItem].count + itemCountToMove
		table.insert(movePlan[originInventoryslot], createMovePlanEntry(inventoryInfo, itemCountToMove, slotWithItem))
		itemCountToMove = 0
		break
	end
	return itemCountToMove
end

---add nescesary stuff to moveplan for moving items into bulk storage
---@param movePlan table current plan of moves to be executed
---@param inventoryInfo table inventoryInfo entry from this.storageInventories
---@param invName string name of inventoryInfo inventory
---@param itemListCopy table copy of items contained in storage array
---@param originInventoryslot number inventory slot in the inventory we are moving the items from
---@param itemInfo table information about the item (name, nbt, damage)
---@param itemCountToMove number amount of items to move
---@param ItemDb table implementation of IitemDb interface
---@return number itemsLeftToMove items that can not fit in this inventory and still need to be moved to another inventory
local function addItemToMovePlanForNormalStorage(movePlan, inventoryInfo, invName, itemListCopy, originInventoryslot,
												 itemInfo, itemCountToMove, ItemDb)
	local maxStackSize = ItemDb.getItem(itemInfo.name, itemInfo.nbt).maxCount
	--find slots that have partial stacks and move items to max stack them in there
	local slots = StorageUtils.getItemCountWithSlot(itemListCopy[invName], itemInfo.name, itemInfo.nbt, itemInfo.damage)
	for slotWithItem, _ in pairs(slots) do
		local moved = math.min(itemCountToMove, maxStackSize - itemListCopy[invName][slotWithItem].count)
		if moved ~= 0 then
			itemListCopy[invName][slotWithItem].count = itemListCopy[invName][slotWithItem].count + moved
			table.insert(movePlan[originInventoryslot], createMovePlanEntry(inventoryInfo, moved, slotWithItem))
			itemCountToMove = itemCountToMove - moved
			if itemCountToMove == 0 then
				return itemCountToMove
			end
		end
	end
	--find empty slots that can be filled
	--check for space
	local invSize = inventoryInfo.IitemInventory.size()
	if Utils.getTableLength(itemListCopy[invName]) < invSize then
		for i = 1, invSize do
			--free slot found move stack size or itemCount in there depending on what is smallest
			if itemListCopy[invName][i] == nil then
				itemListCopy[invName][i] = {}
				local moved = math.min(itemCountToMove, maxStackSize)
				itemListCopy[invName][i].name = itemInfo.name
				itemListCopy[invName][i].damage = itemInfo.damage
				itemListCopy[invName][i].nbt = itemInfo.nbt
				itemListCopy[invName][i].count = moved
				table.insert(movePlan[originInventoryslot], createMovePlanEntry(inventoryInfo, moved, i))
				itemCountToMove = itemCountToMove - moved
				if itemCountToMove == 0 then
					return itemCountToMove
				end
			end
		end
	end
	return itemCountToMove
end

---finds inventory with lowest priority that has item
---@param itemInfo table copy of item list
---@param priorities table priority of each inventory indexed by inventory name
---@param itemName string itemName
---@param nbt string | nil nbthash nil if not apliccable
---@param damage number | nil damage value for item if aplicable nil otherwise
---@return string inventory name that contains item
local function findLowestPriorityInventoryWithItem(itemInfo, priorities, itemName, nbt, damage, inventoryBlackList)
	inventoryBlackList = inventoryBlackList or {}
	local invInfo = nil
	local invName = nil
	local invNumber = math.huge
	if damage ~= nil then
		nbt = nil
	end
	--go through all inventories
	for name, inventoryInfo in pairs(itemInfo) do
		--check if inventory is in blacklist
		if Utils.findElementInTable(inventoryBlackList, name) == nil then
			--check if priority is lower
			if invInfo == nil or invInfo <= priorities[name] then
				--check if item is present in inventory
				if Utils.getTableLength(StorageUtils.getItemCountWithSlot(inventoryInfo, itemName, nbt, damage)) > 0 then
					if invNumber > StorageUtils.getInventoryNumber(name) then
						invInfo = priorities[name]
						invName = name
						invNumber = StorageUtils.getInventoryNumber(name)
					end
				end
			end
		end
	end
	return invName
end

function Storage.new(ItemDb, ThreadingManager)
	PC.expectInterface(1, IitemDb, ItemDb)
	PC.expectInterface(2, IThreads, ThreadingManager)

	local funcTable = {}
	local this = {
		storageInventories = {},
		externalInventories = {},
		locked = Utils.createBinarySemaphore()
	}

	---add inventory to storage array
	---@param itemInventory table implementation of IitemInventory interface
	---@param priority number | nil priority of storage within the array
	---@param whitelist table | nil whitelist, nil is no whitelist
	function funcTable.addInventoryToStorageArray(itemInventory, priority, whitelist)
		PC.expectInterface(1, IitemInventory, itemInventory)
		PC.expect(2, priority, "number", "nil")
		PC.expect(3, whitelist, "table", "nil")
		--filter duplicates out
		local name = itemInventory.getName()
		for _, inventory in pairs(this.storageInventories) do
			if inventory.IitemInventory.getName() == name then
				return
			end
		end
		this.storageInventories[name] = StorageUtils.createInventoryInfo(ItemDb, itemInventory, priority, whitelist)
	end

	---remove storage from array by name
	---@param inventoryName string storage to remove from storageArray
	function funcTable.removeInventoryFromStorageArray(inventoryName)
		PC.expect(1, inventoryName, "string")
		for index, inventory in pairs(this.storageInventories) do
			if inventory.IitemInventory.getName() == inventoryName then
				--wait until it is done with the current action
				Utils.takeBinarySemaphore(this.locked)
				this.storageInventories[index] = nil
				Utils.freeBinarySemaphore(this.locked)
			end
		end
	end

	---get itemcount for item in storage array
	---@param name string item name
	---@param nbt string | nil nbthash of the item to find
	---@param damage number | nil damage for item, nil if item doesnt have damage value
	function funcTable.GetItemCount(name, nbt, damage)
		PC.expect(1, name, "string")
		PC.expect(2, nbt, "string", "nil")
		PC.expect(3, damage, "number", "nil")
		local count = 0
		if damage ~= nil then
			nbt = nil
		end
		for _, inventoryInfo in pairs(this.storageInventories) do
			local itemInfo = StorageUtils.getItemCountWithSlot(inventoryInfo.items, name, nbt, damage)
			for _, itemCount in pairs(itemInfo) do
				count = count + itemCount
			end
		end
		return count
	end

	---import items from a slot into storage
	---@param fromIitemInventory table implementation of IitemInventory interface
	---@param slotInfo table information about items needing to be moved indexed by slot {count, name, <optional>damage, <optional>nbt}
	---@return table | StatusCodeCode itemsMoved amount of items moved per slot, statuscode on failure
	function funcTable.importFromSlots(fromIitemInventory, slotInfo)
		PC.expectInterface(1, IitemInventory, fromIitemInventory)
		PC.expect(2, slotInfo, "table")
		if Utils.getTableLength(slotInfo) == 0 then
			return {}
		end

		local needsToBeAddedToDb = {}
		for slot, itemInfo in pairs(slotInfo) do
			if itemInfo.damage ~= nil then
				itemInfo.nbt = nil
			end
			if not ItemDb.itemIsInDb(itemInfo.name, itemInfo.nbt) then
				table.insert(needsToBeAddedToDb, slot)
			end
		end
		--yields once if items need to be in db
		addMissingEntriesToItemDb(ThreadingManager, fromIitemInventory, needsToBeAddedToDb, ItemDb)

		--copy item list so we can modify without issues
		Utils.takeBinarySemaphore(this.locked)
		local itemListCopy = {}
		for name, inventory in pairs(this.storageInventories) do
			itemListCopy[name] = Utils.copyTable(inventory.items)
		end

		--generate plan for moving all items
		local movePlan = {}
		for slot, info in pairs(slotInfo) do
			local toMove = info.count
			local blacklist = {}
			movePlan[slot] = {}
			while toMove > 0 do
				local inventoryInfo = StorageUtils.findInventoryForItem(this.storageInventories, info.name, blacklist)
				--no inventory for item found
				if inventoryInfo == nil then
					return StatusCode.Code.NoSpace
				end
				local invName = inventoryInfo.IitemInventory.getName()

				table.insert(blacklist, invName)
				--ignore the limits and shove everything in the slot
				if inventoryInfo.IitemInventory.isBulkStorage() then
					addItemToMovePlanForBulkStorage(movePlan, inventoryInfo, invName, itemListCopy, slot, info, toMove)
				else
					toMove = addItemToMovePlanForNormalStorage(movePlan, inventoryInfo, invName, itemListCopy, slot, info,
						toMove, ItemDb)
				end
			end
		end
		--if properly implemented no yielding so no ticks
		--execute move plan
		--lock all inventories we interact with and start moving
		local moved = {} --items moved
		local sem = Utils.createCountingSemaphore()
		for fromSlot, entry in pairs(movePlan) do
			moved[fromSlot] = 0
			for _, move in pairs(entry) do
				Utils.takeCountingSemaphore(sem)
				ThreadingManager.create(
					function()
						local mv = move.inventoryInfo.IitemInventory.pullItems(fromIitemInventory, fromSlot, move.count,
							move.slot)
						moved[fromSlot] = moved[fromSlot] + mv
						Utils.freeCountingSemaphore(sem)
					end
				)
			end
		end
		if not Utils.runThreadingManagerIfNeeded(ThreadingManager) then
			Utils.awaitCountingSemaphore(sem)
		end
		Utils.freeBinarySemaphore(this.locked)
		--copy itemlist to storage inventories
		for name, itemList in pairs(itemListCopy) do
			this.storageInventories[name].items = itemList
		end
		return moved
	end

	---export item from storage into inventories
	---@param exportInfo table table keyed by inventoryname and each entry having a table containing {name, count, <optional>nbt, <optional> damage, <optional>slot}
	function funcTable.export(exportInfo)
		for inventoryName, info in pairs(exportInfo) do
			for _, inf in pairs(info) do
				PC.field(inf, "name", "string")
				PC.field(inf, "count", "number", "nil")
				PC.field(inf, "nbt", "string", "nil")
				PC.field(inf, "damage", "number", "nil")
				PC.field(inf, "slot", "number", "nil")
			end
			if this.externalInventories[inventoryName] == nil then
				this.externalInventories[inventoryName] = ItemInventoryFactory.new(ThreadingManager, inventoryName)
			end
		end
		--copy item list so we can modify without issues
		--lock storage to prevent changes
		Utils.takeBinarySemaphore(this.locked)
		local itemListCopy = {}
		local priorityList = {}
		for name, inventory in pairs(this.storageInventories) do
			itemListCopy[name] = Utils.copyTable(inventory.items)
			priorityList[name] = inventory.priority
		end
		--generate plan for moving all items
		local movePlan = {}
		for inventoryName, inf in pairs(exportInfo) do
			movePlan[inventoryName] = {}
			for _, info in pairs(inf) do
				if info.damage ~= nil then
					info.nbt = nil
				end
				local toMove = info.count
				local blacklist = {}
				while toMove > 0 do
					local invName = findLowestPriorityInventoryWithItem(itemListCopy, priorityList, info.name, info.nbt,
						info.damage, blacklist)
					--no items left of type
					if invName == nil then
						break
					end
					table.insert(blacklist, invName)
					for slot, itemInfo in pairs(itemListCopy[invName]) do
						if itemInfo.name == info.name then
							if itemInfo.name == info.name and ((info.damage and itemInfo.damage and info.damage == info.damage) or
									(info.nbt and itemInfo.nbt and info.nbt == itemInfo.nbt) or
									(info.damage == nil and info.nbt == nil and itemInfo.nbt == nil and (itemInfo.damage == nil or itemInfo.damage == 0)))
							then
								local moved = math.min(itemInfo.count, toMove)
								table.insert(movePlan[inventoryName],
									createMovePlanEntry(this.storageInventories[invName], moved, info.slot, slot,
										info.name, info.damage, info.nbt))
								toMove = toMove - moved
								itemListCopy[invName][slot].count = itemListCopy[invName][slot].count - moved
								if itemListCopy[invName][slot] == 0 then
									itemListCopy[invName][slot] = nil
								end
								if toMove == 0 then
									break
								end
							end
						end
					end
				end
			end
		end
		-- 0 ticks passed if programmed properly
		--start moving items
		local moved = {}
		for name, _ in pairs(this.storageInventories) do
			moved[name] = {}
		end
		local sem = Utils.createCountingSemaphore()
		for toInventory, entry in pairs(movePlan) do
			for _, move in pairs(entry) do
				Utils.takeCountingSemaphore(sem)
				local ourName = move.inventoryInfo.IitemInventory.getName()
				moved[ourName][move.fromSlot] = moved[ourName][move.fromSlot] or
				{ slot = move.slot, count = 0, name = move.name, nbt = move.nbt, damage = move.damage }
				ThreadingManager.create(
					function()
						local mv = move.inventoryInfo.IitemInventory.pushItems(this.externalInventories[toInventory],
							move.fromSlot, move.count, move.slot)
						moved[ourName][move.fromSlot].count = moved[ourName][move.fromSlot].count + mv
						Utils.freeCountingSemaphore(sem)
					end
				)
			end
		end
		if not Utils.runThreadingManagerIfNeeded(ThreadingManager) then
			Utils.awaitCountingSemaphore(sem)
		end
		Utils.freeBinarySemaphore(this.locked)
		--copy changed items to storageInventories
		local retList = {}
		for name, info in pairs(moved) do
			for slot, mvInfo in pairs(info) do
				this.storageInventories[name].items[slot].count = this.storageInventories[name].items[slot].count -
				mvInfo.count
				if this.storageInventories[name].items[slot].count == 0 then
					this.storageInventories[name].items[slot] = nil
				end
				retList[mvInfo.name] = retList[mvInfo.name] or {}
				mvInfo.nbt = mvInfo.nbt or "None"
				retList[mvInfo.name][mvInfo.nbt] = retList[mvInfo.name][mvInfo.nbt] or 0
				retList[mvInfo.name][mvInfo.nbt] = retList[mvInfo.name][mvInfo.nbt] + mvInfo.count
			end
		end
		return retList
	end

	---dumps a list of all items in the storage array
	---@return table { {name, count, damage, maxDamage},{name, count, damage, maxDamage}}
	function funcTable.dump()
		Utils.takeBinarySemaphore(this.locked)
		local export = {}
		for _, inventory in pairs(this.storageInventories) do
			for _, itemInfo in pairs(inventory.items) do
				export[itemInfo.name] = export[itemInfo.name] or {}
				local exists = false
				for _, info in pairs(export[itemInfo.name]) do
					if info.damage ~= nil and itemInfo.damage ~= nil and info.damage == itemInfo.damage then
						exists = true
						info.count = info.count + itemInfo.count
						break
					elseif info.nbt ~= nil and itemInfo.nbt ~= nil and info.nbt == itemInfo.nbt then
						exists = true
						info.count = info.count + itemInfo.count
						break
					elseif itemInfo.damage == nil and itemInfo.nbt == nil then
						exists = true
						info.count = info.count + itemInfo.count
						break
					end
				end
				if not exists then
					local nbt = itemInfo.nbt
					if itemInfo.damage then
						nbt = nil
					end
					local dbInfo = ItemDb.getItem(itemInfo.name, nbt)
					local item = {
						damage = itemInfo.damage,
						count = itemInfo.count,
						displayName = dbInfo.displayName,
						maxDamage = dbInfo.maxDamage,
						nbt = nbt
					}
					table.insert(export[itemInfo.name], item)
				end
			end
		end
		Utils.freeBinarySemaphore(this.locked)
		return export
	end

	---restacks all inventories
	function funcTable.restack()
		Utils.takeBinarySemaphore(this.locked)
		--local sem = Utils.createCountingSemaphore()
		--for name, _ in pairs(this.storageInventories) do
		--	Utils.takeCountingSemaphore(sem)
		--	ThreadingManager.create(
		--		function()
		--			StorageUtils.defragment(this.storageInventories[name], ItemDb)
		--			Utils.freeCountingSemaphore(sem)
		--		end
		--	)
		--end
		--if not Utils.runThreadingManagerIfNeeded(ThreadingManager) then
		--	Utils.awaitCountingSemaphore(sem)
		--end

		--todo: figure out method of sorting multiple chests at the same time without hitting coroutine limit
		for name, _ in pairs(this.storageInventories) do
			StorageUtils.defragment(this.storageInventories[name], ItemDb)
		end
		Utils.freeBinarySemaphore(this.locked)
	end

	return funcTable
end

return Storage
