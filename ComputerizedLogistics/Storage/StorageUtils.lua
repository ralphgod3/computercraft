local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/IitemInventory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryUtils.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local IitemDb = require("Interfaces.Modules.IitemDb")
local IitemInventory = require("Interfaces.Modules.IitemInventory")
local Threads = require("Modules.Threads")
local ItemInventoryUtils = require("Modules.IitemInventory.ItemInventoryUtils")
local Dbg = require("Modules.Logger")


local StorageUtils = {}
--inventoryInfo mock
--local inventoryInfo = {
--    locked = Utils.createBinarySemaphore(), --semaphore to lock access from inventory until operation completes
--	priority = 10,
--	whitelist = {}, -- optional
--	IitemInventory = {},
--	items = {
--		[slotNr] = {
--			name = "minecraft:cobblestone",
--			nbt = "aioudfahwdoiad",
--			damage = 1,
--			count = 10
--		}
--	}
--}

---rebuild information about items in the inventory, internal only, does not lock the inventory
---@param inventoryInfo table inventoryInfo entry should NOT be a copy
---@param ItemDb table implementation of IitemDb interface
local function internalRebuildInventoryInfo(inventoryInfo, ItemDb)
    inventoryInfo.items = {}
    for slot, itemInfo in pairs(inventoryInfo.IitemInventory.getDetailedItemList()) do
        --add item to database if it doesnt exist yet
        if itemInfo.maxDamage ~= nil then
            itemInfo.nbt = nil
        end
        if ItemDb.getItem(itemInfo.name, itemInfo.nbt) == nil then
            ItemDb.addItem(itemInfo)
        end
        local itemEntry =
        {
            name = itemInfo.name,
            count = itemInfo.count,
            damage = itemInfo.damage,
            nbt = itemInfo.nbt
        }
        inventoryInfo.items[slot] = itemEntry
        if inventoryInfo.IitemInventory.isBulkStorage() then
            inventoryInfo.items[slot].count = inventoryInfo.items[slot].count -1
        end
    end
end

---gets the amount of itemName in a inventory
---@param itemInfo table inventoryInfo table
---@param itemName string itemName
---@param nbt string | nil nbt hash of item to search for
---@param damage number | nil damage of the item to search for
---@return table itemCount amount of items of itemName in inventory given with slot as key, empty table means no items present
function StorageUtils.getItemCountWithSlot(itemInfo, itemName, nbt, damage)
	PC.expect(1, itemInfo, "table")
	PC.expect(2, itemName, "string")
	PC.expect(3, nbt, "string", "nil")
	PC.expect(4, damage, "number", "nil")
    if damage ~= nil then
        nbt = nil
    end
	local count = {}
	for slot, item in pairs(itemInfo) do
        if ItemInventoryUtils.itemsAreEqual({name = itemName, damage = damage, nbt = nbt} ,item, true) then
		    count[slot] = item.count
		end
	end
	return count
end

---check if a slot is empty in inventory by using the cache
---@param itemInfo table inventory.items table
---@param slot number slot to check for free space
---@return boolean slotFree true if slot free, false otherwise
function StorageUtils.isSlotEmpty(itemInfo, slot)
	PC.expect(1, itemInfo, "table")
	PC.expect(2, slot, "number")
	return itemInfo.items[slot] == nil
end

---returns the number in the inventory name ie: minecraft:chest_1 will return 1
---@param inventoryName string inventoryName to get number for
---@return number inventoryNumber inventoryNumber
function StorageUtils.getInventoryNumber(inventoryName)
    PC.expect(1, inventoryName, "string")
    local out = string.reverse(inventoryName)
    out = out:sub(1, out:find("_",nil, true) -1)
    return tonumber(out)
end

---finds inventory with highest priority that accepts the item according to whitelist specification, does NOT guarantee the inventory has space
---@param storageArrayInfo table table with all itemInventoryInfo entries in it
---@param itemName string name of the item to store
---@param inventoryBlackList table table of inventories that where previously tried and have been excluded
---@return table | nil inventoryEntry entry of the inventory with space for the item, nil when no inventory was found for item
function StorageUtils.findInventoryForItem(storageArrayInfo, itemName, inventoryBlackList)
	PC.expect(1, storageArrayInfo, "table")
	PC.expect(2, itemName, "string")
    PC.expect(3, inventoryBlackList, "table")
	local highesPriority = nil
    local inventoryEntry = nil
    local invNumber = -1
	for inventoryName, inventory in pairs(storageArrayInfo) do
        --if inventory is not in blacklist
        if Utils.findElementInTable(inventoryBlackList, inventory.IitemInventory.getName()) == nil then
            --if inventory has no whitelist or item is in whitelist
            if inventory.whitelist == nil or Utils.findElementInTable(inventory.whitelist, itemName) then
                --get highest priority inventory
                if highesPriority == nil or highesPriority <= inventory.priority then
                    if invNumber < StorageUtils.getInventoryNumber(inventoryName) then
                        highesPriority = inventory.priority
                        inventoryEntry = inventory
                        invNumber = StorageUtils.getInventoryNumber(inventoryName)
                    end
                end
            end
        end
	end
	return inventoryEntry
end

---restacks all items in an inventory entry
---@param inventoryInfo table inventoryInfo entry
---@param ItemDb table implementation of IitemDb interface
function StorageUtils.defragment(inventoryInfo, ItemDb)
    PC.expectInterface(2, IitemDb, ItemDb)
    --defragmenting bulk storage is useless
    if inventoryInfo.IitemInventory.isBulkStorage() then
        return
    end
    --paralelize all moves to speed them up
    local ThreadManager = Threads.new()
    Utils.takeBinarySemaphore(inventoryInfo.locked)
    for slot,_ in pairs(inventoryInfo.IitemInventory.list()) do
        ThreadManager.create(function ()
            inventoryInfo.IitemInventory.pushItems(inventoryInfo.IitemInventory, slot)
        end)
    end
    ThreadManager.startRunning()
    internalRebuildInventoryInfo(inventoryInfo, ItemDb)
    Utils.freeBinarySemaphore(inventoryInfo.locked)
end

---create inventoryInfo for IitemInventory
---@param ItemDb table IitemDb interface implementation
---@param itemInventory table Iiteminventory interface implementation
---@param priority number | nil priority of the inventory in the storage array, defaults to 0
---@param whitelist table | nil item whitelist of the inventory
---@return table
function StorageUtils.createInventoryInfo(ItemDb, itemInventory, priority, whitelist)
    PC.expectInterface(1, IitemDb, ItemDb)
    PC.expectInterface(2, IitemInventory, itemInventory)
    PC.expect(3, priority, "number", "nil")
    PC.expect(4, whitelist, "table", "nil")
    priority = priority or 0

    if type(whitelist) == "table" then
        whitelist = Utils.copyTable(whitelist)
    end
    local inventoryInfo = {
        locked = Utils.createBinarySemaphore(),
        priority = priority,
        whitelist = whitelist,
        IitemInventory = itemInventory,
        items = {}
    }
    StorageUtils.defragment(inventoryInfo, ItemDb)
    return inventoryInfo
end

return StorageUtils
