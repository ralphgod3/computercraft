-- please edit the config file and not the program to add and remove inputs/outputs
local configFile = "/cache/CLsorter.cache"

-- load git
if not fs.exists("/Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get(
        "https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end

local Git = require("Modules.Git")
--git load finished
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/InventoryConfig.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Parsers/ItemFilterList.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Sorter/IOHandler.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/IitemInventory/ItemInventoryFactory.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ItemDb.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")

local Dbg = require("Modules.Logger")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local Timer = require("Modules.Timer")
local NetworkingHardware = require("Modules.Networking.HW.Modems")
local Networking = require("Modules.Networking.Networking")
local InventoryConfigParser = require("Modules.Parsers.InventoryConfig")
local ItemFilterListParser = require("Modules.Parsers.ItemFilterList")
local IitemInventoryFactory = require("Modules.IitemInventory.ItemInventoryFactory")
local ItemDb = require("Modules.ItemDb")
local IoHandler = require("ComputerizedLogistics.Sorter.IOHandler")
local Messages = require("Modules.Networking.Messages")

--enable printing of debug output to screen
Dbg.setOutputTerminal(term.current())
local TAG = "CLS"
local config = {}
local types = { Messages.remoteTypes.Sorter }
Threads = Threads.new()
Timer = Timer.new(Threads)
ItemDb = ItemDb.new(false)


---create configuration file
local function createConfigFile()
    local config = {}
    --create networking so we can find default broadcast channel
    NetworkingHardware = NetworkingHardware.new()
    Networking = Networking.new(Threads, Timer, NetworkingHardware)
    config.name = "CLsorter"
    config.modemBroadcastChannel = Networking.broadcastChannel
    config.pollTime = 5
    config.inputs = {
        InventoryConfigParser.createConfigEntry(InventoryConfigParser.configTypes.name),
        InventoryConfigParser.createConfigEntry(InventoryConfigParser.configTypes.type)
    }

    config.outputs = {}
    for i = 1, 3 do
        table.insert(config.outputs, InventoryConfigParser.createConfigEntry(InventoryConfigParser.configTypes.name))
        config.outputs[i].priority = i
    end
    config.outputs[1].whitelist = ItemFilterListParser.createSampleList()
    config.outputs[2].blacklist = ItemFilterListParser.createSampleList()
    config.outputs[3].whitelist = ItemFilterListParser.createSampleList()
    config.outputs[3].blacklist = ItemFilterListParser.createSampleList()

    local text = "--config file for CLsorter\r\n--one or more inputs and outputs can be defined.\r\n\r\n"
    text = text .. ItemFilterListParser.createExplanation()
    text = text .. InventoryConfigParser.createExplanation()
    text = text .. textutils.serialize(config)
    local file = fs.open(configFile, "w")
    file.write(text)
    file.close()
    Dbg.logI(TAG, "created " .. configFile .. " edit it and restart program")
    Dbg.logI(TAG,
        "ensure the names are set to the chests computer craft will use, the provided whitelists/blacklists are examples")
end

---reload input inventories, starts a 30 second timer after which inventories will be reloaded again

---reloads input inventories every 30 seconds
---@param IoHandler IOHandler
local function reloadInputs(IoHandler)
    local foundInputs = 0
    for i = 1, #config.inputs do
        local inventories = InventoryConfigParser.parseConfigEntry(config.inputs[i])
        for j = 1, #inventories do
            foundInputs = foundInputs + 1
            local inventory = IitemInventoryFactory.new(Threads, inventories[j])
            IoHandler.addInput(inventory)
        end
    end
    Dbg.logV(TAG, "Found", foundInputs, "inputs")
    --reload inputs every 30 seconds incase a type is set as an input
    Timer.add(30, reloadInputs, IoHandler)
end

---reload output inventories and asociated white, blacklists
local function reloadOutputs()
    local foundOutputs = 0
    for i = 1, #config.outputs do
        local inventories = InventoryConfigParser.parseConfigEntry(config.outputs[i])
        for j = 1, #inventories do
            foundOutputs = foundOutputs + 1
            local inventory = IitemInventoryFactory.new(Threads, inventories[j])
            local whitelist, blacklist = ItemFilterListParser.createFilteredlists(ItemDb, config.outputs[i].whitelist,
                config.outputs[i].blacklist)
            IoHandler.addOutput(inventory, config.outputs[i].priority, whitelist, blacklist)
        end
    end
    Dbg.logV(TAG, "Found", foundOutputs, "outputs")
end

---callback that is called when item override has been completed
local function itemOverrideCallbackFnc(inventory, itemName, itemCount, itemNbt, itemDamage, callbackFncUserArgs)
    Dbg.logI(TAG, "item override completed", itemName)
    Dbg.logI(TAG, tostring(inventory), tostring(itemName), tostring(itemCount), tostring(itemNbt), tostring(itemDamage))
    local resp = Messages.createItemRedirectCompleted(inventory, itemName, itemCount, itemNbt, itemDamage)
    Networking.transmit(callbackFncUserArgs.from, Networking.QOS.EXACTLY_ONCE, resp, callbackFncUserArgs.llId)
end

---handle messages received on broadcast channel
---@param Networking Inetworking
local function NetworkLoopBroadcast(Networking)
    while true do
        local msg = Networking.receive(nil, Networking.broadcastChannel)
        if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs and Utils.elementOfTableExistInTable(msg.payload.types, types) == true then
                Dbg.logI(TAG, "received who is")
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.ATMOST_ONCE, resp, msg.llId)
            end
        end
    end
end

---handle messages received on private channel
---@param Networking Inetworking
---@param IoHandler IOHandler
local function NetworkLoopRequests(Networking, IoHandler)
    local msgTypes = {
        Messages.messageTypes.WhoIs,
        Messages.messageTypes.ItemRedirect,
        Messages.messageTypes.GetInputInventory,
    }

    while true do
        local msg = Networking.receiveMsgTypes(nil, os.getComputerID(), msgTypes)
        if msg and msg.payload and msg.payload.type then
            --who is
            if msg.payload.type == Messages.messageTypes.WhoIs then
                local resp = Messages.createWhoIsResponse(types, config.name)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
                --item redirect
            elseif msg.payload.type == Messages.messageTypes.ItemRedirect then
                Dbg.logI(TAG, "item override received", msg.payload.name, msg.payload.count)
                local suc, result = pcall(IitemInventoryFactory.new, Threads, msg.payload.inventory)
                if not suc then
                    Dbg.logE(TAG, "could not wrap inventory for redirect", msg.payload.inventory)
                else
                    IoHandler.addOverride(result, msg.payload.name, msg.payload.count, msg.payload.nbt,
                        msg.payload.damage,
                        itemOverrideCallbackFnc, msg)
                end
                --input inventory request
            elseif msg.payload.type == Messages.messageTypes.GetInputInventory then
                Dbg.logV(TAG, "received inputInventoryRequest")
                local inventories = IoHandler.getInputInventories()
                local retInventories = {}
                for i = 1, #inventories do
                    table.insert(retInventories, inventories[i].getName())
                end
                local resp = Messages.createInputInventoryResponse(retInventories)
                Networking.transmit(msg.from, Networking.QOS.EXACTLY_ONCE, resp, msg.llId)
            else
                Dbg.logW(TAG, "unknown msg " .. Utils.printTableRecursive(msg))
            end
        end
    end
end

---sends updates over overrides to CLCrafting on a timer
---@param Networking Inetworking instance
---@param IoHandler IOHandler instance
local function sendOverrideInfoToCLCrafting(Networking, IoHandler)
    local overrides = IoHandler.getOverrides()
    for _, v in pairs(overrides) do
        local msg = Messages.createItemRedirectUpdate(v.inventory.getName(), v.name, v.count, v.nbt, v.damage,
            v.count - v.countLeft)
        Networking.transmit(v.callbackFncUserArgs.from, Networking.QOS.ATMOST_ONCE, msg)
    end
    Timer.add(5, sendOverrideInfoToCLCrafting, Networking, IoHandler)
end

-------------------------------------- MAIN ---------------------------------------

--create config if it does not exist
if not fs.exists(configFile) then
    createConfigFile()
    return
end
--load config
local file = fs.open(configFile, "r")
config = file.readAll()
file.close()
config = textutils.unserialise(config)
if config == nil then
    Dbg.logE(TAG, "corrupt config please fix")
    return
end

--create networking
NetworkingHardware = NetworkingHardware.new(config.modemBroadcastChannel)
Networking = Networking.new(Threads, Timer, NetworkingHardware, config.modemBroadcastChannel)
os.setComputerLabel("CLSorter")

--anounce our presence to the network
local msg = Messages.createWhoIsResponse(types, config.name)
Networking.transmit(Networking.broadcastChannel, Networking.QOS.ATMOST_ONCE, msg)

--create iohandler and load in/outputs
IoHandler = IoHandler.new(Threads, Timer, ItemDb, config.pollTime, reloadOutputs)
--load in and outputs
reloadInputs(IoHandler)
reloadOutputs()
--create network threads
Threads.create(sendOverrideInfoToCLCrafting, Networking, IoHandler)
Threads.create(NetworkLoopBroadcast, Networking)
Threads.create(NetworkLoopRequests, Networking, IoHandler)


--start the program
Threads.startRunning()
