--path to load recipe files from
local recipePath = "DataFiles/Recipes/1.16/Default"
--path to load machine recipe files from
local machinePath = recipePath .. "/Machines"
--optional, chest that will be checked on startup to add items to database for
local chestToAddToItemDb = "ironchest:gold_chest_7"


-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Items.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "ComputerizedLogistics/Crafting/CraftingRecipeSolver.lua")

local Dbg = require("Modules.Logger")
local Utils = require("Modules.Utils")
local Items = require("Modules.Items")
local CraftingRecipeSolver = require("ComputerizedLogistics.Crafting.CraftingRecipeSolver")



local p = peripheral.wrap(chestToAddToItemDb)
if p ~= nil then
    print("checking chest for items to add to itemDB")
    for slot, item in pairs(p.list()) do
        if not Items.itemIsInDb(item.name) then
            print("adding " .. item.name .. " to itemDB")
            Items.addItem(p.getItemDetail(slot))
        end
    end
end



local function loadRecipesFromFile(path)
    local file = fs.open(path, "r")
    if file == nil then
        error("can not open " .. path)
    end
    local tempRecipes = file.readAll()
    file.close()
    tempRecipes = textutils.unserialiseJSON(tempRecipes)
    if tempRecipes == nil then
        error("invalid recipes in " .. path .. " file please fix")
    end
    return tempRecipes
end


local function loadCraftingRecipesFromFolder(path)
    local filesToLoadFrom = 0
    for _, file in pairs(fs.list(path)) do
        local filePath = path .. "/" .. file
        if not fs.isDir(filePath) then
            filesToLoadFrom = filesToLoadFrom + 1
            local text = "loaded ".. tostring(CraftingRecipeSolver.ImportRecipes(loadRecipesFromFile(filePath))) .. " recipes"
            Dbg.logI("main", filePath .. " " ..text)
            print(file .. " " ..text)
        end
    end
    print("loaded from " .. tostring(filesToLoadFrom) .. " files")
end

local function loadMachineRecipesFromFolder(path)
    local filesToLoadFrom = 0
    for _, file in pairs(fs.list(path)) do
        local filePath = path .. "/" .. file
        if not fs.isDir(filePath) then
            filesToLoadFrom = filesToLoadFrom + 1
            local text = "loaded ".. tostring(CraftingRecipeSolver.ImportRecipes(loadRecipesFromFile(filePath), Utils.splitString(file, ".")[1])) .. " recipes"
            Dbg.logI("main", filePath .. " " ..text)
            print(file .. " " ..text)
        end
    end
    print("loaded from " .. tostring(filesToLoadFrom) .. " files")
end

print("loading crafting recipes")
loadCraftingRecipesFromFolder(recipePath)
print("loading machine recipes")
loadMachineRecipesFromFolder(machinePath)
local masterRecipeList = CraftingRecipeSolver.DumpCraftableItems()
print(Utils.getTableLength(masterRecipeList))
