---@diagnostic disable: return-type-mismatch
---@class IitemDb
local ItemDb = {}

---get information of item with itemName
---@param itemName string itemName ie: minecraft:cobblestone
---@param nbt string | nil hexadecimal hash of nbt tag, optional if none given will try to find item without nbt tag
---@return DetailedItemEntry | nil itemInformation table containing name, maxDamage, maxCount, tags
function ItemDb.getItem(itemName, nbt)
    return
    {
        arguments = {"string", "string or nil"},
        returns = {"table or nil"}
    }
end


---returns item names for items with the specified tag
---@param tag string tagname to search items for
---@return string[] itemInformation items that have tag (empty table if no item was found)
function ItemDb.getItemsWithTag(tag)
    return
    {
        arguments = {"string"},
        returns = {"table or nil"}
    }
end

---create item information in database for item
---@param itemDetail DetailedItemEntry item information for item to be added, table that contains: (name, maxCount, tags, optionals( nbt, damage, maxDamage))
function ItemDb.addItem(itemDetail)
    return
    {
        arguments = {"table"},
        returns = {}
    }
end

---check if item exists in the database
---@param itemName string itemName ie: minecraft:cobblestone
---@param nbt string | nil md5 nbt hash of item to check, nil or "None" if no nbt is on the item
---@return boolean itemInDb true item is in db, false item is not in db
function ItemDb.itemIsInDb(itemName, nbt)
    return
    {
        arguments = {"string", "string or nil"},
        returns = {"boolean"}
    }
end

return ItemDb
