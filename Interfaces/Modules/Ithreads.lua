---@diagnostic disable: return-type-mismatch
---@class Ithreads
local Ithreads = {}

---create a new thread (coroutine)
---@param func function function to use as coroutine
---@vararg ... function parameters for the coroutine
---@return number threadId thread id
function Ithreads.create(func, ...)
    return
    {
        arguments = {"function", "varArg"},
        returns = {"number"}
    }
end

---create a new thread as backgroundthread(coroutine)
---@param func function to use ase coroutine
---@vararg ... function parameters for the coroutine
---@return number threadId thread id
function Ithreads.createAsBackground(func, ...)
    return
    {
        arguments = {"function", "varArg"},
        returns = {"number"}
    }
end

---returns true if thread is stil alive
---@param threadId number thread id
---@return boolean threadAlive thread is alive
function Ithreads.isAlive(threadId)
    return
    {
        arguments = {"number"},
        returns = {"boolean"}
    }
end

---stops a running thread by id
---@param threadId number
function Ithreads.stop(threadId)
    return
    {
        arguments = {"number"},
        returns = {}
    }
end

---stops all running threads, also kills background threads, dont use unless absolutely nescesary
function Ithreads.stopAll()
    return
    {
        arguments = {},
        returns = {}
    }
end

---starts the coroutine based threading, threads have to be added using the Create function first,
---this function will block indefinetly until an error is thrown or no threads are alive
function Ithreads.startRunning()
    return
    {
        arguments = {},
        returns = {}
    }
end

---checks if threadingmanager is currently running using startRunning
---@return boolean isRunning threadingManager is running
function Ithreads.isRunning()
    return
    {
        arguments = {},
        returns = {"boolean"}
    }
end

return Ithreads