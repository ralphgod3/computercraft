---@diagnostic disable: return-type-mismatch
---@class Itimer
local Timer = {}

---cancel a running timer
---@param id number timer id of the timer that needs to be canceled
function Timer.cancel(id)
    return {
        arguments = {"number"},
        returns = {}
    }
end

---create a timer, requires calling Threads.StartRunning to function
---@param time number timeout for the timer in seconds
---@param func function function the timer should execute on timeout
---@vararg ... parameters for the function that should be executed, may not contain nil parameters since lua var args wont handle that well
---@return number id timer id used to cancel the timer
function Timer.add(time, func, ...)
    return {
        arguments = {"number", "function", "varArg"},
        returns = {"number"}
    }
end

return Timer
