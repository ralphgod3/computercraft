---@diagnostic disable: return-type-mismatch, missing-return-value
---@class IcraftingRecipeSolver
local IcraftingRecipeSolver = {}

---@class RecipeSolverCraftableItem
---@field name string item name
---@field nbt string | nil item nbt
---@field displayName string display name for item

---@class CraftingStep
---@field recipe CraftingRecipeRecipe
---@field type string recipe type ex: minecraft:crafting
---@field requires ItemEntry[] | nil recipes that have to be crafted before being able to craft this one
---@field creates string itemid of created item
---@field outputPerCraft number amount of items created per craft
---@field craftsNeeded number amount of crafts needed to complete step
---@field itemsCreated number amount of items created in total (outputPerCraft*craftsNeeded)

---@class CraftingSolution
---@field itemsToRequest ItemEntry[]
---@field craftingChain CraftingStep[]

---solve a crafting recipe
---@param itemId string itemName to craft
---@param count number number of items to craft
---@param nbt string | nil nbt data of item to craft (nil if none)
---@param itemsInStorage ItemEntry[] items that are currently in storage should be input as {{name = "minecraft:cobblestone", count = 20, nbt = nil}, etc}
---@param timeout number | nil time after which the solver gets killed, pass nill to continue until a solution is found
---@return boolean success true on success, false on failure
---@return CraftingSolution | string result IcraftingSolution (check IcraftingRecipeSolver.lua for structure) on success, msg on failure
function IcraftingRecipeSolver.solve(itemId, count, nbt, itemsInStorage, timeout)
	return {
		arguments = { "string", "string or nil", "number", "table", "number or nil" },
		returns = { "boolean", "table or string" }
	}
end

---dumps all craftable item entries
---@return RecipeSolverCraftableItem[] craftableItems array where each entry contains {name, displayName, <nbt>, <damage>}
function IcraftingRecipeSolver.dumpCraftableItems()
	return {
		arguments = {},
		returns = { "table" }
	}
end

return IcraftingRecipeSolver
