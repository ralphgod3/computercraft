---@class IrecipeDatabase
local IrecipeDatabase = {}


---@class RecipeItemEntry : ItemEntry
---@field type string item | tag (may expand later)

---@class CraftingRecipeRecipe
---@field recipe table<number, RecipeItemEntry> key = slot
---@field type string what type of crafting recipe ex: minecraft:crafting, minecraft:smelting, thermal:pulverizer
---@field count number items produced
---@field optional RecipeItemEntry[] | nil optional item output
---@field width number | nil optional width parameter if recipe uses non standard size
---@field height number | nil optional height parameter if recipe uses non standard size

---@class CraftingRecipe
---@field name string name of item produced
---@field type string item type produced: item | tag
---@field nbt string | nil nbt hash value of item produced
---@field damage string | nil damage value of item produced
---@field recipes CraftingRecipeRecipe[] recipes to produce item


---get all recipes for a recipe type in database
---@param type string recipe type
---@return CraftingRecipe[] recipesForType outputs all recipes for a specific recipe type
function IrecipeDatabase.getAllRecipesForType(type)
    return
    {
        arguments = { "string" },
        returns = { "table" }
    }
end

---get recipe types in the recipe database
---@return string[] recipeTypes table containing name of the recipe types
function IrecipeDatabase.getRecipeTypes()
    return
    {
        arguments = {},
        returns = { "table" }
    }
end

---get list of inventory's items
---exampleUse: FileBasedRecipeDatabase.getRecipesFor("minecraft:oak_planks", nil, {"minecraft:crafting"}, { "item"})
---@param name string itemName of the item you want to get the recipes for
---@param nbt string | nil nbtHash of the item you want to get the recipes for, if nil recipe without nbt output will be used
---@param recipeTypeFilters string[] | nil recipeTypes to consider for output, ensures it wil only output recipes of the type you want, if nil will output all types
---@param itemTypeFilters string[] | nil item types allowed for output ie: item, tag, fluid, etc
---@return CraftingRecipeRecipe[] itemList crafting recipes
function IrecipeDatabase.getRecipesFor(name, nbt, recipeTypeFilters, itemTypeFilters)
    return
    {
        arguments = { "string", "string or nil", "table or nil" },
        returns = { "table" }
    }
end

---adds a recipe to recipe database
---@param recipe CraftingRecipe recipe to add
function IrecipeDatabase.addRecipe(recipe)
    return
    {
        arguments = { "table" },
        returns = {},
    }
end

--not a true interface but this describes how a recipe is expected to be output from getRecipesFor
local Irecipe = {
    --item type currently tag and item are defined and other types may be in here but are not handled yet
    type = "item",
    --name of the output item
    name = "minecraft:oak_planks",
    --optional nbt tag to indicate output item is produced with a certain nbt value
    nbt = nil,
    --recipes to create output item, table with 1 or more numbered entries
    recipes = {
        {
            --recipe for producint output item
            recipe = {
                --indexed by slot number, machines are also indexed by slot nr
                [1] = {
                    --type of the name index same as output item
                    type = "tag",
                    --name of item needed for this slot in recipe
                    name = "minecraft:oak_logs",
                    --amount of item needed for this slot in recipe (crafting recipes will always be 1 but machien recipes can have more)
                    count = 1,
                    --nbt needed for input item, optional
                    nbt = "awdoawe231904"
                }
            },
            --recipe type (action needed to craft this, minecraft:crafting is for normal crafting recipes)
            type = "minecraft:crafting",
            --amount of output items produced with this recipe
            count = 4
        },
        {
            --example of machine based recipe
            recipe = {
                [1] = {
                    type = "item",
                    name = "minecraft:stripped_oak_wood",
                    count = 1
                }
            },
            type = "create:cutting",
            count = 5
        },
        {
            --example of recipe with optional outputs
            recipe = {
                [1] = {
                    type = "tag",
                    name = "minecraft:oak_logs",
                    count = 1
                }
            },
            --optional, optional tag which indicates recipe may or will output additional items do NOT use these as a guaranteed output because they are not most of the time
            optional = {
                {
                    --as with the other item entries this entry can also have the nbt tag
                    type = "item",
                    name = "thermal:sawdust",
                    count = 1
                }
            },
            count = 6,
            type = "thermal:sawmill"
        },
    }
}


return IrecipeDatabase
