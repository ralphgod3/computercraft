---@class IpreferredItemDb
local IpreferredItemDb = {}

---get preffered items for tag
---@param tag string tag to get items for
---@return string[] | nil items table of items if tag exists, nil otherwise
function IpreferredItemDb.getItemsWithTag(tag)
    return
    {
        arguments = {"string"},
        returns = {"table or nil"},
    }
end


---add item to tag in preferred list
---@param tagName string tagName
---@param itemName string itemName
function IpreferredItemDb.addItem(tagName, itemName)
    return
    {
        arguments = {"string", "string"},
        returns = {},
    }
end

return IpreferredItemDb