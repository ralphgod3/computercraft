---@diagnostic disable: return-type-mismatch
---@class IitemInventory
local IitemInventory = {}

---@class ItemEntry
---@field name string
---@field nbt string | nil
---@field damage number | nil
---@field count number | nil
---@field slot number | nil

---@class DetailedItemEntry : ItemEntry
---@field maxDamage number | nil
---@field tags table<string, boolean> | nil
---@field maxCount number

---get list of inventory's items
---@return table<number, ItemEntry> itemList table indexed by slot (only uses name, nbt, damage)
function IitemInventory.list()
    return
    {
        arguments = {},
        returns = { "table" }
    }
end

---get item detail
---@param slot number
---@return DetailedItemEntry itemDetail table containing name, displayName, count, damage, maxDamage, nbt, tags
function IitemInventory.getItemDetail(slot)
    return
    {
        arguments = { "number" },
        returns = { "table or nil" }
    }
end

---get detailed item list
---@return table<number, DetailedItemEntry> detailedItemList table indexed by slot
function IitemInventory.getDetailedItemList()
    return
    {
        arguments = {},
        returns = { "table" }
    }
end

---get inventory size
---@return number size of inventory
function IitemInventory.size()
    return
    {
        arguments = {},
        returns = { "number" }
    }
end

---get inventory name on wired network
---@return string inventoryName name of the inventory on wired network
function IitemInventory.getName()
    return
    {
        arguments = {},
        returns = { "string" }
    }
end

---push items from this inventory to other inventory
---@param toIitemInventory IitemInventory IitemInventory interface implementation
---@param fromSlot number slot number
---@param limit number | nil limit
---@param toSlot number | nil slot number to push items to
---@return number itemsMoved number of items that were moved
function IitemInventory.pushItems(toIitemInventory, fromSlot, limit, toSlot)
    return
    {
        arguments = { "table", "number", "number or nil", "number or nil" },
        returns = { "number" }
    }
end

---pull items from other inventory to this inventory
---@param fromIitemInventory IitemInventory IitemInventory interface implementation
---@param fromSlot number slot number to pull item from
---@param limit number limit
---@param toSlot number slot number to push items to
---@return number itemsMoved number of items that were moved
function IitemInventory.pullItems(fromIitemInventory, fromSlot, limit, toSlot)
    return
    {
        arguments = { "table", "number", "number or nil", "number or nil" },
        returns = { "boolean" }
    }
end

---push items from multiple slots to other inventory
---@param toIitemInventory IitemInventory IitemInventory interface implementation
---@param fromSlots table<number, number> table indexed by slots with amount as value ie {[5] = 64, [6] = 12} moves 64 items from slot 5 and 12 from slot 6
---@param force boolean | nil keep attempting to move until all items got moved, defaults to false
---@return boolean success all items in slots got moved
function IitemInventory.pushItemsFromSlots(toIitemInventory, fromSlots, force)
    return
    {
        arguments = { "table", "table" },
        returns = { "boolean" }
    }
end

---pull items from multiple slots to this inventory
---@param fromIitemInventory IitemInventory IitemInventory interface implementation
---@param fromSlots table<number,number> table indexed by slots with amount as value ie {[5] = 64, [6] = 12} moves 64 items from slot 5 and 12 from slot 6
---@param force boolean | nil keep attempting to move until all items got moved, defaults to false
---@return boolean success all items in slots got moved
function IitemInventory.pullItemsFromSlots(fromIitemInventory, fromSlots, force)
    return
    {
        arguments = { "table", "table" },
        returns = { "boolean" }
    }
end

---pull entire inventory contents
---@param fromIitemInventory IitemInventory IitemInventory interface implementation
---@param forceEmpty boolean | nil if true keep trying until inventory is empty, defaults to false
---@return boolean result success/fail
function IitemInventory.pullInventory(fromIitemInventory, forceEmpty)
    return
    {
        arguments = { "table", "boolean" },
        returns = { "boolean" }
    }
end

---push entire inventory contents
---@param toIitemInventory IitemInventory IitemInventory interface implementation
---@param forceEmpty boolean | nil if true keep trying until inventory is empty, defaults to false
---@return boolean result success/fail
function IitemInventory.pushInventory(toIitemInventory, forceEmpty)
    return
    {
        arguments = { "table", "boolean" },
        returns = { "boolean" }
    }
end

---check if wrapped inventory is a bulk storage inventory, meaning it is a special snowflake that may not have a set size etc.
---@return boolean isBulkStorage inventory is bulk storage inventory
function IitemInventory.isBulkStorage()
    return
    {
        arguments = {},
        returns = { "boolean" }
    }
end

return IitemInventory
