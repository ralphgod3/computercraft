# computercraft  
  
to use run this in a cc terminal:  
wget https://gitlab.com/api/v4/projects/23712047/repository/files/Installer%2FInstaller.lua/raw?ref=master installer.lua  

## must have  

1. create decent readme for Computerized Logistics  
1. create video showcasing functionality of CL  
1. create seperate stable repo for CL  

## nice to have  

1. add program that can keep certain items automatically stocked by autocrafting them (maybe later might cause problems in v1 with duplicate requests)  
1. make monitor that displays current crafting progress of each active craft being performed  
1. make someone finish the CLPortable implementation  
1. improve error messages from RecipeSolver (might not be an issue if you dont import all recipes automagically)  
1. create GUI configurator for all of the CL programs either wired or with a relay computer  
1. add program that can trigger redstone outputs based on item counts in the network  
1. properly fix treefarm coord generation with both positive and negative numbers  
1. create display to display storage full percentage with top items  

## could have  

## programs to add  

1. program to find chest names  
1. make better version of tunnel program for early game mining  

## bugs and features  

1. verify unstackable crafts like turtles work  
1. fix MachineInterface erroring when no machines are found  
