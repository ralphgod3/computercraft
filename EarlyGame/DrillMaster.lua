-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Websockets.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")

local Dbg = require("Modules.Logger")
local Utils = require("Modules.Utils")
local Threads = require("Modules.Threads")
local Timers = require("Modules.Timer")
local Messages = require("Modules.Networking.Messages")
local Network = require("Modules.Networking.Networking")
local ModemHardware = require("Modules.Networking.HW.Modems")
local Websockets = require("Modules.Networking.HW.Websockets")

Threads = Threads.new()
Timers = Timers.new(Threads)
if peripheral.find("modem") ~= nil then
	Network = Network.new(Threads, Timers, ModemHardware.new())
else
	Network = Network.new(Threads, Timers, Websockets.new())
end




--------------------------------------- Variables --------------------------------------
local cacheFile = "/cache/DrillMaster.cache"
local holeCacheFile = "/cache/HoleCache.cache"
local taskCacheFile = "/cache/TurtleTasks.cache"
local types = { Messages.remoteTypes.DrillerController }
local name = "drillController"
local config = {}
local holes = {}
--contains all information about digging turtles
local turtleTasks = {}
--height above hole level to travel at for turtles
local travelToHoleHeight = 2
local travelFromHoleHeight = 3

--template for mining pattern used to generate the holes
local miningPattern = {
	{ 1, 0, 0, 0, 0 }, -- 0
	{ 0, 0, 1, 0, 0 }, -- 2
	{ 0, 0, 0, 0, 1 }, -- 4
	{ 0, 1, 0, 0, 0 }, -- 1
	{ 0, 0, 0, 1, 0 }, -- 3
}

local minTimeBetweenJoins = 20
local connectedTurtles = {}

--------------------------------------- FUNCTIONS --------------------------------------


---generates the starting locations for the mining pattern
---@param startX integer start location x axis
---@param stopX integer stop location x axis
---@param startZ integer start location z axis
---@param stopZ integer stop location z axis
---@param startY integer start location y axis
---@return table startingLocations startlocations consists of coordinate sets
local function GenerateMiningPattern(startX, stopX, startZ, stopZ, startY)
	--build structure for holeCache file
	local holeData = {}
	--generate mining pattern
	local patternLoc = 1

	for x = startX, stopX do
		for z = startZ, stopZ do
			local zArr = miningPattern[patternLoc]

			if zArr[math.abs(z % #zArr + 1)] == 1 then
				table.insert(holeData, Utils.createCoordinateSet(x, startY, z, Utils.Facing.North))
			end
		end

		patternLoc = patternLoc + 1

		if patternLoc > #miningPattern then
			patternLoc = 1
		end
	end

	return holeData
end

---finds the shortest path between the starting node and any node in the list
---@param startPosition table position that the hop should start at {x, y, z}
---@param listOfPositions table table of possible end positions {x, y, z}
---@return table shortestHopCoordinates table entry containing the closest hop coordinates
local function FindShortestHop(startPosition, listOfPositions)
	local shortestDist = 99999
	local shortestPos

	for i = 1, #listOfPositions do
		local dist = Utils.calculateDistance(startPosition, listOfPositions[i])

		if dist < shortestDist then
			shortestDist = dist
			shortestPos = listOfPositions[i]
		end
	end

	return shortestPos
end

---serialize and save a table to file
---@param fileLocation string file location and name
---@param dataToSave table table to serialize and save
local function SaveTableToFile(fileLocation, dataToSave)
	local file = fs.open(fileLocation, "w")
	file.write(textutils.serialize(dataToSave, { compact = true }))
	file.close()
end

---load and unserialize table from file
---@param fileLocation string file location
---@return table? table or nil depending on success
local function LoadTableFromFile(fileLocation)
	local file = fs.open(fileLocation, "r")

	if file ~= nil then
		local ret = textutils.unserialise(file.readAll())
		file.close()

		return ret
	end

	return nil
end

local function CreateConfigFile()
	local contents = Git.downloadFileFromGit(Git.ComputerCraftProjectID, "DataFiles/ItemCategories.json")
	contents = textutils.unserialiseJSON(contents)
	if not contents then
		error("could not download item file of git, check connection")
	end

	local config = {}
	config.valuableBlocks = contents.Ores
	config.unbreakable = contents.Unbreakable
	config.corner1 = Utils.createCoordinateSet()
	config.corner2 = Utils.createCoordinateSet()
	config.idleLocation = Utils.createCoordinateSet()
	config.fuelChestLocation = Utils.createCoordinateSet()
	config.dropOffLocation = Utils.createCoordinateSet()
	config.maxFuelLevel = 2560 --1 stack of charcoal or coal as min moves

	local fp = fs.open(cacheFile, "w")
	fp.write(textutils.serialize(config))
	fp.close()
end

---estimates fuel cost for digging another hole and dropping items for current turtle position
---@param turtlePosition table {x,y,z,f}
---@param holeDepth number amount of blocks to dig down to reach hole bottom
---@return number estimatedFuelCost estimation of fuel cost
local function EstimateFuelCostsForNearestHole(turtlePosition, holeDepth)
	--find nearest hole to go too
	--todo: check if holes list is empty
	local fuelCost = 0
	if #holes.toDig ~= 0 then
		local nearestHole = FindShortestHop(turtlePosition, holes.toDig)
		fuelCost = Utils.calculateDistance(turtlePosition, nearestHole)
		fuelCost = fuelCost + Utils.calculateDistance(config.fuelChestLocation, nearestHole)
	end
	--calculate in height differences when traveling
	fuelCost = fuelCost + (travelToHoleHeight * 2) + (travelFromHoleHeight * 2)
	--calculate in hole depth
	fuelCost = fuelCost + (holeDepth * 2)
	--calculate in dropping and refueling
	fuelCost = fuelCost + Utils.calculateDistance(config.fuelChestLocation, config.dropOffLocation)
	return fuelCost
end



---generates a set of tasks for turtle and assigns the mto said turtle
---@param turtleId number turtle id
local function GenerateAndAssignTurtleTasks(turtleId)
	local travelYHeight = math.max(config.fuelChestLocation.y, config.dropOffLocation.y, config.corner1.y,
		config.corner2.y, config.idleLocation.y)

	print("generating new task for turtle " .. tostring(turtleId))
	local msg = Messages.createInfoRequest()
	local msgId = Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, msg)
	local resp = Network.receive(5, nil, msgId)
	if resp == nil or resp.payload.type ~= Messages.messageTypes.InfoRequestResponse then
		--turtle didnt respond or wrong response was received, do not add to digging network
		print("no turtle response on info request")
		return
	end
	turtleTasks[turtleId].position = resp.payload.position
	turtleTasks[turtleId].fuelLevel = resp.payload.fuelLevel

	local stopY = math.min(config.corner1.y, config.corner2.y)
	local startY = math.max(config.corner1.y, config.corner2.y)
	local depth = startY - stopY


	local fuelCosts = EstimateFuelCostsForNearestHole(turtleTasks[turtleId].position, depth)
	if fuelCosts > turtleTasks[turtleId].fuelLevel then
		print("turtle " .. tostring(turtleId) .. " is getting refueled")
		--turtle should refuel before starting to dig
		--move turtle into travel height
		local travelHeightPos = Utils.copyTable(turtleTasks[turtleId].position)
		travelHeightPos.y = travelYHeight + travelFromHoleHeight
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(travelHeightPos, false))
		--move turtle to drop location and drop items
		local dropLocation = Utils.copyTable(config.dropOffLocation)
		dropLocation.y = dropLocation.y + 1
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(dropLocation, false))
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createDropItemRequest(Messages.direction.Down))
		--move turtle to refuel location and refuel
		local refuelLocation = Utils.copyTable(config.fuelChestLocation)
		refuelLocation.y = refuelLocation.y + 1
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(refuelLocation, false))
		table.insert(turtleTasks[turtleId].queuedTasks,
			Messages.createRefuelRequest(Messages.direction.Down, config.maxFuelLevel))
		--generated tasks for turtle, pick first from queue, save to file and send to turtle
		turtleTasks[turtleId].currentTask = table.remove(turtleTasks[turtleId].queuedTasks, 1)
		turtleTasks[turtleId].position = Utils.copyTable(refuelLocation)
		SaveTableToFile(taskCacheFile, turtleTasks)
		Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
	elseif #holes.toDig > 0 then
		print(tostring(turtleId) .. " digging next hole, holes left " .. tostring(#holes.toDig))
		--start digging a hole
		local nearestHole = FindShortestHop(turtleTasks[turtleId].position, holes.toDig)
		for i = 1, #holes.toDig do
			if holes.toDig[i].x == nearestHole.x and holes.toDig[i].z == nearestHole.z then
				table.remove(holes.toDig, i)
				break
			end
		end
		print("hole pos x " ..
			tostring(nearestHole.x) .. " y " .. tostring(nearestHole.y) .. " z " .. tostring(nearestHole.z))
		--move turtle into travel height

		local travelHeightPos = Utils.copyTable(turtleTasks[turtleId].position)
		travelHeightPos.y = travelYHeight + travelToHoleHeight
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(travelHeightPos, false))
		--move turtle above hole
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(nearestHole, false))
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createDrillRequest(stopY))
		table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(nearestHole, true))

		turtleTasks[turtleId].currentTask = table.remove(turtleTasks[turtleId].queuedTasks, 1)
		turtleTasks[turtleId].position = Utils.copyTable(nearestHole)
		SaveTableToFile(taskCacheFile, turtleTasks)
		SaveTableToFile(holeCacheFile, holes)
		Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
	else
		--check if turtle is already at idle location
		if turtleTasks[turtleId].position.x ~= config.idleLocation.x and turtleTasks[turtleId].position.z ~= config.idleLocation.z then
			print("sending turtle to idle location")
			--move turtle into travel height
			local travelHeightPos = Utils.copyTable(turtleTasks[turtleId].position)
			travelHeightPos.y = travelYHeight + travelFromHoleHeight
			table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(travelHeightPos, false))
			--move turtle to drop location and drop items
			local dropLocation = Utils.copyTable(config.dropOffLocation)
			dropLocation.y = dropLocation.y + 1
			table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(dropLocation, false))
			table.insert(turtleTasks[turtleId].queuedTasks, Messages.createDropItemRequest(Messages.direction.Down))
			--move turtle to refuel location and refuel
			local refuelLocation = Utils.copyTable(config.fuelChestLocation)
			refuelLocation.y = refuelLocation.y + 1
			table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(refuelLocation, false))
			table.insert(turtleTasks[turtleId].queuedTasks,
				Messages.createRefuelRequest(Messages.direction.Down, config.maxFuelLevel))
			table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(config.idleLocation, false))
			turtleTasks[turtleId].currentTask = table.remove(turtleTasks[turtleId].queuedTasks, 1)
			turtleTasks[turtleId].position = Utils.copyTable(config.idleLocation)
			SaveTableToFile(taskCacheFile, turtleTasks)
			Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
		else
			turtleTasks[turtleId].currentTask = nil
			turtleTasks[turtleId].queuedTasks = {}
			SaveTableToFile(taskCacheFile, turtleTasks)
		end
	end
end

---generates tasks needed to clear inventory and resume mining
---@param turtleId any
local function GenerateTasksForInventoryFull(turtleId)
	local travelYHeight = math.max(config.fuelChestLocation.y, config.dropOffLocation.y, config.corner1.y,
		config.corner2.y, config.idleLocation.y)
	--cache position task and clear queued tasks
	local holeLocation = turtleTasks[turtleId].queuedTasks[1].position
	turtleTasks[turtleId].queuedTasks = {}

	local travelHeightPos = Utils.copyTable(holeLocation)
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(travelHeightPos, true))
	travelHeightPos.y = travelYHeight + travelFromHoleHeight
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(travelHeightPos, false))
	--move turtle to drop location and drop items
	local dropLocation = Utils.copyTable(config.dropOffLocation)
	dropLocation.y = dropLocation.y + 1
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(dropLocation, false))
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createDropItemRequest(Messages.direction.Down))
	--move turtle to refuel location and refuel
	local refuelLocation = Utils.copyTable(config.fuelChestLocation)
	refuelLocation.y = refuelLocation.y + 1
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(refuelLocation, false))
	table.insert(turtleTasks[turtleId].queuedTasks,
		Messages.createRefuelRequest(Messages.direction.Down, config.maxFuelLevel))
	refuelLocation.y = travelYHeight + travelToHoleHeight
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(refuelLocation, false))
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(holeLocation, false))
	-- travel back to y height
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(turtleTasks[turtleId].position, true))
	--resume digging
	table.insert(turtleTasks[turtleId].queuedTasks, Utils.copyTable(turtleTasks[turtleId].currentTask))
	table.insert(turtleTasks[turtleId].queuedTasks, Messages.createGoTo(holeLocation, true))
	--generated tasks for turtle, pick first from queue, save to file and send to turtle
	turtleTasks[turtleId].currentTask = table.remove(turtleTasks[turtleId].queuedTasks, 1)
	turtleTasks[turtleId].position = Utils.createCoordinateSet(turtleTasks[turtleId].currentTask.x,
		turtleTasks[turtleId].currentTask.y, turtleTasks[turtleId].currentTask.z)
	SaveTableToFile(taskCacheFile, turtleTasks)
	Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
end


---adds turtle to network, restores any tasks that were running for turtle, preps turtle for mining
---@param turtleId number id of the turtle that joined
local function HandleTurtleJoiningNetwork(turtleId)
	--we dont care if turtle has previously joined we will send updated item lists and request info from turtle
	local msg = Messages.createBlocksToSearchForList(config.valuableBlocks)
	Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, msg)
	local unbreakable = Messages.createUnbreakableBlockList(config.unbreakable)
	Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, unbreakable)
	local infoRequest = Messages.createInfoRequest()
	local msgId = Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, infoRequest)
	local resp = Network.receive(5, nil, msgId)
	if resp == nil or resp.payload.type ~= Messages.messageTypes.InfoRequestResponse then
		--turtle didnt respond or wrong response was received, do not add to digging network
		print("no turtle response on info request not adding turtle")
		return
	end
	--time is used to prevent turtle from responding multiple times in too short a timespawn (crash loop)
	connectedTurtles[turtleId] = os.clock()

	if turtleTasks[turtleId] == nil then
		print("adding new turtle to swarm " .. tostring(turtleId))
		--create new entry for new turtle and assign tasks
		local turtleEntry = {}
		turtleEntry.fuelLevel = resp.payload.fuelLevel
		turtleEntry.position = resp.payload.position
		turtleEntry.currentTask = nil
		turtleEntry.queuedTasks = {}
		turtleTasks[turtleId] = turtleEntry
		--generate new task for turtle
		GenerateAndAssignTurtleTasks(turtleId)
	else
		--update turtle info
		turtleTasks[turtleId].fuelLevel = resp.payload.fuelLevel
		turtleTasks[turtleId].position = resp.payload.position
		print("restoring tasks for " .. tostring(turtleId))
		if turtleTasks[turtleId].currentTask == nil then
			if #turtleTasks[turtleId].queuedTasks == 0 then
				--generate new task for turtle
				GenerateAndAssignTurtleTasks(turtleId)
			else
				--grab task from queue and give it to turtle
				turtleTasks[turtleId].currentTask = table.remove(turtleTasks[turtleId].queuedTasks)
				Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
				SaveTableToFile(taskCacheFile, turtleTasks)
			end
		else
			--tell turtle to keep performing current task
			Network.transmit(turtleId, Network.QOS.EXACTLY_ONCE, turtleTasks[turtleId].currentTask)
		end
	end
end

--handles messages received on personal channel
local function NetworkLoopMain()
	local msgsToListenTo =
	{
		Messages.messageTypes.WhoIs,
		Messages.messageTypes.WhoIsResponse,
		Messages.messageTypes.InventoryFull,
		Messages.messageTypes.TaskDone,
	}
	while true do
		local msg = Network.receiveMsgTypes(nil, os.getComputerID(), msgsToListenTo)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs then
				local resp = Messages.createWhoIsResponse(types, name)
				Network.transmit(msg.from, Network.QOS.ATLEAST_ONCE, resp, msg.llId)
				local whoIsresp = Messages.createWhoIs({ Messages.remoteTypes.MiningTurtleDriller })
				Network.transmit(msg.from, Network.QOS.ATMOST_ONCE, whoIsresp)
				--who is response
			elseif msg.payload.type == Messages.messageTypes.WhoIsResponse then
				if connectedTurtles[msg.from] == nil or connectedTurtles[msg.from] < os.clock() - minTimeBetweenJoins then
					print("received who is response from " .. tostring(msg.from))
					HandleTurtleJoiningNetwork(msg.from)
				end
				--inventory full
			elseif msg.payload.type == Messages.messageTypes.InventoryFull then
				local resp = Messages.createInfoRequest()
				local msgId = Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp)
				local InvFull = Network.receive(nil, nil, msgId)
				if InvFull ~= nil then
					turtleTasks[msg.from].position = InvFull.payload.position
					turtleTasks[msg.from].fuelLevel = InvFull.payload.fuelLevel
					GenerateTasksForInventoryFull(msg.from)
				end

				--task done
			elseif msg.payload.type == Messages.messageTypes.TaskDone then
				--prevent duplicates from causing issues
				if turtleTasks[msg.from].currentTask == nil or msg.payload.completedTask == turtleTasks[msg.from].currentTask.type then
					local resp = Messages.createInfoRequest()
					local msgId = Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp)
					local TaskDone = Network.receive(nil, nil, msgId)
					if TaskDone ~= nil then
						turtleTasks[msg.from].position = TaskDone.payload.position
						turtleTasks[msg.from].fuelLevel = TaskDone.payload.fuelLevel
						if #turtleTasks[msg.from].queuedTasks > 0 then
							turtleTasks[msg.from].currentTask = table.remove(turtleTasks[msg.from].queuedTasks, 1)
							SaveTableToFile(taskCacheFile, turtleTasks)
							Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, turtleTasks[msg.from].currentTask)
						else
							print("done with current tasks, sending new ones")
							GenerateAndAssignTurtleTasks(msg.from)
						end
					end
				end
			else
				Dbg.logI("MAIN", "unknown msg " .. Utils.printTableRecursive(msg))
			end
		end
	end
end


--handles messages received on broadcast channel
local function NetworkLoopBroadcast()
	while true do
		local msg = Network.receive(nil, Network.broadcastChannel)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
				print("received who is on broadcast channel from " .. msg.from)
				local resp = Messages.createWhoIsResponse(types, name)
				Network.transmit(msg.from, Network.QOS.ATMOST_ONCE, resp, msg.llId)
				local whoIsDriller = Messages.createWhoIs({ Messages.remoteTypes.MiningTurtleDriller })
				Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, whoIsDriller)
			end
		end
	end
end


local function Init()
	--get all turtles on network
	print("locating mining turtles")
	local msg = Messages.createWhoIs({ Messages.remoteTypes.MiningTurtleDriller })
	local msgId = Network.transmit(Network.broadcastChannel, Network.QOS.ATMOST_ONCE, msg)
	local resp = Network.receiveMsgTypes(5, nil, { Messages.messageTypes.WhoIsResponse })
	local i = 0
	while resp do
		i = i + 1
		connectedTurtles[resp.from] = os.clock()
		HandleTurtleJoiningNetwork(resp.from)
		resp = Network.receiveMsgTypes(5, nil, { Messages.messageTypes.WhoIsResponse })
	end
	print("found " .. tostring(i) .. " mining turtles")

	Threads.create(NetworkLoopMain)
	Threads.create(NetworkLoopBroadcast)
end


--------------------------------------- MAIN --------------------------------------

--create config file if needed
if not fs.exists(cacheFile) then
	CreateConfigFile()
	print("created " .. cacheFile .. " edit and restart program")
	return
end

--load config
local config = LoadTableFromFile(cacheFile)
if not config then
	print("config is corrupted, delete or fix")
	return
end

--generate holes based on file if needed
if not fs.exists(holeCacheFile) then
	if fs.exists(taskCacheFile) then
		fs.delete(taskCacheFile)
	end
	local startX = math.min(config.corner1.x, config.corner2.x) + 1
	local stopX = math.max(config.corner1.x, config.corner2.x) - 1
	local stopY = math.min(config.corner1.y, config.corner2.y)
	local startY = math.max(config.corner1.y, config.corner2.y)
	local startZ = math.min(config.corner1.z, config.corner2.z) + 1
	local stopZ = math.max(config.corner1.z, config.corner2.z) - 1

	--if config wasnt set properly return
	if startX == 0 and stopX == 0 and startY == 0 and stopY == 0 and startZ == 0 and stopZ == 0 then
		print("invalid dimensions")
		return
	end
	print("dimensions x: " .. startX - stopX .. " y: " .. startY - stopY .. " z: " .. startZ - stopZ)

	-- do height check
	if stopY < 0 then
		print("stop height can not be < 0")

		return
	end

	if startY > 255 then
		print("start height can not be > 255")

		return
	end

	local holes = GenerateMiningPattern(startX, stopX, startZ, stopZ, startY)
	print("holes: " .. #holes)
	local holeFile = {}
	holeFile.toDig = holes
	holeFile.dug = {}
	holeFile.digging = {}
	SaveTableToFile(holeCacheFile, holeFile)
	print("configuration complete")
end

--load holes
local holes = LoadTableFromFile(holeCacheFile)
if not holes then
	print("holes is corrupted, delete or fix")
	return
end

--reload tasks from file
local turtleTasks = LoadTableFromFile(taskCacheFile)
if not turtleTasks then
	turtleTasks = {}
end

Threads.create(Init)
Threads.startRunning()
