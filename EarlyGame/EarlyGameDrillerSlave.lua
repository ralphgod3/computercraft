-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
	"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished
--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Websockets.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")

local Dbg = require("Modules.Logger")
local Move = require("Modules.Move")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local Threads = require("Modules.Threads")
local Timers = require("Modules.Timer")
local Messages = require("Modules.Networking.Messages")
local Network = require("Modules.Networking.Networking")
local ModemHardware = require("Modules.Networking.HW.Modems")
local Websockets = require("Modules.Networking.HW.Websockets")

Threads = Threads.new()
Timers = Timers.new(Threads)
if peripheral.find("modem") ~= nil then
	print("using modem based communication")
	Network = Network.new(Threads, Timers, ModemHardware.new())
else
	print("using websocket based communication")
	Network = Network.new(Threads, Timers, Websockets.new())
end

--------------------------------------- Variables --------------------------------------
local cacheFile = "/cache/EarlyGameDrillerSlave.cache"
local types = { Messages.remoteTypes.MiningTurtleDriller }
local controller = nil
local name = "driller"
local valuableBlocks = {}
local unbreakableBlocks = {}
--------------------------------------- FUNCTIONS --------------------------------------


--return result from handleMining function
local miningResult =
{
	finished = 0,
	inventoryFull = 1,
}

---handles drilling a hole straight down whilst collecting valuable blocks
---@param blocksToDig table blocks to dig for
---@param unbreakableBlocks table list of unbreakable blocks
---@param stopY number y level to stop digging at
---@return integer miningResult enum
local function HandleMining(blocksToDig, unbreakableBlocks, stopY)
	turtle.select(1)

	--do inspect dance
	for _ = 1, 4 do
		Move.turnRight()
		local suc, details = turtle.inspect()

		if suc and Utils.findElementInTable(blocksToDig, details.name) then
			turtle.dig()
		end
	end
	while Move.getPosition().y > stopY do
		turtle.digDown()
		-- if move down wasnt succesful then check if block below is unbreakable
		while Move.down(true) ~= StatusCode.Code.Ok do
			local suc, details = turtle.inspectDown()
			if suc and Utils.findElementInTable(unbreakableBlocks, details.name) then
				return miningResult.finished
			end
		end
		--do inspect dance
		for _ = 1, 4 do
			Move.turnRight()
			local suc, details = turtle.inspect()

			if suc and Utils.findElementInTable(blocksToDig, details.name) then
				turtle.dig()
			end
		end
		--check if inventory is full
		-- inventory is filled up
		if turtle.getItemCount(16) > 0 then
			return miningResult.inventoryFull
		end
	end
	return miningResult.finished
end

---drops all items in turtle inventory in specified direction
---@param direction number Messages.direction enum
local function DropItems(direction)
	for i = 1, 16 do
		local toDrop = turtle.getItemCount(i)
		while toDrop > 0 do
			local dropped = 0
			turtle.select(i)
			if direction == Messages.direction.Forward then
				dropped = turtle.drop()
			elseif direction == Messages.direction.Up then
				dropped = turtle.dropUp()
			elseif direction == Messages.direction.Down then
				dropped = turtle.dropDown()
			end
			if not dropped then
				print("chest full, waiting a little bit before next attempt")
				sleep(2)
			end
			toDrop = turtle.getItemCount()
		end
	end
end

---refuel turtle up to minFuelLevel
---@param direction number Messages.direction enum
---@param minFuelLevel number fuel level to stop refueling at
local function Refuel(direction, minFuelLevel)
	local fuelLevel = turtle.getFuelLevel()
	while fuelLevel < minFuelLevel do
		local sucked = false
		while sucked == false do
			if direction == Messages.direction.Forward then
				sucked = turtle.suck()
			elseif direction == Messages.direction.Up then
				sucked = turtle.suckUp()
			elseif direction == Messages.direction.Down then
				sucked = turtle.suckDown()
			end
			if sucked == false then
				print("no fuel in chest, waiting before attempting again")
				sleep(10)
			end
		end
		turtle.refuel()
		fuelLevel = turtle.getFuelLevel()
	end
end

--handles simple requests that should not take alot of time to execute (mainly information requests)
local function NetworkLoopSimple()
	local msgsToListenTo =
	{
		Messages.messageTypes.WhoIs,
		Messages.messageTypes.InfoRequest,
		Messages.messageTypes.BlocksToSearchForList,
		Messages.messageTypes.UnbreakableBlocksList,
		Messages.messageTypes.EmergencyStop,
	}
	while true do
		local msg = Network.receiveMsgTypes(nil, os.getComputerID(), msgsToListenTo)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
				print("received who is from " .. tostring(msg.from))
				local resp = Messages.createWhoIsResponse(types, name)
				Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp, msg.llId)
				--handle info request
			elseif msg.payload.type == Messages.messageTypes.InfoRequest then
				print("received info request from " .. tostring(msg.from))
				local resp = Messages.createInfoRequestResponse(Move.getPosition(), turtle.getFuelLevel())
				Network.transmit(msg.from, Network.QOS.ATLEAST_ONCE, resp, msg.llId)
				--valuable block list
			elseif msg.payload.type == Messages.messageTypes.BlocksToSearchForList then
				valuableBlocks = {}
				print("received new valuable block list from " .. tostring(msg.from))
				for _, v in pairs(msg.payload.blockList) do
					table.insert(valuableBlocks, v)
				end
				--unbreakable block list
			elseif msg.payload.type == Messages.messageTypes.UnbreakableBlocksList then
				print("received new unbreakableBlocks list from " .. tostring(msg.from))
				unbreakableBlocks = {}
				for _, v in pairs(msg.payload.blockList) do
					table.insert(unbreakableBlocks, v)
				end
				--emergency stop
			elseif msg.payload.type == Messages.messageTypes.EmergencyStop then
				print("received emergency stop from " .. tostring(msg.from))
				--todo: kill all movement
			else
				print("received unknown msg, check logs")
				Dbg.logI("MAIN", "unknown msg " .. Utils.printTableRecursive(msg))
			end
		end
	end
end

--handles messages received on personal channel
local function NetworkLoopMain()
	local msgsToListenTo =
	{
		Messages.messageTypes.GoTo,
		Messages.messageTypes.DropItems,
		Messages.messageTypes.Drill,
		Messages.messageTypes.Refuel
	}
	local resp = nil
	local newMsg = nil
	local msg = nil
	while true do
		if newMsg == nil then
			if resp ~= nil then
				Network.transmit(msg.from, Network.QOS.EXACTLY_ONCE, resp, msg.llId)
				resp = nil
			end
			msg = Network.receiveMsgTypes(nil, os.getComputerID(), msgsToListenTo)
		else
			print("took msg from queue")
			msg = newMsg
			newMsg = nil
		end
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle go to
			if msg.payload.type == Messages.messageTypes.GoTo then
				print("received goto " ..
				tostring(msg.payload.position.x) ..
				" y " ..
				tostring(msg.payload.position.y) ..
				" z " .. tostring(msg.payload.position.z) .. " from " .. tostring(msg.from))
				while Move.goTo(msg.payload.position, msg.payload.canDestroy, msg.payload.destroyBlacklist) ~= StatusCode.Code.Ok do
					print("something went wrong whilst going to x " ..
					tostring(msg.payload.position.x) ..
					" y " .. tostring(msg.payload.position.y) .. " z " .. tostring(msg.payload.position.z))
					sleep(10)
				end
				newMsg = Network.receiveMsgTypes(0.1, os.getComputerID(), msgsToListenTo)
				resp = Messages.createTaskDone(msg.payload.type)
			elseif msg.payload.type == Messages.messageTypes.DropItems then
				print("received drop items from " .. tostring(msg.from))
				DropItems(msg.payload.direction)
				resp = Messages.createTaskDone(msg.payload.type)
				newMsg = Network.receiveMsgTypes(0.1, os.getComputerID(), msgsToListenTo)
			elseif msg.payload.type == Messages.messageTypes.Drill then
				print("received drill command to dig to y " ..
				tostring(msg.payload.depth) .. " from " .. tostring(msg.from))
				local result = HandleMining(valuableBlocks, unbreakableBlocks, msg.payload.depth)
				if result == miningResult.finished then
					print("digging done")
					resp = Messages.createTaskDone(msg.payload.type)
					newMsg = Network.receiveMsgTypes(0.1, os.getComputerID(), msgsToListenTo)
				elseif result == miningResult.inventoryFull then
					print("inventory full")
					resp = Messages.createInventoryFullMsg()
					newMsg = Network.receiveMsgTypes(0.1, os.getComputerID(), msgsToListenTo)
				end
			elseif msg.payload.type == Messages.messageTypes.Refuel then
				print("received refuel from " .. tostring(msg.from))
				Refuel(msg.payload.direction, msg.payload.minFuelAmount)
				resp = Messages.createTaskDone(msg.payload.type)
				newMsg = Network.receiveMsgTypes(0.1, os.getComputerID(), msgsToListenTo)
			else
				Dbg.logI("MAIN", "unknown msg " .. Utils.printTableRecursive(msg))
			end
		end
	end
end


--handles messages received on broadcast channel
local function NetworkLoopBroadcast()
	while true do
		local msg = Network.receive(nil, Network.broadcastChannel)
		if msg ~= nil and msg.payload ~= nil and msg.payload.type ~= nil then
			--handle who is
			if msg.payload.type == Messages.messageTypes.WhoIs and (msg.payload.types == nil or Utils.elementOfTableExistInTable(msg.payload.types, types) == true) then
				print("received who is on broadcast channel from " .. msg.from)
				local resp = Messages.createWhoIsResponse(types, name)
				Network.transmit(msg.from, Network.QOS.ATMOST_ONCE, resp, msg.llId)
			end
		end
	end
end


local function Init()
	--get controller id
	local msg = Messages.createWhoIs({ Messages.remoteTypes.DrillerController })
	local msgId = Network.transmit(Network.broadcastChannel, Network.QOS.ATMOST_ONCE, msg)
	while controller == nil do
		print("requesting controller id")
		local resp = Network.receive(5, nil, msgId)
		if resp ~= nil and resp.payload ~= nil and resp.payload.type ~= nil then
			print("got controller id " .. tostring(resp.from))
			controller = resp.from
		else
			msgId = Network.transmit(Network.broadcastChannel, Network.QOS.ATMOST_ONCE, msg)
		end
	end

	print("creating coroutines")
	Threads.create(NetworkLoopBroadcast)
	Threads.create(NetworkLoopMain)
	Threads.create(NetworkLoopSimple)
	print("ready for mining")
end



--------------------------------------- MAIN -------------------------------------------
--recover or set turtle position
if Move.recoverPosition() ~= StatusCode.Code.Ok then
	print("could not recover turtle position please enter the current x,y,z and facing")
	print("north = 0 east = 1, south = 2, west = 3")
	print("enter as x,y,z,f")
	local input = read()
	print(input)
	local seperated = Utils.splitString(input, ',')
	if seperated == nil then
		return
	end

	if #seperated ~= 4 then
		error("invalid position entered")
	end

	Move.setPosition({
		x = tonumber(seperated[1]),
		y = tonumber(seperated[2]),
		z = tonumber(seperated[3]),
		f = tonumber(seperated[4])
	})
end

if not fs.exists(cacheFile) then
	local file = fs.open(cacheFile, "w")
	file.write("placeholder")
	file.close()
end

os.setComputerLabel(name .. "_" .. tostring(os.getComputerID()))

Threads.create(Init)
Threads.startRunning()
