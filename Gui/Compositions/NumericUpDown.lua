local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/NumericField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/Button.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local GuiUtils = require("Gui.GuiUtils")
local Dbg = require("Modules.Logger")
local NumericField = require("Gui.Elements.NumericField")
local Button = require("Gui.Elements.Button")
local AdvWindow = require("Gui.advWindow")

---create a new numericField
---@param parentTerm table terminal that is the parent of the element
---@param X number start x coordinate for button
---@param Y number start y coordinate for button
---@param Width number element width
---@param Height number element height
---@param textColorBtns number element text color
---@param backgroundColorBtns number element background color
---@param textColorNumeric number element text color
---@param backgroundColorNumeric number element background color
---@param number number number the element holds at startup and is reset too when clicked on numericField
---@param btnAdd number count that gets added or subtracted when < or > is pressed
---@param min number? minimum number the numeric field may hold, pass nil to not use
---@param max number? maximum number the numeric field may hold, pass nil to not use
---@return NumericUpDown instance NumericField object
local function new(parentTerm, X, Y, Width, Height, textColorBtns, backgroundColorBtns, textColorNumeric,
				   backgroundColorNumeric, number, btnAdd, min, max)
	---@class NumericUpDown : AdvWindow
	local parentWindow = AdvWindow.new(parentTerm, X, Y, Width, Height, true, nil, nil)


	local function btnCallback(eventInfo, this, userParams)
		if eventInfo[1] == "mouse_click" then
			local num = parentWindow.UiElements["numeric"].getNumber()
			if userParams == "<" then
				parentWindow.UiElements["numeric"].setNumber(num - btnAdd)
			elseif userParams == ">" then
				parentWindow.UiElements["numeric"].setNumber(num + btnAdd)
			end
		end
	end

	local function numberCallback(eventInfo, this, userParams)
		if eventInfo[1] == "mouse_click" then
			this.setNumber(0)
		elseif eventInfo[1] == "mouse_scroll" then
			this.setNumber(this.getNumber() - eventInfo[2])
		end
	end
	parentWindow.addUiElement("lessBtn",
		Button.new(parentWindow, 1, 1, 1, Height, textColorBtns, backgroundColorBtns, "<", btnCallback, "<"))
	parentWindow.addUiElement("moreBtn",
		Button.new(parentWindow, Width, 1, 1, Height, textColorBtns, backgroundColorBtns, ">", btnCallback, ">"))
	parentWindow.addUiElement("numeric",
		NumericField.new(parentWindow, 2, 1, Width - 2, Height, textColorNumeric, backgroundColorNumeric, number,
			numberCallback, nil, min, max))

	---set the numeric field number
	---@param numericFieldNumber number number for numericField
	function parentWindow.setNumber(numericFieldNumber)
		return parentWindow.UiElements["numeric"].setNumber(numericFieldNumber)
	end

	---get number in numericField
	---@return number number number in field
	function parentWindow.getNumber()
		return parentWindow.UiElements["numeric"].getNumber()
	end

	return parentWindow
end










return { new = new }
