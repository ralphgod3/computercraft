local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/TextField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/InputField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxScroll.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local GuiUtils = require("Gui.GuiUtils")
local Dbg = require("Modules.Logger")
local TextField = require("Gui.Elements.TextField")
local InputField = require("Gui.Elements.InputField")
local AdvWindow = require("Gui.advWindow")
local ListboxScroll = require("Gui.Compositions.ListboxScroll")

---create new listbox with scroll events and searchbar callbacks taken care of
---@param parentTerm AdvWindow parent terminal
---@param X number startX
---@param Y number startY
---@param Width number width of window
---@param Height number height of window
---@param listboxTextColors number[] colors for listbox
---@param listboxBackgroundColors number[] colors for listbox
---@param callbackFnc function | nil callback function called when listbox item is clicked prototype callbackFnc(eventInfo, this, userParams)
---@param userParams any user paramaters for callback function
---@param textDrawFunction function | nil how to center text, defaults to center center
---@param filterFunction function how to filter for items check ListboxSearchAndFilterFunctions.lua for examples
---@param sortFunction any how to sort items in listbox, check ListboxSearchAndFilterFunctions.lua for examples
---@return ListboxSearch instance
local function new(parentTerm, X, Y, Width, Height, listboxTextColors, listboxBackgroundColors, callbackFnc, userParams, textDrawFunction, filterFunction, sortFunction)
    ---@class ListboxSearch
    local parentWindow = AdvWindow.new(parentTerm, X, Y, Width, Height, true, nil, nil)

    local filteredItems = {}
    local unfilteredItems = {}


    local function callbackInput(eventInfo, this, userParams)
        if eventInfo[1] == "mouse_click" or eventInfo[1] == "monitor_touch" then
            parentWindow.UiElements["listbox"].resetScroll()
        elseif eventInfo[1] == "key" and keys.getName(eventInfo[2]) == "enter" then
            return
        end
        --handle filtering of items
        local text = this.getText()
        filteredItems = filterFunction(unfilteredItems, text)
        table.sort(filteredItems, sortFunction)
        parentWindow.UiElements["listbox"].setItemTable(filteredItems)
    end

    local text = "Search:"
    parentWindow.addUiElement("searchText", TextField.new(parentWindow, 1, Height, string.len(text), 1, colors.white, colors.gray, text, GuiUtils.drawTextAlignmentTopLeft))
    parentWindow.addUiElement("inputSearch", InputField.new(parentWindow, string.len(text) + 1, Height, Width - string.len(text), 1, colors.white, colors.lightGray, "", callbackInput, nil, GuiUtils.drawTextAlignmentCenterLeft))
    parentWindow.addUiElement("listbox",  ListboxScroll.new(parentWindow, 1, 1, Width, Height -1, listboxTextColors, listboxBackgroundColors, callbackFnc, userParams, textDrawFunction))


    -------------------- EXPOSE LISTBOX FUNCTIONS --------------------

    ---scroll lines up or down, scrolling down positive numbers up negative numbers
    ---@param linesToScroll number lines to scroll
    function parentWindow.scroll(linesToScroll)
        return parentWindow.UiElements["listbox"].scroll(linesToScroll)
    end

    ---reset to line 1 of the list
    function parentWindow.resetScroll()
        return parentWindow.UiElements["listbox"].resetScroll()
    end

    ---add item to listbox
    ---@param item ListBoxItem | string item to add
    function parentWindow.addItem(item)
        table.insert(unfilteredItems, item)
    end

    ---sets itemTable to given table
    ---@param itemTable string[] | ListBoxItem[] listbox items
    function parentWindow.setItemTable(itemTable)
        local prevUnfiltered = #unfilteredItems
        unfilteredItems = itemTable
        local eventInfo = { "char"}
        if prevUnfiltered == 0 then
            callbackInput(eventInfo, parentWindow.UiElements["inputSearch"], userParams)
        end
    end

    ---remove item from listbox
    ---@param item any item(s) to remove
    ---@return boolean itemRemoved true yes, false no
    function parentWindow.removeItem(item)
        local removed = false
        for i = #unfilteredItems, 1, -1 do
            if unfilteredItems[i] == item then
                removed = true
                table.remove(unfilteredItems, i)
            end
        end
        return removed
    end

    ---clear items from listbox
    function parentWindow.clearItems()
        unfilteredItems = {}
        callbackInput({[1] = "special"}, parentWindow.UiElements["inputSearch"], userParams)
    end


    return parentWindow
end

return {
    new = new,
}