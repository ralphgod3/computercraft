local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/ListBox.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/Button.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")

local AdvWindow = require("Gui.advWindow")
local ListBox = require("Gui.Elements.ListBox")
local Button = require("Gui.Elements.Button")
local Dbg = require("Modules.Logger")


---comment
---@param parentTerm AdvWindow parent terminal
---@param X number startX
---@param Y number startY
---@param Width number listbox width
---@param Height number listbox height
---@param textColors number[] colors for text
---@param backgroundColors number[] colors for background
---@param callbackFnc function | nil callback function on element click
---@param userParams any paramaters to callback function
---@param textDrawFunction function | nil text draw overrides
---@return ListboxScroll instance
local function new(parentTerm, X, Y, Width, Height, textColors, backgroundColors, callbackFnc, userParams,
				   textDrawFunction)
	---@class ListboxScroll : AdvWindow
	local parentWindow = AdvWindow.new(parentTerm, X, Y, Width, Height, true, nil, nil)

	local function callbackBtn(eventInfo, this, userParams)
		if eventInfo[1] == "mouse_click" then
			if userParams == "+" then
				parentWindow.UiElements["listbox"].scroll(1)
			elseif userParams == "-" then
				parentWindow.UiElements["listbox"].scroll(-1)
			end
		end
	end

	local function callbackList(eventInfo, this, userParams)
		if eventInfo[1] == "mouse_scroll" then
			parentWindow.UiElements["listbox"].scroll(eventInfo[2])
		elseif eventInfo[1] == "bar_click" then
			this.resetScroll()
		elseif eventInfo[1] == "element_click" then
			if callbackFnc ~= nil then
				callbackFnc(eventInfo, this, userParams)
			end
		end
	end

	parentWindow.addUiElement("Up", Button.new(parentWindow, Width, 1, 1, 1, nil, nil, "\24", callbackBtn, "-", nil))
	parentWindow.addUiElement("Down",
		Button.new(parentWindow, Width, Height, 1, 1, nil, nil, "\25", callbackBtn, "+", nil))

	parentWindow.addUiElement("listbox",
		ListBox.new(parentWindow, 1, 1, Width - 1, Height, textColors, backgroundColors, callbackList, userParams,
			textDrawFunction) --[[@as IuiElement]])

	-------------------- EXPOSE LISTBOX FUNCTIONS --------------------

	---scroll lines up or down, scrolling down positive numbers up negative numbers
	---@param linesToScroll number lines to scroll
	function parentWindow.scroll(linesToScroll)
		return parentWindow.UiElements["listbox"].scroll(linesToScroll)
	end

	---reset to line 1 of the list
	function parentWindow.resetScroll()
		return parentWindow.UiElements["listbox"].resetScroll()
	end

	---add item to listbox
	---@param item ListBoxItem | string item to add
	function parentWindow.addItem(item)
		return parentWindow.UiElements["listbox"].addItem(item)
	end

	---sets itemTable to given table
	---@param itemTable string[] | ListBoxItem[] listbox items
	function parentWindow.setItemTable(itemTable)
		return parentWindow.UiElements["listbox"].setItemTable(itemTable)
	end

	---remove item from listbox
	---@param item any item(s) to remove
	---@return boolean itemRemoved true yes, false no
	function parentWindow.removeItem(item)
		return parentWindow.UiElements["listbox"].removeItem(item)
	end

	---clear items from listbox
	function parentWindow.clearItems()
		return parentWindow.UiElements["listbox"].clearItems()
	end

	return parentWindow
end




return { new = new }
