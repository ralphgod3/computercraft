local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/TextField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Elements/InputField.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/advWindow.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/ListboxScroll.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/Compositions/NumericUpDown.lua")

local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local GuiUtils = require("Gui.GuiUtils")
local Dbg = require("Modules.Logger")
local TextField = require("Gui.Elements.TextField")
local InputField = require("Gui.Elements.InputField")
local AdvWindow = require("Gui.advWindow")
local ListboxScroll = require("Gui.Compositions.ListboxScroll")
local NumericUpDown = require("Gui.Compositions.NumericUpDown")

---create new listbox with scroll events, searchbar and numeric callbacks taken care of
---@param parentTerm AdvWindow parent terminal
---@param X number startX
---@param Y number startY
---@param Width number width of window
---@param Height number height of window
---@param listboxTextColors number[] colors for listbox
---@param listboxBackgroundColors number[] colors for listbox
---@param callbackFnc function | nil callback function called when listbox item is clicked prototype callbackFnc(eventInfo, this, userParams)
---@param userParams any user paramaters for callback function
---@param textDrawFunction function | nil how to center text, defaults to center center
---@param filterFunction function | nil how to filter for items check ListboxSearchAndFilterFunctions.lua for examples
---@param sortFunction function | nil how to sort items in listbox, check ListboxSearchAndFilterFunctions.lua for examples
---@return ListboxSearchNumeric instance
local function new(parentTerm, X, Y, Width, Height, listboxTextColors, listboxBackgroundColors, callbackFnc, userParams,
				   textDrawFunction, filterFunction, sortFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, X, "number")
	PC.expect(3, Y, "number")
	PC.expect(4, Width, "number")
	PC.expect(5, Height, "number")
	PC.expect(6, listboxTextColors, "table")
	PC.expect(7, listboxBackgroundColors, "table")
	PC.expect(8, callbackFnc, "function", "nil")
	PC.expect(10, textDrawFunction, "function", "nil")
	PC.expect(11, filterFunction, "function", "nil")
	PC.expect(12, sortFunction, "function", "nil")

	---@class ListboxSearchNumeric : AdvWindow
	local parentWindow = AdvWindow.new(parentTerm, X, Y, Width, Height, true, nil, nil)

	local filteredItems = {}
	local unfilteredItems = {}


	local function callbackInput(eventInfo, this, userParams)
		if eventInfo[1] == "mouse_click" or eventInfo[1] == "monitor_touch" then
			parentWindow.UiElements["listbox"].resetScroll()
		elseif eventInfo[1] == "key" and keys.getName(eventInfo[2]) == "enter" then
			return
		end
		--handle filtering of items
		local text = this.getText()
		if filterFunction ~= nil then
			filteredItems = filterFunction(unfilteredItems, text)
			table.sort(filteredItems, sortFunction)
			parentWindow.UiElements["listbox"].setItemTable(filteredItems)
		end
	end

	local text = "Search:"
	parentWindow.addUiElement("numeric",
		NumericUpDown.new(parentWindow, Width - 7, Height, 8, 1, colors.white, colors.gray, colors.white,
			colors.lightGray, 1,
			64, 1))
	parentWindow.addUiElement("text",
		TextField.new(parentWindow, 1, Height, string.len(text), 1, colors.white, colors.gray, text,
			GuiUtils.drawTextAlignmentTopLeft))
	parentWindow.addUiElement("input",
		InputField.new(parentWindow, string.len(text) + 1, Height, Width - string.len(text) - 8, 1, colors.white,
			colors.lightGray, "", callbackInput, nil, GuiUtils.drawTextAlignmentCenterLeft))
	parentWindow.addUiElement("listbox",
		ListboxScroll.new(parentWindow, 1, 1, Width, Height - 1, listboxTextColors, listboxBackgroundColors, callbackFnc,
			userParams, textDrawFunction))

	-------------------- EXPOSE LISTBOX FUNCTIONS --------------------

	---scroll lines up or down, scrolling down positive numbers up negative numbers
	---@param linesToScroll number lines to scroll
	function parentWindow.scroll(linesToScroll)
		return parentWindow.UiElements["listbox"].scroll(linesToScroll)
	end

	---reset to line 1 of the list
	function parentWindow.resetScroll()
		return parentWindow.UiElements["listbox"].resetScroll()
	end

	---add item to listbox
	---@param item ListBoxItem | string item to add
	function parentWindow.addItem(item)
		table.insert(unfilteredItems, item)
	end

	---sets itemTable to given table
	---@param itemTable string[] | ListBoxItem[] listbox items
	function parentWindow.setItemTable(itemTable)
		--local prevUnfiltered = #unfilteredItems
		unfilteredItems = itemTable
		local eventInfo = { "char" }
		callbackInput(eventInfo, parentWindow.UiElements["input"], userParams)
	end

	---remove item from listbox
	---@param item any item(s) to remove
	---@return boolean itemRemoved true yes, false no
	function parentWindow.removeItem(item)
		local removed = false
		for i = #unfilteredItems, 1, -1 do
			if unfilteredItems[i] == item then
				removed = true
				table.remove(unfilteredItems, i)
			end
		end
		return removed
	end

	---clear items from listbox
	function parentWindow.clearItems()
		unfilteredItems = {}
		callbackInput({ [1] = "special" }, parentWindow.UiElements["inputSearch"], userParams)
	end

	---set the numeric field number
	---@param numericFieldNumber number number for numericField
	function parentWindow.setNumber(numericFieldNumber)
		return parentWindow.UiElements["numeric"].setNumber(numericFieldNumber)
	end

	---get number in numericField
	---@return number number number in field
	function parentWindow.getNumber()
		return parentWindow.UiElements["numeric"].getNumber()
	end

	return parentWindow
end
return {
	new = new,
}
