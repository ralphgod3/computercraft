local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local StatusCode = require("Modules.StatusCodes")
local PC = require("Modules.ParamCheck")

local GuiUtils = {}

---simple wrapper around setVisible, will set visibility of window when available on terminal object
---@param terminal table terminal or window
---@param visible boolean isVisible
function GuiUtils.setTerminalVisible(terminal, visible)
    PC.expect(1, terminal, "table")
    PC.expect(2, visible, "boolean")
    if terminal.setVisible ~= nil then
       terminal.setVisible(visible)
    end
end

---prints text to top, left in a field, cuts text off if it doesnt fit in the field
---@param minX number minimum x coordinate
---@param maxX number maximum x coordinate
---@param minY number minimum y coordinate
---@param maxY number maximum y coordinate
---@param text string text to write
---@param terminal table terminal to write text to
---@return StatusCode statusCode statuscode indicating success
function GuiUtils.drawTextAlignmentTopLeft(minX, maxX, minY, maxY, text, terminal)
    PC.expect(1, minX, "number")
    PC.expect(2, maxX, "number")
    PC.expect(3, minY, "number")
    PC.expect(4, maxY, "number")
    PC.expect(5, text, "string")
    PC.expect(6, terminal, "table")

    local width = (maxX - minX) + 1
    local height = (maxY - minY) + 1
    local lines = math.ceil(string.len(text) / width)
    if lines > height then lines = height end
    local charInString = 1
    for y = 1, lines do
        for x = 1, width do
            terminal.setCursorPos(x + minX -1, y + minY -1)
            local char = string.sub(text, charInString, charInString)
            charInString = charInString + 1
            if char ~= nil then
                terminal.write(char)
            end
        end
    end
    return StatusCode.Code.Ok
end

---prints text to center, left in a field, cuts text off if it doesnt fit in the field
---@param minX number minimum x coordinate
---@param maxX number maximum x coordinate
---@param minY number minimum y coordinate
---@param maxY number maximum y coordinate
---@param text string text to write
---@param terminal table terminal to write text to
---@return StatusCode statusCode statuscode indicating success
function GuiUtils.drawTextAlignmentCenterLeft(minX, maxX, minY, maxY, text, terminal)
    PC.expect(1, minX, "number")
    PC.expect(2, maxX, "number")
    PC.expect(3, minY, "number")
    PC.expect(4, maxY, "number")
    PC.expect(5, text, "string")
    PC.expect(6, terminal, "table")

    local width = (maxX - minX) + 1
    local height = (maxY - minY) + 1
    local lines = math.ceil(string.len(text) / width)
    if lines > height then lines = height end
    local centerY = math.ceil(height / 2)
    local lineStartY = math.floor( (centerY + 1) - (lines / 2 ))
    local charInString = 1
    for y = lineStartY, lineStartY + lines -1 do
        for x = 1, width do
            terminal.setCursorPos(x + minX -1, y + minY -1)
            local char = string.sub(text, charInString, charInString)
            charInString = charInString + 1
            if char ~= nil then
                terminal.write(char)
            end
        end
    end
    return StatusCode.Code.Ok
end

---prints text to top, center in a field, cuts text off if it doesnt fit in the field
---@param minX number minimum x coordinate
---@param maxX number maximum x coordinate
---@param minY number minimum y coordinate
---@param maxY number maximum y coordinate
---@param text string text to write
---@param terminal table terminal to write text to
---@return StatusCode statusCode statuscode indicating success
function GuiUtils.drawTextAlignmentTopCenter(minX, maxX, minY, maxY, text, terminal)
    PC.expect(1, minX, "number")
    PC.expect(2, maxX, "number")
    PC.expect(3, minY, "number")
    PC.expect(4, maxY, "number")
    PC.expect(5, text, "string")
    PC.expect(6, terminal, "table")

    local width = (maxX - minX) + 1
    local height = (maxY - minY) + 1
    local lines = math.ceil(string.len(text) / width)
    if height < lines then lines = height end
    local charsPerLine = math.floor(string.len(text) / lines)
    if charsPerLine > width then charsPerLine = width end
    local centerX = math.ceil(width / 2)
    local letterStartX = math.floor((centerX + 1) - (charsPerLine / 2))
    local charInString = 1
    for y = 1, lines do
        for x = letterStartX, letterStartX + charsPerLine -1 do
            terminal.setCursorPos(x + minX -1, y + minY -1)
            local char = string.sub(text, charInString, charInString)
            charInString = charInString + 1
            if char ~= nil then
                terminal.write(char)
            end
        end
    end
    return StatusCode.Code.Ok
end


---prints text to center, center in a field, cuts text off if it doesnt fit in the field
---@param minX number minimum x coordinate
---@param maxX number maximum x coordinate
---@param minY number minimum y coordinate
---@param maxY number maximum y coordinate
---@param text string text to write
---@param terminal table terminal to write text to
---@return StatusCode statusCode statuscode indicating success
function GuiUtils.drawTextAlignmentCenterCenter(minX, maxX, minY, maxY, text, terminal)
    PC.expect(1, minX, "number")
    PC.expect(2, maxX, "number")
    PC.expect(3, minY, "number")
    PC.expect(4, maxY, "number")
    PC.expect(5, text, "string")
    PC.expect(6, terminal, "table")

    local width = (maxX - minX) + 1
    local height = (maxY - minY) + 1
    local lines = math.ceil(string.len(text) / width)
    local charsPerLine = math.floor(string.len(text) / lines)
    local centerX = math.ceil(width / 2)
    local centerY = math.ceil(height / 2)
    local letterStartX = math.floor( (centerX + 1) - (charsPerLine / 2))
    local lineStartY = math.floor( (centerY + 1) - (lines / 2 ))
    local charInString = 1
    for y = lineStartY, lineStartY + lines -1 do
        for x = letterStartX, letterStartX + charsPerLine -1 do
            terminal.setCursorPos(x + minX -1, y + minY -1)
            local char = string.sub(text, charInString, charInString)
            charInString = charInString + 1
            if char ~= nil then
                terminal.write(char)
            end
        end
    end
    return StatusCode.Code.Ok
end

return GuiUtils