local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")
--local Dbg = require("Modules.Logger")

-- Contains functions for sorting and filtering listbox items


---filters items based on displayName and name
---@param list table item list {displayName, name}
---@param filter string text to filter by
local function listboxItemFilterFunction(list, filter)
    PC.expect(1, list, "table")
    PC.expect(2, filter, "string")
    local filteredItems = {}
    filter = string.lower(filter)
    for _, v in pairs(list) do
        if (v.displayName and string.find(string.lower(v.displayName), filter, nil, true) ~= nil) or string.find(string.lower(v.name), filter, nil, true) then
            table.insert(filteredItems, v)
        end
    end
    return filteredItems
end

---sample sort function that sorts by entry.count >
---@param a table
---@param b table
---@return boolean higher a.count > b.count
local function listboxItemSortFunctionHighCount(a, b)
    PC.expect(1, a, "table")
    PC.expect(1, b, "table")
    if a.count > b.count then
        return true
    else
        return false
    end
end



return {
    listboxItemFilterFunction = listboxItemFilterFunction,
    listboxItemSortFunctionHighCount = listboxItemSortFunctionHighCount,
}
