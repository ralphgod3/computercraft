local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Gui/IuiElement.lua")
local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")
local IuiElement = require("Interfaces.Gui.IuiElement")

local TAG = "WIN"
--Dbg.setLogLevel(TAG, Dbg.Levels.Warning)
local advWindow = {}

---wraps around the window api to give it more advanced functions
---@param parentTerm table parent terminal
---@param x number x start
---@param y number y start
---@param width number window with
---@param height number window height
---@param visible boolean? window visible by default ?
---@param callbackFnc function? function to be called when any function giving coordinates happens in the window prototype callbackFnc(eventInfo, this, userParams)
---@param userParams any? parameters for callbackFnc
---@return AdvWindow Window new window
function advWindow.new(parentTerm, x, y, width, height, visible, callbackFnc, userParams)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, x, "number")
	PC.expect(3, y, "number")
	PC.expect(4, width, "number")
	PC.expect(5, height, "number")
	PC.expect(6, visible, "boolean", "nil")
	PC.expect(7, callbackFnc, "function", "nil")
	local this =
	{
		startX = x,
		stopX = x + width - 1,
		startY = y,
		stopY = y + height - 1,
		callbackFnc = callbackFnc,
		userParams = userParams,
		terminal = parentTerm,
		elements = {}
	}
	local win = window.create(parentTerm, x, y, width, height, visible)
	Dbg.logV(TAG, "creating window", "x", x, "y", y, "stopX", this.stopX, "stopY", this.stopY)

	---@class AdvWindow : IuiElement
	local funcTable = {
		UiElements = this.elements,
		-- default window functions
		redirect = win.redirect,
		clear = win.clear,
		clearLine = win.clearLine,
		getCursorPos = win.getCursorPos,
		setCursorPos = win.setCursorPos,
		setCursorBlink = win.setCursorBlink,
		getCursorBlink = win.getCursorBlink,
		isColor = win.isColor,
		isColour = win.isColour,
		setTextColor = win.setTextColor,
		setTextColour = win.setTextColour,
		setPaletteColour = win.setPaletteColour,
		setPaletteColor = win.setPaletteColor,
		getPaletteColour = win.getPaletteColour,
		getPaletteColor = win.getPaletteColor,
		setBackgroundColor = win.setBackgroundColor,
		setBackgroundColour = win.setBackgroundColour,
		getSize = win.getSize,
		scroll = win.scroll,
		getTextColor = win.getTextColor,
		getTextColour = win.getTextColour,
		getBackgroundColor = win.getBackgroundColor,
		getBackgroundColour = win.getBackgroundColour,
		getLine = win.getLine,
		isVisible = win.isVisible,
		redraw = win.redraw,
		restoreCursor = win.restoreCursor,
		getPosition = win.getPosition,
		reposition = win.reposition,
		write = win.write,
		blit = win.blit,
		current = win.current
	}


	--------------------------------------- FUNCTIONS --------------------------------------
	---handles events for all elements in this window
	---@events needs all events that are nescesary by its ui components currently all events are mouse_click, mouse_up, mouse_scroll, mouse_drag, monitor_touch, key, char
	---@info remaps any event that contains coordinates to be an offset of the absolute coordinates to ensure they work properly
	---@param eventInfo table event parameters in order they are gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		PC.expect(1, eventInfo, "table")
		local info = Utils.copyTable(eventInfo)
		if win.isVisible() then
			local Xe = 0
			local Ye = 0
			--remap the x, y pos for any event that throws them to be relative to this window
			if info[1] == "mouse_click" or info[1] == "mouse_up" or info[1] == "mouse_scroll" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
				info[3] = info[3] - this.startX + 1
				info[4] = info[4] - this.startY + 1
				if ((this.terminal.isVisible ~= nil and this.terminal.isVisible()) or this.terminal.isVisible == nil) then
					if (info[3] >= 1 and info[3] <= this.stopX - this.startX + 1 and info[4] >= 1 and info[4] <= this.stopY - this.startY + 1) then
						if this.callbackFnc ~= nil then
							this.callbackFnc(info, funcTable, this.userParams)
						end
						for _, v in pairs(this.elements) do
							v.handle(info)
						end
					end
				end
			else
				for _, v in pairs(this.elements) do
					v.handle(info)
				end
			end
		end
	end

	---adds ui element to this windows responsibility
	---@param name string name the ui element will have in the elements table to access it from the outside
	---@param UiElement IuiElement created ui element
	---@return StatusCodeCode statusCode statuscode indicating success, can only fail if a same name element was already in the window
	function funcTable.addUiElement(name, UiElement)
		PC.expect(1, name, "string")
		PC.expect(2, UiElement, "table")
		PC.expectInterface(3, IuiElement, UiElement)
		if this.elements[name] ~= nil then
			error("elementName already exists", 2)
		end
		this.elements[name] = UiElement

		return StatusCode.Code.Ok
	end

	---draw the window and all ui elements in it
	---@param Visible boolean? is the window visible (used internally to clear invisible windows when switching visibility) leave this empty for external calls to Draw
	function funcTable.draw(Visible)
		if Visible == nil then
			Visible = win.isVisible()
		end
		local oldCol = win.getBackgroundColor()
		if not Visible then
			win.setBackgroundColor(this.terminal.getBackgroundColor())
		end
		local sizeX, sizeY = win.getSize();
		for X = 1, sizeX do
			for Y = 1, sizeY do
				win.setCursorPos(X, Y)
				win.write(" ")
			end
		end
		if Visible then
			for _, v in pairs(this.elements) do
				v.draw()
			end
		else
			win.setBackgroundColor(oldCol)
		end
	end

	---sets visibility of window
	---@param Visible boolean Whether this window is visible.
	function funcTable.setVisible(Visible)
		PC.expect(1, visible, "boolean")
		local oldVis = win.isVisible()
		funcTable.draw(Visible)
		win.setVisible(Visible)
		--redraw parent window if this window is set to invisible
		if oldVis ~= Visible and Visible == false then
			if this.terminal.redraw ~= nil then
				this.terminal.redraw()
			end
			if this.terminal.draw ~= nil then
				this.terminal.draw()
			end
		end
	end

	--------------------------------------- return table --------------------------------------

	local vis = true
	if visible == nil then
		vis = false
	end
	funcTable.setVisible(vis)
	funcTable.draw()
	return funcTable
end

return advWindow
