local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local GuiUtils = require("Gui.GuiUtils")
local Dbg = require("Modules.Logger")

local ListBox = {}

---create a new listbox
---@param parentTerm table parent terminal
---@param X number X start pos
---@param Y number Y start pos
---@param Width number width of list
---@param Height number height of list
---@param textColors table? table of textcolors, will alternate between them for each item displayed
---@param backgroundColors table? table of background colors will alternate between them for each item displayed
---@param userParams any? user definable parameter called when callback function is called
---@param callbackFnc function? function that is called when button was pressed prototype = void callbackFnc(eventInfo, self, userParams), possible events element_click, bar_click, mouse_scroll
---@param textDrawFunction function? function used for drawing the text with
---@return ListBox instance
function ListBox.new(parentTerm, X, Y, Width, Height, textColors, backgroundColors, callbackFnc, userParams,
					 textDrawFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, X, "number")
	PC.expect(3, Y, "number")
	PC.expect(4, Width, "number")
	PC.expect(5, Height, "number")
	PC.expect(6, textColors, "table", "nil")
	PC.expect(7, backgroundColors, "table", "nil")
	PC.expect(8, callbackFnc, "function", "nil")
	PC.expect(10, textDrawFunction, "function", "nil")
	if textColors == nil then textColors = { colors.white } end
	if backgroundColors == nil then backgroundColors = { colors.black, colors.gray } end
	local this = {
		startX = X,
		stopX = X + Width - 1,
		startY = Y,
		stopY = Y + Height - 1,
		terminal = parentTerm,
		colors = Utils.copyTable(textColors),
		backgroundColors = Utils.copyTable(backgroundColors),
		items = {},
		curIndex = 1,
		callbackFnc = callbackFnc,
		userParams = userParams
	}

	if textDrawFunction == nil then
		this.textDrawFunction = GuiUtils.drawTextAlignmentCenterCenter
	else
		this.textDrawFunction = textDrawFunction
	end

	---@class ListBox : IuiElement
	local funcTable = {}

	---Draw the listBox
	function funcTable.draw()
		local oldfg = this.terminal.getTextColor()
		local oldbg = this.terminal.getBackgroundColor()
		local oldX, oldY = this.terminal.getCursorPos()
		local height = this.stopY - this.startY --1 smaller than actual size, bottom row is used to display information
		this.terminal.setCursorPos(1, 1)
		--draw items
		for i = 1, height do
			--todo: add ability to override background color in listboxItem
			this.terminal.setBackgroundColor(this.backgroundColors[(i + this.curIndex - 1) % #this.backgroundColors + 1])
			--draw background
			for x = this.startX, this.stopX do
				this.terminal.setCursorPos(x, this.startY + i - 1)
				this.terminal.write(" ")
			end
			--draw text
			local color = this.colors[(i + this.curIndex - 1) % #this.colors + 1]
			local text = tostring(this.items[i + this.curIndex - 1])
			if type(this.items[i + this.curIndex - 1]) == "table" and this.items[i + this.curIndex - 1].tostring ~= nil then
				text = this.items[i + this.curIndex - 1].tostring(this.items[i + this.curIndex - 1],
					this.stopX - this.startX + 1)
				if this.items[i + this.curIndex - 1].listboxTextColor ~= nil then
					color = this.items[i + this.curIndex - 1].listboxTextColor
				end
			end
			if text ~= "nil" then
				this.terminal.setTextColor(color)
				this.textDrawFunction(this.startX, this.stopX, this.startY + i - 1, this.startY + i - 1, text,
					this.terminal)
			end
		end

		this.terminal.setTextColor(oldfg)
		this.terminal.setBackgroundColor(oldbg)
		--draw bottom bar
		for x = this.startX, this.stopX do
			this.terminal.setCursorPos(x, this.stopY)
			this.terminal.write(" ")
		end

		local curLoc = (#this.items + 2) - (this.stopY - this.startY)
		if (#this.items + 1) <= (this.stopY - this.startY) then
			curLoc = 1
		end
		local text = tostring(this.curIndex .. " / " .. tostring(curLoc))
		this.textDrawFunction(this.startX, this.stopX, this.stopY, this.stopY, text, this.terminal)
		this.terminal.setCursorPos(oldX, oldY)
	end

	---scroll lines up or down, scrolling down positive numbers up negative numbers
	---@param linesToScroll number lines to scroll
	function funcTable.scroll(linesToScroll)
		PC.expect(1, linesToScroll, "number")
		local cacheIndex = this.curIndex
		this.curIndex = this.curIndex + linesToScroll
		if this.curIndex <= 1 then
			this.curIndex = 1
		elseif this.curIndex >= (#this.items + 2) - (this.stopY - this.startY) then
			if ((#this.items + 1) <= this.stopY - this.startY) then
				this.curIndex = 1
			else
				this.curIndex = (#this.items + 2) - (this.stopY - this.startY)
			end
		end
		if cacheIndex ~= this.curIndex then
			funcTable.draw()
		end
	end

	---reset to line 1 of the list
	function funcTable.resetScroll()
		this.curIndex = 1
		funcTable.draw()
	end

	---check what element was clicked and return that element from the listbox
	---@param eventInfo table eventinfo as gotten by os.pullEvent
	---@return nil | any element nil if no element was clicked/ element otherwise
	local function getClickedElementInfo(eventInfo)
		PC.expect(1, eventInfo, "table")
		local Ye = 0
		if eventInfo[1] == "mouse_click" or eventInfo[1] == "monitor_touch" then
			Ye = eventInfo[4]
			--special case for bottom info bar click
			if Ye >= Height then
				return nil
			end
			local element = this.curIndex + Ye - 1
			if element > #this.items then
				return nil
			else
				return this.items[element]
			end
		end
	end

	---check if the bottom bar was clicked/ touched during event
	---@param eventInfo table eventInfo as gotten from os.pullEvent
	---@return boolean bottomBarTouched true bottom bar was clicked, false otherwise
	local function isBottomBarClicked(eventInfo)
		PC.expect(1, eventInfo, "table")
		local event = eventInfo[1]
		local Ye = 0

		if event == "mouse_click" or event == "monitor_touch" or event then
			Xe = eventInfo[3]
			Ye = eventInfo[4]

			if this.stopY - this.startY + 1 == Ye then
				return true
			end
		end
		return false
	end

	---handle incoming events
	---@events mouse_click, mouse_up, mouse_scroll, mouse_drag, monitor_touch
	---@param eventInfo table table of eventinfo as gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		PC.expect(1, eventInfo, "table")
		local info = Utils.copyTable(eventInfo)

		if info[1] == "mouse_click" or info[1] == "mouse_scroll" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
			info[3] = info[3] - this.startX + 1
			info[4] = info[4] - this.startY + 1
			if ((this.terminal.isVisible ~= nil and this.terminal.isVisible()) or this.terminal.isVisible == nil) then
				if (info[3] >= 1 and info[3] <= this.stopX - this.startX + 1 and info[4] >= 1 and info[4] <= this.stopY - this.startY + 1) and this.callbackFnc ~= nil then
					local retTable = {}
					retTable[1] = ""
					retTable[2] = nil
					retTable[3] = info[3]
					retTable[4] = 1
					if info[1] == "mouse_click" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
						local clickedInfo = getClickedElementInfo(info)
						if clickedInfo ~= nil then
							retTable[1] = "element_click"
							retTable[2] = clickedInfo
							this.callbackFnc(retTable, funcTable, this.userParams)
						else
							clickedInfo = isBottomBarClicked(info)
							if clickedInfo then
								retTable[1] = "bar_click"
								retTable[2] = clickedInfo
								this.callbackFnc(retTable, funcTable, this.userParams)
							end
						end
					elseif info[1] == "mouse_scroll" then
						this.callbackFnc(info, funcTable, this.userParams)
					end
				end
			end
		else
			return
		end
	end

	---add item to listbox
	---@param item ListBoxItem | string item to add
	function funcTable.addItem(item)
		PC.expect(1, item, "string", "table")
		table.insert(this.items, item)
		funcTable.draw()
	end

	---sets itemTable to given table
	---@param itemTable string[] | ListBoxItem[] listbox items
	function funcTable.setItemTable(itemTable)
		this.items = itemTable or {}
		funcTable.draw()
	end

	---remove item from listbox
	---@param item any item(s) to remove
	---@return boolean itemRemoved true yes, false no
	function funcTable.removeItem(item)
		local removed = false
		for i = #this.items, 1, -1 do
			if this.items[i] == item then
				removed = true
				table.remove(this.items, i)
			end
		end
		funcTable.draw()
		return removed
	end

	---clear items from listbox
	function funcTable.clearItems()
		this.items = {}
		funcTable.draw()
	end

	return funcTable
end

return ListBox
