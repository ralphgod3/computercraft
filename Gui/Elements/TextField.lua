local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
local PC = require("Modules.ParamCheck")
local GuiUtils = require("Gui.GuiUtils")

local TextField = {}

---create a new textfield
---@param parentTerm table terminal that is the parent of the element
---@param X number start x coordinate for button
---@param Y number start y coordinate for button
---@param Width number element width
---@param Height number element height
---@param textColor number? element text color
---@param backgroundColor number? element background color
---@param text string text for the element
---@param textDrawFunction  function? function that draws the text see GuiUtils.drawTextAlignment.... functions, when not provided uses center center alignment
---@return TextField textfield textfield object
function TextField.new(parentTerm, X, Y, Width, Height, textColor, backgroundColor, text, textDrawFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, X, "number")
	PC.expect(3, Y, "number")
	PC.expect(4, Width, "number")
	PC.expect(5, Height, "number")
	PC.expect(6, textColor, "number", "nil")
	PC.expect(7, backgroundColor, "number", "nil")
	PC.expect(8, text, "string")
	PC.expect(9, textDrawFunction, "function", "nil")
	if textColor == nil then
		textColor = colors.white
	end
	if backgroundColor == nil then
		backgroundColor = colors.black
	end
	local this = {
		startX = X,
		stopX = X + Width - 1,
		startY = Y,
		stopY = Y + Height - 1,
		terminal = parentTerm,
		color = textColor,
		backgroundColor = backgroundColor,
		text = text
	}
	---@class TextField : IuiElement
	local funcTable = {}
	if textDrawFunction == nil then
		this.textDrawFunction = GuiUtils.drawTextAlignmentCenterCenter
	else
		this.textDrawFunction = textDrawFunction
	end

	--------------------------------------- FUNCTIONS --------------------------------------

	---draw the element in the terminal does not have to be called externally
	function funcTable.draw()
		local oldBgColor = this.terminal.getBackgroundColor()
		local oldTextColor = this.terminal.getTextColor()
		local oldx, oldy = this.terminal.getCursorPos()
		this.terminal.setBackgroundColor(this.backgroundColor)
		this.terminal.setTextColor(this.color)

		--draw background
		for y = this.startY, this.stopY do
			for x = this.startX, this.stopX do
				this.terminal.setCursorPos(x, y)
				this.terminal.write(" ")
			end
		end
		this.textDrawFunction(this.startX, this.stopX, this.startY, this.stopY, this.text, this.terminal)
		this.terminal.setCursorPos(oldx, oldy)
		this.terminal.setBackgroundColor(oldBgColor)
		this.terminal.setTextColor(oldTextColor)
	end

	---set the text field text
	---@param textFieldText string text for the text field
	function funcTable.setText(textFieldText)
		PC.expect(1, textFieldText, "string")
		this.text = textFieldText
		funcTable.draw()
	end

	---set the background color for the text
	---@param color number color background color
	function funcTable.SetBackgroundColor(color)
		PC.expect(1, color, "number")
		this.backgroundColor = color
		funcTable.draw()
	end

	---set the text color
	---@param color number color text color
	function funcTable.setColor(color)
		PC.expect(1, color, "number")
		this.color = color
		funcTable.draw()
	end

	---comment handles events empty to maintain compatibility with other ui elements
	---@events none
	---@info doesnt process events only has function to maintain compatibility with other UiElements
	---@param eventInfo table event parameters in order they are gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		return
	end

	--------------------------------------- return table --------------------------------------
	funcTable.draw()
	return funcTable
end

return TextField
