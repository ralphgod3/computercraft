local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local GuiUtils = require("Gui.GuiUtils")
local Button = {}

---create a new button
---@param parentTerm table terminal that is the parent of the button (used to determine if the button is pressed) for the main screen use term.current()
---@param x number start x coordinate for button
---@param y number start y coordinate for button
---@param width number button width
---@param height number button height
---@param textColor number? color button text color
---@param backgroundColor number? color button background color
---@param text string text for the button
---@param callbackFnc function? function that is called when button was pressed prototype = void callbackFnc(eventInfo, self, userParams)
---@param userParams any? user definable parameters passed to the callbackFnc
---@param textDrawFunction  function? function that draws the text see GuiUtils.drawTextAlignment.... functions, when not provided uses center center alignment
---@return Button instance button instance
function Button.new(parentTerm, x, y, width, height, textColor, backgroundColor, text, callbackFnc, userParams,
					textDrawFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, x, "number")
	PC.expect(3, y, "number")
	PC.expect(4, width, "number")
	PC.expect(5, height, "number")
	PC.expect(6, textColor, "number", "nil")
	PC.expect(7, backgroundColor, "number", "nil")
	PC.expect(8, text, "string")
	PC.expect(9, callbackFnc, "function", "nil")
	PC.expect(11, textDrawFunction, "function", "nil")

	if textColor == nil then
		textColor = colors.white
	end
	if backgroundColor == nil then
		backgroundColor = colors.black
	end
	local this = {
		startX = x,
		stopX = x + width - 1,
		startY = y,
		stopY = y + height - 1,
		terminal = parentTerm,
		color = textColor,
		backgroundColor = backgroundColor,
		text = text,
		callbackFnc = callbackFnc,
		userParams = userParams
	}
	---@class Button : IuiElement
	local funcTable = {}

	if textDrawFunction == nil then
		this.textDrawFunction = GuiUtils.drawTextAlignmentCenterCenter
	else
		this.textDrawFunction = textDrawFunction
	end

	--------------------------------------- FUNCTIONS --------------------------------------
	---draw the button in the terminal does not have to be called externally
	function funcTable.draw()
		local oldBgColor = this.terminal.getBackgroundColor()
		local oldTextColor = this.terminal.getTextColor()
		local oldx, oldy = this.terminal.getCursorPos()
		this.terminal.setBackgroundColor(this.backgroundColor)
		this.terminal.setTextColor(this.color)

		--draw background
		for yy = this.startY, this.stopY do
			for xx = this.startX, this.stopX do
				this.terminal.setCursorPos(xx, yy)
				this.terminal.write(" ")
			end
		end

		this.textDrawFunction(this.startX, this.stopX, this.startY, this.stopY, this.text, this.terminal)
		this.terminal.setCursorPos(oldx, oldy)
		this.terminal.setBackgroundColor(oldBgColor)
		this.terminal.setTextColor(oldTextColor)
	end

	---comment handles events that deal with the field
	---@events mouse_click, mouse_up, mouse_scroll, mouse_drag, monitor_touch
	---@info callbacks are called on all the events it is listening too if they happen in the coordinates the button is
	---@param eventInfo table event parameters in order they are gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		PC.expect(1, eventInfo, "table")
		local info = Utils.copyTable(eventInfo)

		if info[1] == "mouse_click" or info[1] == "mouse_up" or info[1] == "mouse_scroll" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
			info[3] = info[3] - this.startX + 1
			info[4] = info[4] - this.startY + 1
			if ((this.terminal.isVisible ~= nil and this.terminal.isVisible()) or this.terminal.isVisible == nil) then
				if (info[3] >= 1 and info[3] <= this.stopX - this.startX + 1 and info[4] >= 1 and info[4] <= this.stopY - this.startY + 1) and this.callbackFnc ~= nil then
					this.callbackFnc(info, funcTable, this.userParams)
				end
			end
		else
			return
		end
	end

	---set the button text
	---@param buttonText string text for the button
	function funcTable.setText(buttonText)
		PC.expect(1, buttonText, "string")
		this.text = buttonText
		funcTable.draw()
	end

	---set the background color for the button
	---@param color number color background color
	function funcTable.setBackgroundColor(color)
		PC.expect(1, color, "number")
		this.backgroundColor = color
		funcTable.draw()
	end

	---set the text color
	---@param color number color text color
	function funcTable.setColor(color)
		PC.expect(1, color, "number")
		this.color = color
		funcTable.draw()
	end

	--------------------------------------- return table --------------------------------------
	funcTable.draw()
	return funcTable
end

return Button
