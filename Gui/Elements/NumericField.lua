local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")

local Dbg = require("Modules.Logger")
local PC = require("Modules.ParamCheck")
local Utils = require("Modules.Utils")
local GuiUtils = require("Gui.GuiUtils")

---create a new numericField
---@param parentTerm table terminal that is the parent of the element
---@param X number start x coordinate for button
---@param Y number start y coordinate for button
---@param Width number element width
---@param Height number element height
---@param textColor number? element text color
---@param backgroundColor number? element background color
---@param number number number the element holds
---@param callbackFnc function? function that is called when button was pressed prototype = void callbackFnc(eventInfo,self, userParams)
---@param userParams any? user definable parameters passed to the callbackFnc
---@param min number? minimum number the numeric field may hold, pass nil to not use
---@param max number? maximum number the numeric field may hold, pass nil to not use
---@param textDrawFunction  function? function that draws the text see GuiUtils.drawTextAlignment.... functions, when not provided uses center center alignment
---@return NumericField instance NumericField object
local function new(parentTerm, X, Y, Width, Height, textColor, backgroundColor, number, callbackFnc, userParams, min, max,
				   textDrawFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, X, "number")
	PC.expect(3, Y, "number")
	PC.expect(4, Width, "number")
	PC.expect(5, Height, "number")
	PC.expect(6, textColor, "number", "nil")
	PC.expect(7, backgroundColor, "number", "nil")
	PC.expect(8, number, "number")
	PC.expect(9, callbackFnc, "function", "nil")
	PC.expect(11, min, "number", "nil")
	PC.expect(12, max, "number", "nil")
	PC.expect(13, textDrawFunction, "function", "nil")
	if textColor == nil then textColor = colors.white end
	if backgroundColor == nil then backgroundColor = colors.black end
	local this = {
		startX = X,
		stopX = X + Width - 1,
		startY = Y,
		stopY = Y + Height - 1,
		terminal = parentTerm,
		color = textColor,
		backgroundColor = backgroundColor,
		number = math.floor(number),
		callbackFnc = callbackFnc,
		userParams = userParams,
		min = min,
		max = max,
	}
	if this.min ~= nil then this.min = math.floor(this.min) end
	if this.max ~= nil then this.max = math.floor(this.max) end

	if this.min ~= nil and this.number < this.min then
		this.number = min
	end
	if this.max ~= nil and this.number > this.max then
		this.number = max
	end

	if textDrawFunction == nil then
		this.textDrawFunction = GuiUtils.drawTextAlignmentCenterCenter
	else
		this.textDrawFunction = textDrawFunction
	end
	---@class NumericField : IuiElement
	local funcTable = {}

	--------------------------------------- FUNCTIONS --------------------------------------
	---draw the element in the terminal does not have to be called externally
	function funcTable.draw()
		local oldBgColor = this.terminal.getBackgroundColor()
		local oldTextColor = this.terminal.getTextColor()
		local oldx, oldy = this.terminal.getCursorPos()
		this.terminal.setBackgroundColor(this.backgroundColor)
		this.terminal.setTextColor(this.color)

		--draw background
		for y = this.startY, this.stopY do
			for x = this.startX, this.stopX do
				this.terminal.setCursorPos(x, y)
				this.terminal.write(" ")
			end
		end

		local suffix = { "", "K", "M", "G", "T", "P", "E" }

		local num = tonumber(this.number)
		local width = this.stopX - this.startX + 1
		local stringLen = string.len(tostring(num))
		local addition = 1

		while width < stringLen do
			addition = addition + 1
			num = math.floor(num / 1000)
			stringLen = string.len(tostring(num)) + 1

			if addition > #suffix then
				Dbg.logE("NUMERICFIELD", "not enough suffixes for level")
				--todo: figure out what below does
				--addition = suffix
			end
		end

		local text = tostring(num) .. suffix[addition]

		if num >= 0 then
			while string.len(text) < width do
				text = "0" .. text
			end
		else
			text = string.sub(text, 2)

			while string.len(text) < width - 1 do
				text = "0" .. text
			end

			text = "-" .. text
		end

		this.textDrawFunction(this.startX, this.stopX, this.startY, this.stopY, text, this.terminal)
		this.terminal.setCursorPos(oldx, oldy)
		this.terminal.setBackgroundColor(oldBgColor)
		this.terminal.setTextColor(oldTextColor)
	end

	---set the numeric field number
	---@param numericFieldNumber number number for numericField
	function funcTable.setNumber(numericFieldNumber)
		PC.expect(1, numericFieldNumber, "number")
		this.number = tonumber(numericFieldNumber)
		if this.min ~= nil and this.number < this.min then
			this.number = min
		end
		if this.max ~= nil and this.number > this.max then
			this.number = max
		end
		funcTable.draw()
	end

	---get number in numericField
	---@return number number number in field
	function funcTable.getNumber()
		return this.number
	end

	---set the background color for the text
	---@param color number color background color
	function funcTable.setBackgroundColor(color)
		PC.expect(1, color, "number")
		this.backgroundColor = color
		funcTable.draw()
	end

	---set the text color
	---@param color number color text color
	function funcTable.setColor(color)
		PC.expect(1, color, "number")
		this.color = color
		funcTable.draw()
	end

	---comment handles events that deal with the field
	---@events mouse_click, mouse_up, mouse_scroll, mouse_drag, monitor_touch
	---@info callback is fired when any of the events happen whilst control is active
	---@param eventInfo table event parameters in order they are gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		PC.expect(1, eventInfo, "table")
		local info = Utils.copyTable(eventInfo)

		if info[1] == "mouse_click" or info[1] == "mouse_up" or info[1] == "mouse_scroll" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
			info[3] = info[3] - this.startX + 1
			info[4] = info[4] - this.startY + 1
			if ((this.terminal.isVisible ~= nil and this.terminal.isVisible()) or this.terminal.isVisible == nil) and (info[3] >= 1 and info[3] <= Width and info[4] >= 1 and info[4] <= Height) and this.callbackFnc ~= nil then
				this.callbackFnc(info, funcTable, this.userParams)
			end
		end
	end

	--------------------------------------- return table --------------------------------------
	funcTable.draw()
	return funcTable
end

return { new = new }
