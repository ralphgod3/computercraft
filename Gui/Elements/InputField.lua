local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Gui/GuiUtils.lua")
local Utils = require("Modules.Utils")
local PC = require("Modules.ParamCheck")
local GuiUtils = require("Gui.GuiUtils")
local InputField = {}

---create a new textfield
---@param parentTerm table terminal that is the parent of the element
---@param X number start x coordinate for button
---@param Y number start y coordinate for button
---@param width number element width
---@param height number element height
---@param textColor any element text color
---@param backgroundColor any element background color
---@param text any text for the element
---@param callbackFnc function? function that is called when text is changed prototype = void callbackFnc(eventInfo, this, userParams)
---@param userParams any? user definable parameters passed to the callbackFnc
---@param textDrawFunction  function? function that draws the text see GuiUtils.drawTextAlignment.... functions, when not provided uses center center alignment
---@return InputField textfield textfield object
function InputField.new(parentTerm, X, Y, width, height, textColor, backgroundColor, text, callbackFnc, userParams,
						textDrawFunction)
	PC.expect(1, parentTerm, "table")
	PC.expect(2, X, "number")
	PC.expect(3, Y, "number")
	PC.expect(4, width, "number")
	PC.expect(5, height, "number")
	PC.expect(6, textColor, "number", "nil")
	PC.expect(7, backgroundColor, "number", "nil")
	PC.expect(8, text, "string", "nil")
	PC.expect(9, callbackFnc, "function", "nil")
	PC.expect(11, textDrawFunction, "function", "nil")
	if textColor == nil then textColor = colors.white end
	if backgroundColor == nil then backgroundColor = colors.black end
	text = text or ""
	local this = {
		startX = X,
		stopX = X + width - 1,
		startY = Y,
		stopY = Y + height - 1,
		terminal = parentTerm,
		color = textColor,
		backgroundColor = backgroundColor,
		text = text,
		callbackFnc = callbackFnc,
		userParams = userParams
	}
	---@class InputField : IuiElement
	local funcTable = {}

	if textDrawFunction == nil then
		this.textDrawFunction = GuiUtils.drawTextAlignmentCenterCenter
	else
		this.textDrawFunction = textDrawFunction
	end

	--------------------------------------- FUNCTIONS --------------------------------------
	---draw the element in the terminal does not have to be called externally
	function funcTable.draw()
		local oldBgColor = this.terminal.getBackgroundColor()
		local oldTextColor = this.terminal.getTextColor()
		local oldx, oldy = this.terminal.getCursorPos()
		this.terminal.setBackgroundColor(this.backgroundColor)
		this.terminal.setTextColor(this.color)

		--draw background
		for y = this.startY, this.stopY do
			for x = this.startX, this.stopX do
				this.terminal.setCursorPos(x, y)
				this.terminal.write(" ")
			end
		end

		local Text = this.text

		if this.isActive then
			Text = Text .. "_"
		end

		this.textDrawFunction(this.startX, this.stopX, this.startY, this.stopY, Text, this.terminal)
		this.terminal.setCursorPos(oldx, oldy)
		this.terminal.setBackgroundColor(oldBgColor)
		this.terminal.setTextColor(oldTextColor)
	end

	---set the text field text
	---@param textFieldText string text for the text field
	function funcTable.setText(textFieldText)
		PC.expect(1, textFieldText, "string")
		this.text = textFieldText
		funcTable.draw()
	end

	---get text in field
	---@return string textFieldText text in field
	function funcTable.getText()
		return this.text
	end

	---set the background color for the text
	---@param color number color background color
	function funcTable.setBackgroundColor(color)
		PC.expect(1, color, "number")
		this.backgroundColor = color
		funcTable.draw()
	end

	---set the text color
	---@param color number color text color
	function funcTable.setColor(color)
		PC.expect(1, color, "number")
		this.color = color
		funcTable.draw()
	end

	---allows user to set control to be active ie overwrite default active behaviour
	--- when control is active keypresses will be entered in the text field
	---@param active boolean is control active
	function funcTable.setActive(active)
		PC.expect(1, active, "boolean")
		this.isActive = active
	end

	---comment handles events that deal with the field
	---@events mouse_click, mouse_up, mouse_drag, monitor_touch, key, char
	---@info mouse and monitor events tell the field when it should process key and char events, the callback is called on all char events and on the key enter and backspace event if the control is active (last control to be clicked on)
	---@param eventInfo table event parameters in order they are gotten from os.pullEvent
	function funcTable.handle(eventInfo)
		PC.expect(1, eventInfo, "table")
		local info = Utils.copyTable(eventInfo)

		if info[1] == "mouse_click" or info[1] == "mouse_up" or info[1] == "mouse_drag" or info[1] == "monitor_touch" then
			info[3] = info[3] - this.startX + 1
			info[4] = info[4] - this.startY + 1

			if ((this.terminal.isVisible ~= nil and this.terminal.isVisible()) or this.terminal.isVisible == nil) and (info[3] >= 1 and info[3] <= width and info[4] >= 1 and info[4] <= height) then
				this.isActive = true
				this.text = ""
				funcTable.draw()
				if this.callbackFnc ~= nil then
					this.callbackFnc(info, funcTable, this.userParams)
				end
			else
				this.isActive = false
				funcTable.draw()
			end
		end

		if this.isActive then
			if info[1] == "key" then
				if keys.getName(eventInfo[2]) == "backspace" then
					if string.len(this.text) > 0 then
						this.text = string.sub(this.text, 1, string.len(this.text) - 1)
						funcTable.draw()

						if this.callbackFnc ~= nil then
							this.callbackFnc(info, funcTable, this.userParams)
						end
					end
				elseif keys.getName(eventInfo[2]) == "enter" and this.callbackFnc ~= nil then
					this.callbackFnc(info, this, this.userParams)
				end
			elseif info[1] == "char" then
				this.text = this.text .. eventInfo[2]
				funcTable.draw()
				if this.callbackFnc ~= nil then
					this.callbackFnc(info, funcTable, this.userParams)
				end
			end
		end
	end

	--------------------------------------- return table --------------------------------------
	funcTable.draw()


	return funcTable
end

return InputField
