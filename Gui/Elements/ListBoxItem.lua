local Git = require("Modules.Git")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/ParamCheck.lua")
local PC = require("Modules.ParamCheck")

---create listbox wrapper with standard tostring function
---@param data table data to store
---@param textColor number | nil text color override for listbox
---@param backgroundColor number | nil background color override for listbox
---@param tostringFnc function | nil override for toString for listboxItem prototype string fnc(table self,number width)
---@return ListBoxItem item item that can be placed in listbox
local function new(data, textColor, backgroundColor, tostringFnc)
	PC.expect(1, data, "table")
	PC.expect(2, textColor, "number", "nil")
	PC.expect(2, backgroundColor, "number", "nil")
	PC.expect(2, tostringFnc, "function", "nil")
	---@class ListBoxItem : IuiElement
	local this = data
	this.listboxTextColor = textColor
	this.listboxBackgroundColor = backgroundColor
	this.tostring = tostringFnc or function(self, width)
		return tostring(data)
	end
	return this
end

---toString function for itemEntries
---@param self table self {name, displayName, count, damage, maxDamage, nbt}
---@param width number width item can take up
---@return string toString item as string
local function itemEntryToString(self, width)
	--smartly truncates/pads item name to fit given width
	if width ~= nil and self.count ~= nil then
		local damageSuffix = ""
		if self.damage and self.maxDamage and self.damage > 0 and self.maxDamage > 0 then --some items have negative durability values, they're probably meaningless to the user
			--damageSuffix = " " .. this.damage .. "/" .. this.maxDamage
			damageSuffix = " " ..
				(math.floor(((1 - self.damage / self.maxDamage) * 100))) ..
				"%"                                                --math.floor is for truncating float
		end
		local countSuffix = " " .. tostring(self.count)
		local itemDisplayNameString = self.displayName

		local maxNameChars = width - #damageSuffix - #countSuffix
		if #itemDisplayNameString > maxNameChars then --display name too long, truncate it
			for i = (maxNameChars - 2), 1, -1 do
				if itemDisplayNameString[i] ~= " " then
					itemDisplayNameString = string.sub(itemDisplayNameString, 1, i) .. ".."
					break
				end
				if i == 1 then --item name is all spaces???
					itemDisplayNameString = string.sub(itemDisplayNameString, 0,
							(width - #damageSuffix - #countSuffix - 2)) .. ".."
				end
			end
		end
		while #itemDisplayNameString < maxNameChars do --display name too short, pad it
			itemDisplayNameString = itemDisplayNameString .. " "
		end

		return itemDisplayNameString .. damageSuffix .. countSuffix
	else
		return self.displayName
	end
end

return {
	new = new,
	itemEntryToString = itemEntryToString,
}
