---@diagnostic disable: redundant-parameter
--fs.copy("disk/veinSlave.lua", "veinSlave.lua")
local p = peripheral.wrap("right")
p.open(1)
p.open(2)
local event, modemside, senderChanel, replyChanel, message, distance = os.pullEvent("modem_message")
p.close(1)
p.close(2)
local splitted = {}

for i in string.gmatch(message, "%S+") do
    table.insert(splitted,i)
end

local x = splitted[1]
local y = splitted[2]
local z = splitted[3]
local offset = splitted[4]
print("dimensions: x "..x.." y "..y.." z "..z.." offset "..offset)

shell.run("disk/veinSlave.lua",x, y, z, offset, replyChanel)
--]]