--command line arguments
local args = {...}

local minFuel = 100
local fuelNames = { "minecraft:planks", "gregtech:log", "minecraft:log",  "primal_tech:charcoal_block", "minecraft:fence", "gregtech:meta_item_1", "minecraft:coal", "minecraft:charcoal"}

if #args ~= 5 then
    print("this should be used in conjunction with masterRun programs, (invalid argument)")
    print("if this program is used as a standalone then prgmName <x> <y> <z> <offset from startpos> <reply channel on modem>")
    return
end

--function redefinitions
local pos = {x = 0, y = 0, z = 0, f = 0}
--hardcoded chestposition
local chestPos = {x = 1, y = 1, z = 0, f = 2}
--hardcoded home
local homePos = { x = 0, y = 0, z = 0, f = 0}
--start position from which the mining will start
local mineStartPos = { x = 1 + tonumber(args[4]) , y = 0, z = 1, f = 3}


local function GetItemSlot(Name)
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and ret.name == Name then
            return true
        end
    end
    return false
end

local function handleFuel()
    local iter = 0
    while turtle.getFuelLevel() < minFuel do
        iter = iter + 1
        local refueled = false
        for i = 1, #fuelNames do
            local ret = turtle.getItemDetail()
            if ret ~= nil and ret.name == fuelNames[i] then
                turtle.refuel(1)
                refueled = true
            end
        end
        if not refueled then
            for i = 1, #fuelNames do
                if GetItemSlot(fuelNames[i]) then
                    break
                end
                turtle.refuel(1)
            end
        end

        if iter > 10 then
            return false
        end
    end
    return true
end

local function goForward(destroy)
    print("x " .. pos.x .. " y " .. pos.y .. " z " .. pos.z .. " f " .. pos.f)
    while not turtle.forward() do
        if destroy == false then
            turtle.attack()
        elseif not turtle.dig() then
            turtle.attack()
        end
        if turtle.getFuelLevel() < 20 and not handleFuel() then
            print("waiting for fuel")
            sleep(5)
        end
    end

    if pos.f  == 0 then
        pos.x = pos.x + 1
    elseif pos.f == 1 then
        pos.z = pos.z -1
    elseif pos.f == 2 then
        pos.x = pos.x -1
    elseif pos.f == 3 then
        pos.z = pos.z + 1
    end
end

local function goUp(destroy)
    while not turtle.up() do
        if destroy == false then
            turtle.attackUp()
        elseif not turtle.digUp() then
            turtle.attackUp()
        end
        if turtle.getFuelLevel() < 20 and not handleFuel() then
            print("waiting for fuel")
            sleep(5)
        end
    end
    pos.y = pos.y + 1
end

local function goDown(destroy)
    while not turtle.down() do
        local suc, ret = turtle.inspectDown()
        if suc and ret.name == "minecraft:bedrock" then
            return
        end
        if destroy == false then
            turtle.attackDown()
        elseif not turtle.digDown() then
            turtle.attackDown()
        end
        if turtle.getFuelLevel() < 20 and not handleFuel() then
            print("waiting for fuel")
            sleep(5)
        end
    end
    pos.y = pos.y - 1
end

local function goLeft()
    turtle.turnLeft()
    pos.f = pos.f-1
    if pos.f < 0 then
        pos.f = 3
    end
end

local function goRight()
    turtle.turnRight()
    pos.f = pos.f + 1
    if pos.f > 3 then
        pos.f = 0
    end
end

local function goTo(position, destroy)
    while pos.y < position.y do
        goUp(destroy)
    end
    while  pos.y > position.y do
        goDown(destroy)
    end

    while pos.x > position.x do
        while pos.f ~= 2 do
            goRight()
        end
        goForward(destroy)
    end
    while pos.x < position.x do
        while pos.f ~= 0 do
            goRight()
        end
        goForward(destroy)
    end

    while pos.z > position.z do
        while pos.f ~= 1 do
            goRight()
        end
        goForward(destroy)
    end
    while pos.z < position.z do
        while pos.f ~= 3 do
            goRight()
        end
        goForward(destroy)
    end

    while position.f ~= pos.f do
        goRight()
    end
end

local function dropStuff()
    local oldPos = {}
    oldPos.x = pos.x
    oldPos.y = pos.y
    oldPos.z = pos.z
    oldPos.f = pos.f
    goTo(mineStartPos,true)
    goTo(chestPos,false)
    for i = 1, 16 do
        turtle.select(i)
        local isFuel = false
        local ret = turtle.getItemDetail()
        for j = 1,#fuelNames do
            if ret ~= nil and ret.name == fuelNames[j] then
                print("fuel found")
                isFuel = true
                break
            end
        end
        if not isFuel then
            while turtle.getItemCount() > 0 do
                turtle.drop()
            end
        end
    end
    goTo(mineStartPos,false)
    goTo(oldPos,true)
end

local curSlotNum = 1
local function inventoryIsFull()
    local iter = 1
    while turtle.getItemCount(curSlotNum) > 0 do
        curSlotNum = curSlotNum + 1
        if curSlotNum > 16 then
            curSlotNum = 1
            iter = iter + 1
            if iter > 4 then
                return true
            end
        end
    end
    return false
end

local function mine()
    turtle.digUp()
    turtle.digDown()
    sleep(0.1)
    while turtle.detectUp() do
        sleep(0.2)
        turtle.digUp()
    end
end

local function mineLine(x)
    for i = 1, x do
        mine()
        if i ~= x-1 then
            goForward(true)
        end
        if inventoryIsFull() then
            dropStuff()
        end
    end
    if not handleFuel() then
        print("waiting for more fuel")
        local oldPosition = {x = pos.x, y = pos.y, z = pos.z, f = pos.f }
        goTo(mineStartPos,true)
        while turtle.getFuelLevel() < 100 do
            if not handleFuel() then
                sleep(5)
            end
        end
        print("resuming the mining")
        goTo(oldPosition,true)
    end
end

local function handleCorner(sideToRotate, goForwards)
    if sideToRotate ~= "left" and sideToRotate ~= "right" then
        print("invalid argument")
        return
    end

    if sideToRotate == "left" then
        goLeft()
    elseif sideToRotate == "right" then
        goRight()
    end
    mine()
    if goForwards == true then
        goForward(true)
    end
    mine()
    if sideToRotate == "left" then
        goLeft()
    elseif sideToRotate == "right" then
        goRight()
    end
end

local function mineLayer(x,z, direction)
    local otherDirection = ""
    if direction == "left" then
        otherDirection = "right"
    elseif direction == "right" then
        otherDirection = "left"
    end

    for i = 1, x do
        mineLine(z)
        if i ~= x then
            if i % 2 == 0 then
                handleCorner(direction,true)
            else
                handleCorner(otherDirection,true)
            end
        else
            if i % 2 == 0 then
                handleCorner(direction,false)
            else
                handleCorner(otherDirection,false)
            end
        end
    end
end

local function mineAll(x,y,z)
    local digDepthLeft = y
    local running = true

    while running == true do

        mineLayer(x,z,"left")
        for i = 1,3 do
            digDepthLeft = digDepthLeft -1
            goDown(true)
            if digDepthLeft-1 < 1 then
                break
            end
        end

        if digDepthLeft-1 < 1 then
            running = false
        end

        mineLayer(x,z,"right")
        for i = 1,3 do
            digDepthLeft = digDepthLeft -1
            goDown(true)
            if digDepthLeft-1 < 1 then
                break
            end
        end
    end
end

local x = tonumber(args[1])
local y = tonumber(args[2])
local z = tonumber(args[3])
--local offset = tonumber(args[4])
local sendChannel = args[5]

while turtle.getFuelLevel() < minFuel do
    print("getting fuel")
    handleFuel()
end

goForward(false)
goTo(mineStartPos,false)
print("at start pos")

goDown(true)
mineAll(x,y,z)
goTo(mineStartPos,true)
goTo(chestPos,false)
for i = 1,16 do
    turtle.select(i)
    turtle.drop()
    while turtle.getItemCount() > 0 do
        if not turtle.drop() then
            sleep(1)
        end
    end
end

goTo(homePos,false)
local p = peripheral.wrap("right")
p.open(1)
p.transmit(tonumber(sendChannel),1,"im done")

