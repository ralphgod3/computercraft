-- put chest, fuel, 2 wired thick modems a disk drive and a disk with slave programs anywhere in my inventory and launch program
--
--
--
--

local minFuelLevel = 32
local fuelNames = { "minecraft:planks", "gregtech:log", "minecraft:log",  "primal_tech:charcoal_block", "minecraft:fence", "gregtech:meta_item_1", "minecraft:coal"}
local chestNames = { "enderstorage:ender_storage", "minecraft:chest", "ironchest:iron_chest", "ironchest:gold_chest"}
local modemName = "computercraft:wired_modem_full"
local diskDriveName = "computercraft:peripheral"
local diskName = "computercraft:disk_expanded"

local function printHelp()
    print("place some fuel, a chest, 2 wired thick modems, a disk drive and a disk with the slave programs in my inventory")
end


local function GetItemSlot(Name)
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and ret.name == Name then
            return true
        end
    end
    return false
end


local function GoForward()
    while not turtle.forward() do
        if not turtle.dig() then
            turtle.attack()
        end
    end
end

local function Place()
    while not turtle.place() do
        if not turtle.dig() then
            turtle.attack()
        end
    end
end

local function PlaceUp()
    while not turtle.placeUp() do
        if not turtle.digUp() then
            turtle.attackUp()
        end
    end
end

local function PlaceDown()
    while not turtle.placeDown() do
        if not turtle.digDown() then
            turtle.attackDown()
        end
    end
end


printHelp()
--start refueling stuffs
while turtle.getFuelLevel() < minFuelLevel do
    local InFuelSlot = false
    local ret = turtle.getItemDetail()
    if ret ~= nil then
        for i = 1, #fuelNames do
            if ret.name == fuelNames[i] then
                print("found fuel slot")
                InFuelSlot = true
                turtle.refuel(1)
                break
            end
        end
    end
    if InFuelSlot == false then
        for i = 1, #fuelNames do
            if GetItemSlot(fuelNames[i]) == true then
                break
            end
        end
        turtle.refuel(1)
    end
end

print("placing placement setup for turtles")
--place disk and drive
GoForward()
turtle.turnLeft()
GetItemSlot(diskDriveName)
Place()
GetItemSlot(diskName)
turtle.drop()
turtle.turnRight()

--place chest
for i = 1, #chestNames do
    if GetItemSlot(chestNames[i]) then
        break
    end
end
PlaceUp()
--place modem
turtle.turnRight()
GetItemSlot(modemName)
Place()
turtle.turnRight()
GoForward()
turtle.turnLeft()
GetItemSlot(modemName)
Place()
turtle.turnLeft()
print("done placing setup")

