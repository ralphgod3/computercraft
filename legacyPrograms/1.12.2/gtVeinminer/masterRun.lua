
--command line arguments
local args = {...}

---@type number?
local fuelPerTurtle = 16
local fuelNames = { "minecraft:planks", "gregtech:log", "minecraft:log",  "primal_tech:charcoal_block", "minecraft:fence", "gregtech:meta_item_1", "minecraft:coal"}
local turtleName = { "computercraft:turtle_expanded" , "computercraft:turtle_expanded" }
local chestNames = { "enderstorage:ender_storage","minecraft:chest", "ironchest:iron_chest", "ironchest:gold_chest", "thermalexpansion:strongbox"}

local sendChannel = 1
local repChannel = 2

local function elementIsInTable(table, elementToFind)
    for _, v in pairs(table) do
        if v == elementToFind then return true end
    end

    return false
end

local function GetItemSlot(Name)
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and ret.name == Name then
            return true
        end
    end
    return false
end

local function GetItemSlotByType(Type)
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and elementIsInTable(Type, ret.name) then
            return true
        end
    end
    return false
end

local function GetItemCountByType(type)
    local count = 0
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if  ret ~= nil and elementIsInTable(type,ret.name) then
            count = count + turtle.getItemCount()
            break
        end
    end
    return count
end

local function dropFuel(turtleFuel)
    local fuelToDrop = turtleFuel
    while fuelToDrop > 0 do
        local isFuel = false
        for i = 1, #fuelNames do
            local ret = turtle.getItemDetail()
            if ret and ret.name == fuelNames[i] then
                isFuel = true
                break
            end
        end
        if not isFuel then
            GetItemSlotByType(fuelNames)
        end
        if fuelToDrop <= turtle.getItemCount() then
            if turtle.drop(fuelToDrop) then
                fuelToDrop = 0
            end

        else
            local count = turtle.getItemCount()
            if turtle.drop() then
                fuelToDrop = fuelToDrop - count
            end
        end
    end
end



local function printHelp()
    print("usage: <x>,<y>,<z>, <opt: startingOffset> <opt: fuel per turtle>")
    print("will dig a hole to the front and left of the mining turtle")
end



------------------------------------- Main -------------------------------------
--input validation --
term.clear()
term.setCursorPos(1,1)
if #args < 3 then
    printHelp()
    return
end

local x = tonumber(args[1])
local y = tonumber(args[2])
local z = tonumber(args[3])
---@type number?
local startOffset = 0

if #args > 3 then
    startOffset = tonumber(args[4])
end

if #args > 4 then
    fuelPerTurtle = tonumber(args[5])
end

if x == nil or y == nil or z == nil or startOffset == nil then
    printHelp()
    return
end

if x < 0 or y < 0 or z < 0 or startOffset < 0 then
    print("this script only accepts positive numbers")
    return
end

local turtles = GetItemCountByType(turtleName)
local fuel = GetItemCountByType(fuelNames)
local turtleX = math.ceil(x / turtles)
local deployedTurtles = x / turtleX
if turtleX < 1 then
    deployedTurtles = x
    turtleX = 1
end



local totalFuelNeeded = deployedTurtles * fuelPerTurtle

print("amount of turtles: " .. turtles)
print("amount of fuel: " .. fuel)
print("total fuel needed: " .. totalFuelNeeded)
print("dimensions: x " .. x .. " y " .. y .. " z " .. z)
print("each turtle will mine: x " .. turtleX .. " y " .. y .. " z " .. z)
print("turtles that will be deployed: " .. deployedTurtles)

local chest = false
if totalFuelNeeded - fuel > 0 then
    local suc, ret = turtle.inspectDown()
    for i = 1, #chestNames do
        if suc and ret.name == chestNames[i] then
            print("chest below detected, assuming it is fuel")
            chest = true
        end
    end

    if not chest then
        print("missing " .. totalFuelNeeded-fuel .. " fuel in turtle")
        return
    end
end


--start setting up turtles --
local p = peripheral.wrap("right")
p.open(repChannel)
for i = 1, deployedTurtles do
    for j = 1, #turtleName do
        if GetItemSlot(turtleName[j]) then
            break
        end
    end
    while not turtle.place() do
        sleep(1)
    end

    if chest then
        turtle.suckDown()
    end
    dropFuel(fuelPerTurtle)

    peripheral.call("front","turnOn")
    sleep(1)
    p.transmit(sendChannel, repChannel, tostring(turtleX) .. " " .. tostring(y) .. " " .. tostring(z) .. " " .. tostring(((i-1) * turtleX) + startOffset))
end

if chest then
    for i = 1,16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and elementIsInTable(fuelNames, ret.name ) then
            turtle.dropDown()
        end
    end
end

--wait for turtles to return
for i = 1, deployedTurtles do
---@diagnostic disable-next-line: redundant-parameter
    local _, _, _, _, _, _ = os.pullEvent("modem_message")
    turtle.dig()
end
print("all done here")
