local m = peripheral.wrap("right")
local ammount = 0

local function replant()
	print("replanting")
	turtle.select(1)
	turtle.suck()
	ammount = turtle.getItemCount(1)
	while ammount ~= 1 do
		ammount = turtle.getItemCount(1)
		if ammount > 1 then
			turtle.drop(ammount - 1)
		else
			if ammount < 1 then
				turtle.suck()
			end
		end
	end
end

m.setAutoCollect(true)

while true do
	sleep(20)
	print(m.getLevels())
	if m.getLevels() > 30 then
		replant()
		turtle.enchant(30)
		turtle.turnRight()
		turtle.drop()
		turtle.turnLeft()
	end
end
