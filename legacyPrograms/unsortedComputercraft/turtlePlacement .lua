local tArgs = { ... }

if not tArgs[1] then
	error("insert tArgs")
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
end


local function place()
	turtle.turnLeft()
	turtle.select(1)
	turtle.place()
	up()
	turtle.select(2)
	turtle.place()
	turtle.select(3)
	turtle.drop()
	down()
	peripheral.call("front", "turnOn")
	turtle.select(4)
	turtle.drop(1)
	turtle.select(5)
	turtle.drop(1)
	turtle.select(3)
	up()
	turtle.suck()
	turtle.select(2)
	turtle.dig()
	down()
	turtle.turnRight()
end

turtle.turnRight()
for i = 1, tArgs[1] - 1 do
	place()
	forward()
end
place()
