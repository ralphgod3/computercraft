--time before attempting to move the contraption
local timeUntilMove = 18

--sends the message to these id's works as a range
local fromID = 590
local tillID = 680

rednet.open("right")


local function pulse(side)
	redstone.setOutput(side, true)
	sleep(0.4)
	redstone.setOutput(side, false)
	sleep(0.4)
end

local function frameMove()
	pulse("back")
	pulse("left")
end

--main
while true do
	while rs.getInput("top") == true do
		print("mining")
		for i = fromID, tillID do
			rednet.send(i, "cycle," .. timeUntilMove - 2)
		end
		sleep(timeUntilMove)
		print("moving")
		frameMove()
	end
	sleep(0.5)
end
