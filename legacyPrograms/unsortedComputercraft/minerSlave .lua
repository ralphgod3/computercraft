local tArgs = { ... }
local full = 3




if tArgs[1] == "help" then
	print("usage <depth>")
	return
end

if not tArgs[1] then
	tArgs[1] = 10
end


local function forward()
	while not turtle.forward() do
		sleep(0.3)
		if not turtle.dig() then
			turtle.attack()
		end
	end
end

local function checkFuel()
	if turtle.getFuelLevel() < 200 then
		turtle.digDown()
		turtle.select(1)
		while not turtle.placeDown() do
			turtle.digDown()
			sleep(1)
		end
		turtle.suckDown()
		while turtle.getItemCount(1) < 8 do
			turtle.suckDown()
			sleep(1)
		end
		while turtle.getItemCount(1) > 8 do
			turtle.dropDown(turtle.getItemCount(1) - 8)
		end
		turtle.refuel()
		turtle.digDown()
	end
end

local function mine()
	checkFuel()
	turtle.select(1)
	turtle.dig()
	turtle.digUp()
	sleep(0.3)
	while turtle.detectUp() do
		turtle.digUp()
		sleep(0.4)
	end
	turtle.digDown()
	forward()
end

local function dropOff()
	turtle.digDown()
	turtle.select(2)
	turtle.placeDown()
	for i = 3, 16 do
		turtle.select(i)
		turtle.dropDown()
		while turtle.getItemCount(i) ~= 0 do
			turtle.dropDown()
			sleep(1)
		end
	end
	turtle.select(2)
	turtle.digDown()
end

local function checkDrop()
	if turtle.getItemCount(full) ~= 0 then
		full = full + 1
		if debug then print(full) end
		if full == 15 then
			dropOff()
			full = 3
		end
	end
end




sleep(math.random(1, 100) / 10)
for i = 1, tArgs[1] do
	print("iteration " .. i)
	checkDrop()
	mine()
end
turtle.digUp()
turtle.digDown()
dropOff()
print("im done")
sleep(2)
os.reboot()
