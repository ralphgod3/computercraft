--variables changeable by user
--energy storage levels to kick turbine on and off
local start = 90
local stop = 95

--delay between each check
local delay = 1

--turbine variables
--speed for turbine to keep in RPM
local turbineSpeed = 1760
--turbine marge to keep speed around as to not keep turning turbine on and off in %
local marge = 2
--------------------------------------------------------------------------------
-------------------------------------FUNCTIONS----------------------------------
--------------------------------------------------------------------------------
--finds the attached reactor
local function findPeripherals()
  local data = peripheral.getNames()
  --no peripherals found
  if not data then
    return false
  end
  local peripherals = {}
  peripherals.turbine = {}
  --find all the needed peripherals for proper operation
  for i = 1,#data do
    if peripheral.getType(data[i]) == "BigReactors-Reactor" then
      peripherals.reactor = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "BigReactors-Turbine" then
      table.insert(peripherals.turbine,peripheral.wrap(data[i]))
    elseif peripheral.getType(data[i]) == "tile_blockcapacitorbank_name" then
      peripherals.capacitor = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "draconic_rf_storage" then
      peripherals.capacitor = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "drum" then
      peripherals.drum = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "monitor" then
      peripherals.monitor = peripheral.wrap(data[i])
    end
  end
  --not all the required peripherals are found
  if not peripherals.reactor or not peripherals.turbine[1] or not peripherals.capacitor or not peripherals.drum then
    return false
  end
  return peripherals
end

--gets energy level as a percentage
local function getEnergyLevel(capacitor)
  return (capacitor.getEnergyStored()/capacitor.getMaxEnergyStored())*100
end

local oldEnergy = 0
local function getEnergyGain(capacitor)
  local energy = capacitor.getEnergyStored()
  local gain = energy - oldEnergy
  oldEnergy = energy
  return gain*delay/20
end

local function updateGUI(p)
  p.monitor.clear()
  p.monitor.setCursorPos(1,1)

  p.monitor.write("turbine RPM, active")
  local j = 2
  local row = 1
  for i = 1,#p.turbine do
    --fix indentation for single digits
    if i <10 then
      p.monitor.setCursorPos(row,j)
    else
      p.monitor.setCursorPos(row-1,j)
    end

    --grab relevant turbine data
    local speed = math.floor(p.turbine[i].getRotorSpeed()*10)/10
    local active = tostring(p.turbine[i].getInductorEngaged())

    --print turbine data with proper seperation
    p.monitor.write(i..": "..speed)
    if math.floor(p.turbine[i].getRotorSpeed()) == speed then
      p.monitor.write(".0 ")
    else
      p.monitor.write(" ")
    end
    if p.turbine[i].getInductorEngaged() then
      p.monitor.setTextColor(colors.green)
    else
      p.monitor.setTextColor(colors.red)
    end
    p.monitor.write(active)
    p.monitor.setTextColor(colors.white)
    --switch rows on monitor when over half
    if j > #p.turbine/2 then
      j = 1
      row = 20
    end
    j = j+1
  end

  local newNull = #p.turbine/2
  --update reactor stats
  p.monitor.setCursorPos(1,newNull+3)
  p.monitor.write("reactor stats")
  p.monitor.setCursorPos(1,newNull+4)
  p.monitor.write("reactor engaged: ")
  if p.reactor.getActive() then
    p.monitor.setTextColor(colors.green)
  else
    p.monitor.setTextColor(colors.red)
  end
  p.monitor.write(tostring(p.reactor.getActive()))
  p.monitor.setTextColor(colors.white)
  p.monitor.setCursorPos(25,newNull+4)
  p.monitor.write("fuel use: ".. p.reactor.getFuelConsumedLastTick())

  --power status
  newNull = newNull +6
  p.monitor.setCursorPos(1,newNull)
  p.monitor.write("power stats")
  p.monitor.setCursorPos(1,newNull+1)
  p.monitor.write("storage filled: "..getEnergyLevel(p.capacitor).."%")
  p.monitor.setCursorPos(1,newNull+2)
  p.monitor.write("rf/t: ")
  local gain = getEnergyGain(p.capacitor)
  if gain >0 then
    p.monitor.setTextColor(colors.green)
  else
    p.monitor.setTextColor(colors.red)
  end
  p.monitor.write(gain)
  p.monitor.setTextColor(colors.white)
  p.monitor.setCursorPos(1,newNull+3)
  p.monitor.write("energy stored: "..p.capacitor.getEnergyStored())
  p.monitor.setCursorPos(1,newNull+4)
  p.monitor.write("max energy: "..p.capacitor.getMaxEnergyStored())
end



--------------------------------------------------------------------------------
-------------------------------------MAIN---------------------------------------
--------------------------------------------------------------------------------
local p = findPeripherals()

if not p then
  print("please attach a reactor, turbine, capacitor bank and a drum for steam storage")
  return
end

local turbineRunning = false

--control loop
while true do
  --handle energyProduction
  if getEnergyLevel(p.capacitor) < start then
    turbineRunning = true
  elseif getEnergyLevel(p.capacitor) > stop then
    turbineRunning = false
  end
  for i = 1,#p.turbine do
    p.turbine[i].setInductorEngaged(turbineRunning)
    --handle turbine speed
    if p.turbine[i].getRotorSpeed() < turbineSpeed *((100 - marge) / 100) then
      p.turbine[i].setFluidFlowRateMax(2000)
    elseif p.turbine[i].getRotorSpeed() > turbineSpeed * (100 + marge) / 100 then
      p.turbine[i].setFluidFlowRateMax(0)
    end
  end

  local tankInfo = p.drum.getTankInfo()
  --handle steam production
  if not tankInfo[1].contents or tankInfo[1].contents.amount/tankInfo[1].capacity < (start/100)  then
    p.reactor.setActive(true)
  elseif tankInfo[1].contents.amount/tankInfo[1].capacity > (stop/100) then
    p.reactor.setActive(false)
  end

  --update monitor if there is one
  if p.monitor then
    updateGUI(p)
  end
  sleep(delay)
end