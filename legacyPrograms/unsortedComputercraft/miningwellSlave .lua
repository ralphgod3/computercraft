--[[
this code should not be used when the server is low on ram because the miningwells might cause some lag then.
all chests and stuff are changeable but i recommend keeping em on default so the sister program works with it
]]
   --

local tArgs = { ... }
local fuelchest = 1
local dropchest = 2
local miningwell = 3
local tesseract = 4
local time = 15
if tArgs[1] == "help" or tonumber(tArgs[1]) == nil then
	print("usage by default")
	print("slot 1 fuelchest")
	print("slot 2 dropchest")
	print("slot 3 miningwell")
	print("slot 4 tesseract")
	return
end
local function forward()
	while not turtle.forward() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.dig() then
			turtle.attack()
		end
	end
end

local function up()
	while not turtle.up() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

local function checkFuel() --basic refueling function
	if turtle.getFuelLevel() < 200 then
		turtle.select(fuelchest)
		turtle.placeUp()
		turtle.suckUp()
		while turtle.getItemCount(fuelchest) < 8 do
			turtle.suckUp()
			sleep(1)
		end
		while turtle.getItemCount(fuelchest) > 8 do
			turtle.dropUp(turtle.getItemCount(fuelchest) - 8)
		end
		turtle.refuel()
		turtle.digUp()
	end
end

local function place()
	turtle.select(miningwell)
	while not turtle.place() do
		sleep(0.5)
	end
	up()
	turtle.select(dropchest)
	while not turtle.place() do
		sleep(0.5)
	end
	turtle.select(tesseract)
	while not turtle.placeDown() do
		sleep(0.5)
	end
end

local function gather()
	turtle.select(tesseract)
	turtle.digDown()
	turtle.select(dropchest)
	turtle.dig()
	down()
	turtle.select(miningwell)
	turtle.dig()
	forward()
end





sleep(math.random(1, 100) / 10) -- giving it a random time to start after command so server doesnt crap out
for i = 1, tArgs[1] do
	print("iteration " .. i)
	checkFuel()
	place()
	sleep(time)
	gather()
end
sleep(5)
os.reboot()
