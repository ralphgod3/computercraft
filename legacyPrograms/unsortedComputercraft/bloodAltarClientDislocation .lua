rednet.open("left")

local id = 1816
local data = {}

while true do
	local i, mess, dist = rednet.receive()
	if i == id then
		print(mess)
		if mess == "craft" then
			data = turtle.getItemDetail(1)
			if data ~= nil then
				if data.name == "AWWayofTime:AlchemicalWizardrybloodRune" then
					turtle.select(2)
					while not turtle.digUp() do
						if not turtle.detectUp() then
							break
						end
						sleep(1)
					end
					turtle.select(1)
					while not turtle.placeUp() do
						sleep(1)
					end
				end
			end
		elseif mess == "blood" then
			data = turtle.getItemDetail(2)
			if data ~= nil then
				if data.name == "AWWayofTime:runeOfSacrifice" then
					turtle.select(1)
					while not turtle.digUp() do
						if not turtle.detectUp() then
							break
						end
						sleep(1)
					end
					turtle.select(2)
					while not turtle.placeUp() do
						sleep(1)
					end
					turtle.select(1)
				end
			end
		end
	end
end
