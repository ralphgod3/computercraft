local tArgs = { ... }
local slot = 1
local placed = 0
local placed2 = 0
local slot2 = 9
turtle.select(1)

if not tArgs[1] then
	print("usage: <height> <width> slot 1,8 frames slot 9,16 covers")
	return
end
local function place()
	turtle.select(slot)
	while not turtle.place() do
		turtle.attack()
		sleep(0.5)
	end
	placed = placed + 1
	if placed == 64 then
		slot = slot + 1
		if slot == 9 then
			slot = 1
		end
		turtle.select(slot)
		placed = 0
	end
	turtle.select(slot2)
	while not turtle.place() do
		turtle.attack()
		sleep(0.5)
	end
	placed2 = placed2 + 1
	if placed2 == 64 then
		slot2 = slot2 + 1
		if slot2 == 17 then
			slot2 = 9
		end
		turtle.select(slot2)
		placed2 = 0
	end
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
			sleep(0.5)
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
			sleep(0.5)
		end
	end
end

local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
			sleep(0.5)
		end
	end
end


local function line()
	for i = 1, tArgs[1] - 1 do
		place()
		up()
	end
	place()
	turtle.turnRight()
	forward()
	turtle.turnLeft()
	for i = 1, tArgs[1] - 1 do
		place()
		down()
	end
	place()
	turtle.turnRight()
	forward()
	turtle.turnLeft()
end
for i = 1, tArgs[2] / 2 do
	print(i * 2)
	line()
end
