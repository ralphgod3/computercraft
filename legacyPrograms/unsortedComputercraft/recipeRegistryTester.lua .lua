--amount of recipes to check for each crafting handler if it has this many
local recipesToCheck = 100
--logs the recipe if a problem is found in it, logs handlers without problems for sanity checking
local enableExtendedLogging = false


------------------------------ FUNCTIONS COPIED FROM LIBRARIES -----------------------------
-- recursive table printing for debugging
local Utils = {}
---internal function to format recursive table printing
---@param curDepth number current depth in table
---@return string string string containing apropriate amount of tabs
local function HandleCurDepthTabs(curDepth)
    --PC.expect(1, curDepth, "number")
    local retString = ""
    for _ = 1, curDepth do
        retString = retString .. "\t"
    end
    return retString
end

---print tables recursively (will print tables in tables in tables ...)
---@param tableToPrint table
---@param curDepth number | nil optional parameter used by recursive calls that gets current array Depth used for fancy printing
---@return string table table in string form to feed to your printer of choice
function Utils.PrintTableRecursive(tableToPrint, curDepth)
    --PC.expect(1, tableToPrint, "table")
    --PC.expect(2, curDepth, "number", "nil")
    if curDepth == nil then curDepth = 1 end

    local retString = HandleCurDepthTabs(curDepth -1) .. "{\r\n"
    for k, v in pairs(tableToPrint) do
        if type(v) == "table" then
            retString = retString .. HandleCurDepthTabs(curDepth) .. tostring(k) .. " :\r\n"
            retString = retString .. Utils.PrintTableRecursive(v, curDepth + 1)
        else
            retString = retString .. HandleCurDepthTabs(curDepth) .. tostring(k) .. " : " .. tostring(v) .. "\r\n"
        end
    end
    retString = retString .. HandleCurDepthTabs(curDepth -1) .. "}\r\n"
    return retString
end

--relevant debug logger functions for easy logging to file
local LogFile = "logs.txt"
local DebugLevel = {
    None = { prefix = "", color = colors.black, severity = 0},
    error = {prefix = "E", color = colors.red, severity = 1},
    Warning = {prefix = "W", color = colors.yellow, severity = 2},
    Info = {prefix = "I", color = colors.green, severity = 3},
    Debug = {prefix = "D", color = colors.blue, severity = 4},
    Verbose = {prefix = "V", color = colors.cyan, severity = 5},
}

local Logger = {
    Levels = DebugLevel,
    Logs = {},
    _p =
    {
        Tags = {},
        LogLevel = DebugLevel.Verbose,
        LogsToKeep = 100000
    }
}

---opens log files, writes all logs to it, closes log file
local function AddToLogFile()
    local file = fs.open(LogFile, "w")
    if file then
        for _, v in pairs(Logger.Logs) do
            file.write(v.tostring())
        end
        file.close()
    end
end

---Log a error
---@param tag string tag to log for
---@param text string text to log
function Logger.LogE(tag, text)
    if type(tag) ~= "string" then
        error(("bad argument #%d (expected %s, got %s)"):format(1, "string", type(tag)), 2)
    end
    if type(text) ~= "string" then
        error(("bad argument #%d (expected %s, got %s)"):format(2, "string", type(text)), 2)
    end
    Logger.Log(tag, DebugLevel.error, text)
end

---log a message with level
---@param tag string tag to log as
---@param level table logging level
---@param text string text to log
function Logger.Log(tag, level, text)
    if Logger._p.Tags[tag] == nil then
        Logger._p.Tags[tag] = Logger._p.LogLevel
    end
    if Logger._p.Tags[tag].severity >= level.severity then
        local logItem = {
            listboxTextColor = level.color,
            tostring = function (width)
                return level.prefix .. ": " .. tag .. ": " .. text .. "\r\n"
            end
        }
        table.insert(Logger.Logs, logItem)
        AddToLogFile()
        --delete oldest logs if size was exceeded
        while #Logger.Logs > Logger._p.LogsToKeep do
            table.remove(Logger.Logs,1)
        end
    end
end

---------------------------------------- LOCAL FUNCTIONS ------------------------------------
-- create a pool of recipe registries for faster testing
local recipePool = {}
---creates a recipe registry pool
---@return table reciperegistry pool with the same functions as a normal recipe registry
function recipePool.new()
    local fncTable = {}
    local curRegistry = 1
    local registries = {peripheral.find("recipeRegistry")}
    if #registries == 0 then
        error("no recipe registries found")
    end
    --automaticly add functions to fncTable and override to iterate through each registry in turn
    for k,_ in pairs(registries[1]) do
        fncTable[k] = function (...)
            while true do
                local retVals = {registries[curRegistry][k](...)}
                if retVals[2] ~= nil and retVals[2] == "queryRegistry is on cooldown" then
                    --print("waiting for registry cooldown", curRegistry)
                    sleep(1)
                else
                    curRegistry = curRegistry + 1
                    if curRegistry > #registries then
                        curRegistry = 1
                    end
                    return table.unpack(retVals)
                end
            end
        end
    end
    return fncTable
end

---gets table length for tables that may not be indexed by number
---@param tableToGetLengthOn table
local function getTableLength(tableToGetLengthOn)
    local len = 0
    for _,_ in pairs(tableToGetLengthOn) do
        len = len + 1
    end
    return len
end

--return codes for verifyItemEntry
local verifyRetCodes = {
    ["ok"] = 0,
    ["noItem"] = 1,
    ["isAir"] = 2,
    ["noEntries"] = 3,
}

--enum that maps to recipe entry,
--used to ensure compatibility should recipe format change entry name
local entries = {
    ["inputs"] = "inputs",
    ["outputs"] = "output",
}

---checks if item entry in a recipe is ok
---@param itemEntryToCheck table recipe.inputs[n] or recipe.output from getAllRecipesForType()
---@return integer verifyRetCodes enum
local function verifyItemEntry(itemEntryToCheck)
    if itemEntryToCheck["1"] ~= nil then
        --nested table entry with alternatives
        --we can assume this works since it would not create such a table if the serializer is failing
        return verifyRetCodes.ok
    elseif itemEntryToCheck["name"] ~= nil then
        if itemEntryToCheck["name"] == "minecraft:air" then
            return verifyRetCodes.isAir
        else
            return verifyRetCodes.ok
        end
    elseif itemEntryToCheck["id"] ~= nil then
        if itemEntryToCheck["id"] == "minecraft:air" then
            return verifyRetCodes.isAir
        else
            return verifyRetCodes.ok
        end
    elseif itemEntryToCheck["item"] ~= nil then
        if itemEntryToCheck["item"] == "minecraft:air" then
            return verifyRetCodes.isAir
        else
            return verifyRetCodes.ok
        end
    elseif itemEntryToCheck["tag"] ~= nil then
        if itemEntryToCheck["tag"] == "minecraft:air" then
            return verifyRetCodes.isAir
        else
            return verifyRetCodes.ok
        end
    end
    return verifyRetCodes.noItem
end

---verify inputs for a recipe to be not air and contain a valid item or tag
---@param recipe table recipe from recipeRegistry.getAllRecipesForType()
---@param entryToVerify string entries enum
---@return integer verifyRetCodes enum
local function verifyRecipeItems(recipe, entryToVerify)
    if getTableLength(recipe[entryToVerify]) == 0 then
        return verifyRetCodes.noItem
    end
    local noItems = true
    for _,item in pairs(recipe[entryToVerify]) do
        local retCode = verifyItemEntry(item)
        if retCode == verifyRetCodes.ok then
            noItems = false
        elseif retCode ~= verifyRetCodes.noItem then
            return retCode
        end
    end
    if noItems then
        return verifyRetCodes.noItem
    end
    return verifyRetCodes.ok
end

---cheks if a recipe is a valid recipe
---@param recipe table recipe as output from recipeRegistry.GetAllRecipesForType()
---@return boolean success
---@return string | nil message on succes nothing is returned, on failure reason for failure is returned
local function verifyRecipe(recipe)
    local inputRet = verifyRetCodes.noEntries
    local outputRet = verifyRetCodes.noEntries
    if recipe[entries.inputs] ~= nil then
        inputRet = verifyRecipeItems(recipe, entries.inputs)
    end
    if recipe[entries.outputs] ~= nil then
        outputRet = verifyRecipeItems(recipe, entries.outputs)
    end
    local retMessage = ""
    local recipeOk = true
    if inputRet == verifyRetCodes.noItem then
        retMessage = retMessage .. "No input item, "
        recipeOk = false
    elseif inputRet == verifyRetCodes.isAir then
        retMessage = retMessage .. "Input is air, "
        recipeOk = false
    elseif inputRet == verifyRetCodes.noEntries then
        retMessage = retMessage .. "No inputs, "
        recipeOk = false
    end
    if outputRet == verifyRetCodes.noItem then
        retMessage = retMessage .. "No output item"
        recipeOk = false
    elseif outputRet == verifyRetCodes.isAir then
        retMessage = retMessage .. "Output is air"
        recipeOk = false
    elseif outputRet == verifyRetCodes.noEntries then
        retMessage = retMessage .. "No outputs"
        recipeOk = false
    end
    if recipeOk == false then
        return recipeOk, retMessage
    end
    return recipeOk
end
---------------------------------------- MAIN -----------------------------------------
local registryPool = recipePool.new()
local craftingHandlers = registryPool.getRecipeTypes()
for _, handlerName in ipairs(craftingHandlers) do
    print("current craftingHandler: " .. handlerName)
    local recipes = registryPool.getAllRecipesForType(handlerName)
    local endRecipe = recipesToCheck
    if #recipes < endRecipe then
        endRecipe = #recipes
    end
    if endRecipe ~= 0 then
        local recipesOk = true
        for i = 1, endRecipe do
            local recipeIsOk, message = verifyRecipe(recipes[i])
            if not recipeIsOk then
                Logger.LogE("recipeChecker", "problem with " .. handlerName .. " reason, " .. message)
                if enableExtendedLogging then
                    Logger.LogE("recipeChecker", Utils.PrintTableRecursive(recipes[i]))
                end
                recipesOk = false
                break
            end
        end
        if recipesOk and enableExtendedLogging then
            Logger.LogE("recipeChecker", "no problems found with " .. handlerName)
        end
    else
        Logger.LogE("recipeChecker", "problem with " .. handlerName .. " reason, No recipes")
    end
end