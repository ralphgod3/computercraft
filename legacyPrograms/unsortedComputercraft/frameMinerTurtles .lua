---@diagnostic disable: redundant-parameter
---@type integer?
local timeUntilDone = 10

--seperates the msg from csv to a table
local function csvToTable(s)
	local tab = {}
	for match in string.gmatch(s, "[ t]*([^,]+)") do
		tab[#tab + 1] = match
	end
	return tab
end




--deploy the miningwell
local function deploy()
	turtle.select(1)
	turtle.place()
end


--clear turtle inventory
local function clearInv()
	for i = 1, 16 do
		turtle.select(i)
		turtle.dropDown()
	end
	turtle.select(1)
end


--runs clearinv forever only to be used in parallel api
local function clearInvLoop()
	while true do
		clearInv()
	end
end

--breaks the miningwell
local function getMiner()
	turtle.select(1)
	turtle.dropDown()
	sleep(0.5)
	while turtle.getItemCount(1) > 0 do
		clearInv()
		sleep(0.5)
	end
	turtle.dig()
end

--for use in parallel api to run a function until timer expires
local function timer()
	os.startTimer(timeUntilDone)
	os.pullEvent("timer")
end


--main
rednet.open("right")
while true do
	local senderID, message, distance = rednet.receive()
	local data = csvToTable(message)
	if data[1] == "cycle" then
		deploy()
		timeUntilDone = tonumber(data[2])
		parallel.waitForAny(timer, clearInvLoop)
		getMiner()
	end
end
