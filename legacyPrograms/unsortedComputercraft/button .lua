local mon
local button = {}

function button.clear()
	button = {}
end

function button.startup(monDirection)
	mon = peripheral.wrap(monDirection)
	mon.setTextScale(1)
	mon.setTextColor(colors.white)
	mon.setBackgroundColor(colors.black)
end

function button.addButton(name, func, param, x, y, width, height, backColorInactive, backColorActive,
						  textColorInactive,
						  textColorActive)
	if (backColorInactive == nil or backColorInactive == "") then
		backColorInactive = colors.red
	end
	if (backColorActive == nil or backColorActive == "") then
		backColorActive = colors.lime
	end
	if (textColorInactive == nil or textColorInactive == "") then
		textColorInactive = colors.white
	end
	if (textColorActive == nil or textColorInactive == "") then
		textColorActive = colors.white
	end

	button[name] = {}
	button[name]["func"] = func
	button[name]["active"] = false
	button[name]["enabled"] = true
	button[name]["param"] = param
	button[name]["xmin"] = x
	button[name]["ymin"] = y
	button[name]["xmax"] = x + width
	button[name]["ymax"] = y + height
	button[name]["bcolin"] = backColorInactive
	button[name]["bcolac"] = backColorActive
	button[name]["tcolin"] = textColorInactive
	button[name]["tcolac"] = textColorActive
end

-- This doesn't remove the button from any screen it's drawn on, it simply removes all functionality of the button and prevents it from being re-drawn.
function button.removeButton(name)
	if (button[name] ~= nil) then
		button[name] = nil
	end
end

function button.enableButton(name)
	if (button[name] ~= nil) then
		button[name]["enabled"] = true
	end
end

function button.drawButton(name)
	local backColor
	local textColor
	local y
	local padding =
	"                                                                                                    "

	if (button[name]["active"] == true) then
		backColor = button[name]["bcolac"]
		textColor = button[name]["tcolac"]
	else
		backColor = button[name]["bcolin"]
		textColor = button[name]["tcolin"]
	end

	mon.setBackgroundColor(backColor)
	mon.setTextColor(textColor)

	-- Writes the button active/inactive color
	for y = button[name]["ymin"], button[name]["ymax"] do
		mon.setCursorPos(button[name]["xmin"], y)
		mon.write(string.sub(padding, 1, button[name]["xmax"] - button[name]["xmin"]))
	end

	-- Write the button text centered in the button
	mon.setCursorPos(
		button[name]["xmin"] + math.floor((button[name]["xmax"] - button[name]["xmin"] - string.len(name)) / 2),
		button[name]["ymin"] + math.floor((button[name]["ymax"] - button[name]["ymin"]) / 2))
	mon.write(string.upper(string.sub(name, 1, 1)) .. string.sub(name, 2))
end

function button.renameButton(oldName, newName, reDraw)
	local k, v

	button[newName] = {}

	for k, v in pairs(button[oldName]) do
		button[newName][k] = v
	end

	button[oldName] = nil

	if (reDraw == true) then
		button.drawButton(newName)
	end
end

function button.disableButton(name)
	if (button[name] ~= nil) then
		button[name]["enabled"] = false
	end
end

function button.drawButtons()
	local name

	for name in pairs(button) do
		button.drawButton(name)
	end
end

function button.toggleButton(name, reDraw)
	if (button[name] ~= nil) then
		button[name]["active"] = not button[name]["active"]

		if (reDraw == true) then
			button.drawButton(name)
		end
	end
end

function button.flash(name, duration)
	if (duration == nil or duration == "") then
		duration = 0.15
	end

	button.toggleButton(name, true)
	sleep(duration)
	button.toggleButton(name, true)
end

function button.activate(name)
	if (button[name] ~= nil) then
		button[name]["active"] = true
		button.drawButton(name)
	end
end

function button.deActivate(name)
	if (button[name] ~= nil) then
		button[name]["active"] = false
		button.drawButton(name)
	end
end

function button.checkxy(x, y)
	local name

	if (button ~= nil) then
		for name in pairs(button) do
			if (y >= button[name]["ymin"] and y <= button[name]["ymax"]) then
				if (x >= button[name]["xmin"] and x <= button[name]["xmax"]) then
					if (button[name]["enabled"] == true) then
						if (button[name]["param"] == "") then
							button[name]["func"]()
						else
							button[name]["func"](button[name]["param"])
						end

						return true
					end
				end
			end
		end
	end

	return false
end
