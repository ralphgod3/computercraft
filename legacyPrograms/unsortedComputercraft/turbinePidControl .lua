local function findPeripherals()
  local data = peripheral.getNames()
  if not data then
    return false
  end
  local peripherals = {}
  for i = 1,#data do
    if peripheral.getType(data[i]) == "BigReactors-Turbine" then
      peripherals.turbine = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "tileenergyacceptor" then
      peripherals.ae = peripheral.wrap(data[i])
    end
  end
  if not peripherals.turbine or not peripherals. ae then
    return false
  end
  return peripherals
end

local function powerToRpm(power)
  return 105*math.pow(10, (0.0004*power))
end

-- pid spul
local stored = 0

local kp = 0.15
local ki = 0.5
local kd = 9

local previous_error = 1
local integral = 1
local derivative = 1
local dt = 1
local error = 1
local setpoint = 900
local output = 0
local p = findPeripherals()

while true do
  local power = p.ae.getAvgPowerUsage()
  error = powerToRpm(power) - p.turbine.getRotorSpeed()
  integral = integral + error * dt
  derivative = (error + previous_error) /dt
  if(integral < 0) then 
  integral = 0
  end
  output = kp * error + ki * integral + kd * derivative
  previous_error = error
  --output to turbine
  term.clear()
  term.setCursorPos(1,1)
  write("power: ")
  print(power)
  write("setpoint: ")
  print(powerToRpm(power))
  write("error: ")
  print(error)
  write("proportional: ")
  print(kp*error)
  write("integral: ")
  print(integral)
  write("derivative: ")
  print(derivative)
  write("output: ")
  print(output)
  p.turbine.setFluidFlowRateMax(output) 
  sleep(dt)
end