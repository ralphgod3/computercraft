---@diagnostic disable: redundant-parameter
local homes = {}
local blacklist = {}
local op = { "ralphgod3" }


while true do
	local seperated = {}
	local event, side, player, uuid, message = os.pullEvent("glasses_chat_command")

	for word in message:gmatch("%w+") do
		table.insert(seperated, word)
	end

	for i = 1, #seperated do
		print(seperated[i])
	end

	local command = seperated[1]
	local arg1 = seperated[2]
	local arg2 = seperated[3]
	local blacklisted = false
	local operator = false

	for i = 1, #op do
		if player == op[i] then
			operator = true
		end
	end

	for i = 1, #blacklist do
		if player == blacklist[i] then
			blacklisted = true
			commands.exec("tell " .. player .. " Je bent geblacklist vanwege misbruik maken van commands!")
		end
	end

	if blacklisted == false then
		if command == "tp" then
			if arg1 and arg2 then
				print("tp " .. player .. " " .. arg2)
				commands.exec("tp " .. player .. " " .. arg2)
			end
		elseif command == "spawnpoint" then
			commands.exec("spawnpoint " .. player)
			commands.exec("tell " .. player .. " your spawn point is set")
		elseif command == "spawn" then
			commands.exec("tp " .. player .. " 230 115 143")
		elseif command == "help" then
			commands.exec("tell " .. player .. " spawn, spawnpoint, tp, home")
		elseif command == "home" then
			if homes[player] ~= nil then
				commands.exec("tp " .. player .. " " .. homes[player][1] .. " " ..
				homes[player][2] .. " " .. homes[player][3])
			end
		elseif command == "slap" then
			if operator == true then
				commands.exec("tp " .. arg1 .. " ~ 400 ~")
			else
				commands.exec("tell " .. player .. " Je bent geen OP")
			end
		else
			commands.exec("tell " .. player .. " leer typen idioot")
		end
		for i = 1, #seperated do
			seperated[i] = nil
		end
	end
end
