local tArgs = { ... }
local fuelchest = 1
local dropchest = 2
local quarry = 3
local tesseract = 4
local freq = 200
local count = 900

if tArgs[1] == "help" or tonumber(tArgs[1]) == nil then
	print("usage by default")
	print("slot 1 fuelchest")
	print("slot 2 dropchest")
	print("slot 3 quarry")
	print("slot 4 tesseract or enderchest containing energy cells")
	return
end

local function forward()
	while not turtle.forward() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.dig() then
			turtle.attack()
		end
	end
end

local function up()
	while not turtle.up() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		sleep(0.5)
		turtle.select(16)
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

local function checkFuel() --basic refueling function
	if turtle.getFuelLevel() < 200 then
		turtle.select(fuelchest)
		turtle.placeUp()
		turtle.suckUp()
		while turtle.getItemCount(fuelchest) < 8 do
			turtle.suckUp()
			sleep(1)
		end
		while turtle.getItemCount(fuelchest) > 8 do
			turtle.dropUp(turtle.getItemCount(fuelchest) - 8)
		end
		turtle.refuel()
		turtle.digUp()
	end
end

local function place()
	turtle.select(quarry)
	while not turtle.place() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
	turtle.select(16)
	up()
	turtle.select(dropchest)
	while not turtle.place() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
	turtle.select(tesseract)
	while not turtle.placeUp() do
		sleep(2)
	end
	turtle.suckUp()
	turtle.placeDown()
end

local function gather()
	turtle.select(tesseract)
	turtle.digDown()
	turtle.dropUp()
	turtle.digUp()
	turtle.select(dropchest)
	turtle.dig()
end

local function move()
	checkFuel()
	gather()
	for i = 1, 9 do
		forward()
	end
	down()
	turtle.select(16)
	turtle.dig()
	turtle.drop()
	place()
	for i = 1, 8 do
		while not turtle.back() do
			sleep(2)
		end
	end
	turtle.select(quarry)
	turtle.digDown()
	for i = 1, 8 do
		forward()
	end
end

place()

for i = 1, tArgs[1] - 1 do
	for i = 1, count / 2 do
		sleep(1)
		print(i .. " seconds")
	end
	turtle.select(tesseract)
	turtle.digDown()
	turtle.dropUp()
	sleep(2)
	turtle.suckUp()
	turtle.placeDown()
	for i = 1, count / 2 do
		sleep(1)
		print(i + count / 2 .. "seconds")
	end
	print("iteration " .. i)
	move()
end
