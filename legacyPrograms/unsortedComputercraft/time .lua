--[[made by ezra3131]]--
--clock program
local time = http.get("http://www.timeapi.org/cet/now").readAll()
time = string.sub(time, 12, 19)
 
local m = peripheral.wrap("top")
m.setTextScale(5)
m.setCursorPos(1,1)
 
local hours = time
hours = tonumber(string.sub(hours, 1, 2))
local minutes = time
minutes = tonumber(string.sub(minutes, 4,5))
local seconds = time
seconds = tonumber(string.sub(seconds, 7,8))
 
local addHour = function()
  hours = hours + 1
  if hours >= 24 then
    hours = hours - 24
  end
end
 
local addMinute = function()
  minutes = minutes + 1
  if minutes >= 60 then
    minutes = minutes - 60
    addHour()
  end
end
 
local addSecond = function()
  seconds = seconds + 1
  if seconds >= 60 then
    seconds = seconds - 60
    addMinute()
  end
end
 
local printTime = function()
  m.clear()
  if hours < 10 then
    m.setCursorPos(1,1)
    m.write(0)
    m.setCursorPos(2,1)
    m.write(hours)
  else
    m.setCursorPos(1,1)
    m.write(hours)
  end
  m.setCursorPos(3,1)
  m.write(":")
  if minutes < 10 then
    m.setCursorPos(4,1)
    m.write(0)
    m.setCursorPos(5,1)
    m.write(minutes)
  else
    m.setCursorPos(4,1)
    m.write(minutes)
  end
  m.setCursorPos(6,1)
  m.write(":")
  if seconds < 10 then
    m.setCursorPos(7,1)
    m.write(0)
    m.setCursorPos(8,1)
    m.write(seconds)
  else
    m.setCursorPos(7,1)
    m.write(seconds)
  end
end
 
while true do
  addSecond()
  printTime()
  os.sleep(1)
end