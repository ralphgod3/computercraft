---@diagnostic disable: redundant-parameter
if rs.getAnalogInput("back") > 2 then
	rs.setOutput("left", false)
	rs.setOutput("right", false)
else
	rs.setOutput("left", true)
	rs.setOutput("right", true)
end

while true do
	local event, p1 = os.pullEvent("redstone")
	if rs.getAnalogInput("back") < 2 then
		rs.setOutput("left", true)
		rs.setOutput("right", true)
		while rs.getAnalogInput("back") < 2 do
			sleep(1)
		end
		rs.setOutput("left", false)
		rs.setOutput("right", false)
	else
		rs.setOutput("left", false)
		rs.setOutput("right", false)
	end
end
