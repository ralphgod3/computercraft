local tree = 0


local function chop()
	turtle.dig()
	turtle.forward()
	while turtle.digUp() do
		if turtle.getFuelLevel() < 200 then
			turtle.refuel(1)
		end
		turtle.up()
		tree = tree + 1
	end
	turtle.dig()
	turtle.forward()
	while turtle.digUp() do
		turtle.up()
		tree = tree + 1
	end
	for i = 1, tree do
		turtle.digDown()
		turtle.down()
		tree = tree - 1
	end
end

turtle.dig()
turtle.refuel()
for i = 1, 10 do
	chop()
end
