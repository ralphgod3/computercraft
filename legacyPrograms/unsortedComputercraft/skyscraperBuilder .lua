local maxHeight = 240
local startHeight = 3

print("refueling")
shell.run("refuel", "all")

sleep(5)

--move to start location
for i = 1, startHeight do
	while not turtle.up() do
		turtle.attackUp()
	end
end

--start placing blocks
local slot = 1
turtle.select(slot)
for i = startHeight, maxHeight do
	while not turtle.place() do
		slot = slot + 1
		if slot == 17 then
			slot = 1
		end
		turtle.select(slot)
	end
	while not turtle.up() do
		turtle.attackUp()
	end
end

-- move back down
for i = 1, maxHeight + startHeight do
	turtle.down()
end
