--variables changeable by user
--energy storage levels to kick turbine on and off
local start = 10
local stop = 90

--delay between each check
local delay = 2

--turbine variables
--speed for turbine to keep in RPM
local turbineSpeed = 1800
--turbine marge to keep speed around as to not keep turning turbine on and off in %
local marge = 2
--------------------------------------------------------------------------------
-------------------------------------FUNCTIONS----------------------------------
--------------------------------------------------------------------------------
--finds the attached reactor
local function findPeripherals()
  local data = peripheral.getNames()
  --no peripherals found
  if not data then
    return false
  end
  local peripherals = {}
  --find all the needed peripherals for proper operation
  for i = 1,#data do
    if peripheral.getType(data[i]) == "BigReactors-Reactor" then
      peripherals.reactor = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "BigReactors-Turbine" then
      peripherals.turbine = peripheral.wrap(data[i])
    elseif peripheral.getType(data[i]) == "tile_blockcapacitorbank_name" then
      peripherals.capacitor = peripheral.wrap(data[i])
    end
  end
  --not all the required peripherals are found
  if not peripherals.reactor or not peripherals.turbine or not peripherals.capacitor then
    return false
  end
  return peripherals
end

--gets energy level as a percentage
local function getEnergyLevel(capacitor)
  return (capacitor.getEnergyStored()/capacitor.getMaxEnergyStored())*100
end

--------------------------------------------------------------------------------
-------------------------------------MAIN---------------------------------------
--------------------------------------------------------------------------------
local p = findPeripherals()

if not p then
  print("please attach a reactor, turbine and a capacitor bank")
  return
end

--control loop
while true do
  --handle energy production
  if getEnergyLevel(p.capacitor) < start then
    p.turbine.setInductorEngaged(true)
  elseif getEnergyLevel(p.capacitor) > stop then
    p.turbine.setInductorEngaged(false)
  end

  --handle steam production
  if p.turbine.getRotorSpeed() < turbineSpeed *((100 - marge) / 100) then
    p.reactor.setActive(true)
  elseif p.turbine.getRotorSpeed() > turbineSpeed * (100 + marge) / 100 then
    p.reactor.setActive(false)
  end
  sleep(delay)
end