local blacklist = {}

--checks if a item is contained in the blacklist
local function isInBlacklist()
	local data = turtle.getItemDetail()
	for i = 1, #blacklist do
		if data.name == blacklist[i] then
			return true
		end
	end
	return false
end

--drops a item in a direction will make sure the item is dropped
local up, down, forward = 0, 1, 2
local function drop(side)
	local dropped = false
	while not dropped do
		if side == up then
			dropped = turtle.dropUp()
		elseif side == down then
			dropped = turtle.dropDown()
		elseif side == forward then
			dropped = turtle.drop()
		end
		if not dropped then
			sleep(0.5)
		end
	end
end

---------------------------------------------------------
----------------------------MAIN-------------------------
---------------------------------------------------------
while true do
	if turtle.suck() then
		if isInBlacklist() then
			drop(down)
		else
			drop(up)
		end
	else
		sleep(2)
	end
end
