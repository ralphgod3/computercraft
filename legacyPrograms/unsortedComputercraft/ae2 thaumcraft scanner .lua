local tArgs = {...}
local list = {}

local ae = peripheral.wrap("right")
list = ae.getAvailableItems()

if not tArgs[1] then
  tArgs[1] = 1
end

print(#list)
sleep(1)
for i = tArgs[1],#list do
  print(i.." : "..list[i].fingerprint.id)
  ae.exportItem(list[i].fingerprint,"up",1,0)
  sleep(2)
end