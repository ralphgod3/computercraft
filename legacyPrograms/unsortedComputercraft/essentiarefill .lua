---@diagnostic disable: cast-local-type, lowercase-global
local m = peripheral.wrap("top")
local button = require("button")
button.startup("top")
local run = true
local disableFillRedstoneDirection = "bottom"
local redstoneWaitTicks = 3
local alchemicalFurnaceFromChestDirection = "auto"
local essentia
local essentiasCount = 51
local emptyJars
local totalJars
local fillAmount
local fillEssentia
local fillAvailableSpace
local topOffJarsEssentiaList
local fillAllTo64EssentiaList
local fillAllTo128EssentiaList
local chests = {}
local aspectAnalyzers = {}
local chestAspects
local distributeBurnRequests = true



-- Print debug output during startup: 0 = off, 1 = summary debug, 2 = everything
-- Errors are still shown regardless of the debug output level.
local debugOutput = 1
local padding = "                                                                                                    "
local numberPadding = 3

-- If true, the more intensive portions of this script sleep between each action
-- If false, the script runs at maximum speed
local serverFriendly = true

--[[

        --Locations and colors of different visuals displayed on the monitor.--

        * Auto positioning is done per-item depending on where it normally sits in relation
        to the other items. For the final position of a control see the setAutoDetectVariables() function below.

        ** Virtually every "auto" set location for the main display is relative to the essentiaX/Y locations. essentiaX = "auto" will
        center the display on the monitor or if it can't be centered due to being too small it will be left-aligned.

        ** Virtually every "auto" set location for the refill essentia display is relative to the refillScreenX/Y locations.
        refillScreenX = "auto" will center the display on the monitor or if it can't be centered due to being too small it will be left-aligned.

        ** If a location isn't default to "auto" it does not currently support auto positioning.

        * Any location or color that defaults to "auto" can be manually specified if that's desired. However, no error
        checking is done to make sure the manual positions are valid.

        * Button height is calculated as 1 characters height. A button with the height of 1 will be the height of the button text.

        * The inactive colors are the colors used during normal display. Active colors are during button presses.
--]]
local essentiaColumnWidth = 19
local essentiaColumnHeight = 17
local essentiaColumnSpacing = 2
local essentiaX = "auto"
local essentiaY = 1

local refreshButtonX = "auto"
local refreshButtonY = "auto"
local refreshButtonWidth = 19
local refreshButtonHeight = 2

local topoffAllButtonX = "auto"
local topoffAllButtonY = "auto"
local topoffAllButtonWidth = 19
local topoffAllButtonHeight = 2

local emptyJarsX = "auto"
local emptyJarsY = "auto"

local totalJarsX = "auto"
local totalJarsY = "auto"

local refillScreenX = "auto"
local refillScreenY = 1

local fillAllTo64ButtonX = "auto"
local fillAllTo64ButtonY = "auto"
local fillAllTo64ButtonWidth = 18
local fillAllTo64ButtonHeight = 2

local fillAllTo128ButtonX = "auto"
local fillAllTo128ButtonY = "auto"
local fillAllTo128ButtonWidth = 18
local fillAllTo128ButtonHeight = 2

local otherOptionsButtonX = "auto"
local otherOptionsButtonY = "auto"
local otherOptionsButtonWidth = 18
local otherOptionsButtonHeight = 2

local essentiaTopOffJarsX = "auto"
local essentiaTopOffJarsY = 5

local essentiaTopOffLabelX = "auto"
local essentiaTopOffLabelY = 2

local essentiaFillAllTo64X = "auto"
local essentiaFillAllTo64Y = 5

local essentiaFillAllTo64LabelX = "auto"
local essentiaFillAllTo64LabelY = 2

local essentiaFillAllTo128X = "auto"
local essentiaFillAllTo128Y = 5

local essentiaFillAllTo128LabelX = "auto"
local essentiaFillAllTo128LabelY = 2

local cancelTopOffJarsButtonX = "auto"
local cancelTopOffJarsButtonY = "auto"
local cancelTopOffJarsButtonWidth = 19
local cancelTopOffJarsButtonHeight = 2

local beginTopOffJarsFillButtonX = "auto"
local beginTopOffJarsFillButtonY = "auto"
local beginTopOffJarsFillButtonWidth = 19
local beginTopOffJarsFillButtonHeight = 2

local cancelFillAllTo64ButtonX = "auto"
local cancelFillAllTo64ButtonY = "auto"
local cancelFillAllTo64ButtonWidth = 19
local cancelFillAllTo64ButtonHeight = 2

local beginFillAllTo64ButtonX = "auto"
local beginFillAllTo64ButtonY = "auto"
local beginFillAllTo64ButtonWidth = 19
local beginFillAllTo64ButtonHeight = 2

local cancelFillAllTo128ButtonX = "auto"
local cancelFillAllTo128ButtonY = "auto"
local cancelFillAllTo128ButtonWidth = 19
local cancelFillAllTo128ButtonHeight = 2

local beginFillAllTo128ButtonX = "auto"
local beginFillAllTo128ButtonY = "auto"
local beginFillAllTo128ButtonWidth = 19
local beginFillAllTo128ButtonHeight = 2

local burnScreenLabelX = "auto"
local burnScreenLabelY = 3

local burnScreenBeanLabelX = 6
local burnScreenBeanLabelY = 10

local burnScreenStatusX = 6
local burnScreenStatusY = 13

local burnScreenBeanTextLabel = "Currently bean type: "
local burnScreenStatusTextLabel = "Current status: "


local monBackColor = colors.black

local essentiaInactiveTextColorEmpty = colors.gray
local essentiaInactiveTextColorLow = colors.red
local essentiaInactiveTextColorMedium = colors.yellow
local essentiaInactiveTextColorHigh = colors.green
local essentiaInactiveBackColor = monBackColor
local essentiaActiveTextColor = colors.white
local essentiaActiveBackColor = colors.lime

local refreshActiveBackColor = colors.lime
local refreshInactiveBackColor = colors.blue
local refreshActiveTextColor = colors.white
local refreshInactiveTextColor = colors.white

local topoffAllJarsActiveTextColor = colors.white
local topoffAllJarsInactiveTextColor = colors.white
local topoffAllJarsActiveBackColor = colors.lime
local topoffAllJarsInactiveBackColor = colors.blue

local fillAllTo64InactiveTextColor = colors.white
local fillAllTo64ActiveTextColor = colors.white
local fillAllTo64InactiveBackColor = colors.blue
local fillAllTo64ActiveBackColor = colors.lime

local fillAllTo128InactiveTextColor = colors.white
local fillAllTo128ActiveTextColor = colors.white
local fillAllTo128InactiveBackColor = colors.blue
local fillAllTo128ActiveBackColor = colors.lime

local refillButtonsInactiveBackColor = colors.blue
local refillButtonsActiveBackColor = colors.lime
local refillButtonsInactiveTextColor = colors.white
local refillButtonsActiveTextColor = colors.white

local otherOptionsButtonInactiveBackColor = colors.blue
local otherOptionsButtonActiveBackColor = colors.lime
local otherOptionsButtonInactiveTextColor = colors.white
local otherOptionsButtonActiveTextColor = colors.white

local cancelTopOffJarsButtonInactiveBackColor = colors.blue
local cancelTopOffJarsButtonActiveBackColor = colors.lime
local cancelTopOffJarsButtonInactiveTextColor = colors.white
local cancelTopOffJarsButtonActiveTextColor = colors.white

local beginTopOffJarsFillButtonInactiveBackColor = colors.blue
local beginTopOffJarsFillButtonActiveBackColor = colors.lime
local beginTopOffJarsFillButtonInactiveTextColor = colors.white
local beginTopOffJarsFillButtonActiveTextColor = colors.white

local cancelFillAllTo64ButtonInactiveBackColor = colors.blue
local cancelFillAllTo64ButtonActiveBackColor = colors.lime
local cancelFillAllTo64ButtonInactiveTextColor = colors.white
local cancelFillAllTo64ButtonActiveTextColor = colors.white

local beginFillAllTo64ButtonInactiveBackColor = colors.blue
local beginFillAllTo64ButtonActiveBackColor = colors.lime
local beginFillAllTo64ButtonInactiveTextColor = colors.white
local beginFillAllTo64ButtonActiveTextColor = colors.white

local cancelFillAllTo128ButtonInactiveBackColor = colors.blue
local cancelFillAllTo128ButtonActiveBackColor = colors.lime
local cancelFillAllTo128ButtonInactiveTextColor = colors.white
local cancelFillAllTo128ButtonActiveTextColor = colors.white

local beginFillAllTo128ButtonInactiveBackColor = colors.blue
local beginFillAllTo128ButtonActiveBackColor = colors.lime
local beginFillAllTo128ButtonInactiveTextColor = colors.white
local beginFillAllTo128ButtonActiveTextColor = colors.white


local emptyJarsTextColor = colors.white
local emptyJarsBackColor = monBackColor

local totalJarsTextColor = colors.white
local totalJarsBackColor = monBackColor

local refillLabelTextColor = colors.white
local refillCurrentAmountTextColor = colors.white
local refillAddingAmountTextColor = colors.green

local topOffAllJarsEssentiaBackColor = monBackColor
local topOffAllJarsEssentiaTextColorZero = essentiaInactiveTextColorEmpty
local topOffAllJarsEssentiaTextColorNonZero = colors.white

local topOffAllJarsEssentiaLabelBackColor = monBackColor
local topOffAllJarsEssentiaLabelTextColor = colors.white

local fillAllTo64EssentiaBackColor = monBackColor
local fillAllTo64EssentiaTextColorZero = essentiaInactiveTextColorEmpty
local fillAllTo64EssentiaTextColorNonZero = essentiaInactiveTextColorMedium

local fillAllTo64EssentiaLabelBackColor = monBackColor
local fillAllTo64EssentiaLabelTextColor = colors.white

local fillAllTo128EssentiaBackColor = monBackColor
local fillAllTo128EssentiaTextColorZero = essentiaInactiveTextColorEmpty
local fillAllTo128EssentiaTextColorNonZero = essentiaInactiveTextColorHigh

local fillAllTo128EssentiaLabelBackColor = monBackColor
local fillAllTo128EssentiaLabelTextColor = colors.white


local burnScreenStatusBackColor = monBackColor
local burnScreenStatusTextColor = colors.white
local burnScreenBeanLabelBackColor = monBackColor
local burnScreenBeanLabelTextColor = colors.white
local burnScreenLabelBackColor = monBackColor
local burnScreenLabelTextColor = colors.white
local burnScreenBorderColor = colors.blue




local function setAutoDetectVariables()
	local monWidth
	local monHeight

	monWidth, monHeight = m.getSize()

	if (alchemicalFurnaceFromChestDirection ~= "auto") then
		if (alchemicalFurnaceFromChestDirection ~= "north"
			and alchemicalFurnaceFromChestDirection ~= "south"
			and alchemicalFurnaceFromChestDirection ~= "east"
			and alchemicalFurnaceFromChestDirection ~= "west") then
			print("Invalid alchemical furnace from chest direction. !!RESTART REQUIRED!!")
			while (true) do
				sleep(60)
			end
		end
	end

	if (essentiaX == "auto") then
		-- Centers the essentia display on the monitor horizontally
		--[[@as integer]]
		essentiaX = math.floor((monWidth - ((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing)) /
				2) + 1

		-- The monitor is too small
		if (essentiaX < 1) then
			print("The monitor is too small width wise to display all the needed main screen information correctly.")

			essentiaX = 1
		end
	end

	if (refreshButtonX == "auto") then
		-- Centers the refresh button on the monitor horizontally
		refreshButtonX = essentiaX +
			math.floor(((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing) /
				2) - math.ceil(refreshButtonWidth / 2) + 1
	end

	if (refreshButtonY == "auto") then
		-- Puts the refresh button directly below the essentia list
		refreshButtonY = essentiaY + essentiaColumnHeight + 1
	end

	if (topoffAllButtonX == "auto") then
		-- Puts the top off all button in line with the refresh button
		topoffAllButtonX = essentiaX +
			math.floor(((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing) /
				2) - math.ceil(topoffAllButtonWidth / 2) + 1
	end

	if (topoffAllButtonY == "auto") then
		-- Puts the top off all button directly below the refresh button
		topoffAllButtonY = refreshButtonY + refreshButtonHeight + 2
	end

	if (fillAllTo64ButtonX == "auto") then
		-- Puts the fill all to 64 button directly to the right of the refresh button
		fillAllTo64ButtonX = refreshButtonX + refreshButtonWidth + 2
	end

	if (fillAllTo64ButtonY == "auto") then
		-- Puts the fill all to 64 button in line with the refresh button
		fillAllTo64ButtonY = refreshButtonY
	end

	if (fillAllTo128ButtonX == "auto") then
		-- Puts the fill all to 64 button directly to the right of the fill all to 64 button
		fillAllTo128ButtonX = fillAllTo64ButtonX
	end

	if (fillAllTo128ButtonY == "auto") then
		-- Puts the fill all to 64 button directly below the fill all to 64 button
		fillAllTo128ButtonY = fillAllTo64ButtonY + fillAllTo64ButtonHeight + 2
	end

	if (otherOptionsButtonX == "auto") then
		-- Puts the other button directly to the left of the top off all jars button
		otherOptionsButtonX = topoffAllButtonX - otherOptionsButtonWidth - 2
	end

	if (otherOptionsButtonY == "auto") then
		-- Puts the other button at the same level as the top off all button
		otherOptionsButtonY = topoffAllButtonY
	end

	if (emptyJarsX == "auto") then
		emptyJarsX = essentiaX + 1
	end

	if (emptyJarsY == "auto") then
		emptyJarsY = essentiaY + essentiaColumnHeight + 1
	end

	if (totalJarsX == "auto") then
		totalJarsX = emptyJarsX
	end

	if (totalJarsY == "auto") then
		totalJarsY = emptyJarsY + 1
	end

	if (refillScreenX == "auto") then
		-- Centers the refill screen on the monitor - the refill screen is 41 pixels wide hard coded
		refillScreenX = math.floor((monWidth - 41) / 2) + 1

		-- The monitor is too small
		if (refillScreenX < 1) then
			print("The monitor is too small width wise to display all the needed refill screen information correctly.")

			refillScreenX = 1
		end
	end

	if (essentiaTopOffJarsX == "auto") then
		-- Centers the top off all essentia display on the monitor horizontally
		essentiaTopOffJarsX = math.floor((monWidth - ((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing)) /
				2) + 1

		-- The monitor is too small
		if (essentiaTopOffJarsX < 1) then
			print(
				"The monitor is too small width wise to display all the needed top off all screen information correctly.")

			essentiaTopOffJarsX = 1
		end
	end

	if (essentiaTopOffLabelX == "auto") then
		essentiaTopOffLabelX = essentiaTopOffJarsX +
			math.floor(((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing) /
				2) - math.ceil(string.len("Top off jars fill request") / 2)
	end

	if (cancelTopOffJarsButtonX == "auto") then
		cancelTopOffJarsButtonX = math.floor((monWidth / 2) - cancelTopOffJarsButtonWidth) + 1
	end

	if (cancelTopOffJarsButtonY == "auto") then
		cancelTopOffJarsButtonY = essentiaTopOffJarsY + essentiaColumnHeight + 1
	end

	if (beginTopOffJarsFillButtonX == "auto") then
		beginTopOffJarsFillButtonX = math.floor((monWidth / 2)) + 2
	end

	if (beginTopOffJarsFillButtonY == "auto") then
		beginTopOffJarsFillButtonY = essentiaTopOffJarsY + essentiaColumnHeight + 1
	end


	if (essentiaFillAllTo64X == "auto") then
		-- Centers the top off all essentia display on the monitor horizontally
		essentiaFillAllTo64X = math.floor((monWidth - ((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing)) /
				2) + 1

		-- The monitor is too small
		if (essentiaFillAllTo64X < 1) then
			print(
				"The monitor is too small width wise to display all the needed fill all to 64 screen information correctly.")

			essentiaFillAllTo64X = 1
		end
	end

	if (essentiaFillAllTo64LabelX == "auto") then
		essentiaFillAllTo64LabelX = essentiaFillAllTo64X +
			math.floor(((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing) /
				2) - math.ceil(string.len("Fill all jars to 64 request") / 2) + 1
	end

	if (cancelFillAllTo64ButtonX == "auto") then
		cancelFillAllTo64ButtonX = math.floor((monWidth / 2) - cancelFillAllTo64ButtonWidth) + 1
	end

	if (cancelFillAllTo64ButtonY == "auto") then
		cancelFillAllTo64ButtonY = essentiaFillAllTo64Y + essentiaColumnHeight + 1
	end

	if (beginFillAllTo64ButtonX == "auto") then
		beginFillAllTo64ButtonX = math.floor((monWidth / 2)) + 2
	end

	if (beginFillAllTo64ButtonY == "auto") then
		beginFillAllTo64ButtonY = essentiaFillAllTo64Y + essentiaColumnHeight + 1
	end


	if (essentiaFillAllTo128X == "auto") then
		-- Centers the top off all essentia display on the monitor horizontally
		essentiaFillAllTo128X = math.floor((monWidth - ((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing)) /
				2) + 1

		-- The monitor is too small
		if (essentiaFillAllTo128X < 1) then
			print(
				"The monitor is too small width wise to display all the needed fill all to 128 screen information correctly.")

			essentiaFillAllTo128X = 1
		end
	end

	if (essentiaFillAllTo128LabelX == "auto") then
		essentiaFillAllTo128LabelX = essentiaFillAllTo128X +
			math.floor(((math.ceil(essentiasCount / essentiaColumnHeight) * (essentiaColumnWidth + essentiaColumnSpacing)) - essentiaColumnSpacing) /
				2) - math.ceil(string.len("Fill all jars to 128 request") / 2) + 1
	end

	if (cancelFillAllTo128ButtonX == "auto") then
		cancelFillAllTo128ButtonX = math.floor((monWidth / 2) - cancelFillAllTo128ButtonWidth) + 1
	end

	if (cancelFillAllTo128ButtonY == "auto") then
		cancelFillAllTo128ButtonY = essentiaFillAllTo128Y + essentiaColumnHeight + 1
	end

	if (beginFillAllTo128ButtonX == "auto") then
		beginFillAllTo128ButtonX = math.floor((monWidth / 2)) + 2
	end

	if (beginFillAllTo128ButtonY == "auto") then
		beginFillAllTo128ButtonY = essentiaFillAllTo128Y + essentiaColumnHeight + 1
	end

	if (burnScreenLabelX == "auto") then
		burnScreenLabelX = math.floor((monWidth / 2) - (string.len("Burn request information") / 2)) + 1
	end

	if (serverFriendly == true) then
		serverFriendly = .05
	else
		serverFriendly = 0
	end
end

function sortEss(t)
	local keys = {}
	local k, i

	for k in pairs(t) do
		keys[#keys + 1] = k
	end
	table.sort(keys)

	i = 0
	return function()
		i = i + 1
		if keys[i] then
			return keys[i], t[keys[i]]
		end
	end
end

function scanJars()
	local myEmptyJars = 0
	local myTotalJars = 0
	local jars = {}
	local jarID = 1
	local aspect
	local aspectCount
	local i, j

	for i, j in ipairs(peripheral.getNames()) do
		if (peripheral.getType(j) == "tt_aspectContainer") then
			aspect = peripheral.call(j, "getAspects")

			aspectCount = peripheral.call(j, "getAspectCount", aspect)

			if (aspectCount > 0) then
				jars[jarID] = {}
				jars[jarID]["aspect"] = aspect
				jars[jarID]["count"] = aspectCount
				jarID = jarID + 1
				myTotalJars = myTotalJars + 1
			elseif (aspectCount == 0) then
				myEmptyJars = myEmptyJars + 1
				myTotalJars = myTotalJars + 1
			end
		end
	end

	emptyJars = myEmptyJars
	totalJars = myTotalJars

	return jars
end

function updateEssentia()
	local myJars
	local myEssentia
	local i, j

	myJars = scanJars()
	myEssentia = essentiaList()

	for i in pairs(myJars) do
		myEssentia[myJars[i]["aspect"]] = myEssentia[myJars[i]["aspect"]] + myJars[i]["count"]
	end

	for i, j in sortEss(myEssentia) do
		if (essentia[i] ~= j) then
			essentia[i] = j
		end
	end
end

function printEssentia()
	local inactiveTextColor
	local buttonName
	local x, y
	local i, j

	x = essentiaX
	y = essentiaY

	for i, j in sortEss(essentia) do
		inactiveTextColor = getEssentiaColor(j)

		buttonName = i .. string.sub(padding, 1, essentiaColumnWidth - (string.len(i) + string.len(pad(j)))) .. pad(j)
		button.addButton(buttonName, essentiaButton, buttonName, x, y, essentiaColumnWidth, 0, essentiaInactiveBackColor,
			essentiaActiveBackColor, inactiveTextColor, essentiaActiveTextColor)

		if (y < (essentiaY + essentiaColumnHeight - 1)) then
			y = y + 1
		else
			y = essentiaY
			x = x + essentiaColumnWidth + essentiaColumnSpacing
		end
	end
end

function getEssentiaColor(currentEssentiaAmount)
	local myColor

	if (currentEssentiaAmount == 0) then
		myColor = essentiaInactiveTextColorEmpty
	elseif (currentEssentiaAmount <= 20) then
		myColor = essentiaInactiveTextColorLow
	elseif (currentEssentiaAmount < 100 and currentEssentiaAmount > 20) then
		myColor = essentiaInactiveTextColorMedium
	else
		myColor = essentiaInactiveTextColorHigh
	end

	return myColor
end

function printEmptyJars()
	m.setTextColor(emptyJarsTextColor)
	m.setBackgroundColor(emptyJarsBackColor)

	m.setCursorPos(emptyJarsX, emptyJarsY)
	m.write("Empty jars:" ..
		string.sub(padding, 1, essentiaColumnWidth - (string.len("Empty jars:") + string.len(pad(emptyJars))) - 1) ..
		pad(emptyJars))
end

function printTotalJars()
	m.setTextColor(totalJarsTextColor)
	m.setBackgroundColor(totalJarsBackColor)

	m.setCursorPos(totalJarsX, totalJarsY)
	m.write("Total jars:" ..
		string.sub(padding, 1, essentiaColumnWidth - (string.len("Total jars:") + string.len(pad(totalJars))) - 1) ..
		pad(totalJars))
end

function pad(number, amount)
	local myPadding = "000000"
	local paddingRequired = 0

	if not (amount) then
		amount = numberPadding
	end

	paddingRequired = amount - string.len(tostring(number))

	if (paddingRequired ~= 0 and paddingRequired > 0) then
		return string.sub(myPadding, 1, paddingRequired) .. tostring(number)
	end

	return tostring(number)
end

function getClick()
	local event
	local side
	local x, y

	event, side, x, y = os.pullEvent()

	if (event == "monitor_touch") then
		button.checkxy(x, y)
	end
end

function refresh(buttonToFlash)
	-- Flash the refresh button
	if (buttonToFlash) then
		button.flash(buttonToFlash)
	end

	-- Clear the screen and button list
	m.setBackgroundColor(monBackColor)
	m.clear()
	button.clear()

	-- Scan and print the essentias
	updateEssentia()
	printEssentia()

	-- Print the number of empty jars
	printEmptyJars()

	-- Print the number of jars
	printTotalJars()

	-- Create the refresh button
	addRefreshButton()

	-- Create the top off all jars button
	addTopoffAllJarsButton()

	-- Create the fill all to 64 button
	addFillAllTo64Button()

	-- Create the fill all to 128 button
	addFillAllTo128Button()

	-- Create the other button
	addOtherOptionsButton()

	-- Draw all the buttons on the screen
	button.drawButtons()
end

function addRefreshButton()
	button.addButton("Refresh", refresh, "Refresh", refreshButtonX, refreshButtonY, refreshButtonWidth,
		refreshButtonHeight, refreshInactiveBackColor, refreshActiveBackColor, refreshInactiveTextColor,
		refreshActiveTextColor)
end

function addTopoffAllJarsButton()
	button.addButton("Top off all jars", topoffAllJars, "Top off all jars", topoffAllButtonX, topoffAllButtonY,
		topoffAllButtonWidth, topoffAllButtonHeight, topoffAllJarsInactiveBackColor, topoffAllJarsActiveBackColor,
		topoffAllJarsInactiveTextColor, topoffAllJarsActiveTextColor)
end

function addFillAllTo64Button()
	button.addButton("Fill all to 64", fillAllTo64, "Fill all to 64", fillAllTo64ButtonX, fillAllTo64ButtonY,
		fillAllTo64ButtonWidth, fillAllTo64ButtonHeight, fillAllTo64InactiveBackColor, fillAllTo64ActiveBackColor,
		fillAllTo64InactiveTextColor, fillAllTo64ActiveTextColor)
end

function addFillAllTo128Button()
	button.addButton("Fill all to 128", fillAllTo128, "Fill all to 128", fillAllTo128ButtonX, fillAllTo128ButtonY,
		fillAllTo128ButtonWidth, fillAllTo128ButtonHeight, fillAllTo128InactiveBackColor, fillAllTo128ActiveBackColor,
		fillAllTo128InactiveTextColor, fillAllTo128ActiveTextColor)
end

function addOtherOptionsButton()
	button.addButton("Reboot", otherOptions, "Reboot", otherOptionsButtonX, otherOptionsButtonY, otherOptionsButtonWidth,
		otherOptionsButtonHeight, otherOptionsButtonInactiveBackColor, otherOptionsButtonActiveBackColor,
		otherOptionsButtonInactiveTextColor, otherOptionsButtonActiveTextColor)
end

function otherOptions(buttonName)
	button.flash(buttonName)
	os.reboot()
end

function fillAllTo64(buttonName)
	local aspect
	local count
	local essentiaToFill
	local haveEssentiaToFill
	local myEmptyJars

	-- The button is erased if jars to top off are found else it's toggled back below.
	button.toggleButton(buttonName, true)
	sleep(.15)

	essentiaToFill = essentiaList()
	myEmptyJars = emptyJars
	haveEssentiaToFill = false

	-- Update the list of essentias in all jars
	updateEssentia()

	-- Checks each jar to see if any of them aren't full.
	for aspect, count in pairs(essentia) do
		if (count < 64) then
			if (count == 0) then
				myEmptyJars = myEmptyJars - 1
			end

			essentiaToFill[aspect] = 64 - count
			haveEssentiaToFill = true
		end
	end

	if (haveEssentiaToFill == true) then
		-- Not all essentia is at 64 or more, display the found essentias and confirm filling.
		showFillAllTo64(essentiaToFill, myEmptyJars)
	else
		-- All essentia are above 64, rename the button and hold the text for a second so it can be read before reverting to the normal text and color.
		button.renameButton(buttonName, "All jars at 64", true)
		sleep(2.5)
		button.toggleButton("All jars at 64", false)
		button.renameButton("All jars at 64", buttonName, true)
	end
end

function showFillAllTo64(essentiaList, emptyJarsAfterFill)
	local x, y
	local myEssentia
	local amount
	local errorText

	fillAllTo64EssentiaList = essentiaList

	-- Clear the screen and button list
	m.setBackgroundColor(monBackColor)
	m.clear()
	button.clear()

	x = essentiaFillAllTo64X
	y = essentiaFillAllTo64Y

	m.setBackgroundColor(fillAllTo64EssentiaLabelBackColor)
	m.setTextColor(fillAllTo64EssentiaLabelTextColor)

	m.setCursorPos(essentiaFillAllTo64LabelX, essentiaFillAllTo64LabelY)
	m.write("Fill all jars to 64 request")

	m.setBackgroundColor(fillAllTo64EssentiaBackColor)

	for myEssentia, amount in sortEss(essentiaList) do
		if (amount == 0) then
			m.setTextColor(fillAllTo64EssentiaTextColorZero)
		else
			m.setTextColor(fillAllTo64EssentiaTextColorNonZero)
		end

		m.setCursorPos(x, y)
		m.write(string.upper(string.sub(myEssentia, 1, 1)) ..
			string.sub(myEssentia, 2) ..
			string.sub(padding, 1, essentiaColumnWidth - (string.len(myEssentia) + string.len(pad(amount)))) ..
			pad(amount))

		if (y < (essentiaFillAllTo64Y + essentiaColumnHeight - 1)) then
			y = y + 1
		else
			y = essentiaFillAllTo64Y
			x = x + essentiaColumnWidth + essentiaColumnSpacing
		end
	end

	button.addButton("Cancel filling", cancelFillAllTo64Button, "Cancel filling", cancelFillAllTo64ButtonX,
		cancelFillAllTo64ButtonY, cancelFillAllTo64ButtonWidth, cancelFillAllTo64ButtonHeight,
		cancelFillAllTo64ButtonInactiveBackColor, cancelFillAllTo64ButtonActiveBackColor,
		cancelFillAllTo64ButtonInactiveTextColor, cancelFillAllTo64ButtonActiveTextColor)
	button.addButton("Begin filling", beginFillAllTo64Button, "Begin filling", beginFillAllTo64ButtonX,
		beginFillAllTo64ButtonY, beginFillAllTo64ButtonWidth, beginFillAllTo64ButtonHeight,
		beginFillAllTo64ButtonInactiveBackColor, beginFillAllTo64ButtonActiveBackColor,
		beginFillAllTo64ButtonInactiveTextColor, beginFillAllTo64ButtonActiveTextColor)

	if (emptyJarsAfterFill < 0) then
		errorText = "NOT ENOUGH EMPTY JARS; HAVE " .. emptyJars .. " NEED " .. (emptyJars - emptyJarsAfterFill)
		m.setCursorPos(
			essentiaFillAllTo64LabelX + math.floor(string.len("Fill all jars to 64 request") / 2) -
			math.floor(string.len(errorText) / 2), essentiaFillAllTo64LabelY + 1)
		m.setTextColor(colors.red)
		m.write(errorText)
		button.disableButton("Begin filling")
	end

	button.drawButtons()
end

function fillAllTo128(buttonName)
	local aspect
	local count
	local essentiaToFill
	local haveEssentiaToFill
	local myEmptyJars
	local myJars
	local jar
	local essentiaJarCount

	-- The button is erased if jars to top off are found else it's toggled back below.
	button.toggleButton(buttonName, true)
	sleep(.15)

	essentiaToFill = essentiaList()
	essentiaJarCount = essentiaList()
	myJars = scanJars()
	myEmptyJars = emptyJars
	haveEssentiaToFill = false

	-- Count how many jars of each aspect exist
	for jar in pairs(myJars) do
		essentiaJarCount[myJars[jar]["aspect"]] = essentiaJarCount[myJars[jar]["aspect"]] + 1
	end

	-- Update the list of essentias in all jars
	updateEssentia()

	-- Checks each jar to see if any of them aren't full.
	for aspect, count in pairs(essentia) do
		if (count < 128) then
			if (count == 0) then
				myEmptyJars = myEmptyJars - 2
			else
				if (essentiaJarCount[aspect] == 1) then
					if (count <= 64) then -- if it's above 64 the jar is probably an advanced thaumaturgy jar that holds 256
						myEmptyJars = myEmptyJars - 1
					end
				end
			end

			essentiaToFill[aspect] = 128 - count
			haveEssentiaToFill = true
		end
	end

	if (haveEssentiaToFill == true) then
		-- Jars have been found that aren't full, display the found jars and confirm filling.
		showFillAllTo128(essentiaToFill, myEmptyJars)
	else
		-- All essentia are above 128, rename the button and hold the text for a second so it can be read before reverting to the normal text and color.
		button.renameButton(buttonName, "All jars at 128", true)
		sleep(2.5)
		button.toggleButton("All jars at 128", false)
		button.renameButton("All jars at 128", buttonName, true)
	end
end

function showFillAllTo128(essentiaList, emptyJarsAfterFill)
	local x, y
	local myEssentia
	local amount
	local errorText

	fillAllTo128EssentiaList = essentiaList

	-- Clear the screen and button list
	m.setBackgroundColor(monBackColor)
	m.clear()
	button.clear()

	x = essentiaFillAllTo128X
	y = essentiaFillAllTo128Y

	m.setBackgroundColor(fillAllTo128EssentiaLabelBackColor)
	m.setTextColor(fillAllTo128EssentiaLabelTextColor)
	m.setCursorPos(essentiaFillAllTo128LabelX, essentiaFillAllTo128LabelY)
	m.write("Fill all jars to 128 request")

	m.setBackgroundColor(fillAllTo128EssentiaBackColor)

	for myEssentia, amount in sortEss(essentiaList) do
		if (amount == 0) then
			m.setTextColor(fillAllTo128EssentiaTextColorZero)
		else
			m.setTextColor(fillAllTo128EssentiaTextColorNonZero)
		end

		m.setCursorPos(x, y)
		m.write(string.upper(string.sub(myEssentia, 1, 1)) ..
			string.sub(myEssentia, 2) ..
			string.sub(padding, 1, essentiaColumnWidth - (string.len(myEssentia) + string.len(pad(amount)))) ..
			pad(amount))

		if (y < (essentiaFillAllTo128Y + essentiaColumnHeight - 1)) then
			y = y + 1
		else
			y = essentiaFillAllTo128Y
			x = x + essentiaColumnWidth + essentiaColumnSpacing
		end
	end

	button.addButton("Cancel filling", cancelFillAllTo128Button, "Cancel filling", cancelFillAllTo128ButtonX,
		cancelFillAllTo128ButtonY, cancelFillAllTo128ButtonWidth, cancelFillAllTo128ButtonHeight,
		cancelFillAllTo128ButtonInactiveBackColor, cancelFillAllTo128ButtonActiveBackColor,
		cancelFillAllTo128ButtonInactiveTextColor, cancelFillAllTo128ButtonActiveTextColor)
	button.addButton("Begin filling", beginFillAllTo128Button, "Begin filling", beginFillAllTo128ButtonX,
		beginFillAllTo128ButtonY, beginFillAllTo128ButtonWidth, beginFillAllTo128ButtonHeight,
		beginFillAllTo128ButtonInactiveBackColor, beginFillAllTo128ButtonActiveBackColor,
		beginFillAllTo128ButtonInactiveTextColor, beginFillAllTo128ButtonActiveTextColor)

	if (emptyJarsAfterFill < 0) then
		errorText = "NOT ENOUGH EMPTY JARS; HAVE " .. emptyJars .. " NEED " .. (emptyJars - emptyJarsAfterFill)

		m.setCursorPos(
			essentiaFillAllTo128LabelX + math.floor(string.len("Fill all jars to 128 request") / 2) -
			math.floor(string.len(errorText) / 2), essentiaFillAllTo128LabelY + 1)
		m.setTextColor(colors.red)
		m.write(errorText)
		button.disableButton("Begin filling")
	end

	button.drawButtons()
end

function topoffAllJars(buttonName)
	local myJars
	local jar
	local essentiaToFill
	local haveEssentiaToFill

	-- The button is erased if jars to top off are found else it's toggled back below.
	button.toggleButton(buttonName, true)
	sleep(.15)

	-- Get a table of all the essentia jars
	myJars = scanJars()
	essentiaToFill = essentiaList()
	haveEssentiaToFill = false

	-- Checks each jar to see if any of them aren't full.
	for jar in pairs(myJars) do
		if (myJars[jar]["count"] < 64) then
			essentiaToFill[myJars[jar]["aspect"]] = essentiaToFill[myJars[jar]["aspect"]] + (64 - myJars[jar]["count"])
			haveEssentiaToFill = true
		elseif (myJars[jar]["count"] > 64) then -- Advanced Thaumaturgy jars hold 256
			essentiaToFill[myJars[jar]["aspect"]] = essentiaToFill[myJars[jar]["aspect"]] + (256 - myJars[jar]["count"])
			haveEssentiaToFill = true
		end
	end

	if (haveEssentiaToFill == true) then
		-- Jars have been found that aren't full, display the found jars and confirm filling.
		showTopOffAllJars(essentiaToFill)
	else
		-- No jars are found, rename the button and hold the text for a second so it can be read before reverting to the normal text and color.
		button.renameButton(buttonName, "All jars full", true)
		sleep(2.5)
		button.toggleButton("All jars full", false)
		button.renameButton("All jars full", buttonName, true)
	end
end

function showTopOffAllJars(essentiaList)
	local x, y
	local myEssentia
	local essentiaText
	local amount

	topOffJarsEssentiaList = essentiaList

	-- Clear the screen and button list
	m.setBackgroundColor(monBackColor)
	m.clear()
	button.clear()

	x = essentiaTopOffJarsX
	y = essentiaTopOffJarsY

	m.setBackgroundColor(topOffAllJarsEssentiaLabelBackColor)
	m.setTextColor(topOffAllJarsEssentiaLabelTextColor)
	m.setCursorPos(essentiaTopOffLabelX, essentiaTopOffLabelY)
	m.write("Top off jars fill request")

	m.setBackgroundColor(topOffAllJarsEssentiaBackColor)

	for myEssentia, amount in sortEss(essentiaList) do
		if (amount == 0) then
			m.setTextColor(topOffAllJarsEssentiaTextColorZero)
		else
			m.setTextColor(topOffAllJarsEssentiaTextColorNonZero)
		end

		m.setCursorPos(x, y)
		m.write(string.upper(string.sub(myEssentia, 1, 1)) ..
			string.sub(myEssentia, 2) ..
			string.sub(padding, 1, essentiaColumnWidth - (string.len(myEssentia) + string.len(pad(amount)))) ..
			pad(amount))

		if (y < (essentiaTopOffJarsY + essentiaColumnHeight - 1)) then
			y = y + 1
		else
			y = essentiaTopOffJarsY
			x = x + essentiaColumnWidth + essentiaColumnSpacing
		end
	end

	button.addButton("Cancel filling", cancelTopOffJarsButton, "Cancel filling", cancelTopOffJarsButtonX,
		cancelTopOffJarsButtonY, cancelTopOffJarsButtonWidth, cancelTopOffJarsButtonHeight,
		cancelTopOffJarsButtonInactiveBackColor, cancelTopOffJarsButtonActiveBackColor,
		cancelTopOffJarsButtonInactiveTextColor, cancelTopOffJarsButtonActiveTextColor)
	button.addButton("Begin filling", beginTopOffJarsFillButton, "Begin filling", beginTopOffJarsFillButtonX,
		beginTopOffJarsFillButtonY, beginTopOffJarsFillButtonWidth, beginTopOffJarsFillButtonHeight,
		beginTopOffJarsFillButtonInactiveBackColor, beginTopOffJarsFillButtonActiveBackColor,
		beginTopOffJarsFillButtonInactiveTextColor, beginTopOffJarsFillButtonActiveTextColor)

	button.drawButtons()
end

function cancelTopOffJarsButton(buttonName)
	button.flash(buttonName)
	topOffJarsEssentiaList = nil
	refresh()
end

function beginTopOffJarsFillButton(buttonName)
	local beanType
	local amountToBurn
	local burnCount

	button.flash(buttonName)

	drawBurnMainScreen()
	allBurnResults = {}
	for beanType, amountToBurn in sortEss(topOffJarsEssentiaList) do
		if (amountToBurn ~= 0) then
			updateBurnBeanType(beanType)
			burnCount = burnBeans(beanType, amountToBurn)


			print("")
			print(beanType)
			print("Burned: " .. burnCount)

			if (burnCount == -100) then
				print(translateBurnResultToText(burnCount))
				break
			end
		end
	end

	refresh()
end

function cancelFillAllTo64Button(buttonName)
	button.flash(buttonName)
	fillAllTo64EssentiaList = nil
	refresh()
end

function beginFillAllTo64Button(buttonName)
	local beanType
	local amountToBurn
	local burnCount

	button.flash(buttonName)

	drawBurnMainScreen()
	allBurnResults = {}
	for beanType, amountToBurn in sortEss(fillAllTo64EssentiaList) do
		if (amountToBurn ~= 0) then
			updateBurnBeanType(beanType)
			burnCount = burnBeans(beanType, amountToBurn)


			print("")
			print(beanType)
			print("Burned: " .. burnCount)

			if (burnCount == -100) then
				print(translateBurnResultToText(burnCount))
				break
			end
		end
	end

	refresh()
end

function cancelFillAllTo128Button(buttonName)
	button.flash(buttonName)
	fillAllTo128EssentiaList = nil
	refresh()
end

function beginFillAllTo128Button(buttonName)
	local beanType
	local amountToBurn
	local burnCount
	local bk, bv

	button.flash(buttonName)

	drawBurnMainScreen()
	for beanType, amountToBurn in sortEss(fillAllTo128EssentiaList) do
		if (amountToBurn ~= 0) then
			updateBurnBeanType(beanType)
			burnCount = burnBeans(beanType, amountToBurn)

			print("")
			print(beanType)
			print("Burned: " .. burnCount)

			if (burnCount == -100) then
				print(translateBurnResultToText(burnCount))
				break
			end
		end
	end

	refresh()
end

function essentiaButton(which)
	local buttonEssentiaName
	local availableEssentiaSpace = 0
	local myJars
	local essentiaCount = 0
	local i

	myJars = scanJars()
	button.flash(which)
	buttonEssentiaName = string.sub(which, 1, string.find(which, " ") - 1)

	for i in pairs(myJars) do
		if (myJars[i]["aspect"] == buttonEssentiaName) then
			availableEssentiaSpace = availableEssentiaSpace + (64 - myJars[i]["count"])
			essentiaCount = essentiaCount + myJars[i]["count"]
		end
	end

	showRefillEssentia(buttonEssentiaName, essentiaCount, availableEssentiaSpace)
end

function showRefillEssentia(essentiaName, essentiaCount, availableEssentiaSpace)
	local fillingText

	fillEssentia = essentiaName
	fillCurrentAmount = essentiaCount
	fillAmount = 0
	fillAvailableSpace = availableEssentiaSpace

	button.clear()
	m.setBackgroundColor(monBackColor)
	m.clear()

	m.setTextColor(refillLabelTextColor)
	fillingText = "Refilling: " .. string.upper(string.sub(fillEssentia, 1, 1)) .. string.sub(fillEssentia, 2)
	m.setCursorPos(refillScreenX + math.floor((41 - string.len(fillingText)) / 2), refillScreenY)
	m.write(fillingText)

	if (fillAvailableSpace == 0 and emptyJars == 0) then
		m.setCursorPos(refillScreenX + math.floor((41 - string.len("ALL JARS USED OR FULL")) / 2), refillScreenY + 1)
		m.setTextColor(colors.red)
		m.write("ALL JARS USED OR FULL")
	end


	m.setTextColor(refillCurrentAmountTextColor)
	m.setCursorPos(refillScreenX, refillScreenY + 2)
	m.write("Currently contains:" ..
		string.sub(padding, 1, 41 - (string.len("Currently contains:") + string.len(pad(fillCurrentAmount)))) ..
		pad(fillCurrentAmount))

	m.setTextColor(refillAddingAmountTextColor)
	m.setCursorPos(refillScreenX, refillScreenY + 3)
	m.write("Currently adding:" ..
		string.sub(padding, 1, 41 - (string.len("Currently adding:") + string.len(pad(fillAmount)))) .. pad(fillAmount))

	m.setTextColor(getEssentiaColor(fillCurrentAmount))
	m.setCursorPos(refillScreenX, refillScreenY + 4)
	m.write("Total after filling:" ..
		string.sub(padding, 1, 41 - (string.len("Total after filling:") + string.len(pad(fillCurrentAmount)))) ..
		pad(fillCurrentAmount))

	button.addButton("+1", addEssentia, "+1", refillScreenX, refillScreenY + 6, 10, 2, refillButtonsInactiveBackColor,
		refillButtonsActiveBackColor, refillButtonsInactiveTextColor, refillButtonsActiveTextColor)
	button.addButton("+5", addEssentia, "+5", refillScreenX + 11, refillScreenY + 6, 9, 2, refillButtonsInactiveBackColor,
		refillButtonsActiveBackColor, refillButtonsInactiveTextColor, refillButtonsActiveTextColor)
	button.addButton("+10", addEssentia, "+10", refillScreenX + 21, refillScreenY + 6, 9, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)
	button.addButton("+64", addEssentia, "+64", refillScreenX + 31, refillScreenY + 6, 10, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)

	button.addButton("-1", addEssentia, "-1", refillScreenX, refillScreenY + 10, 10, 2, refillButtonsInactiveBackColor,
		refillButtonsActiveBackColor, refillButtonsInactiveTextColor, refillButtonsActiveTextColor)
	button.addButton("-5", addEssentia, "-5", refillScreenX + 11, refillScreenY + 10, 9, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)
	button.addButton("-10", addEssentia, "-10", refillScreenX + 21, refillScreenY + 10, 9, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)
	button.addButton("-64", addEssentia, "-64", refillScreenX + 31, refillScreenY + 10, 10, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)

	button.addButton("Top off jar(s)", addEssentia, "Top off jar(s)", refillScreenX, refillScreenY + 14, 20, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)
	button.addButton("Max fill", addEssentia, "Max fill", refillScreenX + 21, refillScreenY + 14, 20, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)

	button.addButton("Cancel filling", refresh, "Cancel filling", refillScreenX, refillScreenY + 18, 41, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)

	button.addButton("Begin filling", buttonBeginRefill, "Begin filling", refillScreenX, refillScreenY + 22, 41, 2,
		refillButtonsInactiveBackColor, refillButtonsActiveBackColor, refillButtonsInactiveTextColor,
		refillButtonsActiveTextColor)

	button.drawButtons()
end

function addEssentia(amount)
	local myFillAmount

	myFillAmount = fillAmount

	--print(fillAmount .. " before")

	button.flash(amount)

	if (amount == "Top off jar(s)") then
		fillAmount = fillAvailableSpace
	elseif (amount == "Max fill") then
		fillAmount = fillAvailableSpace + (emptyJars * 64)
	else
		fillAmount = fillAmount + tonumber(amount)
	end

	if (fillAmount < 0) then
		--print(fillAmount .. " is less than 0 - setting to 0")
		fillAmount = 0
	elseif (fillAmount > fillAvailableSpace) then
		if (emptyJars > 0) then
			if (fillAmount > (fillAvailableSpace + (emptyJars * 64))) then
				fillAmount = fillAvailableSpace + (emptyJars * 64)
			end
		else
			fillAmount = fillAvailableSpace
		end
	end

	if (fillAmount ~= myFillAmount) then
		m.setBackgroundColor(monBackColor)

		m.setTextColor(refillAddingAmountTextColor)
		m.setCursorPos(refillScreenX, refillScreenY + 3)
		m.write("Currently adding:" ..
			string.sub(padding, 1, 41 - (string.len("Currently adding:") + string.len(pad(fillAmount)))) ..
			pad(fillAmount))

		m.setTextColor(getEssentiaColor(fillCurrentAmount + fillAmount))
		m.setCursorPos(refillScreenX, refillScreenY + 4)
		m.write("Total after filling:" ..
			string.sub(padding, 1,
				41 - (string.len("Total after filling:") + string.len(pad(fillCurrentAmount + fillAmount)))) ..
			pad(fillCurrentAmount + fillAmount))
	end
end

-- Standard refill button
function buttonBeginRefill(buttonName)
	local burnCount

	button.flash(buttonName)

	if (fillAmount ~= 0) then
		drawBurnMainScreen()
		updateBurnBeanType(fillEssentia)
		burnCount = burnBeans(fillEssentia, fillAmount)

		print("")
		print("Burned: " .. burnCount)

		print(translateBurnResultToText(burnCount))
	end

	refresh()
end

function translateBurnResultToText(burnResult)
	if (burnResult == -100) then
		return "Aborted"
	end
end

maximumLength = 0

function updateBurnStatus(newStatus)
	local monWidth, monHeight
	local pos
	local writeWidth
	local y

	monWidth, monHeight = m.getSize()
	writeWidth = monWidth - 10
	pos = 1

	for y = 0, 3 do
		m.setCursorPos(burnScreenStatusX, y + burnScreenStatusY + 2)
		m.write(string.sub(padding, 1, writeWidth))
		m.setCursorPos(burnScreenStatusX, y + burnScreenStatusY + 2)
		m.write(string.sub(newStatus, pos, writeWidth + pos - 1))
		pos = pos + writeWidth
	end
end

function updateBurnBeanType(newBean)
	m.setCursorPos(burnScreenBeanLabelX + string.len(burnScreenBeanTextLabel) + 1, burnScreenBeanLabelY)
	m.write(string.upper(string.sub(newBean, 1, 1)) ..
		string.sub(newBean, 2) .. string.sub(padding, 1, 15 - string.len(newBean)))
end

function drawBurnMainScreen()
	local monWidth
	local monHeight
	local x, y

	-- Clears the screen before creating the information burn screen
	button.clear()
	m.setBackgroundColor(monBackColor)
	m.clear()

	-- Draws the border on the screen
	monWidth, monHeight = m.getSize()
	m.setBackgroundColor(burnScreenBorderColor)

	m.setCursorPos(1, 1)
	m.write(string.sub(padding, 1, monWidth))
	m.setCursorPos(1, monHeight)
	m.write(string.sub(padding, 1, monWidth))

	for y = 0, monHeight do
		m.setCursorPos(1, y)
		m.write("  ")
		m.setCursorPos(monWidth - 1, y)
		m.write("  ")
	end

	-- Draws the label text
	m.setBackgroundColor(burnScreenLabelBackColor)
	m.setTextColor(burnScreenLabelTextColor)
	m.setCursorPos(burnScreenLabelX, burnScreenLabelY)
	m.write("Burn request information")

	m.setBackgroundColor(burnScreenBeanLabelBackColor)
	m.setTextColor(burnScreenBeanLabelTextColor)
	m.setCursorPos(burnScreenBeanLabelX, burnScreenBeanLabelY)
	m.write(burnScreenBeanTextLabel)

	m.setBackgroundColor(burnScreenBeanLabelBackColor)
	m.setTextColor(burnScreenBeanLabelTextColor)
	m.setCursorPos(burnScreenStatusX, burnScreenStatusY)
	m.write(burnScreenStatusTextLabel)
end

--[[

        The functions below are used to track mana beans in connected chests and put them in the alchemical furnaces.

--]]
function executeBurn(chest, beanType, amount, returnIfStuck)
	local amountBurned
	local stack
	local slot
	local direction
	local directions
	local aspect
	local count
	local validBeanCount
	local waitingForBeans
	local amountToBurn
	local waitingForFurnace

	-- No beans for this type have ever been found.
	if (chests[chest][beanType] == nil) then
		return 0
	end

	if (returnIfStuck ~= false and returnIfStuck ~= true) then
		returnIfStuck = false
	end

	updateBurnStatus("attempting to burn beans")

	if (alchemicalFurnaceFromChestDirection == "auto") then
		directions = validFurnaceDirections()
	else
		directions = { [alchemicalFurnaceFromChestDirection] = true }
	end

	amountToBurn = amount
	waitingForBeans = 0
	waitingForFurnace = 0

	while (amountToBurn > 0) do
		amountBurned = 0
		for slot in pairs(chests[chest][beanType]) do
			stack = peripheral.call(chests[chest]["peripheral name"], "getStackInSlot", slot)

			if (stack ~= nil and stack.qty > 2) then
				count = amountToBurn - amountBurned

				if (count > (stack.qty - 2)) then
					count = stack.qty - 2
					waitingForBeans = 0
				end
			else
				count = 0
			end

			if (count ~= 0) then
				while (count > 0) do
					for direction in pairs(directions) do
						if (count > 0) then
							burned = peripheral.call(chests[chest]["peripheral name"], "pushItemIntoSlot", direction,
								slot, count, 1)
							count = count - burned
							amountBurned = amountBurned + burned

							if (burned ~= 0) then
								waitingForFurnace = 0
							end

							sleep(serverFriendly)
						end
					end

					if (count > 0) then
						-- Failed to put all the beans into the attached furnace(s)
						if (waitingForFurnace == 1 and returnIfStuck == true) then
							return amount - (amountToBurn - amountBurned)
						elseif (waitingForFurnace == 0) then
							print("Furnace(s) full, waiting for furnace(s) to have room...")
							updateBurnStatus("furnace(s) full, waiting for them to have room")
						end

						waitingForFurnace = 1
						sleep(1)
					end
				end
			else
				sleep(serverFriendly)
			end
		end

		if (amountBurned == 0) then
			-- Out of beans
			if (waitingForBeans == 1 and returnIfStuck == true) then
				return amount - (amountToBurn - amountBurned)
			elseif (waitingForBeans == 0) then
				print("Out of bean type " ..
					beanType ..
					" in chest " ..
					chests[chest]["peripheral name"] ..
					". There needs to be more than 2 beans (in a given stack) to burn them...")
				updateBurnStatus("out of " ..
					beanType .. " in chest " .. chests[chest]["peripheral name"] .. " - waiting for refill")
			end

			waitingForBeans = 1
			--enableRefill()
			sleep(1)
			--disableRefill()
		else
			waitingForBeans = 0
		end

		amountToBurn = amountToBurn - amountBurned
	end

	-- This should always calculate to amount but just in case it doesn't, it's not set static.
	return (amount - amountToBurn)
end

function validFurnaceDirections()
	return { ["north"] = true,["south"] = true,["east"] = true,["west"] = true }
end

function burnBeans(beanType, amount)
	local remainingAmount
	local ci
	local continue
	local amountBurned
	local amountToBurn
	local chestsWithAspect

	amountBurned = 0
	remainingAmount = amount
	chestsWithAspect = {}
	chestsBurnResult = {}

	if (chestAspects[beanType] == 0) then
		-- No chests contain the bean type
		return 0, nil
	else
		for ci in pairs(chests) do
			if (chests[ci][beanType] ~= nil) then
				chestsWithAspect[ci] = true
			end
		end

		if (distributeBurnRequests == true) then
			continue = true

			while (continue) do
				for ci in pairs(chestsWithAspect) do
					if (rs.getInput("left") == true) then
						return -100
					end
					amountToBurn = 64

					if ((remainingAmount - amountToBurn) < 0) then
						amountToBurn = remainingAmount
					end

					amountBurned = executeBurn(ci, beanType, amountToBurn, true)
					remainingAmount = remainingAmount - amountBurned

					if (remainingAmount <= 0) then
						continue = false
						break
					end
				end

				if (continue == true) then
					sleep(5)
				end
			end
		else
			for ci in pairs(chestsWithAspect) do
				amountBurned = executeBurn(ci, beanType, remainingAmount)

				remainingAmount = remainingAmount - amountBurned

				if (remainingAmount <= 0) then
					break
				end
			end
		end
	end

	return (amount - remainingAmount)
end

function getValidChestNames()
	local chests = {
		["container_chest"] = true,
		["iron"] = true,
		["gold"] = true,
		["diamond"] = true,
		["crystal"] = true,
		["obsidian"] = true,
		["copper"] = true,
		["silver"] = true
	}

	return chests
end

function findChestsAndAnalyzers()
	local i, j
	local peripheralType
	local validChestNames
	local myChestID
	local myAnalyzerID

	validChestNames = getValidChestNames()
	myChestId = 1
	myAnalyzerID = 1

	for i, j in ipairs(peripheral.getNames()) do
		peripheralType = peripheral.getType(j)

		if (validChestNames[peripheralType] == true) then
			if (debugOutput > 1) then
				print("Valid chest found: " .. peripheralType .. " size: " .. peripheral.call(j, "getInventorySize"))
			end

			chests[myChestId] = {}
			chests[myChestId]["peripheral name"] = j
			myChestId = myChestId + 1
		elseif (peripheralType == "tt_aspectanalyzer") then
			if (debugOutput > 1) then
				print("Aspect analyzer found: " .. j)
			end

			aspectAnalyzers[myAnalyzerID] = {}
			aspectAnalyzers[myAnalyzerID]["peripheral name"] = j
			myAnalyzerID = myAnalyzerID + 1
		end
	end
end

function printChestAspects()
	local ci
	local e, s, i
	local myEssentias
	local chestEssentia

	myEssentias = essentiaList()

	for ci in pairs(chests) do
		chestEssentia = ""

		for e, i in pairs(chests[ci]) do
			if not (string.find(e, " ")) then
				for s in pairs(i) do
					chestEssentia = chestEssentia .. e .. ":" .. s .. ", "
				end
			end
		end

		if (chestEssentia ~= nil) then
			print("Chest " .. ci .. ": " .. string.sub(chestEssentia, 1, string.len(chestEssentia) - 2))
		end

		print("")
	end

	for ci in pairs(chests) do
		print("Chest: " .. ci .. " analyze time: " .. chests[ci]["analyze time"])
	end
end

function analyzeChestsContents()
	local ci
	local e
	local slot
	local chestContents
	local aspect
	local count
	local startTime
	local endTime
	local missingAspects

	chestAspects = essentiaList()

	for ci in pairs(chests) do
		if (debugOutput > 1) then
			print("Analyzing chest contents " .. ci)
		end

		chestContents = peripheral.call(chests[ci]["peripheral name"], "getAllStacks")
		startTime = os.clock()
		for slot in pairs(chestContents) do
			if (chestContents[slot].name == "Mana Bean") then
				aspect, count = scanItemInSlot(chests[ci]["aspect analyzer"], chests[ci]["peripheral name"], slot,
					chestContents[slot].qty)

				if (aspect ~= nil) then
					if (chests[ci][aspect] == nil) then
						chests[ci][aspect] = {}
					end

					chests[ci][aspect][slot] = true
					chestAspects[aspect] = 1

					if (debugOutput > 1) then
						print("Found mana bean in chest " .. ci .. " slot " .. slot .. " of aspect type " .. aspect)
					end
				else
					print("Mana bean in chest " ..
						ci .. " slot " .. slot .. " needs refilling; only " .. count .. " left.")
				end
			end
		end

		chests[ci]["analyze time"] = os.clock() - startTime

		if (debugOutput > 0) then
			print("Time to analyze chest " .. ci .. " " .. (os.clock() - startTime))
		end
	end

	missingAspects = ""
	for e in pairs(chestAspects) do
		if (chestAspects[e] == 0) then
			if (missingAspects ~= "") then
				missingAspects = missingAspects .. ", "
			end

			missingAspects = missingAspects .. e
		end
	end

	if (missingAspects ~= "") then
		print(
			"WARNING: Not every aspect was found in the connected chests. You won't be able to refill them unless they're added to the chest(s).")
		print("")
		print("Missing aspects: " .. missingAspects)
	end
end

function scanItemInSlot(aspectAnalyzer, chest, slot, quantity)
	local aspect
	local stack

	if (quantity == nil) then
		stack = peripheral.call(chest, "getStackInSlot", slot)
		if (stack ~= nil) then
			quantity = stack.qty
		else
			quantity = 0
		end
	end

	if (quantity <= 2) then
		return nil, quantity
	end

	if (peripheral.call(chest, "pushItemIntoSlot", "down", slot, 1, 1) == 0) then
		print("Failed to push mana bean from chest " ..
			chest .. " slot " .. slot .. " into aspect analyzer. Program halted. !!RESTART REQUIRED!!")

		-- Something went wrong, wait for human intervention.
		while (true) do
			sleep(60)
		end
	end

	aspect = peripheral.call(aspectAnalyzer, "getAspects")

	if (peripheral.call(chest, "pullItemIntoSlot", "down", 1, 1, slot) == 0) then
		print("Failed to pull mana bean from aspect analyzer " ..
			aspectAnalyzer ..
			" back into chest " .. chest .. " slot " .. slot .. ". Program halted. !!RESTART REQUIRED!!")

		-- Something went wrong, wait for human intervention.
		while (true) do
			sleep(60)
		end
	end

	return aspect, quantity
end

function matchChestsWithAnalyzer()
	local ci
	local ai
	local hasItem
	local chestContents
	local slot
	local foundSlot
	local itemStack

	-- Checks to make sure none of the analyzers have any items in them
	for ai in pairs(aspectAnalyzers) do
		hasItem = peripheral.call(aspectAnalyzers[ai]["peripheral name"], "hasItem")
		if (hasItem) then
			print("One of the aspect analyzer(s) has an item in it. All aspect analyzer should be empty.")

			while (hasItem) do
				sleep(1)
				hasItem = peripheral.call(aspectAnalyzers[ai]["peripheral name"], "hasItem")
			end
		end
	end


	for ci in pairs(chests) do
		chestContents = peripheral.call(chests[ci]["peripheral name"], "getAllStacks")
		foundSlot = false

		-- See if the chest contains anything and record the slot of any item if it does.
		for slot in pairs(chestContents) do
			if (chestContents[slot].name == "Mana Bean") then
				foundSlot = slot
				break
			end
		end

		-- Something went wrong, try to get it fixed.
		if not (foundSlot) then
			print("No beans found in chest: " ..
				chests[ci]["peripheral name"] ..
				" at least one bean type needs to be present during startup. Please put some beans in the chest.")
			print("")

			while not (foundSlot) do
				sleep(5)
				chestContents = peripheral.call(chests[ci]["peripheral name"], "getAllStacks")

				for slot in pairs(chestContents) do
					if (chestContents[slot].name == "Mana Bean") then
						foundSlot = slot
						break
					end
				end
			end

			print("Found mana bean in chest slot: " .. foundSlot)
			print("")
		end


		-- Put one of the random items from the chest into the aspect analyzer above it.
		if not (peripheral.call(chests[ci]["peripheral name"], "pushItemIntoSlot", "down", foundSlot, 1, 1)) then
			print("Failed to put item from chest " ..
				chests[ci]["peripheral name"] ..
				" slot " .. foundSlot .. " into aspect analyzer. Make sure everything in the chest is a mana bean.")
			print("")
			sleep(1)

			while not (peripheral.call(chests[ci]["peripheral name"], "pushItemIntoSlot", "down", foundSlot, 1, 1)) do
				sleep(1)
			end
		end


		-- Scan through the aspect analyzers and find which one has the item in it.
		for ai in pairs(aspectAnalyzers) do
			hasItem = peripheral.call(aspectAnalyzers[ai]["peripheral name"], "hasItem")

			if (hasItem) then
				if not (peripheral.call(chests[ci]["peripheral name"], "pullItemIntoSlot", "down", 1, 1, foundSlot)) then
					print(
						"Failed to pull the item back out of the aspect analyzer. Make sure the redstone signal from the computer is disabling refilling of the chests during startup. !!RESTART REQUIRED!!")

					-- This is too difficult to explain how to recover from. Wait for human intervention.
					while (true) do
						sleep(60)
					end
				end

				chests[ci]["aspect analyzer"] = aspectAnalyzers[ai]["peripheral name"]

				if (debugOutput) then
					print("Matched chest: " ..
						chests[ci]["peripheral name"] ..
						" with aspect analyzer: " .. aspectAnalyzers[ai]["peripheral name"])
					print("")
				end

				break
			end
		end

		if not (hasItem) then
			print("Failed to match chest " ..
				chests[ci]["peripheral name"] ..
				" with any connected aspect analyzer. Make sure all chests have aspect analyzers below them and all chests and aspect analyzers have peripherals/proxies connected and enabled. !!RESTART REQUIRED!!")

			-- Some part of the peripheral configuration is wrong and needs to be fixed. Wait for human intervention.
			while (true) do
				sleep(60)
			end
		end
	end
end

function disableRefill()
	rs.setOutput(disableFillRedstoneDirection, false)
	redstone.setBundledOutput(disableFillRedstoneDirection, colors.white)
	sleep(redstoneWaitTicks * .05)
end

function enableRefill(skipSleep)
	rs.setOutput(disableFillRedstoneDirection, true)
	redstone.setBundledOutput(disableFillRedstoneDirection, 0)

	if (skipSleep ~= true) then
		sleep(redstoneWaitTicks * .05)
	end
end

function essentiaList()
	local essentias

	essentias = {
		["aer"] = 0,
		["alienis"] = 0,
		["aqua"] = 0,
		["arbor"] = 0,
		["auram"] = 0,
		["bestia"] = 0,
		["cognitio"] = 0,
		["corpus"] = 0,
		["exanimis"] = 0,
		["fabrico"] = 0,
		["fames"] = 0,
		["gelum"] = 0,
		["granum"] = 0,
		["herba"] = 0,
		["humanus"] = 0,
		["ignis"] = 0,
		["instrumentum"] = 0,
		["iter"] = 0,
		["limus"] = 0,
		["lucrum"] = 0,
		["lux"] = 0,
		["machina"] = 0,
		["messis"] = 0,
		["metallum"] = 0,
		["meto"] = 0,
		["mortuus"] = 0,
		["motus"] = 0,
		["ordo"] = 0,
		["pannus"] = 0,
		["perditio"] = 0,
		["perfodio"] = 0,
		["permutatio"] = 0,
		["potentia"] = 0,
		["praecantatio"] = 0,
		["sano"] = 0,
		["saxum"] = 0,
		["sensus"] = 0,
		["spiritus"] = 0,
		["telum"] = 0,
		["tempus"] = 0,
		["tempestas"] = 0,
		["tenebrae"] = 0,
		["terra"] = 0,
		["tutamen"] = 0,
		["vacuos"] = 0,
		["venenum"] = 0,
		["victus"] = 0,
		["vinculum"] = 0,
		["vitium"] = 0,
		["vitreus"] = 0,
		["volatus"] = 0
	}

	return essentias
end

m.clear()
m.setCursorPos(1, 1)
term.redirect(m)
print("Analyzing chests, watch the computer for more information.")
print("")
print("The monitor will refresh once chest analysis is finished.")
term.restore()
essentia = essentiaList()
setAutoDetectVariables()

disableRefill()
findChestsAndAnalyzers()
matchChestsWithAnalyzer()
analyzeChestsContents()
enableRefill()

print("")
print("Chest analysis finished. Showing interface.")
sleep(1)


refresh()

while (run) do
	-- uses os.pullevent so this isn't triggered constantly
	getClick()
end
