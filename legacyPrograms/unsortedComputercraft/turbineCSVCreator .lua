local fileName = "spoolDown.csv"
local dataPerSecond = 5

local function writeToFile(time, speed, rf)
	local fp = fs.open(fileName, "a")
	fp.writeLine(time .. "," .. speed .. "," .. rf)
	fp.close()
end

local fp = fs.open(fileName, "w")
if fp == nil then
	print("could not open file")
	return
end
fp.writeLine("time,speed,rf/t")
fp.close()

local p = peripheral.wrap("back")
p.setInductorEngaged(true)
p.setActive(true)

os.startTimer(1 / dataPerSecond)
local running = true
local i = 0
while running do
	local event = os.pullEvent()
	if event == "redstone" then
		running = false
	elseif event == "timer" then
		os.startTimer(1 / dataPerSecond)
		i = (1 / dataPerSecond) + i
		local speed = p.getRotorSpeed()
		local rf = p.getEnergyProducedLastTick()
		writeToFile(i, speed, rf)
	end
end
