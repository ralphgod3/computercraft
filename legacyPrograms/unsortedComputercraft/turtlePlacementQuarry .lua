local tArgs = { ... }

-- change this to something more decent default is enough to mine from y = 80 to bedrock with enderchests
--  y65 ish to bedrock without enderchests
-- 0.72 coal per 3 layers of mining use multiples of 3 when starting
local fuel = 18

local m = peripheral.getNames()
local wireles = false

for i = 1, #m do
	if peripheral.getType(m[i]) == "modem" then
		rednet.open(m[i])
		local p = peripheral.wrap(m[i])
		wireles = true
	end
end

if not tArgs[1] then
	print("usage")
	print("slot 1 turtles")
	print("slot 2 disk drive")
	print("slot 3 disk with the programs")
	print("slot 4 strongbox or enderchest with fuel")
	print("slot 5 optional enderchests for dropping stuff in")
	print("<length> <side> <master or slave>")
	error()
end

if not tArgs[2] then
	tArgs[2] = "left"
end

if not tArgs[3] then
	tArgs[3] = "master"
end

if tArgs[2] ~= "right" and tArgs[2] ~= "left" or tArgs[3] ~= "master" and tArgs[3] ~= "slave" then
	error("enter correct values please")
end




if wireles == true then
	if tArgs[3] == "master" then
		rednet.broadcast("start placing")
		print("sending start message")
	elseif tArgs[3] == "slave" then
		while true do
			local x, y, z = rednet.receive()
			if y == "start placing" then
				break
			end
		end
	end
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
end

local function turn(side)
	if side == "l" then
		turtle.turnRight()
	elseif side == "r" then
		turtle.turnLeft()
	end
end

local function place()
	if tArgs[2] == "right" then
		turn("l")
	elseif tArgs[2] == "left" then
		turn("r")
	end
	turtle.select(1)
	turtle.place()
	up()
	turtle.select(2)
	turtle.place()
	turtle.select(3)
	turtle.drop()
	down()
	turtle.select(4)
	turtle.placeUp()
	turtle.suckUp(fuel)
	turtle.drop()
	turtle.digUp()
	turtle.select(5)
	turtle.drop(1)
	peripheral.call("front", "turnOn")
	turtle.select(3)
	up()
	turtle.suck()
	turtle.select(2)
	turtle.dig()
	down()
	if tArgs[2] == "right" then
		turn("r")
	elseif tArgs[2] == "left" then
		turn("l")
	end
end

if tArgs[2] == "left" then
	turn("l")
elseif tArgs[2] == "right" then
	turn("r")
end
for i = 1, tArgs[1] - 1 do
	place()
	forward()
end
place()
if tArgs[2] == "right" then
	turn("l")
elseif tArgs[2] == "left" then
	turn("r")
end
