---@diagnostic disable: duplicate-set-field

function os.pullEvent()
	local event, p1, p2, p3, p4, p5 = os.pullEventRaw()
	if event == "terminate" then
		os.reboot() -- Remove this for do not reboot on Ctrl+T
	end
	return event, p1, p2, p3, p4, p5
end

local function getPass(checkpass)
	local pass = ""

	term.write("Please enter the password: ")
	pass = read("*")
	if pass ~= checkpass then
		print("pass is invalid")
		sleep(0.5)
		os.reboot()
	else
		print("correct")
	end
end
getPass("invalid")
