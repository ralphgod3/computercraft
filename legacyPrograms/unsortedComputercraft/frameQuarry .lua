local headUp = {1,4,16,64}
local headDown = {2,8,32,128}
local dig = {256,512,1024,2048}

local function pulseUp(head)
  rs.setBundledOutput("back",headUp[head])
  sleep(0.6)
  rs.setBundledOutput("back",0)
  sleep(0.4)
end

local function pulseDown(head)
  rs.setBundledOutput("back",headDown[head]+dig[head])
  sleep(0.6)
  rs.setBundledOutput("back",0)
  sleep(0.4)
end
 
 ;local function pulseBoth(head1,head2)
  rs.setBundledOutput("back",headUp[head1])
  rs.setBundledOutput("back",headDown[head2])
  sleep(0.6)
  rs.setBundledOutput("back",0)
  sleep(0.4)
end




for _ = 1,100 do
  pulseDown(1)
end

for _ = 1,100 do
  pulseBoth(1,2)
end
for _ = 1,100 do
  pulseBoth(2,3)
end

for _ =1,100 do
  pulseBoth(3,4)
end

for _ = 1,100 do
  pulseUp(4)
end