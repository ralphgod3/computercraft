---@diagnostic disable: redundant-parameter
-- wrapping modem
local m = peripheral.wrap("top")

-- variables
local send = "a"
local reply = "b"
local message = "c"

-- code
while true do
	os.pullEvent("redstone")
	m.transmit(send, reply, message)
	print("ping")
end
