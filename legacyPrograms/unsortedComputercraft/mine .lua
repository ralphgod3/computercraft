---@diagnostic disable: undefined-global
-- defining variables

-- this variable can be changed by user to determine length of quarry

local length = 48

-- these variables are for the program and may NOT be changed it will cause problems


local bedrock = { "BuildCraft:Core:eternalSpring", "minecraft:bedrock", "factorization:FracturedBedrock" }
local coords = { f = 0, x = 0, y = 0, z = 0 }
local coordsSave = { f = 0, x = 0, y = 0, z = 0 }
local finish = 0
local full = 1
local data = {}
local enderchest = false
-- defining functions

local function turn(side, ammount)
	for i = 1, ammount do
		if side == "r" then
			turtle.turnRight()
			coords.f = coords.f + 1
		elseif side == "l" then
			turtle.turnLeft()
			coords.f = coords.f - 1
		end
		if coords.f > 3 then
			coords.f = 0
		elseif coords.f < 0 then
			coords.f = 3
		end
	end
end

local function forward()
	while not turtle.forward() do
		local succes, data = turtle.inspect()
		if succes then
			for p = 1, #bedrock do
				if data.name == bedrock[p] then
					finish = finish + 1
					if finish == 10 then
						drop()
					end
				end
			end
		end
		if not turtle.dig() then
			turtle.attack()
		end
	end
	if coords.f == 0 then
		coords.z = coords.z + 1
	elseif coords.f == 1 then
		coords.x = coords.x + 1
	elseif coords.f == 2 then
		coords.z = coords.z - 1
	elseif coords.f == 3 then
		coords.x = coords.x - 1
	end
end

local function down(ammount)
	for i = 1, ammount do
		while not turtle.down() do
			local succes, data = turtle.inspectDown()
			if succes then
				for p = 1, #bedrock do
					if data.name == bedrock[p] then
						finish = finish + 1
						if finish == 10 then
							drop()
						end
					end
				end
			end
			if not turtle.digDown() then
				turtle.attackDown()
			end
		end
		finish = 0
		coords.y = coords.y + 1
	end
end

local function up(ammount)
	for i = 1, ammount do
		while not turtle.up() do
			if not turtle.digUp() then
				turtle.attackUp()
			end
		end
		coords.y = coords.y - 1
	end
end

local function drop()
	if enderchest then
		turtle.select(1)
		while not turtle.placeUp() do
			turtle.digUp()
			sleep(1)
		end
		for i = 1, 16 do
			turtle.select(i)
			turtle.dropUp()
			while turtle.getItemCount(i) ~= 0 do
				sleep(1)
				turtle.drop()
			end
		end
		turtle.select(1)
		turtle.digUp()
		if finish == 10 then
			up(coords.y)
			while coords.f ~= 2 do
				turn("l", 1)
			end
			for i = 1, coords.z do
				forward()
			end
			error("done mining")
		end
	else
		coordsSave.f = coords.f
		coordsSave.x = coords.x
		coordsSave.y = coords.y
		coordsSave.z = coords.z
		up(coords.y)
		while coords.f ~= 2 do
			turn("l", 1)
		end
		for i = 1, coords.z do
			forward()
		end
		for i = 1, 16 do
			turtle.select(i)
			turtle.drop()
		end
		turtle.select(1)
		if finish == 10 then
			error("done mining")
		end
		while coords.f ~= 0 do
			turn("l", 1)
		end
		while coordsSave.z > coords.z do
			forward()
		end
		while coordsSave.y > coords.y do
			down(1)
		end
		while coordsSave.f ~= coords.f do
			turn("l", 1)
		end
	end
end

local function check()
	while turtle.getItemCount(full) ~= 0 do
		full = full + 1
		if full == 16 then
			full = 1
			drop()
		end
	end
end

local function quarry()
	for i = 1, length - 1 do
		turtle.digUp()
		turtle.digDown()
		turtle.dig()
		forward()
		check()
	end
	turtle.digUp()
	turtle.digDown()
	turn("l", 2)
	down(3)
	for i = 1, length - 1 do
		turtle.digUp()
		turtle.digDown()
		turtle.dig()
		forward()
		check()
	end
	turtle.digUp()
	turtle.digDown()
	turn("l", 2)
	down(3)
end

-- code which will call functions

for f = 1, 16 do
	turtle.select(f)
	turtle.refuel()
	data = turtle.getItemDetail(f)
	if data then
		if data.name == "EnderStorage:enderChest" then
			if f ~= 1 then
				while turtle.getItemCount(1) ~= 0 do
					for i = 1, 16 do
						turtle.select(1)
						turtle.transferTo(i)
					end
				end
				turtle.select(f)
				turtle.transferTo(1)
			end
			enderchest = true
		end
	end
end


turtle.select(1)
for i = 1, 40 do
	quarry()
end
