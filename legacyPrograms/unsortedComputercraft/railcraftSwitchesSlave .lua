shell.run("id")
rednet.open("top")

local queue = {}
local side = "right"
local masterId = 140
rs.setOutput("back",true)

while true do
	local event,p1,p2,sender,msg = os.pullEvent()
	if event == "redstone" then
		if rs.getInput(side) then
			print("redstone incoming")
			if queue[1] == 1 then
				rs.setOutput("back",false)
			else
				rs.setOutput("back",true)
			end
			sleep(1)
			table.remove(queue,1)
		end
	elseif event == "modem_message" and sender == masterId then
		print("message received")
		queue[#queue +1] = msg.message
	end
end