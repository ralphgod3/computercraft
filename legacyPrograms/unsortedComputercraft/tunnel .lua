--variable for commandline argument
local tArgs = {...}

--functions
-- redefining basic movement functions to make it not be able to get stuck
local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
    end
	end
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

--drops the entire inventory infront of the turtle or a chest infront of it
local function drop()
	for i=1,16 do
		turtle.select(i)
		turtle.drop()
	end
	turtle.select(1)
end

--digs above and below the turtle
local function digAboveAndBelow()
	turtle.digUp()
	turtle.digDown()
end

--mines a distance
local function mine(distance)
	for i = 1, distance do
		digAboveAndBelow()
		forward()
		print(i)
	end
end

--turns to a side 180 degrees moving 1 block over accepts a "enum" or 0 and 1 which is left and right respectively
--crappy hack for enums since lua does not support them
local left, right = 0, 1
local function turn(side)
	if side == left then
		turtle.turnLeft()
		forward()
		turtle.turnLeft()
	elseif side == right then
		turtle.turnRight()
		forward()
		turtle.turnRight()
	end
end

----------------------------------------------
---------------------MAIN---------------------
----------------------------------------------
if not tArgs[1] then
  tArgs[1] = 48
end

print("i need some fuel")

for i=1,16 do
	turtle.select(i)
	turtle.refuel(64)
end

up()
--start of 1st line
mine(tArgs[1])
digAboveAndBelow()
--end of 1st line
turn(right)

--start 2nd line
mine(tArgs[1])
digAboveAndBelow()
drop()
--end of 2nd line
turn(left)

--start 3d line
mine(tArgs[1])
digAboveAndBelow()
--end of 3d line
turn(right)

--start of 4th line
mine(tArgs[1])
digAboveAndBelow()
drop()
--end of 4th line

print("im done master")