local tArgs = { ... }
local slot = 1
-- checking input from user and fuel
if not tArgs[1] then
	print("u have to input a length")
	return
end

if turtle.getFuelLevel() < 200 then
	print("i need some fuel")
	return
end
-- redefining basic functions to make it not be able to get stuck
local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end
-- will check the ammount of items in the slot before trying to place wil switch if neccecary
local function placeDown()
	while turtle.getItemCount(slot) < 1 do
		slot = slot + 1
		if slot == 17 then
			slot = 1
		end
		turtle.select(slot)
	end
	turtle.placeDown()
end

local function placeUp()
	while turtle.getItemCount(slot) < 1 do
		slot = slot + 1
		if slot == 17 then
			slot = 1
		end
		turtle.select(slot)
	end
	turtle.placeUp()
end

-- line function makes it make a line
local function lineSide()
	for i = 1, tArgs[1] do
		placeDown()
		placeUp()
		forward()
	end
	placeDown()
	placeUp()
end

local function line()
	for i = 1, tArgs[1] do
		placeDown()
		forward()
	end
	placeDown()
end

turtle.select(1)
lineSide()
turtle.turnRight()
forward()
down()
turtle.turnRight()
line()
turtle.turnLeft()
forward()
turtle.turnLeft()
line()
turtle.turnRight()
forward()
turtle.turnRight()
line()
turtle.turnLeft()
up()
forward()
turtle.turnLeft()
lineSide()

turtle.turnLeft()
for i = 1, 2 do
	forward()
end
turtle.turnLeft()
down()

for i = 1, tArgs[1] do
	forward()
end
print("im done master")
