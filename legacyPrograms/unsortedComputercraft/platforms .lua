local slot = 1

local function forward()
	while not turtle.forward() do
		turtle.dig()
		sleep(0.5)
		turtle.attack()
	end
end

local function up()
	while not turtle.up() do
		turtle.digUp()
		sleep(0.5)
		turtle.attack()
	end
end

local function selectMaterial()
	while turtle.getItemCount(slot) < 1 do
		slot = slot + 1
		if slot == 17 then
			slot = 1
		end
		turtle.select(slot)
	end
end

local function buildLine(n)
	for i = 1, n do
		selectMaterial()
		turtle.placeDown()
		forward()
	end
	while not turtle.back() do
		sleep(1)
	end
end

local function buildSquareFilled(n)
	for i = 1, n do
		buildLine(n)
		if (i == n) then
			break
		end
		if ((i % 2) == 1) then
			turtle.turnRight()
			forward()
			turtle.turnRight()
		else
			turtle.turnLeft()
			forward()
			turtle.turnLeft()
		end
	end
	if ((n % 2) == 1) then
		for i = 1, 2 do
			turtle.turnLeft()
			for j = 1, (n - 1) do
				forward()
			end
		end
		turtle.turnLeft()
		turtle.turnLeft()
	else
		turtle.turnRight()
		for i = 1, (n - 1) do
			forward()
		end
		turtle.turnRight()
	end
end

local function buildSquare(n)
	for i = 1, 4 do
		buildLine(n)
		turtle.turnRight()
	end
end

local function buildSquareCorners(n)
	for i = 1, 4 do
		selectMaterial()
		turtle.placeDown()
		for j = 1, (n - 1) do
			forward()
		end
		turtle.turnRight()
	end
end





local function moveToBuildingPosition(n)
	if ((n % 2) == 1) then
		local steps = (n / 2)
		turtle.turnLeft()
		for i = 1, steps do
			forward()
		end
		turtle.turnRight()
	else
		local steps = ((n / 2) - 1)
		turtle.turnLeft()
		for i = 1, steps do
			forward()
		end
		turtle.turnRight()
	end
end

local function moveToStartingPosition(n)
	if ((n % 2) == 1) then
		local steps = (n / 2)
		turtle.turnRight()
		for i = 1, steps do
			forward()
		end
		turtle.turnLeft()
	else
		local steps = ((n / 2) - 1)
		turtle.turnRight()
		for i = 1, steps do
			forward()
		end
		turtle.turnLeft()
	end
end

local tArgs = { ... }
if #tArgs == 0 then
	print("Usage: Platform <size>")
	return
end
local size = tonumber(tArgs[1])
turtle.select(1)
forward()
moveToBuildingPosition(size)
buildSquareFilled(size)
up()
buildSquare(size)
up()
buildSquareCorners(size)
up()
buildSquare(size)
moveToStartingPosition(size)
