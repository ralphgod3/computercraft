---@diagnostic disable: redundant-parameter
os.loadAPI("ocs/apis/sensor")
local prox = sensor.wrap("right")

term.clear()
term.setCursorPos(1, 1)
while true do
	local event, param1 = os.pullEvent("redstone")
	print("scanning")
	local targets = prox.getTargets()
	for name, basicDetails in pairs(targets) do
		for key, value in pairs(basicDetails) do
			if (tostring(key) == "Name") and (tostring(value) == "Player") then
				if name ~= "ralphgod3" then
					local logs = fs.open("logs", "a")
					logs.writeLine(os.time())
					logs.writeLine(os.day())
					logs.writeLine(name)
					logs.close()
				end
			end
		end
	end
	sleep(60)
end
