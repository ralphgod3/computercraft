rednet.open("right")
local distance = 0
debug = false
term.write("how far? ")
distance = read()
for i = 1, 5 do
	rednet.broadcast(distance)
	sleep(1)
end

local function forward()
	sleep(18)
	while not turtle.forward() do
		turtle.attack()
		sleep(2)
	end
end

local function checkFuel()
	if turtle.getFuelLevel() < 200 then
		if debug then print("diggingDown") end
		turtle.digDown()
		if debug then print("selecting 1") end
		turtle.select(1)
		if debug then print("placingDown") end
		turtle.placeDown()
		if debug then print("suckingDown") end
		turtle.suckDown()
		if debug then print(turtle.getItemCount(1)) end
		while turtle.getItemCount(1) < 8 do
			turtle.suckDown()
			sleep(1)
		end
		while turtle.getItemCount(1) > 8 do
			turtle.dropDown(turtle.getItemCount(1) - 8)
		end
		if debug then print(turtle.getItemCount(1)) end
		turtle.refuel()
		turtle.digDown()
		if debug then print(turtle.getFuelLevel()) end
	end
end

for i = 1, distance do
	checkFuel()
	forward()
end
os.reboot()
