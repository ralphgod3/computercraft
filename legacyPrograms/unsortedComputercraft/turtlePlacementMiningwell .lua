local tArgs = { ... }

if tArgs[1] == "help" or not tArgs[1] then
	print("usage")
	print("slot 1 turtle")
	print("slot 2 disk drive")
	print("slot 3 program disk")
	print("slot 4 fuelchest")
	print("slot 5 dropchest")
	print("slot 6 miningwells")
	print("slot 7 strongbox with tesseracts")
end

local function up()
	while not turtle.up() do
		if not turtle.digUp() then
			turtle.attackUp()
		end
	end
end

local function down()
	while not turtle.down() do
		if not turtle.digDown() then
			turtle.attackDown()
		end
	end
end

local function forward()
	while not turtle.forward() do
		if not turtle.dig() then
			turtle.attack()
		end
	end
end


local function place()
	down()
	down()
	turtle.turnLeft()
	turtle.select(1) -- placing turtle
	turtle.place()
	up()
	turtle.select(2) -- placing disk drive and disk in
	turtle.place()
	turtle.select(3)
	turtle.drop()
	down()
	turtle.select(4) --fuel
	turtle.drop(1)
	turtle.select(5) -- drop
	turtle.drop(1)
	turtle.select(6) -- miningwell
	turtle.drop(1)
	turtle.select(7)
	turtle.placeUp()
	turtle.suckUp(1)
	turtle.drop(1)
	turtle.digUp()
	peripheral.call("front", "turnOn")
	turtle.select(3)
	up()
	turtle.suck()
	turtle.select(2)
	turtle.dig()
	down()
	turtle.turnRight()
	forward()
	turtle.turnLeft()
	turtle.up()
	turtle.up()
	turtle.select(1)
	turtle.place()
	up()
	turtle.select(2)
	turtle.place()
	turtle.select(3)
	turtle.drop()
	down()
	turtle.select(4)
	turtle.drop(1)
	turtle.select(5)
	turtle.drop(1)
	turtle.select(6)
	turtle.drop(1)
	turtle.select(7)
	turtle.placeUp()
	turtle.suckUp(1)
	turtle.drop(1)
	turtle.digUp()
	peripheral.call("front", "turnOn")
	turtle.select(3)
	up()
	turtle.suck()
	turtle.select(2)
	turtle.dig()
	down()
	turtle.turnRight()
end

turtle.turnRight()
for i = 1, tArgs[1] - 1 / 2 do
	place()
	forward()
end
place()
turtle.turnLeft()
