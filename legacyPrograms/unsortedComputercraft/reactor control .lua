local con = {
	peripheral.wrap("nuclear_reactor_1")
	, peripheral.wrap("nuclear_reactor_0")
, peripheral.wrap("nuclear_reactor_2")
, peripheral.wrap("nuclear_reactor_3")
, peripheral.wrap("nuclear_reactor_4")
, peripheral.wrap("nuclear_reactor_5")
}

local output = { 1, 2, 4, 8, 16, 32 }



while true do
	if rs.getInput("left") == false then
		sleep(5)
		rs.setBundledOutput("right", 0)
	else
		for i = 1, 6 do
			if con[i].getHeat() < 6000 then
				if con[i].isActive() == false then
					print(rs.getBundledInput("right"))
					local active = rs.getBundledInput("right")
					rs.setBundledOutput("right", active + output[i])
					print("turning on")
					print(output[i])
				end
			elseif con[i].getHeat() > 6000 then
				if con[i].isActive() then
					local active = rs.getBundledInput("right")
					rs.setBundledOutput("right", active - output[i])
					print("turning off")
					print(output[i])
				end
			end
		end
		sleep(2)
	end
end
