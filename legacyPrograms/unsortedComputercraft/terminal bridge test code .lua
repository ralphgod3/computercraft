local p = peripheral.wrap("right")

local color = 0xFFFFFF
local quarry1 = "off"
local quarry2 = "off"
local power1 = "off"
local power2 = "off"
local reactor = "off"

local function reset()
	p.clear()
	p.addText(5, 5, "quarry1: ", color)
	p.addText(5, 15, "quarry2: ", color)
	p.addText(5, 25, "power1: ", color)
	p.addText(5, 35, "power2: ", color)
	p.addText(5, 45, "reactor: ", color)
end

reset()

--[[
function state(x,y,state)
  reset()
  if state == "off" then
    p.addText(x,y,state,0xFF0000)
  elseif state == "on" then
    p.addText(x,y,state,0x00FF00)
  end
end

state(60,5,"off")
sleep(1)
state(60,5,"on")
]]
   --
