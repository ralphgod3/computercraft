--Player interface Peripherals++

--example code:

local sensor= peripheral.wrap("right")
local player = sensor.gePlayerInv("ralphgod3")

player.getStackInSlot(4) --returns data from slot 4 in table

player.pushToSlot(4,0,2) -- should push to player slot 4 (is broken) pulls from slot 0 from host inventory, pushes 2 items.