os.loadAPI("ocs/apis/sensor")
local prox = sensor.wrap("right")
local allowed = false
local detected = false
local names = "test"
local whitelist = {"ralphgod3","DutchDonut","shinytouch","Raimbur","Dwarf_Tasael"}
while true do
term.clear()
term.setCursorPos(1,1)
  local targets = prox.getTargets()
  for name,basicDetails in pairs(targets) do
    for key,value in pairs(basicDetails) do
      if basicDetails.Name == "Player" then
        detected = true
        for i = 1,#whitelist do
          if name == whitelist[i] then
            allowed = true
          end
        end
      end
    end
  end
  rs.setBundledOutput("left",0)
  if detected then
    if allowed then
      rs.setBundledOutput("left",1)
      print("allowed")
    else
      rs.setBundledOutput("left",2)
      sleep(5)
      rs.setBundledOutput("left",0)
      print("killing")
    end
  end
  sleep(0.5)
  allowed = false
  detected = false
end