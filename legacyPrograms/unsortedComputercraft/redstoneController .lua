local receive = 1
local send = 2

-- directions for cables
local check = "back"
local set = "right"
-- names you want to display too the monitor
local names = { "ae", "treefarm", "spawners" }
-- true or false
local outputs = {}
--variables which shouldnt be changed

local tableOutput = {}
local online = 0
local m = peripheral.find("modem")
m.open(receive)
m.open(send)

local function test(color)
	online = rs.getBundledInput(check)
	if color then
		if colors.test(online, color) then
			return true
		else
			return false
		end
	end
	return online
end

local function switch(status, color)
	if test(color) ~= status then
		rs.setBundledOutput(set, color)
		sleep(0.1)
		rs.setBundledOutput(set, 0)
	end
end

local function refresh()
	for i = 1, 16 do
		if rs.testBundledInput(check, 2 ^ (i - 1)) then
			outputs[i] = true
		else
			outputs[i] = false
		end
	end
	table.insert(tableOutput, 1, names)
	table.insert(tableOutput, 2, outputs)
	m.transmit(send, receive, tableOutput)
end

refresh()
