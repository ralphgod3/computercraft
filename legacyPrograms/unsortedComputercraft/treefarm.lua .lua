-- type of tree sapling in farm
local saplingNames = {"minecraft:sapling"}

local logNames = {"minecraft:log"}

--local fuelNames = { "minecraft:planks", "gregtech:log", "minecraft:log",  "primal_tech:charcoal_block", "minecraft:fence", "gregtech:meta_item_1", "minecraft:coal"}
local noDig = {"minecraft:cobblestone", "minecraft:dirt", "minecraft:grass", "minecraft:glass", "minecraft:stonebrick", "minecraft:sand" }

local minFuel = 2000
local harvestInterval = 5
local watchdogelimit = 20

--function redefinitions
local pos = {
    x = 0,
    y = 0,
    z = 0,
    f = 0
}

--hardcoded chestposition
local chestPos = {
    x = 0,
    y = 0,
    z = 0,
    f = 2
}

--hardcoded home
local saplingChestPos = {
    x = 0,
    y = 0,
    z = 0,
    f = 3
}

local homePos = {
    x = 0,
    y = 0,
    z = 0,
    f = 0
}

local function elementIsInTable(table, elementToFind)
    for _, v in pairs(table) do
        if v == elementToFind then return true end
    end

    return false
end

local function goForward(destroy, blocksToNotDig)
    print("x " .. pos.x .. " y " .. pos.y .. " z " .. pos.z .. " f " .. pos.f)

    while not turtle.forward() do
        if destroy == false then return false end
        local suc, ret = turtle.inspect()

        for i = 1, #blocksToNotDig do
            if suc and ret.name == blocksToNotDig[i] then return false end
        end

        if not turtle.dig() then
            turtle.attack()
        end
    end

    if pos.f == 0 then
        pos.x = pos.x + 1
    elseif pos.f == 1 then
        pos.z = pos.z - 1
    elseif pos.f == 2 then
        pos.x = pos.x - 1
    elseif pos.f == 3 then
        pos.z = pos.z + 1
    end
    return true
end

local function goUp(destroy, blocksToNotDig)
    while not turtle.up() do
        if destroy == false then return false end
        local suc, ret = turtle.inspectUp()

        for i = 1, #blocksToNotDig do
            if suc and ret.name == blocksToNotDig[i] then return false end
        end

        if not turtle.digUp() then
            turtle.attackUp()
        end
    end

    pos.y = pos.y + 1

    return true
end

local function goDown(destroy, blocksToNotDig)
    while not turtle.down() do
        if destroy == false then return false end
        --check for blocks we should not dig up
        local suc, ret = turtle.inspectDown()

        for i = 1, #blocksToNotDig do
            if suc and ret.name == blocksToNotDig[i] then return false end
        end

        if not turtle.digDown() then
            turtle.attackDown()
        end
    end

    pos.y = pos.y - 1

    return true
end

local function goLeft()
    turtle.turnLeft()
    pos.f = pos.f - 1

    if pos.f < 0 then
        pos.f = 3
    end
end

local function goRight()
    turtle.turnRight()
    pos.f = pos.f + 1

    if pos.f > 3 then
        pos.f = 0
    end
end

local function goTo(position, destroy)
    while pos.x > position.x do
        while pos.f ~= 2 do
            goRight()
        end

        goForward(destroy, noDig)
    end

    while pos.x < position.x do
        while pos.f ~= 0 do
            goRight()
        end

        goForward(destroy, noDig)
    end

    while pos.z > position.z do
        while pos.f ~= 1 do
            goRight()
        end

        goForward(destroy, noDig)
    end

    while pos.z < position.z do
        while pos.f ~= 3 do
            goRight()
        end

        goForward(destroy, noDig)
    end

    while pos.y < position.y do
        goUp(destroy, noDig)
    end

    while pos.y > position.y do
        goDown(destroy, noDig)
    end

    while position.f ~= pos.f do
        goRight()
    end
end

local function getSapplingsForPlanting()
    turtle.select(1)
    turtle.suck()

    while turtle.getItemCount() < 64 do
        print("waiting for more sapplings in chest")
        turtle.suck()
        sleep(10)
    end

    for i = 2, 16 do
        turtle.select(i)
        turtle.drop()
    end
end

local function DropAllItemsFromCategory(category)
    for i = 1, 16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()

        if ret and elementIsInTable(category, ret.name) then
            while turtle.getItemCount() > 0 do
                if not turtle.drop() then
                    print("waiting for inventory space to drop item")
                    sleep(5)
                end
            end
        end
    end
end

local function dropAll()
    for i = 1, 16 do
        turtle.select(i)
        while turtle.getItemCount() > 0 do
            if not turtle.drop() then
                print("waiting for inventory space to drop item")
                sleep(5)
            end
        end
    end
end

local function dropStuff()
    local oldPos = {}
    oldPos.x = pos.x
    oldPos.y = pos.y
    oldPos.z = pos.z
    oldPos.f = pos.f
    goTo(saplingChestPos,true)
    DropAllItemsFromCategory(saplingNames)
    goTo(chestPos, true)
    dropAll()
    goTo(saplingChestPos,true)
    getSapplingsForPlanting()
    goTo(oldPos, true)
end

local curSlotNum = 1

local function inventoryIsFull()
    local iter = 1

    while turtle.getItemCount(curSlotNum) > 0 do
        curSlotNum = curSlotNum + 1

        if curSlotNum > 16 then
            curSlotNum = 1
            iter = iter + 1
            if iter > 4 then return true end
        end
    end

    return false
end

local function replantTree()
    for i = 1, 16 do
        local ret = turtle.getItemDetail(i)

        if ret and elementIsInTable(saplingNames, ret.name) then
            turtle.select(i)
            turtle.placeDown()
            break
        end
    end
end

local function mineTree()
    local wentUp = 0
    turtle.digDown()
    local suc, ret = turtle.inspectUp()

    while suc and elementIsInTable(logNames, ret.name) do
        goUp(true, noDig)
        wentUp = wentUp + 1
        suc, ret = turtle.inspectUp()

        if inventoryIsFull() then
            dropStuff()
        end
    end

    for i = 1, wentUp do
        goDown(true, noDig)
    end
end

local function handleCorner(sideToRotate, goForwards)
    if sideToRotate ~= "left" and sideToRotate ~= "right" then
        print("invalid argument")

        return
    end

    if sideToRotate == "left" then
        goLeft()
    elseif sideToRotate == "right" then
        goRight()
    end

    if goForwards == true then
        goForward(true, noDig)
        mineTree()

        if not turtle.detectDown() then
            replantTree()
        end
    end

    if sideToRotate == "left" then
        goLeft()
    elseif sideToRotate == "right" then
        goRight()
    end
end

local function mineLine(x)
    for i = 1, x do
        if i ~= x - 1 then
            goForward(true, noDig)
            turtle.suckDown()
            local suc, ret = turtle.inspectUp()

            if suc and elementIsInTable(logNames, ret.name) then
                mineTree()
                replantTree()
            end

            if not turtle.detectDown() then
                replantTree()
            end
        end

        if inventoryIsFull() then
            dropStuff()
        end
    end
end

local function runFarm(x, z, direction)
    local otherDirection = ""

    if direction == "left" then
        otherDirection = "right"
    elseif direction == "right" then
        otherDirection = "left"
    end

    for i = 1, x do
        mineLine(z)

        if i ~= x then
            if i % 2 == 0 then
                handleCorner(otherDirection, true)
            else
                handleCorner(direction, true)
            end
        else
            if i % 2 == 0 then
                handleCorner(otherDirection, false)
            else
                handleCorner(direction, false)
            end
        end
    end
end

local function checkParked()
    if rs.getInput("right") then return true end

    return false
end

local function findRoof()
    local watchdoge = 0

    while goUp(true, noDig) do
        if watchdoge > watchdogelimit then
            print("BORK BORK")
            break
        end

        watchdoge = watchdoge + 1
    end

    print("Bonked into roof")
end

local function findWall()
    local watchdoge = 0

    while goForward(true, noDig) do
        if watchdoge > watchdogelimit then
            print("BORK BORK")
            break
        end

        watchdoge = watchdoge + 1
    end

    print("Bonked into wall")
end

local function findCorner()
    findRoof()

    while true do
        if rs.getInput("left") then break end
        findWall()
        goRight()
    end

    print("found upper starting corner")
end

local function findHome()
    if checkParked() then return end
    findCorner()

    while not checkParked() do
        goDown(true, noDig)
    end
end

local function checkFarmSizeY()
    local watchdoge = 0
    local roofHeight = 0

    while goUp(true, noDig) do
        if watchdoge > watchdogelimit then
            print("BORK BORK")
            break
        end

        roofHeight = roofHeight + 1
        watchdoge = watchdoge + 1
    end

    print("Bonked into roof")

    return roofHeight
end

local function checkFarmSizeWall()
    local wallSize = 0
    local watchdoge = 0

    while goForward(true, noDig) do
        if watchdoge > watchdogelimit then
            print("BORK BORK")
            break
        end

        wallSize = wallSize + 1
        watchdoge = watchdoge + 1
    end

    print("Bonked into wall")

    return wallSize
end

local function FindFarmSize()
    checkFarmSizeY()
    checkFarmSizeWall()
    goRight()
    checkFarmSizeWall()
    goLeft()

    local ret = {
        x = 0,
        y = 0,
        z = 0
    }

    ret.x = math.abs(pos.z) + 1
    ret.y = math.abs(pos.y) + 1
    ret.z = math.abs(pos.x) + 1

    return ret
end

local function handleFuel()
    if turtle.getFuelLevel() > minFuel then return end
    print("refueling")

    for i = 1, 16 do
        turtle.select(i)
        local ret = turtle.getItemDetail()
        if ret ~= nil and not elementIsInTable(saplingNames, ret.name) then
            turtle.refuel(64)
        end
        if turtle.getFuelLevel() > minFuel then break end
    end
end

print("finding home")
findHome()
--reset the position to be home
pos.x = 0
pos.y = 0
pos.z = 0
pos.f = 0
--get the farm size
print("finding farm size")
local farmSize = FindFarmSize()

if farmSize.x == 0 or farmSize.y == 0 or farmSize.z == 0 then
    print("invalid farm dimensions, stopping turtle")
    return
end

goTo(homePos, true)
print("Dimensions: " .. farmSize.x .. ", " .. farmSize.y .. ", " .. farmSize.z)
print("returning home")

while true do
    print("starting farming loop")
    goUp(true, noDig)
    runFarm(farmSize.x, farmSize.z, "right")
    goTo(saplingChestPos, true)
    DropAllItemsFromCategory(saplingNames)
    handleFuel()
    goTo(chestPos, true)
    dropAll()
    goTo(saplingChestPos, true)
    getSapplingsForPlanting()
    goTo(homePos, true)
    sleep(harvestInterval)
end
