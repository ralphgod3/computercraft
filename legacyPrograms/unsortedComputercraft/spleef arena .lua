---@diagnostic disable: redundant-parameter
local m = peripheral.wrap("bottom")

local event, param1 = os.pullEvent("chat_command")
if param1 == "countdown" then
	m.say("handing out shovels")
	rs.setBundledOutput("left", 2)
	sleep(1)
	rs.setBundledOutput("left", 0)
	m.say("starting in")
	m.say("5")
	sleep(1)
	m.say("4")
	sleep(1)
	m.say("3")
	sleep(1)
	m.say("2")
	sleep(1)
	m.say("1")
	sleep(1)
	m.say("starting")
	rs.setOutput("back", true)
	sleep(1)
	rs.setOutput("back", false)
elseif param1 == "door" then
	rs.setBundledOutput("left", 4)
	sleep(10)
end

os.reboot()
