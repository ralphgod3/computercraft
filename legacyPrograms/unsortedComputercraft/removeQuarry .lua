local tArgs = {...}

if not tArgs or not tArgs[1] or not tArgs[2] then
    print("usage: <side> <length>")
    error()
end

for i = 1,3 do
    turtle.down()
end
turtle.suck()
turtle.dig()
turtle.forward()

if tArgs[1] == "right" then
    turtle.turnRight()
elseif tArgs[1] == "left" then
    turtle.turnLeft()    
else
    print("invalid input")
    error()
end

for i = 1,tArgs[2] do
    turtle.suck()
    turtle.dig()
    turtle.forward()
end