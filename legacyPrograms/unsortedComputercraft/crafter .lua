--written by ralphgod
--setup:
--front a chest for input
--above temp storage
--below perm storage

local recipes = {}
local match = {}
local accesible = {}

--filling match with alot of 0's
for i = 1, 500 do
	match[i] = 0
end

--define all recipes like this slot 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
--keep in mind that for crafting only these slots will be used in this pattern so fill the rest with nil
--  1,2,3
--  5,6,7
--  9,10,11
--to find the local name of an item go into lua and then turtle.getItemDetail(slot).name


recipes[1] = {
	"minecraft:wheat", "minecraft:wheat", "minecraft:wheat"
}

recipes[2] = {
	"minecraft:wheat", "minecraft:dye", "minecraft:wheat"
}

recipes[3] = {
	"harvestcraft:cuttingboardItem", "minecraft:apple", nil, nil,
	"minecraft:apple", "harvestcraft:mixingbowlItem" }

recipes[4] = {
	"harvestcraft:skilletItem", "minecraft:egg" }

recipes[5] = {
	"harvestcraft:juicerItem", "minecraft:apple" }

recipes[6] = {
	"harvestcraft:saucepanItem", "harvestcraft:heavycreamItem", nil, nil, "harvestcraft:saltItem" }

recipes[7] = {
	"harvestcraft:skilletItem", "harvestcraft:onionItem", nil, nil, "minecraft:potato", "harvestcraft:butterItem" }

recipes[8] = {
	"harvestcraft:firmtofuItem", "harvestcraft:friedeggItem", nil, nil, "harvestcraft:toastItem",
	"harvestcraft:potatocakesItem", nil, nil, "harvestcraft:applejuiceItem" }



local function craft(thing)
	while turtle.suckUp() do
		local info = turtle.getItemDetail(16)
		for i = 1, 16 do
			if recipes[thing][i] ~= nil then
				for i = 1, 16 do
					if recipes[thing][i] == info.name then
						turtle.transferTo(i, 1)
					end
				end
				turtle.drop()
			else
				turtle.drop()
			end
		end
		turtle.drop()
	end
	turtle.craft()
	turtle.dropDown()
	for i = 1, 16 do
		turtle.select(i)
		turtle.drop()
		while turtle.suckUp() do
			turtle.drop()
		end
	end
end


--general code that runs the thing
turtle.select(16)
local count = 0
while turtle.suck() do
	count = count + 1
	accesible[count] = turtle.getItemDetail(16).name
	turtle.dropUp()
end

for i = 1, #accesible do
	print(accesible[i])
end

for i = 1, #accesible do                --ammount of ingredients to use
	for p = 1, #recipes do              -- ammount of recipes to search through
		for k = 1, table.getn(recipes[p]) do --length of eeach recipe
			if recipes[p][k] == accesible[i] then
				match[p] = match[p] + 1
			end
		end
	end
end

for i = 1, #recipes do
	for p = 1, table.getn(recipes[i]) do
		if recipes[i][p] == nil then
			match[i] = match[i] + 1
		end
	end
	if match[i] == table.getn(recipes[i]) then
		craft(i)
		break -- breaks loop so it wont try to add a number to nil
	end
end
