-- Settings
local outputs = {"top", "bottom", "left", "right", "back"}

local switch = "front"
--times are in miliseconds
--time that the output will stay high each cycle, can not be higher than the cycleLength
local pulseLength = 200
--time that each cycle takes
local cycleLength = 1000
-- invert output logic (makes the pulse output 0 instead of 1)
local invertedOutputLogic = false


-- not user editable
local timerFormula = (cycleLength - pulseLength) / 1000
local pulseFormula = pulseLength / 1000

local function setOutputs(tableOfOutputSides, outputLevel)
    for _, v in pairs(tableOfOutputSides) do
        if not invertedOutputLogic then
            rs.setOutput(v, outputLevel)
        else
            rs.setOutput(v, not outputLevel)
        end
    end
end

local function handleTimer()
    setOutputs(outputs, true)
    os.sleep(pulseFormula)
    setOutputs(outputs, false)
    return os.startTimer(timerFormula)
end

local function handleRedstone(timerID, switchside)
    if rs.getInput(switchside) == true then
        if timerID ~= nil then
            os.cancelTimer(timerID)
            return nil
        end
    else
        return os.startTimer(timerFormula)
    end
end


--------------------------------------- main -------------------------------
local state = rs.getInput(switch)
local oldState = rs.getInput(switch)

local timerID = nil
if state == false then
  timerID = os.startTimer(timerFormula)
end

while true do
    local event = os.pullEvent()
    if event == "redstone" then
        state = rs.getInput(switch)
        if state ~= oldState then
            oldState = state
            timerID = handleRedstone(timerID, switch)
        end
    elseif event == "timer" then
        timerID = handleTimer()
    end
end