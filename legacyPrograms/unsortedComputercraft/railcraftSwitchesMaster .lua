---@diagnostic disable: redundant-parameter
rednet.open("top")

local switches = { 283, 288, 290, 291, 292 }

local state = 0

while true do
	local os, event = os.pullEvent("redstone")
	if rs.getInput("right", true) then
		print(state)
		state = state + 1

		if state == #switches + 1 then
			state = 1
		end

		for i = 1, state - 1 do
			rednet.send(switches[i], 0)
		end
		rednet.send(switches[state], 1)
	end
end
