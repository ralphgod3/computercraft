local p = peripheral.wrap("left")
local devices = peripheral.getNames()
local turbines = {}

local start = 40
local stop = 90
local delay = 2
local percent = 0
local activate = false
local speed = 10

for i = 1, #devices do
	if peripheral.getType(devices[i]) == "BigReactors-Turbine" then
		table.insert(turbines, devices[i])
	end
end
print(#turbines)
sleep(1)
for i = 1, #turbines do
	local t = peripheral.wrap(turbines[i])
	t.setInductorEngaged(false)
end

local function power()
	percent = p.getEnergyStored() / p.getMaxEnergyStored()
	if percent < start / 100 then
		activate = true
	elseif percent > stop / 100 then
		activate = false
	end
end

local function standby()
	for i = 1, #turbines do
		local t = peripheral.wrap(turbines[i])
		if math.floor(t.getRotorSpeed()) < 1800 then
			t.setFluidFlowRateMax(2000)
		elseif math.floor(t.getRotorSpeed()) > 1840 then
			t.setFluidFlowRateMax(0)
		end
	end
end

local function switch()
	if activate then
		for i = 1, #turbines do
			local t = peripheral.wrap(turbines[i])
			if math.floor(t.getRotorSpeed()) > 1780 then
				t.setInductorEngaged(true)
			else
				t.setInductorEngaged(false)
			end
		end
	else
		for i = 1, #turbines do
			local t = peripheral.wrap(turbines[i])
			t.setInductorEngaged(false)
		end
	end
end

while true do
	power()
	standby()
	switch()
	term.clear()
	term.setCursorPos(1, 1)
	local p1 = percent * 100
	p1 = math.floor(p1)
	print(p1)
	for i = 1, #turbines do
		local t = peripheral.wrap(turbines[i])
		if t.getInductorEngaged() then
			print("active")
		else
			print("not active")
		end
	end
	sleep(delay)
end
