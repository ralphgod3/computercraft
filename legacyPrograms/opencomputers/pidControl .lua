local varipass = require("varipass")
local component = require("component")
local term = require("term")

--setpoint in rpm for the turbine
local setpoint = varipass.read("Nvtj7C2ZJGKeb91C","bE929FH3",varipass.TYPE_INT)
--time between web ui updates
--write
local timeBetweenUpdates = 2
--read
local timeBetweenUpdates2 = 10
--time between each loop in seconds
local dt = 0.1

-- find the turbine and wrap it as a proxy
local function findTurbine()
  local data = component.list()
  if not data then
    return false
  end
  for k,v in pairs(data) do
    if v == "bigreactors_turbine" then
      return component.proxy(k)
    end
  end
  return false
end

--counts to timeBetweenUpdates/dt to calculate at which loop to update web ui
local updateCounter = 0;
local updateCounter2 = 0;

-- pid stuff
--large turbine settings
--settings according to matlab kp = 193.8, ki = 4.067, kd = 231, outMin = 0, outMax = 2000
--small turbine settings < 400 rpm
--settings according to matlab kp = 11.09, ki = 8.794, kd = 0.35,outMin = 0, outMax = 223
--tall turbine settings
--settings according to matlab kp = 94, ki = 5.558, kd = 39.79,outMin = 0, outMax = 2000

local kp = 94
local ki = 5.558*dt
local kd = 39.79/dt
local outMin = 0
local outMax = 2000

local previous_error = 0
local previous_input = 0
local integral = 0
local turbine = findTurbine()

while true do

  local input = turbine.getRotorSpeed()
  local error = setpoint - input
  integral = integral +(ki*error)
  if integral > outMax then
    integral = outMax
  elseif integral < outMin then
    integral = outMin
  end

  local derivative = (input- previous_input)
  local output = kp * error + integral - kd * derivative
  if output>outMax then
    output = outMax
  elseif output<outMin then
    output = outMin
  end

  previous_error = error
  previous_input = input

  --output value to turbine and values to screen
  turbine.setFluidFlowRateMax(output)
  term.clear()
  term.setCursor(1,1)
  term.write("setpoint: ")
  print(setpoint)
  term.write("current speed: ")
  print(turbine.getRotorSpeed())
  term.write("error: ")
  print(error)
  term.write("proportional: ")
  print(kp*error)
  term.write("integral: ")
  print(integral)
  term.write("derivative: ")
  print(kd*derivative)
  term.write("output: ")
  print(output)


  --update web ui
  if updateCounter > timeBetweenUpdates/dt then
    updateCounter = 0
    varipass.write("Nvtj7C2ZJGKeb91C","5bKO8pEa",tostring(turbine.getRotorSpeed()))
  end
  updateCounter = updateCounter+1

  if updateCounter2> timeBetweenUpdates2/dt then
    updateCounter2 = 0
    setpoint = varipass.read("Nvtj7C2ZJGKeb91C","bE929FH3",varipass.TYPE_INT)
  end
  updateCounter2 = updateCounter2 + 1

  os.sleep(dt)
end