local tArgs = {...}
local slot = 1
local turtle = require("robot")
local maxSlots = turtle.inventorySize()

-- checking input from user
if not tArgs[1] then
  print("you have to input a length")
  return  
end


-- redefining basic functions to make it not be able to get stuck
local function forward()
  while not turtle.forward() do
    if not turtle.swing() then
	  os.sleep(1)
	end
  end
end

local function up()
  while not turtle.up() do
    if not turtle.swingUp() then
	  os.sleep(1)
	end
  end
end

local function down()
  while not turtle.down() do
    if not turtle.swingDown() then
	  os.sleep(1)
	end
  end
end
-- will check the ammount of items in the slot before trying to place wil switch if neccecary
local function placeDown()
  while turtle.count(slot) <1 do
    slot = slot +1
	if slot == maxSlots then
	    slot = 1
        end
    turtle.select(slot)
  end
  turtle.placeDown()
end

local function placeUp()
  while turtle.count(slot) <1 do
    slot = slot +1
	if slot == maxSlots then
	  slot = 1
        end
    turtle.select(slot)
  end
  turtle.placeUp()
end

-- line function makes it make a line
local function lineSide()
  for i = 1,tArgs[1] do
    placeDown()
	placeUp()
    forward()
  end
  placeDown()
  placeUp()
end

local function line()
  for i = 1,tArgs[1] do
    placeDown()
    forward()
  end
  placeDown()
end

turtle.select(1)
lineSide()
turtle.turnRight()
forward()
down()
turtle.turnRight()
line()
turtle.turnLeft()
forward()
turtle.turnLeft()
line()
turtle.turnRight()
forward()
turtle.turnRight()
line()
turtle.turnLeft()
up()
forward()
turtle.turnLeft()
lineSide()

turtle.turnLeft()
for i = 1,2 do
  forward()
end
turtle.turnLeft()
down()

for i = 1,tArgs[1] do
  forward()
end
print("im done master")