local robot = require("robot")
local slot = 1
local maxSlots = robot.inventorySize()

local function forward()
	while not robot.forward() do
		robot.swing()
		os.sleep(0.5)
	end
end

local function up()
	while not robot.up() do
		robot.swingUp()
		os.sleep(0.5)
	end
end

local function selectMaterial()
	while robot.count(slot) < 1 do
		slot = slot + 1
		if slot == robot.inventorySize() then
			slot = 1
		end
		robot.select(slot)
	end
end

local function buildLine(n)
	for i = 1, n do
		selectMaterial()
		robot.placeDown()
		forward()
	end
	while not robot.back() do
		os.sleep(1)
	end
end

local function buildSquareFilled(n)
	for i = 1, n do
		buildLine(n)
		if (i == n) then
			break
		end
		if ((i % 2) == 1) then
			robot.turnRight()
			forward()
			robot.turnRight()
		else
			robot.turnLeft()
			forward()
			robot.turnLeft()
		end
	end
	if ((n % 2) == 1) then
		for i = 1, 2 do
			robot.turnLeft()
			for j = 1, (n - 1) do
				forward()
			end
		end
		robot.turnLeft()
		robot.turnLeft()
	else
		robot.turnRight()
		for i = 1, (n - 1) do
			forward()
		end
		robot.turnRight()
	end
end

local function buildSquareCorners(n)
	for i = 1, 4 do
		selectMaterial()
		robot.placeDown()
		for j = 1, (n - 1) do
			forward()
		end
		robot.turnRight()
	end
end

local function buildSquare(n)
	for i = 1, 4 do
		buildLine(n)
		robot.turnRight()
	end
end

local function moveToBuildingPosition(n)
	if ((n % 2) == 1) then
		local steps = (n / 2)
		robot.turnLeft()
		for i = 1, steps do
			forward()
		end
		robot.turnRight()
	else
		local steps = ((n / 2) - 1)
		robot.turnLeft()
		for i = 1, steps do
			forward()
		end
		robot.turnRight()
	end
end

local function moveToStartingPosition(n)
	if ((n % 2) == 1) then
		local steps = (n / 2)
		robot.turnRight()
		for i = 1, steps do
			forward()
		end
		robot.turnLeft()
	else
		local steps = ((n / 2) - 1)
		robot.turnRight()
		for i = 1, steps do
			forward()
		end
		robot.turnLeft()
	end
end

local tArgs = { ... }
if #tArgs == 0 then
	print("Usage: Platform <size>")
	return
end

local size = tonumber(tArgs[1])
robot.select(1)
forward()
moveToBuildingPosition(size)
buildSquareFilled(size)
up()
buildSquare(size)
up()
buildSquareCorners(size)
up()
buildSquare(size)
moveToStartingPosition(size)
