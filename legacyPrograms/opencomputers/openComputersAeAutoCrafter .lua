local component = require("component")
local meController = component.proxy(component.me_controller.address)
local gpu = component.gpu

-- Each element of the array is "item", "damage", "number wanted", "max craft size"
-- Damage value should be zero for base items

local items = {
    { "minecraft:glass",      0, 4096, 256 },
    { "minecraft:sand",      0, 4096, 256 },
    { "appliedenergistics2:material",       10, 1024, 256 }, -- Pure Certus Quartz Crystal
    { "appliedenergistics2:material",       7, 1024, 256 }, -- Fluix Crystal
    { "appliedenergistics2:material",       12, 1024, 256 }, -- Pure Fluix Crystal
    { "appliedenergistics2:material",       8, 1024, 256 }, -- Fluix Dust
    { "appliedenergistics2:material",       2, 1024, 256 }, -- Certus Quartz Dust
}

local loopDelay = 60 -- Seconds between runs

-- Init list with crafting status
for curIdx = 1, #items do
    items[curIdx][5] = false -- Crafting status set to false
    items[curIdx][6] = nil -- Crafting object null
end

while true do
    for curIdx = 1, #items do
        local curName = items[curIdx][1]
        local curDamage = items[curIdx][2]
        local curMinValue = items[curIdx][3]
        local curMaxRequest = items[curIdx][4]
        local curCrafting = items[curIdx][5]
        local curCraftStatus = items[curIdx][6]

        -- io.write("Checking for " .. curMinValue .. " of " .. curName .. "\n")
        local storedItem = meController.getItemsInNetwork({
            name = curName,
            damage = curDamage
            })
        if storedItem[1] == nil then
          local temp = {}
          temp.size = 0
          temp.label = curName..": "..curDamage
          temp.name = temp.label
          storedItem[1] = temp
        end

        io.write("Network contains ")
        gpu.setForeground(0xCC24C0) -- Purple-ish
        io.write(storedItem[1].size)
        gpu.setForeground(0xFFFFFF) -- White
        io.write(" items with label ")
        gpu.setForeground(0x00FF00) -- Green
        io.write(storedItem[1].label .. "\n")
        gpu.setForeground(0xFFFFFF) -- White
        if storedItem[1].size < curMinValue then
            local delta = curMinValue - storedItem[1].size
            local craftAmount = delta
            if delta > curMaxRequest then
                craftAmount = curMaxRequest
            end

            io.write("  Need to craft ")
            gpu.setForeground(0xFF0000) -- Red
            io.write(delta)
            gpu.setForeground(0xFFFFFF) -- White
            io.write(", requesting ")
            gpu.setForeground(0xCC24C0) -- Purple-ish
            io.write(craftAmount .. "... ")
            gpu.setForeground(0xFFFFFF) -- White

            local craftables = meController.getCraftables({
                name = curName,
                damage = curDamage
                })
            if craftables.n >= 1 then
                local cItem = craftables[1]
                if curCrafting then
                    if curCraftStatus.isCanceled() or curCraftStatus.isDone() then
                        io.write("Previous Craft completed\n")
                        items[curIdx][5] = false
                        curCrafting = false
                    end
                end
                if curCrafting then
                        io.write("Previous Craft busy\n")
                end
                if not curCrafting then
                    local retval = cItem.request(craftAmount)
                    items[curIdx][5] = true
                    items[curIdx][6] = retval
                    gpu.setForeground(0x00FF00) -- Green
                    io.write("Requested - ")
		    --while (not retval.isCanceled()) and (not retval.isDone()) do
	            --		os.sleep(1)
                    --        io.write(".")
		    -- end
                    gpu.setForeground(0xFFFFFF) -- White
                    io.write("Done \n")
                end
            else
                gpu.setForeground(0xFF0000) -- Red
                io.write("    Unable to locate craftable for " .. storedItem[1].name .. "\n")
                gpu.setForeground(0xFFFFFF) -- White
            end
        end
    end
    io.write("Sleeping for " .. loopDelay .. " seconds...\n\n")
    os.sleep(loopDelay)
end