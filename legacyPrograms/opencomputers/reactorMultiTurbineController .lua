local component = require("component")
local term = require("term")

--variables changeable by user
--energy storage levels to kick turbine on and off
local start = 10
local stop = 95

--delay between each check
local delay = 1

--turbine variables
--speed for turbine to keep in RPM
local turbineSpeed = 1800
--turbine marge to keep speed around as to not keep turning turbine on and off in %
local marge = 2
--------------------------------------------------------------------------------
-------------------------------------FUNCTIONS----------------------------------
--------------------------------------------------------------------------------
--finds the attached reactor
local function findPeripherals()
  local data = component.list()
  --no peripherals found
  if not data then
    return false
  end
  local peripherals = {}
  peripherals.turbine = {}
  --find all the needed peripherals for proper operation
  for k,v in pairs(data) do
    if v == "bigreactors_reactor" then
      peripherals.reactor = component.proxy(k)
    elseif v == "bigreactors_turbine" then
      table.insert(peripherals.turbine,component.proxy(k))
    elseif v == "energy_device" then
      peripherals.capacitor = component.proxy(k)
    elseif v == "extrautils2_drum_drum_65536" then
      peripherals.drum = component.proxy(k)
    elseif v == "gpu" then
      peripherals.gpu = component.proxy(k)
    end
  end
  --not all the required peripherals are found
  if not peripherals.reactor or not peripherals.turbine[1] or not peripherals.capacitor or not peripherals.drum then
    return false
  end
  return peripherals
end

--gets energy level as a percentage
local function getEnergyLevel(capacitor)
  return (capacitor.getEnergyStored()/capacitor.getMaxEnergyStored())*100
end

local oldEnergy = 0
local function getEnergyGain(capacitor)
  local energy = capacitor.getEnergyStored()
  local gain = energy - oldEnergy
  oldEnergy = energy
  return gain*delay/20
end

local function updateGUI(p)
  local w, h = p.gpu.getResolution()
  term.clear()
  term.write("turbine RPM, active")
  local j = 2
  local row = 1
  for i = 1,#p.turbine do
    --fix indentation for single digits
    if i <10 then
      term.setCursor(row,j)
    else
      term.setCursor(row-1,j)
    end

    --grab relevant turbine data
    local speed = math.floor(p.turbine[i].getRotorSpeed()*10)/10
    local active = tostring(p.turbine[i].getInductorEngaged())

    --print turbine data with proper seperation
    term.write(i..": "..speed)
    if p.turbine[i].getInductorEngaged() then
      p.gpu.setForeground(0x00FF00)
    else
      p.gpu.setForeground(0xFF0000)
    end
    term.write(active)
    p.gpu.setForeground(0xFFFFFF)
    --switch rows on gpu when over half
    if j > #p.turbine/2 then
      j = 1
      row = 20
    end
    j = j+1
  end

  local newNull = #p.turbine/2
  --update reactor stats
  term.setCursor(1,newNull+3)
  term.write("reactor stats")
  term.setCursor(1,newNull+4)
term.write("reactor engaged: ")
  if p.reactor.getActive() then
    p.gpu.setForeground(0x00FF00)
  else
    p.gpu.setForeground(0xFF0000)
  end
  term.write(tostring(p.reactor.getActive()))
  p.gpu.setForeground(0xFFFFFF)
  term.setCursor(25,newNull+4)
  term.write("fuel use: ".. p.reactor.getFuelConsumedLastTick())

  --power status
  newNull = newNull +6
  term.setCursor(1,newNull)
  term.write("power stats")
  term.setCursor(1,newNull+1)
  term.write("storage filled: "..getEnergyLevel(p.capacitor).."%")
  term.setCursor(1,newNull+2)
  term.write("rf/t: ")
  local gain = getEnergyGain(p.capacitor)
  if gain >0 then
    p.gpu.setForeground(0x00FF00)
  else
    p.gpu.setForeground(0xFF0000)
  end
  term.write(gain)
  p.gpu.setForeground(0xFFFFFF)
  term.setCursor(1,newNull+3)
  term.write("energy stored: "..p.capacitor.getEnergyStored())
  term.setCursor(1,newNull+4)
  term.write("max energy: "..p.capacitor.getMaxEnergyStored())
end



--------------------------------------------------------------------------------
-------------------------------------MAIN---------------------------------------
--------------------------------------------------------------------------------
local p = findPeripherals()

if not p then
  print("please attach a reactor, turbine, capacitor bank and a drum for steam storage")
  return
end

local turbineRunning = false
--set proper getResolution for the screen
--standard resolution = 160x50
p.gpu.setResolution(40,12)

--control loop
while true do
  --handle energyProduction
  if getEnergyLevel(p.capacitor) < start then
    turbineRunning = true
  elseif getEnergyLevel(p.capacitor) > stop then
    turbineRunning = false
  end
  for i = 1,#p.turbine do
    p.turbine[i].setInductorEngaged(turbineRunning)
    --handle turbine speed
    if p.turbine[i].getRotorSpeed() < turbineSpeed *((100 - marge) / 100) then
      p.turbine[i].setFluidFlowRateMax(2000)
    elseif p.turbine[i].getRotorSpeed() > turbineSpeed * (100 + marge) / 100 then
      p.turbine[i].setFluidFlowRateMax(0)
    end
  end

  local tankInfo = p.drum.getTankInfo()
  --handle steam production
  if tankInfo.amount/tankInfo.capacity < (start/100)  then
    p.reactor.setActive(true)
  elseif tankInfo.amount/tankInfo.capacity > (stop/100) then
    p.reactor.setActive(false)
  end

  --update gpu if there is one
  if p.gpu then
    updateGUI(p)
  end
  os.sleep(delay)
end