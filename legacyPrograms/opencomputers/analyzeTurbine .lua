local filesystem = require("filesystem")
local component = require("component")
local event = require("event")

local fileName = "/home/spoolUp.csv"
local dataPerSecond = 2

local function writeToFile(time, speed, rf)
  local fp = filesystem.open(fileName,"a")
  fp:write(time/dataPerSecond..","..speed..","..rf.."\n")
  fp:close()
  print(time/dataPerSecond.." : "..speed.." : "..rf)
end

local fp = filesystem.open(fileName,"w")
if fp == nil then
  print("could not open file")
  return
end
fp:write("interval per meassurement: "..1/dataPerSecond.."\n")
fp:write("time,speed,rf/t\n")
fp:close()

local p = component.bigreactors_turbine
p.setInductorEngaged(true)
p.setActive(true)


local running = true
local function stop()
  running = false
end

event.listen("redstone_changed",stop)

local i = 0
while running do
  local speed = p.getRotorSpeed()
  local rf = p.getEnergyProducedLastTick()
  writeToFile(i,speed,rf)
  i = i +1
  os.sleep(1/dataPerSecond)
end