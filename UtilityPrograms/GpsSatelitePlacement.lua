--height that the topmost block of the gps sat will hit
local sateliteHeight = 250
-- use instructions place computer, insert 4 computers 4 modems, a disk drive and a disk
-- disk does not need programs if the disk has a startup program this will be overwritten

-- load git
if not fs.exists("Modules/Git.lua") then
    print("downloading: Git")
    local response = http.get("https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")

    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished

--------------------------------------- INCLUDES --------------------------------------
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Move.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")

local Move = require("Modules.Move")
local StatusCode = require("Modules.StatusCodes")
local Utils = require("Modules.Utils")

--------------------------------------- VARIABLES --------------------------------------
--------------------------------------- FUNCTIONS --------------------------------------
local function DeployComputer(computerSlot, modemSlot, diskDriveSlot, diskSlot)
    Move.turnToDirection(0)
    local computerPos = {}
    computerPos = Move.getPosition()
    print("computer position: x: " .. computerPos.x .. " y: " .. computerPos.y .. " z: " .. computerPos.z)
    computerPos.z = computerPos.z -1
    turtle.select(computerSlot)
    turtle.place()
    Move.up(false)
    turtle.select(modemSlot)
    turtle.place()
    Move.down(false)
    Move.down(false)
    turtle.select(diskDriveSlot)
    turtle.place()
    turtle.select(diskSlot)
    turtle.drop()
    os.sleep(1)

    local mountPath = peripheral.call("front", "getMountPath")
    --create file for computer
    local startFile = fs.open(mountPath .."/startup.lua","w")
    --meta programming :)
    startFile.writeLine("if fs.exists(\"disk/startup.lua\") then")
    startFile.writeLine("fs.copy(\"disk/startup.lua\",\"startup.lua\")")
    startFile.writeLine("end")
    startFile.writeLine("shell.run(\"gps\", \"host\", \"" .. computerPos.x .. "\", \"" .. computerPos.y .. "\", \"" .. computerPos.z .. "\")")
    startFile.close()
    Move.up(false)
    peripheral.call("front", "turnOn")
    Move.down(false)
    turtle.suck()
    turtle.select(diskDriveSlot)
    turtle.dig()
end

local function DeploySatelite(computerSlot, modemSlot, diskDriveSlot, diskSlot)
    local curPos = Move.getPosition()
    DeployComputer(computerSlot, modemSlot, diskDriveSlot, diskSlot)
    curPos.x = curPos.x - 3 --west
    curPos.y = curPos.y + 3 --up
    curPos.z = curPos.z - 3 --north
    Move.goTo(curPos, false)
    DeployComputer(computerSlot, modemSlot, diskDriveSlot, diskSlot)
    curPos.x = curPos.x + 6 -- 3 east of startPos
    Move.goTo(curPos, false)
    DeployComputer(computerSlot, modemSlot, diskDriveSlot, diskSlot)
    curPos.x = curPos.x - 3 -- original startPos x
    curPos.y = curPos.y - 3 -- 3 down
    curPos.z = curPos.z - 3 -- north
    Move.goTo(curPos, false)
    DeployComputer(computerSlot, modemSlot, diskDriveSlot, diskSlot)
end





--------------------------------------- MAIN --------------------------------------

--find items needed in inventory
local computerSlot = Utils.findItemInInventory({"computercraft:computer_normal", "computercraft:computer_advanced"}, 1, true)
local modemSlot = Utils.findItemInInventory({"computercraft:wireless_modem_normal", "computercraft:wireless_modem_advanced"}, 1, true)
local diskDriveSlot = Utils.findItemInInventory("computercraft:disk_drive", 1, true)
local diskSlot = Utils.findItemInInventory("computercraft:disk", 1, true)

if computerSlot == nil or turtle.getItemCount(computerSlot) < 4 then
    print("missing computers")
    return
end
if modemSlot == nil or turtle.getItemCount(modemSlot) < 4 then
    print("missing wireless modems")
    return
end
if diskDriveSlot == nil or diskSlot == nil then
    print("could not find disk drive or disk")
    return
end

--recover or set position if needed
if Move.recoverPosition() ~= StatusCode.Code.Ok then
    print("could not recover position please enter the current x,y,z and facing")
    print("north = 0 east = 1, south = 2, west = 3")
    print("enter as x,y,z,f")
    local input = read()
    print(input)
    local seperated = Utils.splitString(input,',')
    if #seperated ~= 4 then
        error("invalid position entered")
    end
    Move.setPosition({x =  tonumber(seperated[1]), y = tonumber(seperated[2]), z = tonumber(seperated[3]), f = tonumber(seperated[4])})
end

--check if sateliteHeight is realistic
if sateliteHeight > 255 or sateliteHeight < 6 then
    print("sateliteHeight cant be > 255 or lower than 6")
    return
end

--check fuel requirements
local curPos = Move.getPosition()
curPos.y = sateliteHeight
local fuelNeeded = Move.calculateMovesNeeded(curPos)
--multiply by 2 because the turtle also has to travel back down
fuelNeeded = fuelNeeded * 2
fuelNeeded = fuelNeeded + 48 --moves from satelite deployment counted up
if turtle.getFuelLevel() < fuelNeeded then
    print("more fuel is needed, missing: " .. fuelNeeded - turtle.getFuelLevel())
    return
end

--place the satelite
local startPosition = Move.getPosition()
local sateliteDeploymentPosition = Utils.copyTable(startPosition)
sateliteDeploymentPosition.y = sateliteHeight -4
sateliteDeploymentPosition.z = sateliteDeploymentPosition.z - 3
Move.goTo(sateliteDeploymentPosition, false)
DeploySatelite(computerSlot, modemSlot, diskDriveSlot, diskSlot)
Move.goTo(startPosition, false)
print("clear position cache? y/n")
if read() == "y" then
    Move.clearCache()
    print("cache cleared")
end