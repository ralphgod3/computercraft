local modemChannel = 65532 -- default channel to listen too
local TAG = "MS"
local cacheLocation = "cache/ModemShark.txt"

local sides = {
	"top",
	"bottom",
	"left",
	"right",
	"front",
	"back",
}

-- load git
if not fs.exists("Modules/Git.lua") then
	print("downloading: Git")
	local response = http.get(
		"https://gitlab.com/api/v4/projects/23712047/repository/files/Modules%2FGit.lua/raw?ref=master")
	if response then
		local contents = response.readAll()
		response.close()
		local file = fs.open("Modules/Git.lua", "w")
		file.write(contents)
		file.close()
	else
		error("could not download Git")
	end
end
local Git = require("Modules.Git")
--git load finished

Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Timer.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Utils.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Logger.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Threads.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/StatusCodes.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Messages.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/HW/Modems.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Modules/Networking/Networking.lua")
Git.getFileIfNeeded(Git.ComputerCraftProjectID, "Interfaces/Modules/Networking/Inetworking.lua")

local Dbg = require("Modules.Logger")
local Timer = require("Modules.Timer")
local Threads = require("Modules.Threads")
local Utils = require("Modules.Utils")
local StatusCodes = require("Modules.StatusCodes")
local Messages = require("Modules.Networking.Messages")

local INetworking = require("Interfaces.Modules.Networking.Inetworking")
local NetworkingHw = require("Modules.Networking.HW.Modems")


Dbg.setOutputTerminal(term.current())
for _, side in pairs(sides) do
	if peripheral.getType(side) == "monitor" then
		local mon = peripheral.wrap(side)
		Dbg.logI(TAG, "runnin on monitor ", side)
		Dbg.setOutputTerminal(mon)
		break
	end
end
Dbg.setLogSize(10000)
Dbg.setLogLevel(TAG, Dbg.Levels.Info)
Threads = Threads.new()
Timer = Timer.new(Threads)
NetworkingHw = NetworkingHw.new(modemChannel)


---returns Imessage as a printable string
---@param msg ILLmessage
---@param hosts table<number, string>
local function getMsgAsString(msg, hosts)
	local from = tostring(msg.from)
	if hosts[tostring(msg.from)] ~= nil then
		from = from .. " (" .. hosts[tostring(msg.from)] .. ")"
	end

	local to = tostring(msg.to)
	if hosts[tostring(msg.to)] ~= nil then
		to = to .. " (" .. hosts[tostring(msg.to)] .. ")"
	end

	local QOS = tostring(msg.QOS) .. " (UNKNOWN)"
	for key, value in pairs(INetworking.QOS) do
		if value == msg.QOS then
			QOS = tostring(msg.QOS) .. " (" .. tostring(key) .. ")"
			break
		end
	end

	local llId = tostring(msg.llId)
	local llType = tostring(msg.llType) .. " (UNKNOWN)"
	for key, value in pairs(INetworking.MsgTypes) do
		if value == msg.llType then
			llType = tostring(msg.llType) .. " (" .. tostring(key) .. ")"
			break
		end
	end

	local responseTo = ""
	if msg.responseTo ~= nil then
		responseTo = tostring(responseTo)
	end

	if msg.payload then
		local msgType = tostring(msg.payload.type) .. " (UNKNOWN)"
		for key, value in pairs(Messages.messageTypes) do
			if value == msg.payload.type then
				msgType = tostring(msg.payload.type) .. " (" .. tostring(key) .. ")"
			end
		end

		return "from: " .. from ..
			", to: " .. to ..
			", QOS: " .. QOS ..
			", id: " .. llId ..
			", LLtype: " .. llType ..
			", type: " .. msgType
	end
	return "from: " .. from ..
		", to: " .. to ..
		", QOS: " .. QOS ..
		", id: " .. llId ..
		", LLtype: " .. llType
end

local function recv()
	local hosts = {}
	--load hosts from memory if possible.
	if fs.exists(cacheLocation) then
		local file = fs.open(cacheLocation, "r")
		local config = file.readAll()

		file.close()
		config = textutils.unserialise(config)
		if config == nil then
			Dbg.logE(TAG, "corrupt cache, skipping loading hosts from cache")
		else
			hosts = config
		end
	end


	local width, _ = term.getSize()
	hosts[tostring(modemChannel)] = "broadcast"
	local seperatorLine = ""
	for _ = 1, width do
		seperatorLine = seperatorLine .. "-"
	end
	while true do
		local msg = NetworkingHw.receive()
		if msg and msg.from ~= nil and msg.llId ~= nil and msg.llType ~= nil and msg.to ~= nil then
			if msg.payload ~= nil and msg.QOS ~= nil then
				local str = getMsgAsString(msg, hosts)
				Dbg.logI(TAG, seperatorLine)
				Dbg.logI(TAG, tostring(os.clock()) .. " " .. str)
				Dbg.logV(TAG, msg.payload)
				if msg.payload.type == Messages.messageTypes.WhoIsResponse then
					if hosts[tostring(msg.from)] == nil then
						hosts[tostring(msg.from)] = msg.payload.name
						local file = fs.open(cacheLocation, "w")
						file.write(textutils.serialise(hosts))
						file.close()
					end
				end
			else
				local str = getMsgAsString(msg, hosts)
				Dbg.logI(TAG, seperatorLine)
				Dbg.logI(TAG, tostring(os.clock()) .. " " .. str)
				if msg.idAck then
					Dbg.logI(TAG, "msg being Acked: ", msg.idAck)
				end
			end
		else
			Dbg.logW(TAG, "unknown message protocol no decoding ", msg)
		end
	end
end



Threads.create(recv)
Threads.startRunning()
